/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_ae_api.h"

#include "app_common.h"

extern UINT32 dualAe,saveAe,AeTarget,AeRange;
static void cmdAEOff(){
    ybfwAeModeSet(YBFW_3A_WIN_0, YBFW_AE_MODE_OFF);
}
void cmdAEOn(){
    extern void ae_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
    dualAe=0;
    ybfwAeCfgSet(YBFW_3A_WIN_0,YBFW_AE_CONTINOUS_ENABLE,1);
    ybfwSystemCfgSet( YBFW_CDSP_INFO_CFG_GROUP, CDSP_INFO_CFG_AE_BIT_INCREASE, 6);
    ybfwAeCustomAewinSizeSet(YBFW_3A_WIN_0,16, 16);
    ybfwAeCustomCbSet(YBFW_3A_WIN_0,AASTAT_INFO_FLAG_AEWIN_Y | AASTAT_INFO_FLAG_HIS_Y, ae_Cb);
    ybfwAeModeSet(YBFW_3A_WIN_0,YBFW_AE_MODE_INFO_ONLY);
}

static void cmdAEDualon(){
    extern void ae_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
    dualAe=1;
    ybfwAeCfgSet(YBFW_3A_WIN_0,YBFW_AE_CONTINOUS_ENABLE,1);
    ybfwSystemCfgSet( YBFW_CDSP_INFO_CFG_GROUP, CDSP_INFO_CFG_AE_BIT_INCREASE, 6);
    ybfwAeCustomAewinSizeSet(YBFW_3A_WIN_0,16, 16);
    ybfwAeCustomCbSet(YBFW_3A_WIN_0,AASTAT_INFO_FLAG_AEWIN_Y | AASTAT_INFO_FLAG_HIS_Y, ae_Cb);
    ybfwAeModeSet(YBFW_3A_WIN_0,YBFW_AE_MODE_INFO_ONLY);
}
static void cmdAETarget(UINT32 target,UINT32 range) {
    INFO("Current AE Target = %d , Range = %d\n",AeTarget,AeRange);
    if(AeTarget>AeRange){
        AeTarget = target;
        AeRange = range;
        INFO("New AE Target = %d , Range = %d\n",AeTarget,AeRange);
    }else{
        ERR("Error ,  AE Target = %d , Range = %d , AE Target must be bigger than Range.\n",target,range);
    }

}
static void cmdAESave() {
    saveAe = 1;
}
static void cmdAEInit(){
    appAeInit();
}
int cmdAE(int argc,char **argv,UINT32* v){  
    char* cmd = *argv++;v++;argc--;

    /**/
    if(argc<0) {cmdAEInit();}
    else if (isEqualStrings(cmd, "off", CCS_NO ) ) {cmdAEOff();}
    else if (isEqualStrings(cmd, "on", CCS_NO ) ) {cmdAEOn();}
    else if (isEqualStrings(cmd, "dualon", CCS_NO ) ) {cmdAEDualon();}
    else if (isEqualStrings(cmd, "target", CCS_NO ) ) {cmdAETarget(*v++,*v++);}
    else if (isEqualStrings(cmd, "save", CCS_NO ) ) {cmdAESave();}
    else{return 0;}
    return 1;
}




