/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef APP_CMD_WIFI_BT_H_
#define APP_CMD_WIFI_BT_H_




SINT32 cmdWiFi(SINT32 argc,UINT8 **argv,UINT32* v);
void cmdWiFiHelp();

SINT32 cmdBT(SINT32 argc,UINT8 **argv,UINT32* v);
void cmdBTHelp();

#endif /* APP_CMD_WIFI_BT_H_ */
