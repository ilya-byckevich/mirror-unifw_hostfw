/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"

#include "app_common.h"

static void appCmdRun(char* cmd,int priority){
    if(priority){
        ybfwOsThreadCreateEx("start.cmd"/*name*/,
                    ybfwCmdExec/*entry_func*/,
                    (UINT32)cmd/*param*/,
                    priority/*priority*/,
                    0/*preempt_threshold*/,
                    1/*slice*/,
                    1024*64/*stack*/,
                    YBFW_TX_AUTO_START/*autostart*/);
    }else{
        ybfwCmdExec(cmd);
    }
}

static void appCmdListRun(char* cmds){
    int size = strlen(cmds)+1;
    INFO("####cmdlist:%s,%d,%ums\n",cmds,size,ybfwOsTimeGet());
    int i;
    for(i=0;i<size;i++)
        if(cmds[i]<20)
            cmds[i]=0;
    for(i=0;i<size;i++){
        if(cmds[i]){
            INFO("####cmd(%ums):%s\n",ybfwOsTimeGet(),&cmds[i]);
            ybfwCmdExec(&cmds[i]);
            for(;i<size;i++)
                if(!cmds[i])
                    break;
        }
    }
}


static void appCmdFileRun(const char* fname){
    INFO("####cmdfile:%s,%ums\n",fname,ybfwOsTimeGet());
    UINT32 hd = ybfwFsFileOpen(fname,FS_OPEN_RDONLY);
    if(!hd){
        ERR("open cmd file fail:%s\n",fname);
        return;
    }
    int max = 1024;
    char *buf = ybfwMallocCache(1024);

    int size = ybfwFsFileRead(hd,buf,max);
    ybfwFsFileClose(hd);
    buf[size++]=0;
    appCmdListRun(buf);
    ybfwFree(buf);
}

static void appCmdFileSave(const char* fname,const char* cmd){
    /*const char* fname = "b:/start.cmd";*/
    /*if(!fname)fname = "b:/start.cmd";*/
    if(!fname){
        ERR("%s fname is null\n",__FUNCTION__);
        return;
    }
    INFO("%s(%s,%s)\n",__FUNCTION__,fname,cmd);
    UINT32 hd = ybfwFsFileOpen(fname,FS_OPEN_CREATE|FS_OPEN_RDWR);
    if(!hd){
        ERR("open cmd file fail:%s\n",fname);
        return;
    }
    int size = strlen(cmd)+1;
    ybfwFsFileWrite(hd,cmd,size);
    ybfwFsFileClose(hd);
}
static void appCmdFileSaves(const char* fname,char** argv){
    DBG("%s(%s)\n",__FUNCTION__,fname);
    char* buf = ybfwMallocCache(256);
    char* ptr=buf;
    while(*argv){
        char*src=*argv++;
        while(*src)
            *ptr++=*src++;
        *ptr++=' ';
    }
    *ptr++=0;
    appCmdFileSave(fname,buf);
    ybfwFree(buf);
}

static void appCmdRuns(int pri,char** argv){
    char* buf = ybfwMallocCache(256);
    char* ptr=buf;
    while(*argv){
        char*src=*argv++;
        while(*src)
            *ptr++=*src++;
        *ptr++=' ';
    }
    *ptr++=0;
    appCmdRun(buf,pri);
    ybfwFree(buf);
}

static void appCmdStartProcess(UINT32 v){
    DBG("%s start\n",__FUNCTION__);
    /*ybfwCmdExec("cmd run-file b:/start.cmd");*/
    appCmdFileRun("b:/start.cmd");
    DBG("%s end\n",__FUNCTION__);
}
static void appCmdStart(){
    /*ybfwOsThreadCreate("start.cmd",ybfwCmdExec,"omf run-file b:/start.cmd",14,0,1, TX_AUTO_START);*/
    ybfwOsThreadCreateEx("start.cmd"/*name*/,
                    appCmdStartProcess/*entry_func*/,
                    0/*param*/,
                    16/*priority*/,
                    0/*preempt_threshold*/,
                    1/*slice*/,
                    1024*64/*stack*/,
                    YBFW_TX_AUTO_START/*autostart*/);
}

/*-------------------------------------------------------------------------
 *  Function Name : cmdOMF
 *  Description : open media framework cmd entrty
 *------------------------------------------------------------------------*/
SINT32 cmdCmd(int argc,UINT8 **argv,UINT32* v){
    UINT8 * cmd = *argv++;v++;argc--;

    /**/
    if(argc<0){
        return 0;
    }
    else if (isEqualStrings(cmd,(UINT8 *) "start", CCS_NO ) ) {
        appCmdStart();
    }
    else if (isEqualStrings(cmd,(UINT8 *) "run", CCS_NO ) ) {/* run [pri] [cmd...]*/
        appCmdRuns(*v,argv+1);
    }
    else if (isEqualStrings(cmd,(UINT8 *) "run-list", CCS_NO ) ) {
        appCmdListRun(*argv++);
    }
    else if (isEqualStrings(cmd,(UINT8 *) "run-file", CCS_NO ) ) {
        appCmdFileRun(*argv++);
    }
    else if (isEqualStrings(cmd,(UINT8 *) "save", CCS_NO ) ) {
        appCmdFileSaves(*argv++,argv);
    }
    else{
        return 0;
    }
    return 1;
}

