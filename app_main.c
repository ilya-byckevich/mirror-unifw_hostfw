/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "ykn_bfw_api/ybfw_disk_api.h"
#include "ykn_bfw_api/ybfw_msg_def.h"
#include "ykn_bfw_api/ybfw_utility_api.h"
#include "ykn_bfw_api/ybfw_exif_api.h"
#include "ykn_bfw_api/ybfw_dcf_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "customization/dev_init.h"
#include "customization/app_init.h"
#include "app_bt_device.h"

#include GPIO_INC

#include "app_common.h"


/*  autostart preview mode. It is better use  export AUTOSTART_PREVIEW_MODE=YES
    do not commit uncommented to master
    #define AUTO_PREVIEW_MODE
*/
#ifdef AUTO_PREVIEW_MODE
#warning "----------------------------- AUTO_PREVIEW_MODE  ENABLED --------------"
#endif

extern void cmdAEOn();
extern void appHdmiStart(int w,int h,int fps);
extern void cmdAWBInit();
extern void cmdAWBOn();

#ifdef POWERON_PB_WITHOUT_SENSOR
    extern void devFrontEnum(void);
    extern void sensorInit();
    extern UINT32 sensorOsServInit();
    extern UINT32 snapStaticInit(void);
#endif

extern UINT32 pvStaticInit( void );

extern void ndk_bt_hidp_keyevent_register(NDKBtHidpKeyTriggerCb hostcallback);
extern void ndk_st_close_streaming();

/*************************************************************************/
void
appMain(
    UINT32 msg,
    UINT32 param
)
{
    UINT32 val;

    printf("!!! Host Msg %x %x\n", msg, param);

    switch( msg ) {
    case YBFW_MSG_HOST_TASK_INIT:/* the 1'st msg after boot*/
        ybfwDiskMount(YBFW_START_DETECT);
        ybfwExifInit();
        #ifdef SET_MAC_PHY_PWR
        SET_MAC_PHY_PWR(1);
        #endif
        #ifdef SET_MAC_PHY_PWR2
        SET_MAC_PHY_PWR2(1);
        #endif


        printf("++++++++++++++++++++++ start +++++++++++++++++++++\n");
#ifdef POWERON_PB_WITHOUT_SENSOR
        printf("+++ Manual sensor init +++\n");
        devFrontEnum();
        sensorOsServInit();
        sensorInit();
        pvStaticInit();
        snapStaticInit();
        panoramaInit();
#define SNAP_FAST_BK_PREVIEW_SUPPORT  0
#define SNAP_HEAP_RAW_BLOCKS      1 /* If 0, auto mode */
#define SNAP_HEAP_JPEG_BLOCKS     2 /* If 0, auto mode */
#define SNAP_HEAP_YUV_BLOCKS      2 /* for icatune */
#define SNAP_MAX_COMPRESSION_RATIO  500
        snapFastBkPreviewModAdd(SNAP_FAST_BK_PREVIEW_SUPPORT);
        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_FAST_BK_PREVIEW, SNAP_FAST_BK_PREVIEW_SUPPORT);

        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_RAW_BUF_NUM, SNAP_HEAP_RAW_BLOCKS);
        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_YUV_BUF_NUM, SNAP_HEAP_YUV_BLOCKS);
        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_VLC_BUF_NUM, SNAP_HEAP_JPEG_BLOCKS);
        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_MAX_COMPRESSION_RATIO, SNAP_MAX_COMPRESSION_RATIO);
        ybfwStillCapCfgSet( 0 ,YBFW_CAPTURE_BURST_FAST_SENSOR_TRIG, 0);

        ybfwSystemCfgSet(YBFW_CLK_PV_CFG, PV_SYS_CFG_TIMEOUT_OPT, 1); /* 0: wait forever, 1: skip */
        ybfwSystemCfgSet(YBFW_CDSP_SYS_CFG,CDSP_SYS_CFG_GLOBALGAIN_LOCK_RBCLAMP,1);

        printf("+++ Manual sensor end +++\n");
#endif
#ifdef AUTO_PREVIEW_MODE
#warning "------------------------------  AUTOSTART PrEVIEW MODE ENABLED ------------"
        appStreamPv(PV_DFLT_WIDTH,PV_DFLT_HEIGHT,PV_DFLT_FPS,PV_DFLT_CDSPFPS,PV_DFLT_RAWBIT_MODE,PV_DFLT_FMT);
        cmdAEOn();
        appStreamPvYuvEnable(1,0); //yuv for recording
        appStreamPvYuvEnable(1,DISPLAY_YUV_ID); //yuv for display
        cmdAWBInit();
        cmdAWBOn();

        appHdmiStart(1920,1080,60);
#endif

        break;

    case YBFW_MSG_POWER_OFF:
        INFO("YBFW_MSG_POWER_OFF\n");
        #if defined(ICAT_WIFI) && ((SBC==1) || (YKN==1))
        printf("wifi off\n");
        ybfwRtcCfgSet(YBFW_RTC_CFG_WIFI_PWR_EN, 0);
        #endif
        #if 0
        HOST_ASSERT(0);
        #endif
        #ifdef IN_PWR_KEY
        while (IN_PWR_KEY) {
            printf(".");
            ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 20);
        }
        #endif
        ybfwPowerCtrl(YBFW_POWER_OFF, 0);
        break;

    case YBFW_MSG_DISK_REMOVAL:
        ybfwDiskUnMount(YBFW_DRIVE_REMOVE|param);
        break;
    case YBFW_MSG_DISK_INSERT:
        #ifdef IN_CARD_DETECT
        if (param==YBFW_DRIVE_NAND && IN_CARD_DETECT)
            break;
        #endif
        ybfwDiskMount(param);
        break;
    case YBFW_MSG_DISK_MOUNT_START:
        break;
    case YBFW_MSG_DISK_MOUNT_COMPLETE:
        ybfwDiskInfoGet(param, YBFW_DISK_SIZE_BY_MEGA_BYTE, &val);
        UINT32 free;
        ybfwDiskInfoGet(param, YBFW_DISK_FREE_SIZE_BY_KBYTE, &free);
        char drv = param==YBFW_DRIVE_NAND ? 'C' : 'D';
        INFO("Drive %c(%d) [%d] MB free in [%d] MB totally\n", drv, param, free/1024, val);

        ybfwDcfStdSysInit();
        ybfwDcfFsInit(param, 0, 0);
        ybfwDcfFsActive(param);
        break;
    case YBFW_MSG_DISK_MOUNT_FAIL:
        break;
    }
}
