/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_MSDC_H_
#define SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_MSDC_H_


int cmdMSDC(int argc,char **argv,UINT32* v);
void cmdMSDCHelp();

int cmdRamDisk(int argc,char **argv,UINT32* v);
void cmdRamDiskHelp();


#endif /* SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_MSDC_H_ */
