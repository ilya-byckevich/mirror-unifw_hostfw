/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
//#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_sensor_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_ae_api.h"
//#include "ykn_bfw_api/ybfw_af_api.h"
//#include "ykn_bfw_api/ybfw_awb_api.h"
//#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "app_common.h"

void cmdAE(int argc,char **argv,UINT32**v){
    extern UINT32 dualAe,saveAe,AeTarget,AeRange;
    if (argc>=2) {
        if (isEqualStrings(argv[1], "off", CCS_NO)) {
            ybfwAeModeSet(YBFW_3A_WIN_0, YBFW_AE_MODE_OFF);
        } else if(isEqualStrings(argv[1], "on", CCS_NO)) {
            extern void ae_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
            dualAe=0;
            ybfwAeCfgSet(YBFW_3A_WIN_0,YBFW_AE_CONTINOUS_ENABLE,1);
            ybfwSystemCfgSet( YBFW_CDSP_INFO_CFG_GROUP, CDSP_INFO_CFG_AE_BIT_INCREASE, 6);
            ybfwAeCustomAewinSizeSet(YBFW_3A_WIN_0,16, 16);
            ybfwAeCustomCbSet(YBFW_3A_WIN_0,AASTAT_INFO_FLAG_AEWIN_Y | AASTAT_INFO_FLAG_HIS_Y, ae_Cb);
            ybfwAeModeSet(YBFW_3A_WIN_0,YBFW_AE_MODE_INFO_ONLY);
        } else if(isEqualStrings(argv[1], "dualon", CCS_NO)) {
            extern void ae_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
            dualAe=1;
            ybfwAeCfgSet(YBFW_3A_WIN_0,YBFW_AE_CONTINOUS_ENABLE,1);
            ybfwSystemCfgSet( YBFW_CDSP_INFO_CFG_GROUP, CDSP_INFO_CFG_AE_BIT_INCREASE, 6);
            ybfwAeCustomAewinSizeSet(YBFW_3A_WIN_0,16, 16);
            ybfwAeCustomCbSet(YBFW_3A_WIN_0,AASTAT_INFO_FLAG_AEWIN_Y | AASTAT_INFO_FLAG_HIS_Y, ae_Cb);
            ybfwAeModeSet(YBFW_3A_WIN_0,YBFW_AE_MODE_INFO_ONLY);
        }
                 else if(isEqualStrings(argv[1], "target", CCS_NO)) {
                    INFO("Current AE Target = %d , Range = %d\n",AeTarget,AeRange);
                    if(argc==4)
                    {
                        if(AeTarget>AeRange)
                        {
                            AeTarget = v[2];
                            AeRange = v[3];
                            INFO("New AE Target = %d , Range = %d\n",AeTarget,AeRange);
                        }
                        else
                        {
                            ERR("Error ,  AE Target = %d , Range = %d , AE Target must be bigger than Range.\n",v[2],v[3]);
                        }
                    }
                 }
        else if (isEqualStrings(argv[1], "save", CCS_NO))
            saveAe = 1;
    } else
        appAeInit();

}
void cmdAWB(int argc,char **argv,UINT32**v){
    extern UINT32 dualAwb,saveAwb;
    if (argc>=2) {
        if (isEqualStrings(argv[1], "off", CCS_NO))
            ybfwAwbModeSet(YBFW_3A_WIN_0, YBFW_AWB_MODE_OFF);
        else if(isEqualStrings(argv[1], "on", CCS_NO)) {
            extern void awb_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
            dualAwb=0;
            ybfwAwbCfgSet(YBFW_3A_WIN_0,YBFW_AWB_USE_BAYER_PATTERN, 1 );
            ybfwAwbCustomAwbwinSizeSet( YBFW_SEN_ID_0, 16 , 16 );
            ybfwAwbCfgSet( YBFW_3A_WIN_0,YBFW_AWB_ACCUM_PERIOD , 1 );
            ybfwAwbCustomCbSet(YBFW_SEN_ID_0,YBFW_AEAWB_INFO_BIT_AWB_RGB, awb_Cb );
            ybfwAwbModeSet(YBFW_3A_WIN_0,YBFW_AWB_MODE_INFO_ONLY );
        } else if(isEqualStrings(argv[1], "dualon", CCS_NO)) {
            extern void awb_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
            dualAwb=1;
            ybfwAwbCfgSet(YBFW_3A_WIN_0,YBFW_AWB_USE_BAYER_PATTERN, 1 );
            ybfwAwbCustomAwbwinSizeSet( YBFW_3A_WIN_0, 16 , 16 );
            ybfwAwbCfgSet( YBFW_3A_WIN_0,YBFW_AWB_ACCUM_PERIOD , 1 );
            ybfwAwbCustomCbSet(YBFW_3A_WIN_0,YBFW_AEAWB_INFO_BIT_AWB_RGB, awb_Cb );
            ybfwAwbModeSet(YBFW_3A_WIN_0,YBFW_AWB_MODE_INFO_ONLY );
        } else if (isEqualStrings(argv[1], "save", CCS_NO))
            saveAwb = 1;
    } else
        appAwbInit();
}
void cmdAF(int argc,char **argv,UINT32**v){
    extern UINT32 saveAf;
    if (argc>=2) {
        if (isEqualStrings(argv[1], "off", CCS_NO))
            ybfwAfModeSet(YBFW_3A_WIN_0, YBFW_AF_MODE_OFF);
        else if (isEqualStrings(argv[1], "save", CCS_NO))
            saveAf = 1;
    } else
        appAfInit();
}

