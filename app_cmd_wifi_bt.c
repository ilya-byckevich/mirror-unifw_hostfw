/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_disk_api.h"
//#include "ykn_bfw_api/ybfw_usb_api.h"
//#include "ykn_bfw_api/ybfw_msg_def.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "common.h"
#include "gpio_custom.h"
#include "app_wifi_utility.h"
#include "app_bt_utility.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "app_common.h"

#define ALL_SERVICES  WIFI_INIT

#define WIFI_TEST_GPIO
#define BT_TEST_GPIO

char * wifi_modes[WIFI_MODE_TOTAL]={
  [WIFI_MODE_STATION]   ="station",
  [WIFI_MODE_AP]        ="ap",
  [WIFI_MODE_CONCURRENT]="concurrent",
  [ETHERNET_MODE]       ="ethernet",
};


void wifi_bt_init(){
    static UINT8 inited=0;

    if (!inited){
       // net_device_poweron(); //enable gpio reg
       // ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 100);/* 100 ms */
        net_system_init();
    }
}

#if SP5K_WIFION
    void getCurWiFiMode(){
        UINT8 mode;
        mode = appWifiInitModeGet();
        if (mode >= WIFI_MODE_TOTAL){
            printf("Current WiFi Mode - UNKNOWN\n");

        }else{
            printf("Current WiFi Mode - '%s'\n",wifi_modes[mode]);
        }
    }

    void setCurWiFiMode(UINT8 * mode_str){
        UINT8 mode,i;
        DBG("trace\n");
        if (!mode_str){
            printf("Wrong mode value : mode is null\n");
            return;
        }
        mode = WIFI_MODE_TOTAL;
        for (i = 0; i < WIFI_MODE_TOTAL; i++){
            if (isEqualStrings(mode_str, wifi_modes[i], CCS_NO )){
                mode = i;
                break;
            }
        }
        if (mode >= WIFI_MODE_TOTAL){
            printf("Wrong - UNKNOWN\n");
        }else{
            printf("apply WiFi Mode - '%s'\n",wifi_modes[mode]);
            appWifiInitModeSet(mode);
        }
    }

    #ifdef WIFI_TEST_GPIO
    void wifiGpioSwitch(UINT8 * en_str){
        if (!en_str){
                printf("Wrong gpio switch value : it is null\n");
                return;
        }

        if(isEqualStrings(en_str, "on", CCS_NO )){
            if (GRP_WIFI_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
                ybfwGpioCfgSet(GRP_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN);
                SET_WIFI_SHUTDOWN(0);
                ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 50);/* 50 ms */
                SET_WIFI_SHUTDOWN(1);
            }
        }else{
            if (GRP_WIFI_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
                ybfwGpioCfgSet(GRP_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN);
                SET_WIFI_SHUTDOWN(0);
                printf("disable %d for WIFI\n", CFG_WIFI_SHUTDOWN);
            }
        }
    }
    #endif

    void wifiSwitch(UINT8 en,UINT32 mask ){
        UINT8 mode;
        DBG("mask prev = %d\n",mask);
        if (mask == 0){
            mode = appWifiInitModeGet();
            mask = mode == WIFI_MODE_AP ? WIFI_INIT: STA_INIT ;
            DBG("mask after = %d\n",mask);
        }
        if (en){
            wifi_bt_init();
            appWiFiStartConnection(mask);
        }else{
            appWiFiStopConnection(mask);
        }
    }

    SINT32 cmdWiFi(SINT32 argc,UINT8 **argv,UINT32* v){
        UINT8* cmd = *argv++;v++;argc--;
        UINT32 mask=*v++;
        UINT8 mode;


        if (isEqualStrings(cmd, "on", CCS_NO ) ) {
            wifiSwitch(1,mask);
       }
       else if (isEqualStrings(cmd, "off", CCS_NO ) ) {
           wifiSwitch(0,mask);
       }else if (isEqualStrings(cmd, "gmode", CCS_NO ) ) {
           getCurWiFiMode();
       }else if (isEqualStrings(cmd, "smode", CCS_NO ) ) {
           cmd=*argv++;
           setCurWiFiMode(cmd);
       }
    #ifdef WIFI_TEST_GPIO
       else if (isEqualStrings(cmd, "gpio", CCS_NO ) ) {
           DBG("trace\n");
              cmd=*argv++;
              wifiGpioSwitch(cmd);
          }
    #endif
       else{
           return 0;
       }
       return 1;
    }


    void cmdWiFiHelp(){
        printf("wifi - wifi control\n\n");
        printf("\twifi <on/off> [bitmask in digit] \n");
        printf("\t\t bitmask - what is servises need to change: \n");
        printf("\t\t\t bit 0x%X  - on/off WiFi Connection\n",WIFI_LOAD);
        printf("\t\t\t bit 0x%X  - on/off DHCP Server\n",DHCP_SERVER);
        printf("\t\t\t bit 0x%X  - on/off Host AP Server\n",HOST_AP);
    //    printf("\t\t\t bit 0x%X  - on/off PTP Server \n",PTP_IP);
    //    printf("\t\t\t bit 0x%X  - on/off RTP Stream \n",RTP_STREM);
        printf("\t\t\t bit 0x%X  - on/off FTP Server\n",FTP_SRV);
        printf("\t\t\t bit 0x%X  - on/off BTN_LCK (change mode of processing)\n",BTN_LCK);
        printf("\t\t\t bit 0x%X  - on/off DO_ASYNC (change mode of processing)\n",DO_ASYNC);
        printf("\t\t\t bit 0x%X  - on/off STATION_MODE (change mode of processing)\n",STATION_MODE);
        printf("\t\t\t bit 0x%X  - on/off WIFI_SUSPEND (change mode of processing)\n",WIFI_SUSPEND);
        printf("\t\t\t bit 0x%X  - on/off DHCP_CLIENT (change mode of processing)\n",DHCP_CLIENT);
        printf("\twifi <on/off> without bitmask\n");
        printf("\t\t AP mode uses mask=0x%X \n",WIFI_INIT);
        printf("\t\t Station mode uses mask=0x%X \n",STA_INIT);
        printf("\n");
        printf("\twifi <gmode/smode> [mode]\n\n");
        printf("\t\t mode - mode of station: \n");
        printf("\t\t\t 'station'  - station mode\n");
        printf("\t\t\t 'ap'  - access point mode (default)\n");
        printf("\t\t\t 'concurrent' - concurrent mode ????\n");
        printf("\t\t\t 'ethernet' - ethernet mode\n");
    #ifdef WIFI_TEST_GPIO
        printf("\twifi gpio <on/off>\n\n");
    #endif

    }
#endif
//-----------------------------  BT -----------------

#ifdef BT_TEST_GPIO
void btGpioSwitch(UINT8 * en_str){
    if (!en_str){
            printf("Wrong gpio switch value : it is null\n");
            return;
    }

    if(isEqualStrings(en_str, "on", CCS_NO )){
        if (GRP_BT_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
            ybfwGpioCfgSet(GRP_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN);
            SET_BT_SHUTDOWN(1);
            ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 50); /* 50 ms */
            SET_BT_SHUTDOWN(0);
            printf("enable %d for BT\n", CFG_BT_SHUTDOWN);
        }
        //GRP_BT_PWR_EN is configured as BT_DEV_WAKE for brcm
        if (GRP_BT_PWR_EN != SP5K_GPIO_GRP_NO_USE) {
                ybfwGpioCfgSet(GRP_BT_PWR_EN, 1 << CFG_BT_PWR_EN, 1 << CFG_BT_PWR_EN);
                SET_BT_PWR_EN(0);
                ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 50); /* 50 ms */
                SET_BT_PWR_EN(1);
                printf("power enable %d for BT\n", CFG_BT_PWR_EN);
            }
    }else{
        if (GRP_BT_PWR_EN != SP5K_GPIO_GRP_NO_USE) {
            ybfwGpioCfgSet(GRP_BT_PWR_EN, 1 << CFG_BT_PWR_EN, 1 << CFG_BT_PWR_EN);
            SET_BT_PWR_EN(0);
            printf("power off %d for BT\n", CFG_BT_PWR_EN);
        }
        if (GRP_BT_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
            ybfwGpioCfgSet(GRP_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN);
            SET_BT_SHUTDOWN(1);//SET_BT_SHUTDOWN gpio with !-option
            printf("disable %d for BT\n", CFG_BT_SHUTDOWN);
        }
    }
}
#endif

#if SP5K_BTON
    SINT32 cmdBT(SINT32 argc,UINT8 **argv,UINT32* v){
        UINT8* cmd = *argv++;v++;argc--;
        UINT32 mask=*v++;
        UINT8 mode;
        if (isEqualStrings(cmd, "init", CCS_NO ) ) {
            wifi_bt_init();
            appBTStartConnection(BT_INT);
        }
    #ifdef BT_TEST_GPIO
        else if (isEqualStrings(cmd, "gpio", CCS_NO ) ) {
           DBG("trace\n");
              cmd=*argv++;
              btGpioSwitch(cmd);
        }
    #endif
        else{
           return 0;
       }
        return 1;
    }


    void cmdBTHelp(){
        printf("bt - bluetooth control\n\n");
        printf("\tbt init \n");
    #ifdef BT_TEST_GPIO
        printf("\tbt gpio <on/off>\n\n");
    #endif
    }
#endif
