#/**************************************************************************
# *
# *         Copyright (c) 2002-2016 by iCatch Technology Inc.
# *
# *  This software is copyrighted by and is the property of iCatch
# *  Technology Inc. All rights are reserved by iCatch Technology Inc.
# *  This software may only be used in accordance with the corresponding
# *  license agreement. Any unauthorized use, duplication, distribution,
# *  or disclosure of this software is expressly forbidden.
# *
# *  This Copyright notice MUST not be removed or modified without prior
# *  written consent of iCatch Technology Inc.
# *
# *  iCatch Technology Inc. reserves the right to modify this software
# *  without notice.
# *
# *  iCatch Technology Inc.
# *  19-1, Innovation First Road, Science-Based Industrial Park,
# *  Hsin-Chu, Taiwan.
# *
# **************************************************************************/
#init empty simple flavor variables before any include
CFLAGS :=
ASFLAGS :=

# !!! Adjust for your module !!!
ifndef PROJECT_ROOT_PATH
	PROJECT_ROOT_PATH := ../..
endif

# Block below should be placed in the every module
#------common block START------#
ifndef OUT_PATH
	OUT_PATH := $(PROJECT_ROOT_PATH)/tmp
endif

HOST_ROOT     := $(PROJECT_ROOT_PATH)/OS/v50_rtos/sphost
include $(HOST_ROOT)/host.def

PLATFORM_ROOT := $(HOST_ROOT)/../$(PLATFORM_PATH)
-include $(OUT_PATH)/include/config/auto.conf
include $(PLATFORM_ROOT)/tool_chain.def
#------common block END------#

TOOL_PATH := $(HOST_ROOT)/../tool
# output
SRC_DIR   := .
DEST_DIR  := $(OUT_PATH)/_$(PRJ)
OBJS_PATH := $(DEST_DIR)/_$(PLATFORM_PATH)

#---------------------------------------------------------------------------
# Variable definition:
#   1. Cross-compling tools
#   2. Linker script file
#   3. C complier options
#   4. Assembly compiler options
#   5. Linker options
#---------------------------------------------------------------------------
ifeq (cc,$(CC))
CC   := $(PREFIX)/gcc
AR   := $(PREFIX)/ar
ECHO := echo
endif

GOAL := libhostfw.a

CFLAGS += -Wall
CFLAGS += -Os
CFLAGS += -gstabs+
CFLAGS += -std=c99

CFLAGS += -I$(PLATFORM_ROOT)/inc
CFLAGS += -I$(PLATFORM_ROOT)/inc/api
CFLAGS += -I$(PLATFORM_ROOT)/resource/stream_lib/include

CFLAGS += -I./include
CFLAGS += -I$(HOST_ROOT)/include
CFLAGS += -I$(DEST_DIR)/include

CFLAGS += -I$(HOST_ROOT)/include/net
CFLAGS += -I$(HOST_ROOT)/include/oss

CFLAGS += -I$(PROJECT_ROOT_PATH)/unifw-layers/base-fw/
CFLAGS += -I$(PROJECT_ROOT_PATH)/unifw-layers/host-fw/
CFLAGS += -I$(PROJECT_ROOT_PATH)/unifw-layers/host-fw/network/

# FIXME
CFLAGS += -I$(PROJECT_ROOT_PATH)/tmp/include/generated

CGDBFLAGS = $(CFLAGS)
CGDBFLAGS += -g

ifdef CFG_HFW_BT
    CFLAGS += -DSP5K_BTON=1
endif

ifeq ($(PRJ_NAME), EVB)
    CFLAGS += -DEVB_NOWIFI=1
    CFLAGS += -DEVB_NOBT=1
endif

CFLAGS += -DAPP_BUILD=1
CFLAGS += -DPLATFORM_SP5K

#---------------------------------------------------------------------------
# Intermediate objects
#---------------------------------------------------------------------------
OBJS :=
OBJS += app_main.o
OBJS += app_3a.o
OBJS += app_cmd.o
OBJS += app_cmd_omf.o
OBJS += app_common.o
OBJS += app_cmd_fw.o
OBJS += fw_core.o

ifdef CFG_HFW_WIFI
    OBJS += app_cmd_wifi_bt.o
endif

ifdef CFG_HFW_ETHERNET
	OBJS += app_cmd_wifi_bt.o
endif

ifdef CFG_HFW_BT
	OBJS += app_cmd_wifi_bt.o
endif

OBJS += app_cmd_disk.o
OBJS += app_cmd_uart.o

#OBJS += app_cmd_hdmi.o
OBJS += app_cmd_ae.o
OBJS += app_cmd_awb.o
OBJS += app_cmd_af.o
OBJS += app_cmd_cmd.o
#OBJS += app_cmd_pv.o
#OBJS += app_cmd_360.o
OBJS += app_cmd_diq.o
OBJS += app_cmd_misc.o
#OBJS += app_cmd_ethercam.o
#OBJS += app_pv_cfg.o
#OBJS += app_360.o

ifeq ($(AMP_LINUX), YES)
 CFLAGS += -DCONFIG_AMP_LINUX=1
 CFLAGS += -DAMP_SHARE_MEMSIZE_MB=4
 CFLAGS += -DAMP_LINUX_MEMSIZE_MB=128-4 #reserve 4MB memory(0x5fc00000 ~ 0x60000000) as share memory
 CFLAGS += -DAMP_ICATOS_MEMSIZE_MB=384
endif

#---------------------------------------------------------------------------
# Referring to generic rules
#---------------------------------------------------------------------------
#
$(eval $(call GEN_GOAL_RULES))
