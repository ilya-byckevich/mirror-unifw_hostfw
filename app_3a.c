/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_ae_api.h"
#include "ykn_bfw_api/ybfw_af_api.h"
#include "ykn_bfw_api/ybfw_awb_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"

#include "app_common.h"


UINT32 saveAe, saveAf, saveAwb;
UINT32 dualAe=0, dualAwb=0;
UINT16 AeTarget=11000 , AeRange=800; 

#define AWB_SIZE 64

static void appPvAwbCb(const aaaProcInfo_t *pinfo, aaaProcResult_t *presult)
{
    if(pinfo->pawb_bayer){
        HOST_ASSERT(pinfo->awb_hgrid==AWB_SIZE && pinfo->awb_vgrid==AWB_SIZE);
        INFO("bayer %d %d %d %d\n", pinfo->pawb_bayer[0], pinfo->pawb_bayer[1],
            pinfo->pawb_bayer[pinfo->awb_hgrid*2], pinfo->pawb_bayer[pinfo->awb_hgrid*2+1]);
    } else {
        INFO("awb %d %d %d %d\n", pinfo->pawb_r[0], pinfo->pawb_g[0], pinfo->pawb_b[0], pinfo->pawb_gb[0]);
    }
    if (saveAwb) {
        static UINT32 saveAwbId = 0;
        char fname[32];
        sprintf(fname, "D:\\%02d.RAW", saveAwbId);
        INFO("%d x %d\n", pinfo->awb_hgrid, pinfo->awb_vgrid);
        ybfwFsSimpleWrite((UINT8*)fname, pinfo->pawb_bayer, pinfo->awb_hgrid*pinfo->awb_vgrid*8);
        saveAwb = 0;
        saveAwbId ++;
    }
}

void appAwbInit(void)
{
    ybfwAwbCfgSet(0, YBFW_AWB_ACCUM_PERIOD, 120);
    ybfwAwbCfgSet(0, YBFW_AWB_USE_BAYER_PATTERN, 1);
    ybfwAwbCustomAwbwinSizeSet(0, AWB_SIZE, AWB_SIZE);
    ybfwAwbCustomCbSet(0, YBFW_AEAWB_INFO_BIT_AWB_RGB, appPvAwbCb);
    ybfwAwbModeSet(0, YBFW_AWB_MODE_INFO_ONLY);
}

UINT16 RGBLuma[3][1024]={{0}};

void appPvAeCb(const aaaProcInfo_t *pinfo,
                aaaProcResult_t *presult)
{
    UINT16 i,j,index;
    static UINT32 t0=0;
    UINT32 t = ybfwMsTimeGet();
    INFO("appPvAeCb T:%d\n", t - t0);
    t0 = t;
    if (!pinfo->valid) {
        ERR("%s : psInfo == NULL\n", __FUNCTION__); return;
    }
    if (!pinfo->aewin_hgrid || !pinfo->aewin_vgrid ) {
        ERR("No AE Window!! hgrid=%d , vgrid=%d\n",pinfo->aewin_hgrid,pinfo->aewin_vgrid);
        return;
    }
    i = 0; INFO("%d %d %d\n", pinfo->paewin_r[i], pinfo->paewin_g[i], pinfo->paewin_b[i]);
    i = 16*32+16; INFO("%d %d %d\n", pinfo->paewin_r[i], pinfo->paewin_g[i], pinfo->paewin_b[i]);
    i = 32*32-1; INFO("%d %d %d\n", pinfo->paewin_r[i], pinfo->paewin_g[i], pinfo->paewin_b[i]);

    if (saveAe) {
        static UINT32 saveAeId = 0;
        char fname[32];
        sprintf(fname, "D:\\R%02d.BIN", saveAeId);
        ybfwFsSimpleWrite((UINT8*)fname, pinfo->paewin_r, 32*32*2);
        sprintf(fname, "D:\\G%02d.BIN", saveAeId);
        ybfwFsSimpleWrite((UINT8*)fname, pinfo->paewin_g, 32*32*2);
        sprintf(fname, "D:\\B%02d.BIN", saveAeId);
        ybfwFsSimpleWrite((UINT8*)fname, pinfo->paewin_b, 32*32*2);
        saveAe = 0;
        saveAeId ++;
    }
    for(j=0;j < pinfo->aewin_vgrid;++j) {
        for(i=0;i < pinfo->aewin_hgrid;++i ){
            index = j*pinfo->aewin_hgrid+i;
            RGBLuma[0][index] = pinfo->paewin_r[index];
            RGBLuma[1][index] = pinfo->paewin_g[index];
            RGBLuma[2][index] = pinfo->paewin_b[index];
        }
    }
}

void appAeInit(void)
{
    ybfwSystemCfgSet(YBFW_CDSP_INFO_CFG_GROUP, CDSP_INFO_CFG_AEWIN_VAL_WIDTH, 12);
    ybfwAeCfgSet(YBFW_SEN_ID_0, YBFW_AE_HIS_Y_REQUEST, 1);
    ybfwAeCustomAewinSizeSet(YBFW_SEN_ID_0, 32, 32);
    ybfwAeCustomCbSet(YBFW_SEN_ID_0, AASTAT_INFO_FLAG_AEWIN_Y |
        AASTAT_INFO_FLAG_AEWIN_R | AASTAT_INFO_FLAG_AEWIN_G | AASTAT_INFO_FLAG_AEWIN_B |
        AASTAT_INFO_FLAG_HIS_Y | AASTAT_INFO_FLAG_HIS_RGB ,
        appPvAeCb);
    ybfwAeModeSet(YBFW_SEN_ID_0, YBFW_AE_MODE_INFO_ONLY);
    ybfwAeCfgSet(YBFW_SEN_ID_0, YBFW_AE_ACCUM_PERIOD, 120);
    ybfwAeCfgSet(YBFW_SEN_ID_0, YBFW_AE_RGB_USE_BAYER_PATTERN, 1);
    ybfwSystemCfgSet(YBFW_CDSP_AEAF_SW_FLIP_CFG, CDSP_AA_SW_FLIP_CFG_AEWIN, CDSP_FLIP_VAL_NOFLIP);
    ybfwAeCfgSet(YBFW_SEN_ID_0, YBFW_AE_AA_TASK_PRIORITY_CHANGE, 14);
}


static UINT32 afBuf[255];

static void appPvAfCb(UINT32 fid)
{
    ybfwAfWinGet(YBFW_SEN_ID_0, fid, AF_VAL_SINGLE_AF, afBuf);
    INFO("afCb %d %d\n", fid, afBuf[0]);
}

void appAfInit(void)
{
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_WIN_TYPE, YBFW_AF_WIN_TYPE_AF|YBFW_AF_WIN_TYPE_SAF);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_STAT_MODE, 5);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_NOISE_THR, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_GAIN, 64);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_CORING_THR, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_CFG_GAMMA_TYPE, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_CORING_VALUE, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SINGLE_MODE, 1);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SINGLE_MEDIAN, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SINGLE_LPF, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SINGLE_HPF, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_IIR_ENABLE, 1);
    ybfwAfIirCfg_t iir = {
        { 6, 65536-24, 36, 65536-24, 6, 0 },
        { 0, 4, 0 }
    };
    ybfwAfCfgSet(0, YBFW_AF_IIR_FILTER, (UINT32)&iir);
    ybfwAfEfilterCfg_t eflt = {
        { 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, }
    };
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_E_FILTER, (UINT32)&eflt);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_E_FILTER_TYPE, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SLR_ENABLE, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SLR_THR, 0);
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SLR_POS, 0);
    ybfwAfSlrCfg_t slrTbl = { { 0 } };
    ybfwAfCfgSet(YBFW_3A_WIN_0, YBFW_AF_SLR_TABLE, (UINT32)&slrTbl);
#if 0
    ybfwAfCfgSet(0, YBFW_VAF_CFG_GAMMA_TYPE, 3);
    ybfwAfCfgSet(0, YBFW_VAF_GAIN, 9);
    ybfwAfCfgSet(0, YBFW_VAF_CFG_FILTER_TYPE, 1);
    ybfwAfCfgSet(0, YBFW_VAF_STAT_MODE, 2);
    ybfwAfCfgSet(0, YBFW_VAF_CORING_THR, 31);
    ybfwAfCfgSet(0, YBFW_VAF_CORING_VALUE, 21);
    ybfwAfCfgSet(0, YBFW_VAF_NOISE_THR, 34);
    ybfwAfCfgSet(0, YBFW_VAF_WIN_BIN1_THR, (872<<16) | 755);
    ybfwAfCfgSet(0, YBFW_VAF_WIN_BIN2_THR, (977<<16) | 182);
#endif
    ybfwAfCustomCbSet(YBFW_3A_WIN_0, appPvAfCb);
    ybfwAfModeSet(YBFW_3A_WIN_0, YBFW_AF_MODE_ON);
}

void ae_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult )
{
#if 1
    ybfwExpLimit_t explimit;
    ybfwAgcLimit_t agclimit;
    UINT32 i , Luma=0 , AGCSTEP;
    static SINT32 EXPINIT=0 , EXPMIN UNUSED=0 , EXPMAX=0 , EXP = 0;
    static UINT32 AGCINIT=0 , AGC=0 , AGCLIMIT=0 , AGCSTEPUINT=1 , AGCOFFSET=0;

    for(i=0;i<16*16;i++)
    {
        Luma += pinfo->paewin_y[i];
    }

    Luma/=256;

    if(AGCLIMIT==0)
    {
        ybfwAeExpLimitGet(0,/*sensorModeGet()*/0x30,&explimit);

        EXPMIN = explimit.expIdxTblMin;
        EXPMAX = explimit.expIdxMax;

        ybfwAeAgcLimitGet(0,/*sensorModeGet()*/0x30,&agclimit);

        AGCLIMIT = agclimit.agcIdxMax-agclimit.agcIdxMin;

        if(AGCLIMIT>255)
        {
            AGCLIMIT = 320;
            AGCSTEPUINT=4;
        }
        else
        {
            AGCLIMIT = 80;
        }

        AGCOFFSET = agclimit.agcIdxMin;
        AGCINIT=agclimit.agcIdxMin;

        if(AGCOFFSET==80)/*AB*/
        {
            EXPINIT=144;
        }
        else
        {
            EXPINIT=96;
        }

        EXP = EXPINIT;
        AGC = AGCINIT;

    }

#if 0
    if(Luma>11000)
        AGCSTEP = AGCSTEPUINT*Luma/11000;
    else
        AGCSTEP = AGCSTEPUINT*11000/Luma;

    if(AGCSTEP>16*AGCSTEPUINT)
        AGCSTEP = 16*AGCSTEPUINT;
#else
        AGCSTEP = AGCSTEPUINT;
#endif


    if(AGCOFFSET==80)/*AB*/
    {
        if(Luma>AeTarget+AeRange)
        {
            if(AGC <= AGCLIMIT-AGCSTEP+AGCOFFSET)
                AGC=AGC+AGCSTEP;
            else if(EXP < EXPMAX)
                EXP=EXP+1;
        }
        else if(Luma<AeTarget-AeRange)
        {
            if(EXP > EXPINIT)
                EXP=EXP-1;
            else if(AGC >= AGCSTEP+AGCOFFSET)
                AGC=AGC-AGCSTEP;
        }
    }
    else/*OM*/
    {
        if(Luma>AeTarget+AeRange)
        {
            if(AGC >=AGCSTEP)
                AGC=AGC-AGCSTEP;
            else if(EXP < EXPMAX)
                EXP=EXP+1;
        }
        else if(Luma<AeTarget-AeRange)
        {
            if(EXP > EXPINIT)
                EXP=EXP-1;
            else if(AGC <= AGCLIMIT-AGCSTEP)
                AGC=AGC+AGCSTEP;
        }
    }

    ybfwPreviewExpAgcSet(YBFW_SEN_ID_0,EXP, AGC);
    ybfwStillCapExpAgcSet(YBFW_SEN_ID_0,EXP, AGC);

    if(dualAe==1)
    {
        ybfwPreviewExpAgcSet(YBFW_SEN_ID_1,EXP, AGC);
        ybfwStillCapExpAgcSet(YBFW_SEN_ID_1,EXP, AGC);
    }
#endif

}

void awb_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult )
{
    UINT32 shiftBit=0;
    UINT32 imgWid = pinfo->awb_hgrid*2;
    UINT32 imgHeig = pinfo->awb_vgrid*2;
    UINT32 sampX = 16;
    UINT32 sampY = 16;
    UINT32 bf = pinfo->awb_bayer_fmt;
    UINT32 offsR, offsGr, offsB, offsGb;
    UINT32 x0 = 0 ;
    UINT32 y0 = 0 ;
    UINT32 R=0,G=0,B=0 , RGBCnt=0;
    UINT16 RGain=1024,BGain=1024;
    static UINT16 PreRGain=0,PreBGain=0;
#if 0
    static UINT32 cnt=0;

    cnt++;

    if(cnt==10)
    {
        INFO("wb data %d , %d , %d\n",pinfo->awb_hgrid,pinfo->awb_vgrid , pinfo->awb_bayer_fmt);

        ybfwFsSimpleWrite( "D:\\WB.RAW",pinfo->pawb_bayer, pinfo->awb_hgrid*2*pinfo->awb_vgrid*2*2 );
    }
#endif
    if( (bf&0x2) ) {
        offsR = offsGr = imgWid;
        offsB = offsGb = 0;
    }
    else {
        offsR = offsGr = 0;
        offsB = offsGb = imgWid;
    }

    if( (bf&0x1) ) {
        offsGr += (1);
        offsB += (1);
    }
    else {
        offsR += (1);
        offsGb += (1);
    }

    UINT32 cropWid = imgWid;
    UINT32 cropHeig = imgHeig;

    cropWid = (cropWid&0xFFFFFFFE);
    cropHeig = (cropHeig&0xFFFFFFFE);

    x0 = ((x0&0xFFFFFFFE)<<5);
    y0 = ((y0&0xFFFFFFFE)<<5);

    if( sampX > (cropWid>>1) ) sampX = (cropWid>>1);
    if( sampY > (cropHeig>>1) ) sampY = (cropHeig>>1);

    UINT32 advX = (cropWid<<5) / (sampX-1);
    UINT32 advY = (cropHeig<<5) / (sampY-1);

    UINT16 * raw = pinfo->pawb_bayer;

    UINT16 * line;
    UINT16 * pt;
    UINT32 offsX, offsY;
    UINT32 x, y;

    offsY = y0;

    for( y = 0; y < sampY; y++, offsY+=advY )
    {
        line = raw + ((offsY>>5)&0xFFFFFFFE)*imgWid;
        offsX = x0;

        for( x = 0; x < sampX; x++, offsX+=advX ) {
            pt = line + ((offsX>>5)&0xFFFFFFFE);

                    if(pt[offsB]<50000&&pt[offsGb]<50000&&pt[offsGr]<50000&&pt[offsR]<50000&&
                        pt[offsB]>1000&&pt[offsGb]>1000&&pt[offsGr]>1000&&pt[offsR]>1000)
                    {
                    B += (pt[offsB]>>shiftBit);
                    G += ((pt[offsGb]+pt[offsGr])>>(1+shiftBit));
                    R += (pt[offsR]>>shiftBit);
                    RGBCnt++;
                     }
        }
    }

    R=R/RGBCnt;
    G=G/RGBCnt;
    B=B/RGBCnt;


    if(R!=0&&B!=0)
    {
        RGain = (UINT16)((G*1024)/R);
        BGain = (UINT16)((G*1024)/B);

              if(PreRGain!=0&&PreBGain!=0)
              {
                    RGain = (RGain*20 + PreRGain*80)/100;
                    BGain = (BGain*20 + PreBGain*80)/100;

                    PreRGain = RGain;
                    PreBGain = BGain;
              }
              else
              {
                    PreRGain = RGain;
                    PreBGain = BGain;
              }

            ybfwIqModeSet(YBFW_SEN_ID_0,YBFW_MODE_STILL_PREVIEW  );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_R, RGain );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GR, 1024 );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_B, BGain );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GB, 1024 );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_ENABLE, 1);
            ybfwIqModeSetDone(YBFW_SEN_ID_0,YBFW_MODE_STILL_PREVIEW );

            ybfwIqModeSet(YBFW_SEN_ID_0,YBFW_MODE_STILL_SNAP  );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_R, RGain );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GR, 1024 );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_B, BGain );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GB, 1024 );
            ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_ENABLE, 1);
            ybfwIqModeSetDone(YBFW_SEN_ID_0,YBFW_MODE_STILL_SNAP );

            if(dualAwb==1)
            {
                ybfwIqModeSet(YBFW_SEN_ID_1,YBFW_MODE_STILL_PREVIEW  );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_R, RGain );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GR, 1024 );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_B, BGain );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GB, 1024 );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_ENABLE, 1);
                ybfwIqModeSetDone(YBFW_SEN_ID_1,YBFW_MODE_STILL_PREVIEW );

                ybfwIqModeSet(YBFW_SEN_ID_1,YBFW_MODE_STILL_SNAP  );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_R, RGain );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GR, 1024 );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_B, BGain );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0,YBFW_IQ_WB_GAIN_GB, 1024 );
                ybfwIqCfgSet( YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_ENABLE, 1);
                ybfwIqModeSetDone(YBFW_SEN_ID_1,YBFW_MODE_STILL_SNAP );
            }

     }



#if 0
    UINT32 ClrTempR , ClrTempB;

    if(RGain>1024 && RGain<2048)
        ClrTempR = 240+(RGain-1024)*500/1024;
    else if(RGain<=1024)
        ClrTempR=240;
    else
        ClrTempR=740;

    if(BGain>1024 && BGain<2048)
        ClrTempB = 740-(BGain-1024)*500/1024;
    else if(BGain<=1024)
        ClrTempB = 740;
    else
        ClrTempB = 240;

    ybfwDynIqCfgSet(YBFW_DYN_IQ_CFG_AWB_CT_SET,0,(ClrTempR+ClrTempB)/2,1);

    INFO("awb result : %d , %d , %d\n",RGain,BGain , (ClrTempR+ClrTempB)/2);
#endif
}



