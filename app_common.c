/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app_common.h"



UINT8 isEqualStrings(const UINT8 * str1, const UINT8 *str2, CmpCaseSensitive case_sens_state){

  if ((!str1)||(!str2)) return 0;

  if (case_sens_state == CCS_YES){
    return ((strcmp(str1, str2) == 0) && (strlen(str1) == strlen(str2))) ? 1 : 0;
  }else{
    return ((strcasecmp(str1, str2) == 0) && (strlen(str1) == strlen(str2))) ? 1 : 0;
  }
}
