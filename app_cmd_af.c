/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_af_api.h"
#include "ykn_bfw_api/ybfw_ae_api.h"

#include "app_common.h"

extern UINT32 saveAf;
int cmdAF(int argc,char **argv,UINT32*v){
    char* cmd = *argv++;v++;argc--;
    /**/
    if(argc<0) {appAfInit();}
    else if (isEqualStrings(cmd, "off", CCS_NO ) ) {ybfwAfModeSet(YBFW_3A_WIN_0, YBFW_AF_MODE_OFF);}
    else if (isEqualStrings(cmd, "save", CCS_NO)) {saveAf = 1;}
    else{return 0;}
    return 1;
}




