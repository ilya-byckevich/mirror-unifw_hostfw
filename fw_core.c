/**
 * @file fw_core.c
 * @author ilya(i.byckevich@yukonww.com)
 * @brief Jun 3, 2020
 *
 * Detailed description if necessary
 */

#include <stdio.h>
#include <stdarg.h>
#include "common.h"
#include "app_cmd_fw.h"
#include "api/sp5k_os_api.h"
#include "api/sp5k_fs_api.h"

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"

struct transitions_table {
    fw_state_t new_state;
    const transition_cb def_runner;
    transition_cb custom_runner;
};

static YBFW_THREAD *thread_handle;
static YBFW_QUEUE queue_handle;

static UINT32 file_fd;
static UINT32 enabled_states_mask, watchdog_num;
static UINT8 fw_module_is_active = FALSE;
static char log_name[MAX_LOG_NAME];
static const char const *log_prefix = "fw log: ";

static struct fw_cb_args backup_args;
static struct fw_param_args local_params;

static void fw_thread(ULONG);
static void init_states(struct fw_cb_item *cb_list, UINT8 list_sz);
static void reset_fsm_states();
static int setup_watchdog_timer();
static int log_write(UINT8 *string, ...);
static BOOL state_is_enabled(fw_state_t state);

/* Defaults states */
static int state_idle_def(struct fw_cb_args args);
static int state_check_fw_def(struct fw_cb_args args);
static int state_allocate_space_def(struct fw_cb_args args);
static int state_extract_fm_def(struct fw_cb_args args);
static int state_decrypt_fm_def(struct fw_cb_args args);
static int state_validate_fw_def(struct fw_cb_args args);
static int state_check_fw_comp_def(struct fw_cb_args args);
static int state_update_fw_comp_def(struct fw_cb_args args);
static int state_post_update_fw_def(struct fw_cb_args args);
static int state_reboot_def(struct fw_cb_args args);

/* "post_cb" and "ev_action_cb" only for caller. Do not set it here */
static struct transitions_table fsm[StateMax][evMax] = {
     /* From StateIdle state to other  */
    [StateIdle][evCheckFwFile] = { StateCheckFwFile, state_check_fw_def },
    [StateIdle][evAllocateSpace] = { StateAllocateSpace, state_allocate_space_def },
    [StateIdle][evExtractFw] = { StateExtractFw, state_extract_fm_def },
    [StateIdle][evDecryptFw] = { StateDecryptFw, state_decrypt_fm_def },
    [StateIdle][evValidateFw] = { StateValidateFw, state_validate_fw_def },
    [StateIdle][evCheckFwComp] = { StateCheckFwComp, state_check_fw_comp_def },
    [StateIdle][evUpdateFWComp] = { StateUpdateFWComp, state_update_fw_comp_def },
    [StateIdle][evPostUpdateFW] = { StatePostUpdateFW, state_post_update_fw_def },

    /* From StateCheckFwFile state to other  */
    [StateCheckFwFile][evIdle] = { StateIdle, state_idle_def },
    [StateCheckFwFile][evReboot] = { StateReboot, state_reboot_def},
    [StateCheckFwFile][evAllocateSpace] = { StateAllocateSpace, state_allocate_space_def },

    /* From StateAllocateSpace state to other  */
    [StateAllocateSpace][evIdle] = { StateIdle, state_idle_def },
    [StateAllocateSpace][evReboot] = { StateReboot, state_reboot_def},
    [StateAllocateSpace][evExtractFw] = { StateExtractFw, state_extract_fm_def },

    /* From StateExtractFw state to other  */
    [StateExtractFw][evIdle] = { StateIdle, state_idle_def },
    [StateExtractFw][evReboot] = { StateReboot, state_reboot_def},
    [StateExtractFw][evDecryptFw] = { StateDecryptFw, state_decrypt_fm_def },

    /* From StateDecryptFw state to other  */
    [StateDecryptFw][evIdle] = { StateIdle, state_idle_def },
    [StateDecryptFw][evReboot] = { StateReboot, state_reboot_def},
    [StateDecryptFw][evValidateFw] = { StateValidateFw, state_validate_fw_def },

    /* From StateValidateFw state to other  */
    [StateValidateFw][evIdle] = { StateIdle, state_idle_def },
    [StateValidateFw][evReboot] = { StateReboot, state_reboot_def},
    [StateValidateFw][evCheckFwComp] = { StateCheckFwComp, state_check_fw_comp_def },

    /* From StateCheckFwComp state to other  */
    [StateCheckFwComp][evIdle] = { StateIdle, state_idle_def },
    [StateCheckFwComp][evReboot] = { StateReboot, state_reboot_def},
    [StateCheckFwComp][evUpdateFWComp] = { StateUpdateFWComp, state_update_fw_comp_def },

    /* From StateUpdateFWComp state to other  */
    [StateUpdateFWComp][evIdle] = { StateIdle, state_idle_def },
    [StateUpdateFWComp][evReboot] = { StateReboot, state_reboot_def},
    [StateUpdateFWComp][evPostUpdateFW] = { StatePostUpdateFW, state_post_update_fw_def },

    /* From StatePostUpdateFW state to other  */
    [StatePostUpdateFW][evIdle] = { StateIdle, state_idle_def },
    [StatePostUpdateFW][evReboot] = { StateReboot, state_reboot_def},
};

static int state_idle_def(struct fw_cb_args args)
{
    INFO("");
    return 0;
}

static int state_check_fw_def(struct fw_cb_args args)
{
    UINT32 ms_timeout = 0;

    INFO("");

    if (args.param_store) {

        if (args.param_store->wathdog_def_timeout_ms) {
            ms_timeout = args.param_store->wathdog_def_timeout_ms;
            setup_watchdog_timer(ms_timeout);
        }
        if (!args.param_store->fw_name) {
            INFO("Invalid file name!");
            return STAT_FAIL;
        }
        if (ybfwFsFileOpen(args.param_store->fw_name, YBFW_FS_OPEN_RDONLY)
                == 0) {
            INFO("File not found :(");
            return STAT_FILE_NOT_EXIST;
        }else{
            INFO("File found :)");
        }
    }else if (local_params.wathdog_def_timeout_ms) {
            setup_watchdog_timer(local_params.wathdog_def_timeout_ms);
    }
    return STAT_SUCCESS;
}

static int state_allocate_space_def(struct fw_cb_args args)
{
    INFO("");

    return STAT_SUCCESS;
}

static int state_extract_fm_def(struct fw_cb_args args)
{
    INFO("");
    return STAT_SUCCESS;
}

static int state_decrypt_fm_def(struct fw_cb_args args)
{
    INFO("");
    return STAT_SUCCESS;
}

static int state_validate_fw_def(struct fw_cb_args args)
{
    INFO("");
    return STAT_SUCCESS;
}

static int state_check_fw_comp_def(struct fw_cb_args args)
{
    INFO("");
    return STAT_SUCCESS;
}

static int state_update_fw_comp_def(struct fw_cb_args args)
{
    INFO("");
    return STAT_SUCCESS;
}

static int state_post_update_fw_def(struct fw_cb_args args)
{
    INFO("");
    sp5kTimerEnable(watchdog_num, FALSE);
    return STAT_SUCCESS;
}

static int state_reboot_def(struct fw_cb_args args)
{
    INFO("");
    ybfwPowerCtrl(SP5K_POWER_OFF, 10);
    return STAT_SUCCESS;
}

static void fw_thread(ULONG arg)
{
    int ret, result = 0;
    /* Allocate max msg size */
    UINT8 raw_msg_in[QUEUE_ITEM_SIZE * 4] = { 0 };
    struct fw_msg fw_msg_in = { 0 };
    struct fw_cb_args args = { .curr_state = StateIdle };

    fw_state_t new_state = StateIdle;
    transition_cb runner = NULL;

    if (StateMax > 32) {
        INFO("Error: I support only 32 states, change enabled_states_mask!");
        return;
    }
    while (1) {
        ret = ybfwOsQueueReceive(&queue_handle, (VOID*) &raw_msg_in,
        TX_WAIT_FOREVER);
        if (ret) {
            INFO("Error: sp5kOsQueueReceive\n");
        }
        memcpy(&fw_msg_in, raw_msg_in, sizeof(struct fw_msg));

        if (!state_is_enabled(
                fsm[args.curr_state][fw_msg_in.event].new_state)) {
            INFO("State %d skipped",
                    fsm[args.curr_state][fw_msg_in.event].new_state);
            continue;
        }

        args.curr_event = fw_msg_in.event;
        args.param_store = fw_msg_in.param;
        args.ev_action_cb =
                fw_msg_in.param ? fw_msg_in.param->ev_action_cb : NULL;

        if (fsm[args.curr_state][args.curr_event].custom_runner) {
            runner = fsm[args.curr_state][args.curr_event].custom_runner;
        } else {
            runner = fsm[args.curr_state][args.curr_event].def_runner;
        }

        if (runner) {
            new_state = fsm[args.curr_state][args.curr_event].new_state;
        }

        if (runner != NULL) {

            memcpy(&backup_args, &args, sizeof(struct fw_cb_args));

            result = runner(args);
            if (args.param_store
                    && (args.param_store->param == paramActionOnSuccess
                            || args.param_store->param == paramActionOnFail)
                    && args.ev_action_cb) {
                args.ev_action_cb(args.curr_state, (void*) &result);
            }
            if (!log_write("state %d exit with %d \n", args.curr_state,
                    result)) {
                INFO("write error");
            }
            if (fw_msg_in.ev_post_cb) {
                fw_msg_in.ev_post_cb(new_state, &result);
            }
        } else {
            INFO("Invalid fsm route: from %d to %d state. Go to idle",
                    args.curr_state, new_state);
            args.curr_state = StateIdle;
        }

        if (result != STAT_SUCCESS) {
            INFO("%d state failed. Go to Idle", args.curr_state);
            args.curr_state = StateIdle;
        } else {
            args.curr_state = new_state;
        }
    }
}

static void init_states(struct fw_cb_item *cb_list, UINT8 list_sz)
{
    if (list_sz == 0 || cb_list == NULL ) {
        reset_fsm_states();
        return;
    }

    for (UINT8 i = 0; i < list_sz; i++) {
        if (cb_list[i].cb && cb_list[i].args.curr_state < StateMax) {
            fsm[cb_list[i].args.curr_state][cb_list[i].args.curr_event].custom_runner =
                    cb_list[i].cb;
        }
    }
}

static void reset_fsm_states()
{
    for (UINT8 i = 0; i < StateMax; i++) {
        for (UINT8 y = 0; y < evMax; y++) {
            fsm[i][y].custom_runner = NULL;
        }
    }
}

static void watchdog_tmr_timeout_cb(void)
{
    DBG("");
    if (backup_args.ev_action_cb) {
        backup_args.ev_action_cb(backup_args.curr_state, NULL);
    }
    fsFileClose(file_fd);
    file_fd = 0;
    ybfwPowerCtrl(SP5K_POWER_OFF, 10);
}

static int setup_watchdog_timer(UINT32 ms)
{
    UINT32 is_enabled = 0, ms_now = 0, ret = FAIL;

    for (UINT8 i = 0; i < MAX_HW_TIMERS; i++) {
        ret |= sp5kTimerCfgGet(i, &ms_now, &is_enabled);
        if (is_enabled == 0) {
            INFO("%u %u", i, ms);
            /* FIXME
             * sp5kTimerUsIsrReg - callback called immediately
             * sp5kTimerEnable   - I can't disable timer!
             * */
            //sp5kTimerUsIsrReg(i, 1, ms * 1000, watchdog_tmr_timeout_cb);
            ret |= sp5kTimerIsrReg(i, ms, watchdog_tmr_timeout_cb);
            ret |= sp5kTimerEnable(i, TRUE);
            watchdog_num = i;
            break;
        }
    }
    return ret;
}

static int log_write(UINT8 *string, ...)
{
    UINT32 ret = SUCCESS;
    UINT8 buffer[MAX_LOG_MSG] = { 0 };
    va_list args;

    memcpy(buffer, log_prefix, strlen(log_prefix));

    /* FIXME can be overwritten */
    if (strlen(string) > MAX_LOG_MSG - strlen(log_prefix)) {
        INFO("Error: message too long\n");
        return 0;
    }

    va_start(args, string);
    if (vsprintf(buffer + strlen(log_prefix), string, args) < 0) {
        INFO("Error: vsprintf\n");
        va_end(args);
        return 0;
    }
    va_end(args);

    if (!strlen(log_name) || !local_params.savelog_enabled) {
        return 1;
    }

    if (!file_fd) {
        file_fd =
                sp5kFsFileOpen((UINT8*) log_name,
                        (SP5K_FS_OPEN_RDWR | SP5K_FS_OPEN_APPEND
                                | SP5K_FS_OPEN_CREATE));
        if (file_fd == 0) {
            INFO("Error: file_fd\n");
            return 0;
        }
    }

    ret = sp5kFsFileWrite(file_fd, buffer, strlen(buffer));
    if (ret != strlen(buffer)) {
        INFO("Error: sp5kFsFileWrite %u %u \n", ret, strlen(buffer));
        return 0;
    }
    return ret;
}

static BOOL state_is_enabled(fw_state_t state)
{
    for (UINT8 i = 0; i < StateMax; i++) {
        for (UINT8 y = 0; y < evMax; y++) {
            if (fsm[i][y].new_state == state) {
                return !!(enabled_states_mask & (1 << state));
            }
        }
    }
    return FALSE;
}

UINT32 send_to_fw(struct fw_msg fw_msg_in)
{
    UINT32 ret = STAT_SUCCESS;

    if (queue_handle) {
        ret = sp5kOsQueueSend(&queue_handle, (void*) &fw_msg_in, TX_NO_WAIT);
    } else {
        INFO("Error: call init_fw_module() first !\n");
    }
    return ret;
}

void fw_prm_set_timeout(UINT32 ms)
{
    local_params.wathdog_def_timeout_ms = ms;
}

UINT32 fw_get_timeout()
{
    return local_params.wathdog_def_timeout_ms;
}

void fw_prm_set_log_name(char *name)
{
    strcpy(log_name, name);
}

char* fw_get_log_name()
{
    return log_name;
}

void fw_skip_state(fw_state_t state)
{
    enabled_states_mask &= ~(1 << state);
}

void fw_enable_state(fw_state_t state)
{
    enabled_states_mask |= (1 << state);
}

void fw_prm_delete_enabled(BOOL is_enabled)
{
    local_params.delete_fw_enabled = is_enabled;
}

void fw_prm_reserve_space(UINT32 size_kb)
{
    local_params.reserve_space_size = size_kb;
}

void fw_prm_log_enabled(BOOL state)
{
    local_params.savelog_enabled = state;
}

UINT32 fw_start_update(struct fw_param_args params)
{
    struct fw_cb_item *cb_list = { 0 };
    UINT8 list_sz = 0;

    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, .param = &params,},
            { .event = evAllocateSpace, },
            { .event = evExtractFw, },
            { .event = evDecryptFw, },
            { .event = evValidateFw, },
            { .event = evCheckFwComp, },
            { .event = evUpdateFWComp, },
            { .event = evPostUpdateFW, },
            { .event = evIdle, .param = &params, }, };
    DBG("");
    fw_status_t stat = init_fw_module(cb_list, list_sz);

    if (stat != STAT_SUCCESS && stat != STAT_ALREADY_ACTIVE) {
        return STAT_FAIL;
    }
    DBG("");
    fw_prm_set_timeout(params.wathdog_def_timeout_ms);
    fw_prm_reserve_space(params.reserve_space_size);
    fw_prm_log_enabled(params.savelog_enabled);
    fw_prm_delete_enabled(params.delete_fw_enabled);
    for (int i = 0; i < sizeof(msgs) / sizeof(msgs[0]); i++) {
        send_to_fw(msgs[i]);
    }
    DBG("");
    return STAT_SUCCESS;
}

UINT32 init_fw_module(struct fw_cb_item *cb_list, UINT8 list_sz)
{
    int ret = STAT_FAIL;

    if (fw_module_is_active) {
        return STAT_ALREADY_ACTIVE;
    }

    init_states(cb_list, list_sz);
    enabled_states_mask = ALL_STATES;

    thread_handle = ybfwOsThreadCreate("fw_thread", fw_thread, 0,
                                        THREAD_FW_PRIORITY, THREAD_FW_PRIORITY,
                                        THREAD_FW_TIME_SLICE, TX_AUTO_START);

    if (!thread_handle) {
        INFO("Error: can't create fw_thread\n");
        return STAT_FAIL;
    }

    ret = ybfwOsQueueCreate(&queue_handle, "fw_rx_queue",
                            QUEUE_ITEM_SIZE, NULL,
                            QUEUE_MAX_ITEMS * sizeof(struct fw_msg));

    if (ret) {
        INFO("Error: can't create fw_rx_queue\n");
        return STAT_FAIL;
    }

    fw_module_is_active = TRUE;
    return STAT_SUCCESS;
}

int deinit_fw_module()
{
    UINT32 ret;
    INFO("");

    ret = ybfwOsQueueDelete(&queue_handle);
    if (ret) {
        INFO("Error: sp5kOsQueueDelete\n");
    }

    ret = ybfwOsThreadDelete(thread_handle);
    if (ret) {
        INFO("Error: sp5kOsThreadDelete\n");
    }

    if (file_fd) {
        sp5kFsFileClose(file_fd);
        file_fd = 0;
    }
    reset_fsm_states();
    fw_module_is_active = FALSE;
    return ret;
}
