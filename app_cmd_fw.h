/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
 */
#ifndef APP_CMD_FW_H_
#define APP_CMD_FW_H_

#define THREAD_FW_PRIORITY      25
#define THREAD_FW_TIME_SLICE    100
#define QUEUE_MAX_DATA_SIZE     16
#define QUEUE_MAX_ITEMS         10
/* Fixed https://k-public2.icatchtek.com/doku.php?id=public:api:sp5kosqueuecreate */
#define QUEUE_ITEM_SIZE         4
#define MAX_HW_TIMERS           7
#define MAX_LOG_NAME            32
#define MAX_LOG_MSG             128
#define MAX_FW_NAME             32
#define ALL_STATES              0xFFFFFFFF

typedef enum {
    STAT_SUCCESS,
    STAT_START,
    STAT_FAIL,
    STAT_FILE_NOT_EXIST,
    STAT_FAIL_RESERVE_SPACE,
    STAT_FILE_IS_CORRUPT,
    STAT_FAIL_DECRYPT,
    STAT_WRONG_FW,
    STAT_FAIL_INFO,
    STAT_FAIL_CRC,
    STAT_FAIL_INIT,
    STAT_FAIL_UPDATE,
    STAT_ALREADY_ACTIVE
} fw_status_t;

typedef enum {
    StateIdle = 0,
    StateCheckFwFile,
    StateAllocateSpace,
    StateExtractFw,
    StateDecryptFw,
    StateValidateFw,
    StateCheckFwComp,
    StateUpdateFWComp,
    StatePostUpdateFW,
    StateReboot,
    StateMax
} fw_state_t;

typedef enum {
    evIdle = 0,
    evCheckFwFile,
    evAllocateSpace,
    evExtractFw,
    evDecryptFw,
    evValidateFw,
    evCheckFwComp,
    evUpdateFWComp,
    evPostUpdateFW,
    evReboot,
    evMax
} fw_event_t;

typedef enum {
    paramWatchdog = 1,
    paramSavelog,
    paramActionOnSuccess,
    paramActionOnFail,
    paramStateReboot,
    paramDontdeletefw,
    paramReservSpaceSize
} fw_event_param_t;

typedef int (*action_cb)(fw_state_t state, void *p);
typedef int (*post_cb)(fw_state_t state, void *p);

struct fw_cb_args {
    fw_state_t curr_state;
    fw_event_t curr_event;
    action_cb ev_action_cb;
    struct fw_param_args *param_store;
};

typedef int (*transition_cb)(struct fw_cb_args args);

struct fw_cb_item {
    transition_cb cb;
    struct fw_cb_args args;
};

struct fw_param_args {
    fw_event_param_t param;
    action_cb ev_action_cb;
    UINT32 wathdog_def_timeout_ms;
    UINT32 reserve_space_size;
    UINT8 savelog_enabled;
    UINT8 delete_fw_enabled;
    UINT8 fw_name[MAX_FW_NAME];
};

struct fw_msg {
    struct fw_param_args *param;
    post_cb ev_post_cb;
    fw_event_t event;
};

void cmdFwHelp();
int cmdFw(int argc, char **argv, UINT32 *v);

/**
 *  @brief Common setup for "update firmware" module
 *      If you pass to function NULL or list_sz == 0 module will use
 *      defaults states. You can replace one or all states.
 *  @param      cb_list[in]  - list of states
 *  @param      list_sz      - list items count.
 *  @return     STAT_SUCCESS - Success, otherwise - error
 */
UINT32 init_fw_module(struct fw_cb_item *cb_list, UINT8 list_sz);

/**
 *  @brief Disable "update firmware" module.
 *  @return     STAT_SUCCESS - Success, otherwise - error
 */
int deinit_fw_module();

/**
 *  @brief Send event with parameters to fsm of "update firmware" module.
 *      Event parameters here have higher priority then
 *      parameters applied by fw_prm* functions group
 *  @param      fw_msg_in    - event body
 *  @return     STAT_SUCCESS - Success, otherwise - error
 */
UINT32 send_to_fw(struct fw_msg fw_msg_in);

void fw_prm_set_timeout(UINT32);
void fw_prm_set_log_name(char *name);
void fw_prm_log_enabled(BOOL);
void fw_prm_delete_enabled(BOOL);
void fw_prm_reserve_space(UINT32);

UINT32 fw_get_timeout();
char* fw_get_log_name();

/**
 *  @brief Disable one state.
 *      Check routes in the fsm before disabling state. Only
 *      StateIdle and StateReboot can be safely skipped. For
 *      the other you must customize fsm. StateIdle will be used by default
 *      when errors occurs
 *  @param      state    - this state will be disabled
 */
void fw_skip_state(fw_state_t state);
void fw_enable_state(fw_state_t state);

/**
 *  @brief Run predefined chain of events for default update goal.
 *  @param      params    - chain params
 *  @return     STAT_SUCCESS - Success, otherwise - error
 */
UINT32 fw_start_update(struct fw_param_args params);

#endif /* APP_CMD_FW_H_ */
