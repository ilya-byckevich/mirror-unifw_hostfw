/**
 * @file main.c
 * @author ilya(i.byckevich@yukonww.com)
 * @brief Jun 3, 2020
 *
 * Detailed description if necessary
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <setjmp.h>
#include <cmocka.h>

#include "mc_common.h"
#include "app_cmd_fw.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#define TIMEOUT_MS  3000

static BOOL waiting_the_end = TRUE;

static void call_fw_cmd(void **state)
{
    char *argv_packs_1[] = { "update", "fw_name1" };
    char *argv_packs_2[] = { "update2", "fw_name2" };
    char *argv_packs_3[] = { "update", "fw_name3", "blabla" };
    char *argv_packs_4[] = { "test", "test_log" };

    assert_false(cmdFw(2, argv_packs_1, NULL));

    /* Check with invalid inputs */
    assert_true(cmdFw(2, argv_packs_2, NULL));
    assert_false(cmdFw(3, argv_packs_3, NULL));

    /* It's async call in real world, wait for processing */
    assert_int_equal(cmdFw(2, argv_packs_4, NULL), SUCCESS);
    sleep(1);
}

static void run_fsm_test(struct fw_msg *msgs, int cnt)
{
    int try_cnt = TIMEOUT_MS / 10;

    waiting_the_end = TRUE;

    for (int i = 0; i < cnt; i++) {
        send_to_fw(msgs[i]);
    }

    /* Wait until thread process all messages */
    do {
        usleep(1000 * 10);
        if (!try_cnt--) {
            INFO("ERROR: timeout !\n");
            break;
        }
    } while (waiting_the_end);
}

static int test_stop_wait_event_cb(fw_state_t state, void *p)
{
    INFO("State result is %d!\n", *(UINT32* )p);
    waiting_the_end = FALSE;
    return STAT_SUCCESS;
}

/* Test 1: go to state X and come back to idle */
static void check_fw_fsm_1(void **state)
{
    UINT8 valid_states[] = { StateCheckFwFile, StateAllocateSpace,
            StateExtractFw };

#define VALID_CALL_CNT sizeof(valid_states)/sizeof(valid_states[0])
    UINT8 result_states[VALID_CALL_CNT] = { 0 };
    UINT32 call_cnt = 0;

    struct fw_param_args param_body = { .param = paramActionOnSuccess };

    /* accumulate states calls */
    int action(fw_state_t state, void *p)
    {
        if (call_cnt < VALID_CALL_CNT) {
            result_states[call_cnt++] = state;
        }
        return 0;
    }
    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, .param = &param_body, .ev_post_cb = action },
            { .event = evIdle, },
            { .event = evAllocateSpace, .param = &param_body, .ev_post_cb = action },
            { .event = evIdle, },
            { .event = evExtractFw, .param = &param_body, .ev_post_cb = action },
            { .event = evIdle, .ev_post_cb = test_stop_wait_event_cb, },
    };
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));

    for (UINT8 i = 0; i < VALID_CALL_CNT; i++) {
        assert_true(valid_states[i] == result_states[i]);
    }
}

/* Test 2: go through the basic states chain. Save to log states*/
static void check_fw_fsm_2(void **state)
{
    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, },
            { .event = evAllocateSpace, },
            { .event = evExtractFw, },
            { .event = evDecryptFw, },
            { .event = evValidateFw, },
            { .event = evCheckFwComp, },
            { .event = evUpdateFWComp, },
            { .event = evPostUpdateFW, .ev_post_cb = test_stop_wait_event_cb, },
    };
    fw_prm_set_log_name("test_log");
    fw_prm_log_enabled(TRUE);
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));
    fw_prm_log_enabled(FALSE);
    fw_prm_set_log_name("");
}

/* Test 3: We want to replace 2 default callbacks: StateExtractFw and StateUpdateFWComp
 * Now, event "evExtractFw" from state "StateAllocateSpace" will call
 * custom_extract_fw_cb() and event "evUpdateFWComp" from state
 * "StateCheckFwComp" will call custom_update_fm_comp_cb() instead of defaults cb
 */
static void check_fw_fsm_3(void **state)
{
    UINT8 list_sz = 0;
    int try_cnt = TIMEOUT_MS / 10;

    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, },
            { .event = evAllocateSpace, },
            { .event = evExtractFw, },
            { .event = evDecryptFw, },
            { .event = evValidateFw, },
            { .event = evCheckFwComp, },
            { .event = evUpdateFWComp, },
            { .event = evPostUpdateFW, .ev_post_cb = test_stop_wait_event_cb, },
    };

    int custom_extract_fw_cb(struct fw_cb_args args)
    {
        INFO("");
        return 0;
    }
    int custom_update_fm_comp_cb(struct fw_cb_args args)
    {
        INFO("");
        return 0;
    }
    struct fw_cb_item cb_list[] = {
            { custom_extract_fw_cb,     { StateAllocateSpace, evExtractFw } },
            { custom_update_fm_comp_cb, { StateCheckFwComp, evUpdateFWComp } },
    };

    init_fw_module(cb_list, sizeof(cb_list) / sizeof(cb_list[0]));
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));
    deinit_fw_module();
}

/* Test 4: Enable watchdog */
static void check_fm_watchdog(void **state)
{
    UINT32 timeout_ms = 2000;
    struct fw_param_args param_body = { .param = paramWatchdog,
            .wathdog_def_timeout_ms = timeout_ms };
    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, .param = &param_body },
            { .event = evAllocateSpace, },
            { .event = evExtractFw, },
            { .event = evDecryptFw, },
            { .event = evValidateFw, },
            { .event = evCheckFwComp, },
            { .event = evUpdateFWComp, },
            { .event = evPostUpdateFW, .ev_post_cb = test_stop_wait_event_cb, },
    };
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));
}

/* Here we replaced reboot callback for checking if it active or not */
static void check_reboot(void **state)
{
    BOOL reboot_is_active = FALSE;

    struct fw_msg msgs[] = {
            { .event = evCheckFwFile, },
            { .event = evAllocateSpace, },
            { .event = evExtractFw, },
            { .event = evReboot, },
            { .event = evDecryptFw, },
            { .event = evIdle, .ev_post_cb = test_stop_wait_event_cb, },
    };

    int replaced_reboot_cb(struct fw_cb_args args)
    {
        reboot_is_active = TRUE;
        /* stop waiting, we rebooted now */
        waiting_the_end = FALSE;
        return 0;
    }
    struct fw_cb_item cb_list[] = { { replaced_reboot_cb, { StateExtractFw,
            evReboot } }, };

    /* Skip reboot stage => replaced_reboot_cb will not call */
    init_fw_module(cb_list, sizeof(cb_list) / sizeof(cb_list[0]));
    fw_skip_state(StateReboot);
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));
    assert_false(reboot_is_active);
    deinit_fw_module();

    /* Enable reboot stage =>  replaced_reboot_cb called */
    init_fw_module(cb_list, sizeof(cb_list) / sizeof(cb_list[0]));
    fw_enable_state(StateReboot);
    run_fsm_test(msgs, sizeof(msgs) / sizeof(msgs[0]));
    assert_true(reboot_is_active);
    deinit_fw_module();
}

static int setup(void **state)
{
    struct fw_cb_item *cb_list = { 0 };
    UINT8 list_sz = 0;

    init_fw_module(cb_list, list_sz);

    waiting_the_end = TRUE;
    return 0;
}

static int teardown(void **state)
{
    deinit_fw_module();
    return 0;
}

int main(void)
{
    int ret = 0;

    const struct CMUnitTest tests_fw[] = {
    cmocka_unit_test(call_fw_cmd), };

    const struct CMUnitTest tests_fsm[] = {
    cmocka_unit_test_setup_teardown(check_fw_fsm_1, setup, teardown),
    cmocka_unit_test_setup_teardown(check_fw_fsm_2, setup, teardown), };

    const struct CMUnitTest test_fw_common[] = {
    cmocka_unit_test_setup_teardown(check_fm_watchdog, setup, teardown),
    cmocka_unit_test(check_reboot), };

    const struct CMUnitTest tests_fsm_3[] = {
    cmocka_unit_test(check_fw_fsm_3), };

    /* Internal tests */
    ret |= cmocka_run_group_tests_name("fsm simple", tests_fsm, NULL, NULL);
    ret |= cmocka_run_group_tests_name("fw common", test_fw_common, NULL, NULL);
    ret |= cmocka_run_group_tests_name("fsm custom states", tests_fsm_3, NULL,
            NULL);
    /* External tests */
    ret |= cmocka_run_group_tests_name("fw cli test", tests_fw, NULL, NULL);
    system("rm test_log");
    return ret;
}

