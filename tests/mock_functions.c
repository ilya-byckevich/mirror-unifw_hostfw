/**
 * @file mock_mc_common.c
 * @author ilya(i.byckevich@yukonww.com)
 * @brief May 14, 2020
 *
 * Put here only mocked functions
*/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdbool.h>
#include <stdlib.h>
#include <cmocka.h>
#include <pthread.h>
#include <unistd.h>
#include "sys/queue.h"
#include "stdio.h"
#include "string.h"

#include "common.h"
#include "api/sp5k_global_api.h"
#include "api/sp5k_utility_api.h"
#include "api/sp5k_os_api.h"
#include "api/sp5k_os_api.h"
#include "app_cmd_fw.h"


#define MAX_LOCAL_HANDLERS      128

static FILE *log_file;
static pthread_t fw_thread;
struct fw_mock_msg_queue {
    struct fw_msg msg;
    SIMPLEQ_ENTRY(fw_mock_msg_queue) entries;
};
SIMPLEQ_HEAD(_queuehead, fw_mock_msg_queue)fw_rx_queue;

struct fw_thread_func_container
{
    VOID (*fw_thread)(ULONG);
}fw_thread_func_container;

UINT32 __wrap_sp5kDscFirmwareUpdate(UINT8 *pfilename,
        fpsp5kUtilCardIspCallback_t pfunc)
{
    return 0;
}

UINT32 __wrap_sp5kRtcDateTimeGet(UINT32 option, tmx_t *ptime)
{
    return 0;
}

void __wrap_tmrUsWait(UINT32 us)
{
    usleep(us);
    printf("Dummy waitnig: %u us\n", us);
}

UINT32 __wrap_sp5kRtcDateTimeSet(UINT32 option, tmx_t *ptime)
{
    return 0;
}

UINT32 __wrap_sp5kSystemCfgGet(UINT32 paramId, ...)
{
    va_list ap;
    va_start(ap, paramId);

    for(int i = 0; i < paramId; i++) {
       /* ... */
    }
    va_end(ap);

    return 0;
}

static void* fw_thread_mirror_func(void *fw_thread_orig)
{
    struct fw_thread_func_container *fw_thread_containter = fw_thread_orig;
    fw_thread_containter->fw_thread(0);
    return NULL;
}

SP5K_THREAD* __wrap_sp5kOsThreadCreate(char *name_ptr,
        VOID (*entry_function)(ULONG),
        ULONG entry_input, UINT priority, UINT preempt_threshold,
        ULONG time_slice_milli_sec, UINT auto_start)
{
    SP5K_THREAD *valid_handle = malloc(sizeof(SP5K_THREAD));

    /* FIXME  phread have another thread callback declaration
     * (unlike sp5kOsThreadCreate)/ So, for avoiding UB call original
     * function via "fw_thread_func_container".
     */
    fw_thread_func_container.fw_thread = entry_function;
    if (pthread_create(&fw_thread, NULL, fw_thread_mirror_func,
            (void*)&fw_thread_func_container)) {
        printf("Error creating thread\n");
        return NULL;
    }
    return valid_handle;
}

UINT32 __wrap_sp5kOsThreadDelete(SP5K_THREAD *thread_ptr)
{
    free(thread_ptr);
    pthread_cancel(fw_thread);
    return 0;
}

UINT32 __wrap_sp5kOsQueueCreate(SP5K_QUEUE *queue_ptr, CHAR *name_ptr,
        UINT message_size, VOID *queue_start, ULONG queue_size)
{
    SP5K_QUEUE valid_handle = 0xFF;

    /* Limitations
    /* https://k-public2.icatchtek.com/doku.php?id=public:api:sp5kosqueuecreate
     */
    if(message_size == 1 || message_size == 2 || message_size == 4
            || sizeof(struct fw_msg) > QUEUE_MAX_DATA_SIZE){
        *queue_ptr = valid_handle;
        SIMPLEQ_INIT(&fw_rx_queue);
    }else{
        *queue_ptr = 0;
    }
    return 0;
}

UINT32 __wrap_sp5kOsQueueDelete(SP5K_QUEUE *queue_ptr)
{
    while (!SIMPLEQ_EMPTY(&fw_rx_queue)) {
        struct fw_mock_msg_queue *item = SIMPLEQ_FIRST(&fw_rx_queue);
        SIMPLEQ_REMOVE_HEAD(&fw_rx_queue, entries);
        free(item);
    }
    *queue_ptr = 0;
    return 0;
}

UINT32 __wrap_sp5kOsQueueReceive(SP5K_QUEUE *queue_ptr, VOID *destination_ptr,
        ULONG wait_option)
{
    struct fw_mock_msg_queue *fw_msg_from_queue = NULL;

    do{
        usleep(1000*10);
    }while(SIMPLEQ_EMPTY(&fw_rx_queue));

    fw_msg_from_queue = SIMPLEQ_FIRST(&fw_rx_queue);
    memcpy(destination_ptr, &fw_msg_from_queue->msg, sizeof(struct fw_msg));

    SIMPLEQ_REMOVE(&fw_rx_queue, fw_msg_from_queue, fw_mock_msg_queue, entries);
    free(fw_msg_from_queue);

    return 0;
}

UINT32 __wrap_sp5kOsQueueSend(SP5K_QUEUE *queue_ptr, VOID *source_ptr,
        ULONG wait_option)
{
    struct fw_mock_msg_queue *item = NULL;

    item = malloc(sizeof(struct fw_mock_msg_queue));
    if (!item) {
        perror("Can't create queue item\n");
        return -1;
    }
    memcpy(&item->msg, source_ptr, sizeof(struct fw_msg));
    SIMPLEQ_INSERT_TAIL(&fw_rx_queue, item, entries);

    return 0;
}

UINT32 __wrap_sp5kTimerCfgGet(UINT32 id, UINT32 *pms, UINT32 *penable)
{
    UINT32 timer_ms = 0, timer_is_enabled = 0;

   *pms = timer_ms;
   *penable = timer_is_enabled;

   return 0;
}

UINT32 __wrap_sp5kTimerUsIsrReg(UINT32 tmrId, UINT32 repeat, UINT32 us, void *pfunc)
{
    return 0;
}

UINT32 __wrap_sp5kTimerCfgSet(UINT32 id, UINT32 ms)
{
    return 0;
}

UINT32 __wrap_sp5kTimerEnable(UINT32 id, UINT32 enable)
{
    return 0;
}

UINT32 __wrap_sp5kTimerIsrReg(UINT32 dontCare, UINT32 ms, void *pfunc)
{
    return 0;
}

UINT32 __wrap_sp5kPowerCtrl(UINT32 option, UINT32 ms)
{
    if(option == SP5K_POWER_OFF){
        fail_msg("SP5K_POWER_OFF activated!\n");
    }
    return 0;
}

UINT32 __wrap_fsFileOpen(const UINT8 *fileName, SINT32 flag)
{
    log_file = fopen(fileName, "a");

    if(log_file){
        return 1;
    }
    return 0;
}

UINT32 __wrap_fsFileWrite(UINT32 fd, UINT8 *buf, UINT32 size)
{
    if(!log_file){
        fail_msg("invalid file handle !\n");
        return 1;
    }
    return fwrite(buf, 1, size, log_file);
}

UINT32 __wrap_fsFileClose(UINT32 fd)
{
    if(!log_file){
        return 0;
    }
    fclose(log_file);
    log_file = NULL;
    return 0;
}

