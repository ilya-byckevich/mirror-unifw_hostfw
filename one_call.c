/**
 * @file one_call.c
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

//#include "api/ybfw_global_api.h"
//#include "api/ybfw_os_api.h"
//#include "api/ybfw_dzoom_api.h"
//#include "api/ybfw_disp_api.h"
//#include "api/ybfw_pip_api.h"
//#include "api/ybfw_gfx_api.h"
//#include "api/ybfw_msg_def.h"
//#include "api/ybfw_global_api.h"
//#include "api/ybfw_os_api.h"
//#include "api/ybfw_dbg_api.h"
//#include "api/ybfw_modesw_api.h"
//#include "api/ybfw_cdsp_api.h"
//#include "api/ybfw_sensor_api.h"
//#include "api/ybfw_utility_api.h"
//#include "api/ybfw_media_api.h"
//#include "api/ybfw_aud_api.h"
//#include "api/ybfw_pip_api.h"
//#include "api/ybfw_disp_api.h"

#define FILE_ID "MC04"

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_media_api.h"
#include "ykn_bfw_api/ybfw_aud_api.h"
#include "ykn_bfw_api/ybfw_disp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_errors.h"
#include "ykn_bfw_api/ybfw_sensor_api.h"
#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"



#include "app_common.h"


#define DBG(fmt, ...) do {printf("%s:%d:-D- " fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); }while(0)
#define INFO DBG
#define DISPLAY_CHANNEL YBFW_DISP_CHNL_1
#define PIP_CHANNEL YBFW_PIP_CH_0
#define YUV_ROI_ALIGN_CDSP(x)       ((x) & (~0x7))

//static UINT32 yuvDispMsk[2];

static UINT32 pvRawCbDirectShow(ybfwModePrevYuvInfo_t *ppvRawInfo,UINT32 ppvRawInfoNum)
{
    UINT32 i, retBuf, mode, yuvDispMskCfg;
    ybfwModePrevYuvInfo_t pvyuv[YBFW_PV_MAX_YUV_PER_RAW];
//    DBG("trace\n");
    ybfwGfxObj_t srcImg[1];
    UINT32 srcImgIQinfo[1] = {0};
    ybfwGfxObj_t dstImg[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwGfxRoi_t dstImgSrcRoi[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwPrevYuvRoi_t srcRoi;
    UINT32 ret = FAIL;
    UINT32 dstCnt=0, srcCnt=0, isPipToggle = 0;
    ybfwGfxObjectCdspDoParam_t param;
    UINT32 appendparam  = (1<<16);
    UINT32 tmpSpeedCtrl = (1<<31);
    UINT32 pipChnl, pipHidden, pipGetTime0, pipGetTime1;
    ybfwGfxPageCapabDesc_t appPipPara;
    ybfwGfxMapProjectParam_t  proPrm;
    ybfwModeGetPeek(&mode);

//    yuvDispMskCfg = yuvDispMsk[ (mode== YBFW_MODE_VIDEO_PREVIEW) ?  1 : 0];
    /* adjust the speed of RawToYuv */
    param.cdspDoTargetFps = 120;
//    DBG("trace\n");
    memset(&param, 0, sizeof(param));
    memset(&srcImg, 0, sizeof(srcImg));
    memset(&dstImg, 0, sizeof(dstImg));
    memset(&dstImgSrcRoi, 0, sizeof(dstImgSrcRoi));
    memset(&srcImgIQinfo, 0, sizeof(srcImgIQinfo));
//    if (appHdmiWithOutPowerOffFlagGet() && appIsHdmiPlugIn()) {
//        pipChnl = YBFW_PIP_CH_1;
//        pipHidden = YBFW_PAGE_PIP_1_HIDDEN;
//    } else {
        pipChnl = YBFW_PIP_CH_0;
        pipHidden = YBFW_PAGE_PIP_0_HIDDEN;
//    }
//        DBG("trace\n");
    if( !(ppvRawInfo->attr & (YBFW_MODE_PV_CB_IMG_STOP_IN_PV_EXIT|YBFW_MODE_PV_CB_IMG_PAUSE_IN_PV) )) {
//        DBG("trace\n");
        param.srcBufHandle[srcCnt] = ppvRawInfo->pvCdspBufHandle;
        srcImgIQinfo[srcCnt] = ppvRawInfo->pcdspDoInfo;
        srcImg[srcCnt].pbuf  = ppvRawInfo->pframe.pbuf;
        srcImg[srcCnt].bufW  = ppvRawInfo->pframe.width;
        srcImg[srcCnt].bufH  = ppvRawInfo->pframe.height;
        srcImg[srcCnt].roiX  = (SINT32)ppvRawInfo->pframe.roiX;
        srcImg[srcCnt].roiY  = (SINT32)ppvRawInfo->pframe.roiY;
        srcImg[srcCnt].fmt   = ppvRawInfo->pframe.fmt;
        srcImg[srcCnt].roiW  = ppvRawInfo->pframe.roiW;
        srcImg[srcCnt].roiH  = ppvRawInfo->pframe.roiH;
//        DBG("SRC buf WxH %ux%u, roi (%d,%d,%uX%u) fmt %u",srcImg[srcCnt].bufW,srcImg[srcCnt].bufH,
//                srcImg[srcCnt].roiX,srcImg[srcCnt].roiY,srcImg[srcCnt].roiW, srcImg[srcCnt].roiH, srcImg[srcCnt].fmt);
        srcCnt = 1;
        i = 0;
//        for( i = 0 ; i < YBFW_PV_MAX_YUV_PER_RAW; i ++){
            memset(&pvyuv[i], 0, sizeof(ybfwModePrevYuvInfo_t));
            ybfwModeCfgGet(YBFW_MODE_CFG_PVCB_YUV_SRC_ROI_GET, ppvRawInfo, &srcRoi, i);
//            if( yuvDispMskCfg & (1<<i) ){
//                DBG("trace\n");
                if( srcRoi.roiw && srcRoi.roih ){
                    dstImgSrcRoi[ dstCnt ].roiX = srcRoi.roix;
                    dstImgSrcRoi[ dstCnt ].roiY = srcRoi.roiy;
                    dstImgSrcRoi[ dstCnt ].roiW = srcRoi.roiw;
                    dstImgSrcRoi[ dstCnt ].roiH = srcRoi.roih;

                    memset(&appPipPara, 0, sizeof(appPipPara));
                    pipGetTime0 = ybfwMsTimeGet();
                    ret = ybfwGfxPageCapabilityGet((YBFW_YUV_PIP_BASE+pipHidden), &appPipPara);
                    pipGetTime1 = ybfwMsTimeGet();

                    if( ret == FAIL || !appPipPara.pbuf ) {
                        HOST_ASSERT_MSG(0, "Get Pip Hidden Fail ret=%x, pbuf=%p",ret, appPipPara.pbuf);
                    }
                    else{
                        HOST_ASSERT_MSG( (pipGetTime1-pipGetTime0) < 5,
                            "Get Pip Hidden Too long, need to enlarge YBFW_PIP_INIT_TOGGFLE_FRAME_NUM  (%d, %d)",
                            pipGetTime1, pipGetTime0);
                    }

                    dstImg[ dstCnt ].pbuf = appPipPara.pbuf;
                    dstImg[ dstCnt ].bufW = appPipPara.frmW;
                    dstImg[ dstCnt ].bufH = appPipPara.frmH;
                    dstImg[ dstCnt ].fmt  = appPipPara.fmt;
                    dstImg[ dstCnt ].roiX = appPipPara.frmRoiX;
                    dstImg[ dstCnt ].roiY = appPipPara.frmRoiY;
                    dstImg[ dstCnt ].roiW = appPipPara.frmRoiW;
                    dstImg[ dstCnt ].roiH = appPipPara.frmRoiH;

//                    DBG("DST buf WxH %ux%u, roi (%d,%d,%uX%u) fmt %u",dstImg[dstCnt].bufW,dstImg[dstCnt].bufH,
//                                        dstImg[dstCnt].roiX,dstImg[dstCnt].roiY,dstImg[dstCnt].roiW, dstImg[dstCnt].roiH, dstImg[dstCnt].fmt);
                    dstCnt++;
                    isPipToggle = 1;

                }
                else{
                    HOST_ASSERT_MSG(0, "mode 0x%02x yuv_%d's roiw(=%d) roih(=%d) is zero, this yuv is not exist, but yuvDispMskCfg is %x \n",
                        mode, i, srcRoi.roiw , srcRoi.roih, yuvDispMskCfg );
                }
//            }
//            else{
////                DBG("trace\n");
//                retBuf = ybfwModeCfgSet(YBFW_MODE_CFG_OPENPV_AVAILABLE_YUV_BUF_GET, ppvRawInfo, i, &pvyuv[i] );
//                if( ( retBuf == YBFW_MODE_PREV_OPEN_GET_YUV_SUCCESS ) && pvyuv[i].pvCdspBufHandle ) {
//
//                    dstImgSrcRoi[ dstCnt ].roiX = srcRoi.roix;
//                    dstImgSrcRoi[ dstCnt ].roiY = srcRoi.roiy;
//                    dstImgSrcRoi[ dstCnt ].roiW = srcRoi.roiw;
//                    dstImgSrcRoi[ dstCnt ].roiH = srcRoi.roih;
//                    param.dstBufHandle[dstCnt] = pvyuv[i].pvCdspBufHandle;
//
//                    dstImg[ dstCnt ].pbuf = pvyuv[i].pframe.pbuf;
//                    dstImg[ dstCnt ].bufW = pvyuv[i].pframe.width;
//                    dstImg[ dstCnt ].bufH = pvyuv[i].pframe.height;
//                    dstImg[ dstCnt ].fmt  = pvyuv[i].pframe.fmt;
//                    dstImg[ dstCnt ].roiX = pvyuv[i].pframe.roiX;
//                    dstImg[ dstCnt ].roiY = pvyuv[i].pframe.roiY;
//                    dstImg[ dstCnt ].roiW = pvyuv[i].pframe.roiW;
//                    dstImg[ dstCnt ].roiH = pvyuv[i].pframe.roiH;
//
//                    dstCnt++;
//                }
//
//            }

//        }

        if( srcCnt && dstCnt ) {
            ret = ybfwGfxObjectCdspDo( srcImg, dstImgSrcRoi, dstImg,
                dstCnt | ( srcCnt << 8 ) | appendparam | tmpSpeedCtrl,
                YBFW_GFX_OBJECT_CDSP_DO_YUV, srcImgIQinfo[0],
                YBFW_MODE_STILL_PREVIEW,  &param );

            if(ret == FAIL) HOST_ASSERT_MSG(0,"CDSP DO err!!!");
        }

//        i = 0; //только для yuv
//        for( i = 0 ; i < YBFW_PV_MAX_YUV_PER_RAW; i ++){
//            if(pvyuv[i].pvCdspBufHandle)
//                ybfwModeCfgSet(YBFW_MODE_CFG_OPENPV_BUF_RELEASE, &pvyuv[i]);
//        }

        if(isPipToggle)
            ybfwPipAttrSet(pipChnl, YBFW_PIP_TOGGLE, 1, 0, 0, 0);

    }
}

//only for panamn
void change_raw_capture_test(uint step){
#include "media_core/mc_devices_sencfgs.h"
    static SensModeCfg sencfg_PANAMN34110 [] = {
         {3840,2160, 30,30, 0xC, 160, 160,YBFW_IMG_FMT_RAW_8BIT,YBFW_IMG_FMT_YUV422_NV16}, /* 4k */
         {1920,1080, 30,30, 0xA, 160, 160,YBFW_IMG_FMT_RAW_8BIT,YBFW_IMG_FMT_YUV422_NV16}, /* fhd 30 mode12-30fps*/
         {1280,720,  30,30, 0xA, 160, 160,YBFW_IMG_FMT_RAW_8BIT,YBFW_IMG_FMT_YUV422_NV16}, /* 720p 30*/
    };
    if (step >= sizeof(sencfg_PANAMN34110)/sizeof(sencfg_PANAMN34110[0])){
        return;
    }
    /**sersor mode**/
    SINT32 smode = sencfg_PANAMN34110[step].smode;
    INFO("stream pv sersor mode:0x%02x\n",smode);

    ybfwModeSet(YBFW_MODE_STANDBY);
    ybfwModeWait(YBFW_MODE_STANDBY);

    ybfwSensorModeCfgSet(YBFW_SEN_ID_0 ,YBFW_MODE_VIDEO_PREVIEW, SENSOR_MODE_PREVIEW|smode);

    /**sensor param**/
//    ybfwCapabilityPreview_t senprevCap;
//    ybfwModeCfgGet(YBFW_MODE_CFG_PV_CAP_GET, YBFW_SEN_ID_0, &senprevCap, sizeof(senprevCap));
//    INFO("stream pv bayer =[%x]\n",senprevCap.cfa_pattern);
//    INFO("stream pv cap size=[%d %d],ratio[%d %d]\n",senprevCap.hsize,senprevCap.vsize, senprevCap.hratio,senprevCap.vratio);
//    DBG("trace");
    /**cdsp performance set**/
//   ybfwModePreviewCdspAttr_t cdspattr;
//   ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_ATTR_GET, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr );
//   cdspattr.pvcdspDoTargetFps = 30; //проверить  0
//   ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_ATTR_CFG, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr);

   DBG("trace");
    /**cdsp raw config**/
//    ybfwModePreviewRawCustCfg_t modepvRawcfg={
//       .rawBufNum  = 4,
//       .rawBitMode = YBFW_IMG_FMT_RAW_8BIT, /*YBFW_IMG_FMT_RAW_V50*/
//       .rawWidth = 0,
//       .rawHeight = 0,
//       .rawToModule = YBFW_MODE_PV_CFG_URGENT_CB,
//       .rawOpenPvEn = 1,
//    };
//    DBG("trace");
//    if (senprevCap.hsize >= 3840) {
//       modepvRawcfg.rawWidth = 3840;
//    }
//    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_RAW_CFG,    YBFW_MODE_VIDEO_PREVIEW,YBFW_MODE_PREV_SENSOR_0_RAW,&modepvRawcfg);
   ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
   ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);
}


//only for panamn
void change_raw_roi_test(UINT32 W, UINT32 H){


    ybfwModeSet(YBFW_MODE_STANDBY);
    ybfwModeWait(YBFW_MODE_STANDBY);


   DBG("trace");
    /**cdsp raw config**/
static    ybfwModePreviewRawCustCfg_t modepvRawcfg={
       .rawBufNum  = 4,
       .rawBitMode = YBFW_IMG_FMT_RAW_8BIT, /*YBFW_IMG_FMT_RAW_V50*/
       .rawToModule = YBFW_MODE_PV_CFG_URGENT_CB,
       .rawOpenPvEn = 1,
    };

    modepvRawcfg.rawWidth = W;
    modepvRawcfg.rawHeight = H;
    DBG("trace");
//    if (senprevCap.hsize >= 3840) {
//       modepvRawcfg.rawWidth = 3840;
//    }
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_RAW_CFG,    YBFW_MODE_VIDEO_PREVIEW,YBFW_MODE_PREV_SENSOR_0_RAW,&modepvRawcfg);



    ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
   ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);
}

void one_call(){


//----------------- pv init


    /**sersor mode**/

    SINT32 smode = 0xA;  //default for panamn sensor of sbc

#if defined(CFG_HAL_SENS_0_TYPE_IMX291) || defined(SP5K_SENSOR_YKN_IMX291)
    smode = 0x1;
#endif
    INFO("stream pv sersor mode:0x%02x\n",smode);

    ybfwSensorModeCfgSet(YBFW_SEN_ID_0 ,YBFW_MODE_VIDEO_PREVIEW, SENSOR_MODE_PREVIEW|smode);

    /**sensor param**/
    ybfwCapabilityPreview_t senprevCap;
    ybfwModeCfgGet(YBFW_MODE_CFG_PV_CAP_GET, YBFW_SEN_ID_0, &senprevCap, sizeof(senprevCap));
    INFO("stream pv bayer =[%x]\n",senprevCap.cfa_pattern);
    INFO("stream pv cap size=[%d %d],ratio[%d %d]\n",senprevCap.hsize,senprevCap.vsize, senprevCap.hratio,senprevCap.vratio);
    DBG("trace");
    /**cdsp performance set**/
   ybfwModePreviewCdspAttr_t cdspattr;
   ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_ATTR_GET, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr );
   cdspattr.pvcdspDoTargetFps = 30; //проверить  0
   ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_ATTR_CFG, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr);

   DBG("trace");
    /**cdsp raw config**/
    ybfwModePreviewRawCustCfg_t modepvRawcfg={
       .rawBufNum  = 4,
       .rawBitMode = YBFW_IMG_FMT_RAW_8BIT, /*YBFW_IMG_FMT_RAW_V50*/
       .rawWidth = 0,
       .rawHeight = 0,
       .rawToModule = YBFW_MODE_PV_CFG_URGENT_CB,
       .rawOpenPvEn = 1,
    };
    DBG("trace");
    if (senprevCap.hsize >= 3840) {
       modepvRawcfg.rawWidth = 3840;
    }
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_RAW_CFG,    YBFW_MODE_VIDEO_PREVIEW,YBFW_MODE_PREV_SENSOR_0_RAW,&modepvRawcfg);
    DBG("trace");
    /**preview yuv para**/
    ybfwModePreviewYuvCustCfg_t modePvcfg;
    memset(&modePvcfg, 0, sizeof(modePvcfg));
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, YBFW_MODE_VIDEO_PREVIEW, YBFW_MODE_PREV_YUV_MAX, &modePvcfg);
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER);

    DBG("trace");

    modePvcfg.yuvEn      = YBFW_MODE_PV_CFG_DISP_CB |YBFW_MODE_PV_CFG_STREAM_MEDIA;
    modePvcfg.yuv.width  = YUV_ROI_ALIGN_CDSP(1920);
    modePvcfg.yuv.height = YUV_ROI_ALIGN_CDSP(1080);
    modePvcfg.yuv.roix= 0;
    modePvcfg.yuv.roiy= 0;
    modePvcfg.yuv.roiw= 1920;
    modePvcfg.yuv.roih= 1080;
    modePvcfg.fmt = YBFW_IMG_FMT_YUV422_NV16;
    modePvcfg.bufNum =4;
    DBG("trace");
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG,YBFW_MODE_VIDEO_PREVIEW,    YBFW_MODE_PREV_SENSOR_0_YUV_0,&modePvcfg);

//-------------------- OPENPV MODE ------------

    /* reg rawCb to proc raw data to yuv */
    ybfwPrevUrgentCallback_t pvcb;
    pvcb.ctrl     = 0;
    pvcb.interval = 1;
    pvcb.fp       = pvRawCbDirectShow;
    ybfwVideoUrgentCallbackSet(YBFW_MODE_PREV_SENSOR_0_RAW, &pvcb);
    DBG("trace\n");

//----------------  hdmi start --------------
    int id = YBFW_EDID_1920X1080P_60HZ_16T9;


    /* current V50 basefw seems have some problem, this cmd may need run twice for HDMI TV */
    ybfwDispPowerOff(YBFW_DISP_CHNL_0, 0); /* V50 ES1 cannot support dual panel well */
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 500);
    ybfwDispPowerOff(DISPLAY_CHANNEL, 0);
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 500);

    ybfwDispCfgSet(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, 0, 0);  /* RGB444. */
    ybfwDispCfgSet(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, 1, 0);  /* color depth not indicated. */
    ybfwDispCfgSet(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, 2, 0);  /* default quantization range. */
    ybfwDispCfgSet(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, 3, 0);  /* disable DVI mode. */
    ybfwDispCfgSet(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, 5, 0);  /* disable SCDC function. */
    ybfwDispPowerOn(DISPLAY_CHANNEL, YBFW_DISP_CHNL_1_HDMI_TV, id, 0);

#if 1
    /* PIP Layer */
    ybfwPipTerm(PIP_CHANNEL);
    ybfwPipInitCfgSet(PIP_CHANNEL, YBFW_PIP_INIT_MAIN_FRAME_SIZE, 1920, 1080, 0, 0);


    ybfwPipInitCfgSet(PIP_CHANNEL, PIP_INIT_CFG_TOGGFLE_FRAME_NUM, 4, 0, 0,0);

    ybfwPipInit(PIP_CHANNEL);
    ybfwDispAttrSet(DISPLAY_CHANNEL, YBFW_DISP_IMG_ACTIVE, 1);
#endif

    //--------------------------- OSD



    #define RETICLE_SIZE 300
    #define DISPLAY_PV_WIDTH  1920
    #define DISPLAY_PV_HEIGHT 1080
    #define DISPLAY_GFX_CHANNEL YBFW_GFX_CH_0_FLAG
    #define OFFSET_RET_X 0
    #define OFFSET_RET_Y 0
    #define PIP_COLOR565_WHITE          0xFFFF
    #define PIP_COLOR565_RED            0xF800
    #define PIP_COLOR565_GREEN          0x07E0
    #define PIP_COLOR565_BLUE           0x001F
    #define PIP_COLOR565_GRAY           ((0x0F << 11)|(0x1F << 5)|(0x0F << 0))
        UINT32 dx = 0;
        UINT32 dy = 0;
        UINT32 wSize = DISPLAY_PV_WIDTH;
        UINT32 hSize = DISPLAY_PV_HEIGHT;
        UINT32 dispWidth = DISPLAY_PV_WIDTH;
        UINT32 dispHeight = DISPLAY_PV_HEIGHT;
            //cross lines
        ybfwDispAttrSet(DISPLAY_CHANNEL, YBFW_DISP_OSD_WINDOW, 0, 0, wSize, hSize);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_AB_FRAME_SIZE, wSize, hSize, 0, 0);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_DEMO_MODE, 1, 0, 0, 0);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_DISP_CHNL, DISPLAY_CHANNEL, 0, 0, 0);

        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_PAGE_TOT, 1, 0, 0, 0);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FMT, YBFW_GFX_FMT_RGB565, 0, 0, 0);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FRAME_SIZE, wSize, hSize, 0, 0);
        ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_WINDOW, 0, 0, wSize, hSize);

        ybfwGfxAttrSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_BLEND_FACTOR, 128, 0, 0, 0);

        ybfwGfxInitCfgSet(YBFW_GFX_CH_SEL, DISPLAY_GFX_CHANNEL, 0, 0, 0);
        ybfwGfxInit();
        ybfwGfxAttrSet(DISPLAY_GFX_CHANNEL | YBFW_GFX_PIC_ROTATE, YBFW_GFX_ROTATE_EXT, 0, 0, 0);
        ybfwDispAttrSet(DISPLAY_CHANNEL, YBFW_DISP_OSD_ACTIVE, 1);


        int x_c,y_c;
        x_c=dispWidth/2+OFFSET_RET_X/2;
        y_c=dispHeight/2+OFFSET_RET_Y/2;

        ybfwGfxAttrSet(YBFW_GFX_PEN_WIDTH, 1, 0, 0, 0);
        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_GREEN, 0, 0, 0);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c+dx-RETICLE_SIZE, y_c+dy, x_c+dx + RETICLE_SIZE, y_c+dy);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c+dx, y_c+dy-RETICLE_SIZE, x_c+dx, y_c+dy+RETICLE_SIZE);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0, 0, 1919, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0,1079,1919, 1079);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0, 0, 0, 1079);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 1919,0,1919, 1079);

        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_RED, 0, 0, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c, y_c-RETICLE_SIZE, x_c, y_c-RETICLE_SIZE+20);

        // center of screen
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, dispWidth/2-10, dispHeight/2, dispWidth/2 + 10, dispHeight/2);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, dispWidth/2, dispHeight/2-10, dispWidth/2, dispHeight/2+10);

        // reticle
        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_BLUE, 0, 0, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c-10, y_c, x_c + 10, y_c);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c, y_c-10, x_c, y_c+10);
        ybfwGfxAttrSet(YBFW_GFX_REFRESH_ACTIVE,1, 1, 0, 0);
//-----------------------------


    /**mode switch*/
    ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
    ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);

#if 0
    DBG("test mode switch ");
#define CUR_SWITCH_COUNT 3
    SINT32 i = 0;
    while (1){
        ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 5000);
        change_raw_capture_test(i% CUR_SWITCH_COUNT);
        i++;
    }
#endif

#if 0 //тест raw size
    change_raw_capture_test(0); //max storage

#define CDSP_ALIGN_H(x)         ((x) & (~0x2))
#define CDSP_ALIGN_W(x)       ((x) & (~0x7))
        UINT32 W = (3840);
        UINT32 H = 2160;

        while (1){
            DBG("apply WxH %dx%d",W,H);
            ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 3000);
            change_raw_roi_test(W,H);

            H = CDSP_ALIGN_H(H-300) > 500 ? CDSP_ALIGN_H(H-300) : 3840;
            W = CDSP_ALIGN_W( (H*16/9));
        }


#endif

    DBG("trace");
}
