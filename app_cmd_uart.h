/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef APP_CMD_UART_H_
#define APP_CMD_UART_H_

int cmdUart(int argc,char **argv,UINT32* v);
void cmdUartHelp();

#endif /* APP_CMD_UART_H_ */
