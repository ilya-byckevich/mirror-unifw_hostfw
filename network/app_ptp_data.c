/**************************************************************************
 *
 *       Copyright (c) 20012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_disk_api.h"
#include "ykn_bfw_api/ybfw_ptp_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_media_api.h"

#include "app_battery.h"
#include "app_still.h"
#include "app_video.h"
#include "app_view_param.h"
#include "app_zoom_api.h"
#include "app_General.h"
#include "app_wifi_utility.h"
#include "app_wifi_connection.h"
#include "app_awbalg_api.h"
#include "app_ptp.h"
#include "app_sys_cfg.h"
#include "../app_ver.h"
#include "app_ui_para.h"
#include "app_screen_saver.h"
#include "app_util.h"
#include "app_setup_lib.h"


#define MINUTE (60)
#define UI_PTP_UNSUPPORT_CMD 0x8000
#define UI_PTP_UNDEFINE_CMD  0xFFBF

/*===== PIMA Start =====*/
extern UINT8 gpassword[MAX_PASSWORD_LEN];

/*== EnumForm Start ==*/
enum{
	APP_SWITCH_BUTTON_OFF = 0,
	APP_SWITCH_BUTTON_ON  = 1
};

#ifdef APP_PTP_DPC_STILL_IMG_SIZE
static struct
{
    UINT16 w,h;
    UINT16 UiIndex;
} PTPUI_Map_Size[]=
{
    { UI_PTP_UNDEFINE_CMD, UI_PTP_UNDEFINE_CMD },
	#if	SP5K_CVRCAM_MULTI_MODE
	{ 4992, 2808, UI_STILL_SIZE_14M	},
	{ 2688, 1512, UI_STILL_SIZE_4M	},
	{ 1920, 1080, UI_STILL_SIZE_2M	},
	#else
	{ 4320, 3240, UI_STILL_SIZE_14M },
	{ 4320, 2430, UI_STILL_SIZE_10M },
	{ 3840, 2160, UI_STILL_SIZE_8M  },
	{ 2592, 1944, UI_STILL_SIZE_5M  },
	{ 1920, 1080, UI_STILL_SIZE_2M  },
	#endif
};
#endif

#ifdef APP_PTP_DPC_STILL_IMG_QUALITY
static struct
{
	UINT16 quality;
	UINT16 UiIndex;
} PTPUI_Map_Quality[]=
{
    { UI_PTP_UNDEFINE_CMD, UI_PTP_UNDEFINE_CMD  },
    {  5                 , UI_QUALITY_SUPERFINE },
    {  8                 , UI_QUALITY_FINE      },
    { 12                 , UI_QUALITY_NORMAL    },
};
#endif

#ifdef APP_PTP_DPC_APERTURE
static struct
{
    UINT16 iris;
    UINT16 UiIndex;
} PTPUI_Map_Iris[]=
{
    { UI_PTP_UNDEFINE_CMD, UI_PTP_UNDEFINE_CMD },
    { 280                , APERTURE_2_8        },
    { 560                , APERTURE_5_6        },
};
#endif

#ifdef APP_PTP_DPC_AE_ISO
static struct
{
    UINT16 iso;
    UINT16 UiIndex;
} PTPUI_Map_ISO[]=
{
    { UI_PTP_UNDEFINE_CMD, UI_PTP_UNDEFINE_CMD },
    {   50               , UI_ISO_50           },
    {  100               , UI_ISO_100          },
    {  200               , UI_ISO_200          },
    {  400               , UI_ISO_400          },
    {  800               , UI_ISO_800          },
    { 1600               , UI_ISO_1600         },
    { 3200               , UI_ISO_3200         },
};
#endif

#ifdef APP_PTP_DPC_AE_EV_OFFSET
static struct
{
    SINT16 ev;
    UINT16 UiIndex;
} PTPUI_Map_EV[]=
{
    { -2, UI_EXP_BIAS_N200 },
    { -1, UI_EXP_BIAS_N100 },
    {  0, UI_EXP_BIAS_ZERO },
    {  1, UI_EXP_BIAS_P100 },
    {  2, UI_EXP_BIAS_P200 },
};
#endif

#ifdef APP_PTP_DPC_STILL_SNAP_DELAY
static UINT32 PTPUI_Map_Delay[UI_PHOTO_INTERVAL_MAX]=
{
    2000,
    10000,
    0
};
#endif

/*== EnumForm End ==*/

/*== ItemListForm Start ==*/
#ifdef APP_PTP_DPC_WB_MODE
static UINT16 PTPUI_Map_WB[]=  //PIMA 0x5005
{
    UI_PTP_UNDEFINE_CMD,
    UI_WB_AUTO,
    UI_WB_DAYLIGHT,
    UI_WB_CLOUDY,
    UI_WB_TUNGSTEN,
    UI_WB_FLUORESCENT_H,
    UI_PTP_UNSUPPORT_CMD,
};
#endif

#ifdef APP_PTP_DPC_AF_MODE
static UINT16 PTPUI_Map_AF[]=  //PIMA 0x500A
{
    UI_PTP_UNDEFINE_CMD,
    UI_PTP_UNSUPPORT_CMD,
    UI_FOCUS_NORMAL,
    UI_FOCUS_MACRO,
};
#endif

#ifdef APP_PTP_DPC_AE_METERING
static UINT16 PTPUI_Map_Metering[]=  //PIMA 0x500B
{
    UI_PTP_UNDEFINE_CMD,
    UI_PTP_UNSUPPORT_CMD,
    UI_METERING_CENTER,
    UI_METERING_MULTI,
    UI_METERING_SPOT,
};
#endif

#ifdef APP_PTP_DPC_AE_FLASH_MODE
static UINT16 PTPUI_Map_Flash[]=  //PIMA 0x500C
{
    UI_PTP_UNDEFINE_CMD,
    UI_FLASH_AUTO,
    UI_FLASH_OFF,
    UI_PTP_UNSUPPORT_CMD,
    UI_FLASH_RED_EYE,
    UI_PTP_UNSUPPORT_CMD,
    UI_PTP_UNSUPPORT_CMD,
};
#endif

#ifdef APP_PTP_DPC_STILL_SNAP_MODE
static UINT16 PTPUI_Map_Capture[]=  //PIMA 0x5013
{
    UI_PTP_UNDEFINE_CMD,
    UI_STILL_DRIVERMODE_OFF,
    UI_STILL_DRIVERMODE_AEB,
    UI_PTP_UNSUPPORT_CMD,
};
#endif

#ifdef APP_PTP_DPC_SHARPNESS
static UINT16 PTPUI_Map_Sharpness[]=  //PIMA 0x5015
{
    UI_PTP_UNDEFINE_CMD,
    UI_SHARP_LESS,
    UI_SHARP_NORMAL,
    UI_SHARP_MORE,
};
#endif
/*== ItemListForm End ==*/
/*===== PIMA End =====*/


/*===== iCatch Start =====*/
/*== EnumForm Start ==*/
/*== EnumForm End ==*/

/*== ItemListForm Start ==*/
#ifdef APP_PTP_DPC_DATESTAMP
static UINT16 PTPUI_Map_Stamp[]=  //icatch 0xD607
{
    UI_PTP_UNDEFINE_CMD,
    UI_DATESTAMP_OFF,
    UI_DATESTAMP_DATE,
    UI_DATESTAMP_DATETIME,
};
#endif

/*== ItemListForm End ==*/
#ifdef APP_PTP_DPC_STILL_SNAP_BURST
static UINT16 PTPUI_Map_Burst[]=
{ /* 1 ~ 5, unit : image burst number */
	UI_PTP_UNDEFINE_CMD,
  UI_STILL_DRIVERMODE_BURST_3P_1S,
  UI_STILL_DRIVERMODE_BURST_7P_2S,
  UI_STILL_DRIVERMODE_BURST_15P_4S,
  UI_STILL_DRIVERMODE_BURST_30P_8S,
  UI_STILL_DRIVERMODE_OFF,
};
#endif

#ifdef APP_PTP_DPC_AE_EXPOSURE
UINT32 shutterSpeedVal[] = {
    10000,
    5000,
    2500,
    1250,
    666,
    333,
    166,
    80,
    40,
    20,
    10,
    5,
    2,
    1,
};
#endif

#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
static PtpValMap_t PTPUI_Map_Cap_TimeLpsInt[]={
	{UI_PTP_UNDEFINE_CMD,  UI_PTP_UNDEFINE_CMD   },
    {     0,   UI_TIME_LAPSE_PHOTO_INTERVAL_OFF  },
    {     3,   UI_TIME_LAPSE_PHOTO_INTERVAL_3SEC },
    {     5,   UI_TIME_LAPSE_PHOTO_INTERVAL_5SEC },
    {    10,   UI_TIME_LAPSE_PHOTO_INTERVAL_10SEC},
    {    30,   UI_TIME_LAPSE_PHOTO_INTERVAL_30SEC},
    {MINUTE,   UI_TIME_LAPSE_PHOTO_INTERVAL_1MIN },
};

#ifdef APP_PTP_DPC_TIMELAPSE_LENGTH
static PtpValMap_t PTPUI_Map_TimeLps_Cap_Duration[]={
	{UI_PTP_UNDEFINE_CMD,UI_PTP_UNDEFINE_CMD              },
    {0xffff,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_UNLIMITED},
    {     5,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_5MIN     },
    {    10,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_10MIN    },
    {    15,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_15MIN    },
    {    20,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_20MIN    },
    {    30,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_30MIN    },
    {    60,   UI_TIME_LAPSE_PHOTO_SHOOTING_TIME_60MIN    },
};
#endif
#endif


#ifdef APP_PTP_DPC_POWER_SAVING_TIME
static struct
{
    UINT16 val;
    UINT16 UiIndex;
} PTPUI_Map_SaverScreen[]=
{
	{ 0*MINUTE, UI_GENERAL_SCREEN_SAVER_OFF },
    { 1*MINUTE, UI_GENERAL_SCREEN_SAVER_1MIN},
    { 3*MINUTE, UI_GENERAL_SCREEN_SAVER_3MIN},
    { 5*MINUTE, UI_GENERAL_SCREEN_SAVER_5MIN},
};
#endif

#ifdef APP_PTP_DPC_VIDEO_REC_SLOW_MOTION
static struct
{
    UINT16 val;
    UINT16 UiIndex;
} PTPUI_Map_SlowMotion[]=
{
	{ PTP_DPC_SLOW_MOTION_ON ,  UI_VIDEO_SLOW_MOTION_ON},
	{ PTP_DPC_SLOW_MOTION_OFF, UI_VIDEO_SLOW_MOTION_OFF},
};
#endif

#ifdef APP_PTP_DPC_VIDEO_REC_FAST_MOTION
static struct
{
    UINT16 val;
    UINT16 UiIndex;
} PTPUI_Map_FastMotion[]=
{
	{ 0, UI_VIDEO_FAST_MOTION_OFF},
    { 2, UI_VIDEO_FAST_MOTION_2X },
    { 4, UI_VIDEO_FAST_MOTION_4X },
    { 6, UI_VIDEO_FAST_MOTION_6X },
    {10, UI_VIDEO_FAST_MOTION_10X},
    {15, UI_VIDEO_FAST_MOTION_15X},
};
#endif

#ifdef APP_PTP_DPC_POWER_OFF_TIME
static struct
{
    UINT16 val;
    UINT16 UiIndex;
} PTPUI_Map_AutoPowerOFF[]=
{
	{ 0       , POWER_SAVE_OFF},
	{ 1*MINUTE, POWER_SAVE_1MIN},

};
#endif

#ifdef APP_PTP_DPC_VIDEO_EIS
static UINT32 appSwitchEis[2] = {
	[APP_SWITCH_BUTTON_ON]  = UI_VIDEO_IMAGE_STABILIZATION_ON,
	[APP_SWITCH_BUTTON_OFF] = UI_VIDEO_IMAGE_STABILIZATION_OFF,
};
#endif

#ifdef APP_PTP_DPC_VIDEO_REC_AUTO_START
static UINT32 appSwitchAutoRec[2] = {
	[APP_SWITCH_BUTTON_ON]  = POWER_ON_RECORD_ON,
	[APP_SWITCH_BUTTON_OFF] = POWER_ON_RECORD_OFF,
};
#endif

#ifdef APP_PTP_DPC_VIDEO_REC_WNR
static UINT32 appSwitchWindReduce[2] = {
	[APP_SWITCH_BUTTON_ON]  = UI_WIND_NOISE_REDUCTION_ON,
	[APP_SWITCH_BUTTON_OFF] = UI_WIND_NOISE_REDUCTION_OFF,
};
#endif

#ifdef APP_PTP_DPC_VIDEO_FILE_LENGTH
static UINT32 PTPUI_Map_VideoFileLength[]=
{
    0*MINUTE,
    1*MINUTE,
    5*MINUTE,
};
#endif



#define PTP_H264  0
#define PTP_MJPG  1
#define PTP_H265  1
#define PTP_NOT_H265 0


#ifdef APP_RTP_DPC_RTP_PV_CFG0
static struct
{
   UINT32 vidPvData ;

} PTPUI_Map_VidPVFormatSizeBitrateVid[]=
{
	#if SP5K_360CAM_MODE
	#if APP_360CAM_WIFI_4K_STREAM_CTR_EN
	#if APP_VIDEO_SIZE_6K3K
	{(PTP_H264 << 31 ) |(3840 << 19) | (1920 << 8) | 100 },
	#endif
	{(PTP_H264 << 31 ) |(3840 << 19) | (1920 << 8) | 100 },
	{(PTP_H264 << 31 ) |(3840 << 19) | (1920 << 8) | 100 },
	{(PTP_H264 << 31 ) |(3840 << 19) | (1920 << 8) | 100 },
	#else
	#if APP_VIDEO_SIZE_6K3K
	{(PTP_H264 << 31 ) |(1920 << 19) | (960 << 8) | 40 },
	#endif
	{(PTP_H264 << 31 ) |(1920 << 19) | (960 << 8) | 40 },
	{(PTP_H264 << 31 ) |(1920 << 19) | (960 << 8) | 40 },
	{(PTP_H264 << 31 ) |(1920 << 19) | (960 << 8) | 40 },
	#endif
	#elif SP5K_CVRCAM_MULTI_MODE
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
	{(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
	#else
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
	{(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40 },
    {(PTP_H264 << 31 ) |(640 << 19) | (360  << 8) | 30 },
	#endif
};
#endif

#ifdef APP_RTP_DPC_RTP_PV_CFG1
static struct
{
	UINT32 vidPvData ;

} PTPUI_Map_VidPVFormatFps[]=
{
#if SP5K_360CAM_MODE
	#if APP_360CAM_WIFI_4K_STREAM_CTR_EN
	#if APP_VIDEO_SIZE_6K3K
	{(PTP_NOT_H265 << 8 )| 30},
	#endif
	{(PTP_NOT_H265 << 8 )| 30},
	{(PTP_NOT_H265 << 8 )| 30},
	{(PTP_NOT_H265 << 8 )| 30}
	#else
	#if APP_VIDEO_SIZE_6K3K
	{(PTP_NOT_H265 << 8 )| 15},
	#endif
	{(PTP_NOT_H265 << 8 )| 15},
	{(PTP_NOT_H265 << 8 )| 15},
	{(PTP_NOT_H265 << 8 )| 15}
	#endif
#elif	SP5K_CVRCAM_MULTI_MODE
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30}
#else
    {(PTP_NOT_H265 << 8 )| 12},	/* 6k loading */
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 15},	/* 4k50fps loading */
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 30},
    {(PTP_NOT_H265 << 8 )| 10},
#endif
};
#endif

static UINT32 opModeMap[PTP_DSCOP_MODE_MAX] = {
	[PTP_DSCOP_MODE_VIDEO_OFF]           = YBFW_MODE_VIDEO_PREVIEW,
	[PTP_DSCOP_MODE_SHARE]               = YBFW_MODE_STANDBY,
	[PTP_DSCOP_MODE_CAMERA]              = YBFW_MODE_STILL_PREVIEW,
	[PTP_DSCOP_MODE_APP]                 = YBFW_MODE_STANDBY,
	[PTP_DSCOP_MODE_EDIT]                = YBFW_MODE_STANDBY,
	[PTP_DSCOP_MODE_BACKWARD]            = YBFW_MODE_STANDBY,
	[PTP_DSCOP_MODE_TIMELAPSE_STILL]     = YBFW_MODE_STILL_PREVIEW,
	[PTP_DSCOP_MODE_TIMELAPSE_VIDEO]     = YBFW_MODE_VIDEO_PREVIEW,
	[PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF] = YBFW_MODE_STILL_PREVIEW,
	[PTP_DSCOP_MODE_TIMELAPSE_VIDEO_OFF] = YBFW_MODE_VIDEO_PREVIEW,
	[PTP_DSCOP_MODE_VIDEO_ON]            = YBFW_MODE_VIDEO_PREVIEW,
};
/*===== iCatch End =====*/


/**************************************************************************
 *                 E X T E R N A L    R E F E R E N C E S                 *
 **************************************************************************/
extern UINT32 appFirmwareUpdate(void);
extern UINT32 app_ptpUsbCustomerSet(UINT32 *pdat);

/**************************************************************************
 *               F U N C T I O N    D E C L A R A T I O N S               *
 **************************************************************************/
static UINT32 appPtpEvVal2PtpVal(SINT16 evVal);
static void appStill_PTPToUI_Set(UINT32 *buf);


#define PIMA_DEBUG	0

#if PIMA_DEBUG
static UINT8 *devprop[] =
	{
		"Undefined",
		"BatteryLevel",
		"FunctionalMode",
		"ImageSize",
		"CompressionSetting",
		"WhiteBalance",
		"RGB Gain",
		"F-Number",
		"FocalLength",
		"FocusDistance",
		"FocusMode",
		"ExposureMeteringMode",
		"FlashMode",
		"ExposureTime",
		"ExposureProgramMode",
		"ExposureIndex",
		"ExposureBiasCompensation",
		"DateTime",
		"CaptureDelay",
		"StillCaptureMode",
		"Contrast",
		"Sharpness",
		"DigitalZoom",
		"EffectMode",
		"BurstNumber",
		"BurstInterval",
		"TimelapseNumber",
		"TimelapseInterval",
		"FocusMeteringMode",
		"UploadURL",
		"Artist",
		"CopyrightInfo"
	};
#endif

static UINT32 appPtpVal2UiMap(UINT32 val, PtpValMap_t *ptpValMap,UINT32 cnt){
	UINT32 i = 0;
	UINT32 ret = 0;

	HOST_ASSERT(ptpValMap && cnt);	/* not a runtime issue. ASSERT for engineer. */
	for (i = 0; i < cnt ; i++)
	{
		if(ptpValMap[i].val == UI_PTP_UNSUPPORT_CMD){
			continue;
		}

		if(ptpValMap[i].val == val) {
			ret = ptpValMap[i].UiIndex;
			break;
		}
	}

	return ret;
}

static UINT32 appPtpUiIndex2ValMap(UINT32 UiIdex, PtpValMap_t *ptpValMap,UINT32 cnt){
	UINT32 i = 0;
	UINT32 ret = 0;

	HOST_ASSERT(ptpValMap && cnt);	/* not a runtime issue. ASSERT for engineer. */
	for (i = 0; i < cnt ; i++)
    {
        if(ptpValMap[i].UiIndex == UI_PTP_UNSUPPORT_CMD){
            continue;
    	}
        if(ptpValMap[i].UiIndex == UiIdex) {
			ret = ptpValMap[i].val;
			break;
        }
    }
	return ret;
}

static UINT32 appPtpUiIndex2Switch(UINT32 UiIdex, UINT32 *ptpSWMap)
{
	UINT32 sw;
	if (ptpSWMap[APP_SWITCH_BUTTON_ON] == UiIdex)
		sw = APP_SWITCH_BUTTON_ON;
	else
		sw = APP_SWITCH_BUTTON_OFF;

	return sw;
}

UINT16*
appStill_PIMA_Get_Map(
    UINT32 PTPCode,
    UINT8* ArraySize,
    UINT8* UI_Param
)
{
    UINT16* MappingTab = 0;
	uiPara_t* puiPara = appUiParaGet();

	switch (PTPCode)
    {
    	#ifdef APP_PTP_DPC_WB_MODE
    	case APP_PTP_DPC_WB_MODE: /* bz issue: stand for awb*/
        case PIMA_DPC_WHILE_BALANCE:
            MappingTab = PTPUI_Map_WB;
            *ArraySize = (sizeof(PTPUI_Map_WB)/sizeof(UINT16));
            *UI_Param = pViewParam->wb;
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_METERING
        case APP_PTP_DPC_AE_METERING:
            MappingTab = PTPUI_Map_Metering;
            *ArraySize = (sizeof(PTPUI_Map_Metering)/sizeof(UINT16));
            *UI_Param = pViewParam->metering;
            break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_MODE
        case APP_PTP_DPC_STILL_SNAP_MODE:
            MappingTab = PTPUI_Map_Capture;
            *ArraySize = (sizeof(PTPUI_Map_Capture)/sizeof(UINT16));
            *UI_Param = pViewParam->stillDriverMode;
            break;
		#endif

		#ifdef APP_PTP_DPC_SHARPNESS
        case APP_PTP_DPC_SHARPNESS:
            MappingTab = PTPUI_Map_Sharpness;
            *ArraySize = (sizeof(PTPUI_Map_Sharpness)/sizeof(UINT16));
            *UI_Param = pViewParam->edge;
            break;
		#endif

		#ifdef APP_PTP_DPC_DATESTAMP
        case APP_PTP_DPC_DATESTAMP:
            MappingTab = PTPUI_Map_Stamp;
            *ArraySize = (sizeof(PTPUI_Map_Stamp)/sizeof(UINT16));
			*UI_Param = puiPara->VideoStampMode;
            break;
		#endif

        default:
            printf("No Case in appStill_PTPToUI_MapGet\n");
            break;
    }

    return MappingTab;
}

void appStill_PIMA_Get_cb(UINT32 devPropCode, UINT32 *dat)
{
	UINT32 remain,i, cnt = 1;
    UINT16 CurSetting = 0, DefSetting = 0;
    UINT16* MappingTab;
    UINT8 UI_Param = 0, ArraySize = 0;
	uiPara_t* puiPara = appUiParaGet();
	appVideoConfig_t *pVidData;
	UINT32 len = 0;

	pViewParam->stillDriverMode=puiPara->PhotoBrustMode;
	pViewParam->wb=puiPara->WBMode;

	if (puiPara->SleepTime != POWER_SAVE_OFF){
        appPowerSaveTimeOutReset();
    }

	if ( puiPara->ScreenSaverTime != UI_GENERAL_SCREEN_SAVER_OFF){
		appScreenSaveCfg(TRUE);
    }

	#if PIMA_DEBUG
	printf("Get %x,%x (%s)\n", devPropCode, dat, devprop[devPropCode&0x0ff]);
	printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
        pViewParam->wb, pViewParam->af, pViewParam->metering,
		gStillCB.flashMode, pViewParam->effect, pViewParam->drive,
		pViewParam->quality, gStillCB.ae.iso, pViewParam->ev,
		gStillCB.remainImg, pViewParam->size, pViewParam->edge);
	#endif

	/*printf("cb get:devPropCode:0x%x\n",devPropCode);*/

	switch (devPropCode)
    {
    	#ifdef APP_PTP_DPC_WB_MODE
    	case APP_PTP_DPC_WB_MODE:
        case PIMA_DPC_WHILE_BALANCE:
		#endif
		#ifdef APP_PTP_DPC_AF_MODE
        case APP_PTP_DPC_AF_MODE:
		#endif
		#ifdef APP_PTP_DPC_AE_METERING
        case APP_PTP_DPC_AE_METERING:
		#endif
		#ifdef APP_PTP_DPC_AE_FLASH_MODE
        case APP_PTP_DPC_AE_FLASH_MODE:
		#endif
		#ifdef APP_PTP_DPC_STILL_SNAP_MODE
        case APP_PTP_DPC_STILL_SNAP_MODE:
		#endif
		#ifdef APP_PTP_DPC_SHARPNESS
        case APP_PTP_DPC_SHARPNESS:
		#endif
		#ifdef APP_PTP_DPC_DATESTAMP
        case APP_PTP_DPC_DATESTAMP:
		#endif
            MappingTab = appStill_PIMA_Get_Map(devPropCode, &ArraySize, &UI_Param);
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            for (i = 1 ; i < ArraySize ; i++)
            {
                if(UI_Param == MappingTab[i]){
                    CurSetting = i;
            	}

                if(!MappingTab[i]){
                    DefSetting = i;
            	}
            }
            dat[cnt++] = CurSetting;
            dat[cnt++] = DefSetting;
            printf("Cur = %d, Def = %d\n", CurSetting, DefSetting);
            for (i = 1 ; i < ArraySize ; i++)
            {
                if(MappingTab[i] == UI_PTP_UNSUPPORT_CMD){
                    continue;
            	}
                dat[cnt++] = i;
            }
            break;

		#ifdef APP_PTP_DPC_STILL_IMG_SIZE
        case APP_PTP_DPC_STILL_IMG_SIZE:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;

            for (i = 1 ; i < (sizeof(PTPUI_Map_Size)/sizeof(PTPUI_Map_Size[0])) ; i++)
            {
                if(PTPUI_Map_Size[i].UiIndex == pViewParam->stillSize){
                    CurSetting = i;
            	}

                if(!PTPUI_Map_Size[i].UiIndex){
                    DefSetting = i;
            	}
            }
            dat[cnt++] = PTPUI_Map_Size[CurSetting].w;
            dat[cnt++] = PTPUI_Map_Size[CurSetting].h;
            dat[cnt++] = PTPUI_Map_Size[DefSetting].w;
            dat[cnt++] = PTPUI_Map_Size[DefSetting].h;

            printf("[Image Size]Cur = %d, Def = %d\n", CurSetting, DefSetting);

            for (i = 1 ; i < (sizeof(PTPUI_Map_Size)/sizeof(PTPUI_Map_Size[0])) ; i++)
            {
                if(PTPUI_Map_Size[i].UiIndex == UI_PTP_UNSUPPORT_CMD){
                    continue;
            	}
                dat[cnt++] = PTPUI_Map_Size[i].w;
                dat[cnt++] = PTPUI_Map_Size[i].h;
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_STILL_IMG_QUALITY
        case APP_PTP_DPC_STILL_IMG_QUALITY:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            for (i = 1 ; i < (sizeof(PTPUI_Map_Quality)/sizeof(PTPUI_Map_Quality[0])) ; i++)
            {
                if(PTPUI_Map_Quality[i].UiIndex == pViewParam->stillQuality){
                    CurSetting = i;
            	}

                if(PTPUI_Map_Quality[i].UiIndex == UI_QUALITY_SUPERFINE){
                    DefSetting = i;
            	}
            }
            dat[cnt++] = PTPUI_Map_Quality[CurSetting].quality;
            dat[cnt++] = PTPUI_Map_Quality[DefSetting].quality;

            printf("Cur = %d, Def = %d\n", CurSetting, DefSetting);

            for (i = 1 ; i < (sizeof(PTPUI_Map_Quality)/sizeof(PTPUI_Map_Quality[0])) ; i++)
            {
                if(PTPUI_Map_Quality[i].UiIndex == UI_PTP_UNSUPPORT_CMD){
                    continue;
            	}
                dat[cnt++] = PTPUI_Map_Quality[i].quality;
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_APERTURE
		case APP_PTP_DPC_APERTURE:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            for (i = 1 ; i < (sizeof(PTPUI_Map_Iris)/sizeof(PTPUI_Map_Iris[0])) ; i++)
            {
                if(PTPUI_Map_Iris[i].UiIndex == gStillCB.ae.iris){
                    CurSetting = i;
            	}

                if(PTPUI_Map_Iris[i].UiIndex == APERTURE_2_8){
                    DefSetting = i;
            	}
            }
            dat[cnt++] = PTPUI_Map_Iris[CurSetting].iris;
            dat[cnt++] = PTPUI_Map_Iris[DefSetting].iris;

            printf("Cur = %d, Def = %d\n", CurSetting, DefSetting);

            for (i = 1 ; i < (sizeof(PTPUI_Map_Iris)/sizeof(PTPUI_Map_Iris[0])) ; i++)
            {
                if(PTPUI_Map_Iris[i].UiIndex == UI_PTP_UNSUPPORT_CMD){
                    continue;
            	}

                dat[cnt++] = PTPUI_Map_Iris[i].iris;
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_ISO
        case APP_PTP_DPC_AE_ISO:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            for (i = 1 ; i < (sizeof(PTPUI_Map_ISO)/sizeof(PTPUI_Map_ISO[0])) ; i++)
            {
                if(PTPUI_Map_ISO[i].UiIndex == pViewParam->iso){
                    CurSetting = i;
            	}

                if(!PTPUI_Map_ISO[i].UiIndex){
                    DefSetting = i;
            	}
            }
            dat[cnt++] = PTPUI_Map_ISO[CurSetting].iso;
            dat[cnt++] = PTPUI_Map_ISO[DefSetting].iso;

            printf("Cur = %d, Def = %d\n", CurSetting, DefSetting);

            for (i = 1 ; i < (sizeof(PTPUI_Map_ISO)/sizeof(PTPUI_Map_ISO[0])) ; i++)
            {
                if(PTPUI_Map_ISO[i].UiIndex == UI_PTP_UNSUPPORT_CMD){
                    continue;
            	}
                dat[cnt++] = PTPUI_Map_ISO[i].iso;
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_EV_OFFSET
        case APP_PTP_DPC_AE_EV_OFFSET:
		case APP_PTP_DPC_AE_EV_OFFSET_ALTERNATIVE:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			UINT32 uiParam = 0;
			SINT16 cur, def;
			if(opModeMap[app_PTP_Get_DscOpMode(MODE_ACTIVE)] == YBFW_MODE_STILL_PREVIEW)
			{
				uiParam = puiPara->PhotoAEMode;
			}
			else if(opModeMap[app_PTP_Get_DscOpMode(MODE_ACTIVE)] == YBFW_MODE_VIDEO_PREVIEW)
			{
				uiParam = puiPara->VideoAEMode;
			}
			cur = (SINT16)appPtpUiIndex2ValMap(uiParam, (PtpValMap_t *)PTPUI_Map_EV, 5);
			def = (SINT16)appPtpUiIndex2ValMap(UI_EXP_BIAS_ZERO, (PtpValMap_t *)PTPUI_Map_EV, 5);
			if (devPropCode == APP_PTP_DPC_AE_EV_OFFSET) {	/* pima spec. */
				dat[cnt++] = cur * 1000;
				dat[cnt++] = def * 1000;
			}
			else {	/* icatch app */
				dat[cnt++] = appPtpEvVal2PtpVal(cur);
				dat[cnt++] = appPtpEvVal2PtpVal(def);
			}
			printf("[EV]Cur = %d, Def = %d\n", cur, DefSetting);

			for (i = 0 ; i < (sizeof(PTPUI_Map_EV)/sizeof(PTPUI_Map_EV[0])) ; i++) {
				if(PTPUI_Map_EV[i].UiIndex == UI_PTP_UNSUPPORT_CMD)
				{
					continue;
				}
				if (devPropCode == APP_PTP_DPC_AE_EV_OFFSET)	/* pima spec. */
					dat[cnt++] = PTPUI_Map_EV[i].ev * 1000;
				else
					dat[cnt++] = appPtpEvVal2PtpVal(PTPUI_Map_EV[i].ev);
			}
			break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_DELAY
		case APP_PTP_DPC_STILL_SNAP_DELAY:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            dat[cnt++] = PTPUI_Map_Delay[puiPara->PhotoDelayTimer];
            dat[cnt++] = PTPUI_Map_Delay[UI_PHOTO_DELAY_TIMER_OFF];

            printf("ptp capture delay = %d, Def = %d\n", puiPara->PhotoDelayTimer, UI_PHOTO_DELAY_TIMER_OFF);

            for (i = 0 ; i < UI_PHOTO_INTERVAL_MAX ; i++) {
                dat[cnt++] = PTPUI_Map_Delay[i];
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_SIZE
		case APP_PTP_DPC_VIDEO_SIZE:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;

            #if SP5K_2GB_MEMORY
            if(pViewParam->videoSize == UI_VIDEO_SIZE_6K3K)
                pViewParam->videoSize = UI_VIDEO_SIZE_4K2K;
            #endif
			ArraySize = appVideoSizeGet(-1, 0, NULL);
			appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);
            dat[cnt++] = pVidData->w;
            dat[cnt++] = pVidData->h;

			#if PTP_SUPPORT_VIDEO_SIZE_FPS
			dat[cnt++] = pVidData->frameRate;
			printf("%dx%d, %dfps\n", pVidData->w, pVidData->h, pVidData->frameRate);
			#else
			printf("%dx%d\n", pVidData->w, pVidData->h);
			#endif

			appVideoSizeGet(0, 0, &pVidData);
            dat[cnt++] = pVidData->w;
            dat[cnt++] = pVidData->h;

			#if PTP_SUPPORT_VIDEO_SIZE_FPS
			dat[cnt++] = pVidData->frameRate;
			#endif

            for (i = 0 ; i < ArraySize ; i++) {

                #if SP5K_2GB_MEMORY
                if(i == UI_VIDEO_SIZE_6K3K)
                    continue;
                #endif
				appVideoSizeGet(i, 0, &pVidData);
                dat[cnt++] = pVidData->w;
                dat[cnt++] = pVidData->h;

				#if PTP_SUPPORT_VIDEO_SIZE_FPS
				dat[cnt++] = pVidData->frameRate;
				#endif
            }
            break;
		#else
		#error "r u serious?"
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_PROGRAM
		case APP_PTP_DPC_STILL_SNAP_PROGRAM: /* only read ? */
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = PTP_DPC_DIAL_MODE_AUTO;
			dat[cnt++] = PTP_DPC_DIAL_MODE_AUTO;
			dat[cnt++] = PTP_DPC_DIAL_MODE_MANUAL;
			dat[cnt++] = PTP_DPC_DIAL_MODE_AUTO;
			dat[cnt++] = PTP_DPC_DIAL_MODE_SHUTTER;
			dat[cnt++] = PTP_DPC_DIAL_MODE_FACEDETECT;
			dat[cnt++] = PTP_DPC_DIAL_MODE_PROGRAM;
			dat[cnt++] = PTP_DPC_DIAL_MODE_CONT_SHOOT;
			dat[cnt++] = PTP_DPC_DIAL_MODE_SCENE;
			break;
		#endif

		#ifdef APP_PTP_DPC_BATTERY_LEVEL
		case APP_PTP_DPC_BATTERY_LEVEL:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = appBatteryLevelGet() * 100 / APP_BATT_LEVEL_FULL;
			dat[cnt++] = 100;
			for (i = 0 ; i <= APP_BATT_LEVEL_FULL ; i++){
				dat[cnt++] = i * 100 / APP_BATT_LEVEL_FULL;
			}
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_ELAPSE
		case APP_PTP_DPC_VIDEO_REC_ELAPSE:
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			UINT32 elsTime = 0;
			if(app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_VIDEO_ON ||
				app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_TIMELAPSE_VIDEO){
				elsTime= (appVideoElapseTimeGet()+500)/1000;
			}else if(app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_TIMELAPSE_STILL){
				elsTime = appStill_TlsElapseTimeGet();
			}
			dat[cnt++] = elsTime;  /*current */
			printf("ptp get elapse time %ds(%d:%d:%d)\n",elsTime,elsTime/(60*60),(elsTime%(60*60))/60,elsTime%60);
			dat[cnt++] = 0;/*defaule*/
			dat[cnt++] = 0;
			dat[cnt++] = 0xFFFFFFFF;
			break;
		#endif

		case PIMA_DPC_MAX_ZOOM_RATIO:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = appMaxZoomGetRation()/100;
			dat[cnt++] = appMaxZoomGetRation()/100;
			dat[cnt++] = appMaxZoomGetRation()/100;
			printf("Max Zoom Ratio:%d\n",appMaxZoomGetRation()/100);
			break;

		#ifdef APP_PTP_DPC_DIGITAL_ZOOM
		case APP_PTP_DPC_DIGITAL_ZOOM:
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = appDZoomGetRatio() / 100;
			dat[cnt++] = appDZoomGetRatio() / 100;
			dat[cnt++] = 10;
			dat[cnt++] = 1;
			dat[cnt++] = 40;
			break;
		#endif
		#ifdef APP_PTP_DPC_STILL_SNAP_BURST
		case APP_PTP_DPC_STILL_SNAP_BURST:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
            for (i = 1 ; i < (sizeof(PTPUI_Map_Burst)/sizeof(PTPUI_Map_Burst[0])) ; i++)
            {
                if(PTPUI_Map_Burst[i] == pViewParam->stillDriverMode){
                    CurSetting = PTPUI_Map_Burst[i];
            	}
            }
            /*mapping*/
            if(CurSetting      == UI_STILL_DRIVERMODE_BURST_3P_1S){
            	CurSetting = PTP_DPC_BURST_3;
        	}
			else if(CurSetting == UI_STILL_DRIVERMODE_BURST_7P_2S){
            	CurSetting = PTP_DPC_BURST_7;
			}
            else if(CurSetting == UI_STILL_DRIVERMODE_BURST_15P_4S){
            	CurSetting = PTP_DPC_BURST_15;
        	}
            else if(CurSetting == UI_STILL_DRIVERMODE_BURST_30P_8S){
            	CurSetting = PTP_DPC_BURST_30;
        	}
            else{
            	CurSetting = PTP_DPC_BURST_OFF;
            	pViewParam->stillDriverMode = UI_STILL_DRIVERMODE_OFF;/*for init*/
            }
            DBG_PRINT("%s:%d  CurSetting %d stillDriverMode %d\n",__FUNCTION__,__LINE__,CurSetting,pViewParam->stillDriverMode);
			dat[cnt++] = CurSetting;
			dat[cnt++] = PTP_DPC_BURST_OFF;
			dat[cnt++] = PTP_DPC_BURST_OFF;
			dat[cnt++] = PTP_DPC_BURST_3;
			dat[cnt++] = PTP_DPC_BURST_7;
			dat[cnt++] = PTP_DPC_BURST_15;
			dat[cnt++] = PTP_DPC_BURST_30;
			/*dat[cnt++] = PTP_DPC_BURST_HS;*/
			break;

		#endif

		#ifdef APP_PTP_DPC_AE_EXPOSURE
		case APP_PTP_DPC_AE_EXPOSURE:
			{
			UINT32 mapIdx = (gStillCB.ae.shutter>48 ? gStillCB.ae.shutter-48 : 0) / 16;	/* what? */
			UINT32 max = sizeof(shutterSpeedVal) / sizeof(UINT32) - 1;
			if (mapIdx > max)
				mapIdx = max;
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = shutterSpeedVal[mapIdx];
			dat[cnt++] = shutterSpeedVal[8];
			dat[cnt++] = shutterSpeedVal[max];
			dat[cnt++] = shutterSpeedVal[0];
			}
			break;
		#endif

		#ifdef APP_PTP_DPC_WB_GAIN
		case APP_PTP_DPC_WB_GAIN:
			ybfwWbGain_t wb;
			UINT32 mode5k;
			ybfwModeGet(&mode5k);
			ybfwIqModeSet(YBFW_SEN_ID_0, mode5k);
			ybfwIqCfgGet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_CFG_WB_GAIN_R_GAIN, &wb.rgain );
			ybfwIqCfgGet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_CFG_WB_GAIN_GR_GAIN, &wb.grgain );
			ybfwIqCfgGet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_CFG_WB_GAIN_B_GAIN, &wb.bgain );
			ybfwIqCfgGet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_CFG_WB_GAIN_GB_GAIN, &wb.gbgain );
			ybfwIqModeSetDone( mode5k );
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = wb.rgain;
			dat[cnt++] = wb.grgain;
			dat[cnt++] = wb.bgain;
			dat[cnt++] = 0x400;
			dat[cnt++] = 0x400;
			dat[cnt++] = 0x400;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0x1000;
			dat[cnt++] = 0x1000;
			dat[cnt++] = 0x1000;
			break;
		#endif

		#ifdef APP_PTP_DPC_DATE_TIME
		case APP_PTP_DPC_DATE_TIME:
			dat[cnt++] = DateTimeForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			{
			    ybfwTmx_t dtime;
				UINT8 *str = (UINT8 *)&dat[cnt];
				ybfwRtcDateTimeGet(YBFW_DATE_TIME_OPTION, &dtime);
				sprintf((char *)str, "%04d%02d%02dT%02d%02d%02d.0",
					     dtime.tmx_year+1900, dtime.tmx_mon , dtime.tmx_mday,
				         dtime.tmx_hour,      dtime.tmx_min , dtime.tmx_sec );
				printf("date time = %s\n", str);
				cnt += (1 + strlen((char *)str) + 3) / 4;
			}
			break;
		#endif

		#ifdef APP_PTP_DPC_LIGHT_FREQUENCY
		case APP_PTP_DPC_LIGHT_FREQUENCY:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = pUiSetting->lightFrequency;
			dat[cnt++] = PTP_DPC_BAND_SELECT_AUTO;
			dat[cnt++] = PTP_DPC_BAND_SELECT_AUTO;
			dat[cnt++] = PTP_DPC_BAND_SELECT_50HZ;
			dat[cnt++] = PTP_DPC_BAND_SELECT_60HZ;
			break;
		#endif

		#ifdef APP_PTP_DPC_SCENE_MODE
		case APP_PTP_DPC_SCENE_MODE:
			/* 0xC001:Scenery
			   0xC002:Backlight
			   0xC003:Night Scenery
			   0xC004:Building
			   0xC005:Kids
			   0xC006:Night Portrait
			   0xC007:Food
			   0xC008:Text
			   0xC009:Snow
			   0xC00A:Fireworks
			   0xC00B:Sunset*/
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = 0xC001;
			dat[cnt++] = 0xC001;
			for (i = 0 ; i < 12 ; i++){
				dat[cnt++] = 0xC001+i;
			}
			break;
		#endif

		case APP_PTP_DPC_DSC_OP_MODE:
			/*
				Video Mode OFF: 0x0001
				Video Mode ON: 0x0011
				Share: 0x0002
				Camera Mode: 0x0003
				Application Mode: 0x0004
				Edit Mode: 0x0005
				Backward: 0x0006
			*/
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = app_PTP_Get_DscOpMode(MODE_ACTIVE);
			dat[cnt++] = PTP_DSCOP_MODE_CAMERA;
			dat[cnt++] = PTP_DSCOP_MODE_VIDEO_OFF;	/* default */
			dat[cnt++] = PTP_DSCOP_MODE_VIDEO_ON;
			dat[cnt++] = PTP_DSCOP_MODE_SHARE;
			dat[cnt++] = PTP_DSCOP_MODE_CAMERA;
			dat[cnt++] = PTP_DSCOP_MODE_APP;
			dat[cnt++] = PTP_DSCOP_MODE_EDIT;
			dat[cnt++] = PTP_DSCOP_MODE_BACKWARD;
			#if PTP_TIME_LAPSE_EN
			dat[cnt++] = PTP_DSCOP_MODE_TIMELAPSE_STILL;
			dat[cnt++] = PTP_DSCOP_MODE_TIMELAPSE_VIDEO;
			dat[cnt++] = PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF;
			dat[cnt++] = PTP_DSCOP_MODE_TIMELAPSE_VIDEO_OFF;
			#endif
			break;

		#ifdef APP_PTP_DPC_WIFI_FEATURES
		case APP_PTP_DPC_WIFI_FEATURES:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = app_PTP_Get_WiFiOpFunc();
			printf("WiFi Op Func:0x%x\n",app_PTP_Get_WiFiOpFunc());
			dat[cnt++] = PTP_WIFI_FUNC_PREVIEW;
			dat[cnt++] = PTP_WIFI_FUNC_BROWSER;
			dat[cnt++] = PTP_WIFI_FUNC_CAPTURE;
			dat[cnt++] = PTP_WIFI_FUNC_VIDEO;
			dat[cnt++] = PTP_WIFI_FUNC_PREVIEW;
			break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_SPACE
		case APP_PTP_DPC_STILL_SNAP_SPACE:
			remain = appStillRemainNumGet();
			dat[cnt++] = None;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = remain;
			dat[cnt++] = remain;
			printf("~~SUNYONG:remain capture number:%d\n",remain);
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_SPACE
		case APP_PTP_DPC_VIDEO_REC_SPACE:
			remain = appVideoRemainSecGet();
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = remain;
			dat[cnt++] = remain;
			printf("~~SUNYONG:remain capacity in secs:%d\n",remain);
			dat[cnt++] = 1; /* MIN */
			dat[cnt++] = 1; /* STEP */
			dat[cnt++] = 0xFFFFFFFF;

			#if 0
			if(IS_CARD_EXIST){
				if(remain==0){
					memoryFullEvtFlg=1;
					appStill_PIMA_Send_Event(PTP_EC_STORE_FULL, YBFW_DRIVE_SD, 0, 0);/* param2, 0 : full, 1 : available */
				}else if(memoryFullEvtFlg){
					memoryFullEvtFlg=0;
					appStill_PIMA_Send_Event(PTP_EC_STORE_FULL, YBFW_DRIVE_SD, 1, 0);/* param2, 0 : full, 1 : available */
				}
			}
			#endif
			break;
		#endif

		#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
		case APP_PTP_DPC_TIMELAPSE_INTERVAL: /* UINT32 */
		{
			PtpValMap_t *ptpMap = NULL;
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			if(appPtpIsTimelapseCapMode())
			{
				dat[cnt++] = appStill_TlsIntervalGet(puiPara->PhotoTimeLapseInterval)/1000;
				ptpMap = PTPUI_Map_Cap_TimeLpsInt;
				len = ARRAY_SIZE(PTPUI_Map_Cap_TimeLpsInt);
				dat[cnt++] = 0; /* secs */
				for (i = 1 ; i < len ; i++)
				{
				    if((puiPara->PhotoHDR == UI_PHOTO_HDR_ON) && !appIsHdmiPlugIn() && (PTPUI_Map_Cap_TimeLpsInt[i].UiIndex == UI_TIME_LAPSE_PHOTO_INTERVAL_3SEC))
                        continue;
					dat[cnt++] = ptpMap[i].val;
				}
			}
			else
			{
				dat[cnt++] = appVideoTimelapseIntvGet(puiPara->VideoTimeLapseInterval);	/* current */
				dat[cnt++] = appVideoTimelapseIntvGet(0);	/* default */
				/* enum */
				for (i = 0 ; i < UI_TIME_LAPSE_VIDEO_INTERVAL_MAX ; i++) {
					dat[cnt++] = appVideoTimelapseIntvGet(i);
				}
			}
			break;
		}

		#ifdef APP_PTP_DPC_TIMELAPSE_LENGTH
		case APP_PTP_DPC_TIMELAPSE_LENGTH:/* UINT16 */
		{
			PtpValMap_t *ptpMap = NULL;
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			if(appPtpIsTimelapseCapMode()){
				ptpMap = PTPUI_Map_TimeLps_Cap_Duration;
				len = ARRAY_SIZE(PTPUI_Map_TimeLps_Cap_Duration);
				dat[cnt++] = appPtpUiIndex2ValMap(puiPara->TimeLapsePhotoShootingTime, ptpMap, len);
				dat[cnt++] = ptpMap[1].val; /* secs */
				for (i = 1 ; i < len ; i++)
				{
					dat[cnt++] = ptpMap[i].val;
				}
			}
			else
			{
				UINT32 t = appVideoTimelapseTimeGet(puiPara->TimeLapseVideoShootingTime);
				dat[cnt++] = (t == 0) ? 0xffff : t;	/* current */
				dat[cnt++] = appVideoTimelapseTimeGet(0);	/* default */
				/* enum */
				for (i = 0 ; i < UI_TIME_LAPSE_VIDEO_SHOOTING_TIME_MAX ; i++) {
					t = appVideoTimelapseTimeGet(i);
					dat[cnt++] = (t == 0) ? 0xffff : t;
				}
			}
			break;
		}
		#endif
		#endif

		#ifdef APP_PTP_DPC_PRODUCT_NAME
		case APP_PTP_DPC_PRODUCT_NAME: /* Product Name */
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			UINT8 *artist= (UINT8*)&dat[cnt];
			//sprintf((char *)artist, "%s", "EVB 6330");

            #if defined(HW_EVB)
            sprintf((char *)artist, "%s", "V50 EVB");
            #elif (defined(HW_SBC) || defined(HW_CVR) || defined(HW_YKN))
            sprintf((char *)artist, "%s", "V50 SBC");
			#elif (defined(HW_360))
            sprintf((char *)artist, "%s", "V50 360");
            #else
            sprintf((char *)artist, "%s", "V50 Serial");
            #endif

            cnt+= (1 + strlen((char *)artist) + 3)/4;
			printf("Product Name: %s,  %d items\n", artist, cnt);
			break;
		#endif

		#ifdef APP_PTP_DPC_FW_VERSION
		case APP_PTP_DPC_FW_VERSION: /* FW version */
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			UINT8 *fwversion= (UINT8*)&dat[cnt];
			sprintf((char *)fwversion, "%s %s", (char *)appFWVerGet(),(char *)appSvnGet());
			cnt+= (1 + strlen((char *)fwversion) + 3)/4;
			printf("FW Version: %s,  %d items\n", fwversion, cnt);
			break;
		#endif

		#ifdef APP_PTP_DPC_CUSTOMER_NAME
		case APP_PTP_DPC_CUSTOMER_NAME: /* to replace the customer name */
			cnt = app_ptpUsbCustomerSet(dat);
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_CUSTOM_PB
		case APP_PTP_DPC_VIDEO_CUSTOM_PB:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0;
			dat[cnt++] = 1;
			break;
		#endif

		#ifdef APP_PTP_DPC_RTP_PV_CACHE_TIME
		case APP_PTP_DPC_RTP_PV_CACHE_TIME:
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;

        		dat[cnt++] = 200;

			dat[cnt++] = 100;/* default */
			dat[cnt++] = 0;    /*MIN*/
			dat[cnt++] = 1;  /*STEP*/
			dat[cnt++] = 600;  /*MAX*/
			break;
		#endif

		#ifdef APP_PTP_DPC_RTP_SERVICE
		case APP_PTP_DPC_RTP_SERVICE:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0;
			dat[cnt++] = 1;
			break;

		#ifdef APP_PTP_DPC_RTP_CONDITION0
		case APP_PTP_DPC_RTP_CONDITION0:
			/*
			This property code is for compatibility
			Bit0 is start/stop stream by APP or not when changing resolution in video mode
			*/
			printf("\n/***Set APP compatibility***\n");
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0;
			dat[cnt++] = 1;
			break;
		#endif

		#ifdef APP_PTP_DPC_RTP_CONDITION1
			case APP_PTP_DPC_RTP_CONDITION1:
				dat[cnt++] = EnumForm;
				dat[cnt++] = PTP_PROPGETSET_GETONLY;
				dat[cnt++] = 1;
				dat[cnt++] = 1;
				dat[cnt++] = 1;
				dat[cnt++] = 0;
				break;
		#endif

		#ifdef APP_RTP_DPC_RTP_PV_CFG0
		case APP_RTP_DPC_RTP_PV_CFG0:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
			ArraySize = sizeof(PTPUI_Map_VidPVFormatSizeBitrateVid)/sizeof(PTPUI_Map_VidPVFormatSizeBitrateVid[0]);
			HOST_ASSERT(ArraySize == appVideoSizeGet(-1, 0, NULL));
			dat[cnt++] = PTPUI_Map_VidPVFormatSizeBitrateVid[pViewParam->videoSize].vidPvData;	/* UI current */
			dat[cnt++] = PTPUI_Map_VidPVFormatSizeBitrateVid[0].vidPvData;	/* factory default */
			for (i = 0 ; i < ArraySize ; i++) {
				dat[cnt++] = PTPUI_Map_VidPVFormatSizeBitrateVid[i].vidPvData;
			}
            break;
		#endif

		#ifdef APP_RTP_DPC_RTP_PV_CFG1
        case APP_RTP_DPC_RTP_PV_CFG1:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
			#ifndef APP_RTP_DPC_RTP_PV_CFG1
			ArraySize = appVideoSizeGet(-1, 0, NULL);
			appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);
			dat[cnt++] = pVidData->frameRate;	/* UI current */
			appVideoSizeGet(0, 0, &pVidData);
			dat[cnt++] = pVidData->frameRate;	/* factory default */
			for (i = 0 ; i < ArraySize ; i++) {
				appVideoSizeGet(i, 0, &pVidData);
				dat[cnt++] = pVidData->frameRate;
			}
			#else
			ArraySize = sizeof(PTPUI_Map_VidPVFormatFps)/sizeof(PTPUI_Map_VidPVFormatFps[0]);
			HOST_ASSERT(ArraySize == appVideoSizeGet(-1, 0, NULL));
			dat[cnt++] = PTPUI_Map_VidPVFormatFps[pViewParam->videoSize].vidPvData;	/* UI current */
			dat[cnt++] = PTPUI_Map_VidPVFormatFps[0].vidPvData;	/* factory default */
			for (i = 0 ; i < ArraySize ; i++) {
				dat[cnt++] = PTPUI_Map_VidPVFormatFps[i].vidPvData;
			}
			#endif

            break;
		#endif
		#endif

		#if APP_RTP_DPC_FW_UPDATE_CHECK
        case APP_RTP_DPC_FW_UPDATE_CHECK:
			dat[cnt++] = RangeForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0;
            dat[cnt++] = 1;
		    break;

        case APP_PTP_DPC_FW_STORE_PATH:
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			strFWPath= (UINT8*)&dat[cnt];
			sprintf((char *)strFWPath, "%s","D:\\SPHOST.BRN");
			cnt+= (1 + strlen((char *)strFWPath) + 3)/4;
			printf("FW path: %s,  %d items\n", strFWPath, cnt);
			#if 0
            if(appFileExistCheck(strFWPath))
            {
                ybfwFsFileDelete(strFWPath);
            }
			#endif
            break;

        case APP_RTP_DPC_FW_UPDATE:
            dat[cnt++] = RangeForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            dat[cnt++] = 1;
            dat[cnt++] = 1;
            dat[cnt++] = 0;
            dat[cnt++] = 1;
            break;
		#endif

		/*Upside down*/
   		#ifdef APP_PTP_DPC_VIDEO_REC_CAR_MODE
		case APP_PTP_DPC_VIDEO_REC_CAR_MODE:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = puiPara->InvertMode;
			dat[cnt++] = puiPara->InvertMode;
			dat[cnt++] = PTP_DPC_CAR_MODE_OFF;
			dat[cnt++] = PTP_DPC_CAR_MODE_ON;
			break;
    	#endif

		#ifdef APP_PTP_DPC_SYS_FEATURES
		case USB_PIMA_DPC_APP_SUPPORT_MODE: /*app show mode icon*/
			/*
			  EnumFrom   0~7
			  bit0:video mode
			  bit1:cam mode
			  bit2:timelapse
			default:3 (video + cam mode)
			*/
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
			dat[cnt++] = 7;/* current  video + cam +timelapse mode ICON*/
			dat[cnt++] = 3;/* default?  video + cam Mode ICON*/
			dat[cnt++] = 1;/*not show icon ,default video mode*/
			dat[cnt++] = 3;
			dat[cnt++] = 7;
			#else
			dat[cnt++] = 3;/* current  video + cam +timelapse mode ICON*/
			dat[cnt++] = 3;/* default  video + cam Mode ICON*/
			dat[cnt++] = 1;/*not show icon ,default video mode*/
			dat[cnt++] = 3;
			#endif
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_SLOW_MOTION
		case APP_PTP_DPC_VIDEO_REC_SLOW_MOTION :
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			appVideoConfig_t *pVidData;
			appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);

			for (i = 0 ; i < (sizeof(PTPUI_Map_SlowMotion)/sizeof(PTPUI_Map_SlowMotion[0])) ; i++)
			{
				if(PTPUI_Map_SlowMotion[i].UiIndex == puiPara->VideoSlowMotion){
					CurSetting = i;
					break;
				}
			}

			dat[cnt++] = pVidData->frameRate >= 120 ? PTPUI_Map_SlowMotion[CurSetting].val: PTP_DPC_SLOW_MOTION_OFF;
			dat[cnt++] = PTP_DPC_SLOW_MOTION_OFF;
			printf("slow motion mode:%d\n", pVidData->frameRate >= 120 ? PTPUI_Map_SlowMotion[CurSetting].val : PTP_DPC_SLOW_MOTION_OFF);
			dat[cnt++] = PTP_DPC_SLOW_MOTION_OFF;
			dat[cnt++] = PTP_DPC_SLOW_MOTION_ON;
			break;
		#endif

		#ifdef APP_PTP_DPC_POWER_SAVING_TIME
		case APP_PTP_DPC_POWER_SAVING_TIME:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;

			for (i = 0 ; i < (sizeof(PTPUI_Map_SaverScreen)/sizeof(PTPUI_Map_SaverScreen[0])) ; i++)
			{
				if(PTPUI_Map_SaverScreen[i].UiIndex == puiPara->ScreenSaverTime){
					CurSetting = i;
					break;
				}
			}

			printf("[PTP]screen save get:index=%d   val=%d\n",puiPara->ScreenSaverTime,PTPUI_Map_SaverScreen[CurSetting].val);

			dat[cnt++] = PTPUI_Map_SaverScreen[CurSetting].val;
			dat[cnt++] = PTPUI_Map_SaverScreen[0].val;
			for (i = 0; i < (sizeof(PTPUI_Map_SaverScreen)/sizeof(PTPUI_Map_SaverScreen[0])) ; i++)
            {
                dat[cnt++] = PTPUI_Map_SaverScreen[i].val;
            }
			break;
		#endif
		#ifdef APP_PTP_DPC_POWER_OFF_TIME
		case APP_PTP_DPC_POWER_OFF_TIME:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;

			for (i = 0 ; i < (sizeof(PTPUI_Map_AutoPowerOFF)/sizeof(PTPUI_Map_AutoPowerOFF[0])) ; i++)
	        {
	        	if(PTPUI_Map_AutoPowerOFF[i].UiIndex == puiPara->SleepTime)
        		{
		          	CurSetting = i;
        		}
            }

			dat[cnt++] = PTPUI_Map_AutoPowerOFF[CurSetting].val;
            dat[cnt++] = PTPUI_Map_AutoPowerOFF[CurSetting].val;

            for (i = 0; i < (sizeof(PTPUI_Map_AutoPowerOFF)/sizeof(PTPUI_Map_AutoPowerOFF[0])) ; i++)
            {
                if(PTPUI_Map_AutoPowerOFF[i].val == UI_PTP_UNSUPPORT_CMD)
            	{
                    continue;
            	}
                dat[cnt++] = PTPUI_Map_AutoPowerOFF[i].val;
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_AUTO_START
		case APP_PTP_DPC_VIDEO_REC_AUTO_START:
	        dat[cnt++] = EnumForm;
    		dat[cnt++] = PTP_PROPGETSET_GETSET;
  			dat[cnt++] = appPtpUiIndex2Switch(puiPara->powerOnRecord, appSwitchAutoRec);
  			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
  			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
  			dat[cnt++] = APP_SWITCH_BUTTON_ON;
            break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_EIS
		case APP_PTP_DPC_VIDEO_EIS:
	        dat[cnt++] = EnumForm;
    		dat[cnt++] = PTP_PROPGETSET_GETSET;
  			dat[cnt++] = appPtpUiIndex2Switch(puiPara->VideImageStabilization, appSwitchEis);
  			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
  			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
  			dat[cnt++] = APP_SWITCH_BUTTON_ON;
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_FILE_LENGTH
		case APP_PTP_DPC_VIDEO_FILE_LENGTH:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;

			if(appVideoSeamlessCheck(puiPara->VideoSize, pViewParam->videoQuality, puiPara->Seamless) == FALSE) {
		       			pViewParam->videoSeamless = puiPara->Seamless  = 0;
		        		printf("[PTP] 4K50fps not support seamless 5min\n");
		        }

			dat[cnt++] = PTPUI_Map_VideoFileLength[pViewParam->videoSeamless];
		        dat[cnt++] = PTPUI_Map_VideoFileLength[pViewParam->videoSeamless];

		        printf("[VideoFileLength]Cur = %d, Def = %d\n", CurSetting, DefSetting);

	                for (i = 0 ; i < (sizeof(PTPUI_Map_VideoFileLength)/sizeof(PTPUI_Map_VideoFileLength[0])) ; i++)
	                {
		                if(PTPUI_Map_VideoFileLength[i] == UI_PTP_UNSUPPORT_CMD)
		            	{
			                continue;
		            	}
		                dat[cnt++] = PTPUI_Map_VideoFileLength[i];
	                }
		break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_FAST_MOTION
		case APP_PTP_DPC_VIDEO_REC_FAST_MOTION:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;

			for (i = 0 ; i < (sizeof(PTPUI_Map_FastMotion)/sizeof(PTPUI_Map_FastMotion[0])) ; i++)
            {
                if(PTPUI_Map_FastMotion[i].UiIndex == puiPara->VideoFastMotion)
            	{
	            	CurSetting = i;
            	}
            }

			dat[cnt++] = PTPUI_Map_FastMotion[CurSetting].val;
			dat[cnt++] = PTPUI_Map_FastMotion[CurSetting].val;
			printf("fastMotion motion mode:%d\n",puiPara->VideoFastMotion);
			 for (i = 0 ; i < (sizeof(PTPUI_Map_FastMotion)/sizeof(PTPUI_Map_FastMotion[0])) ; i++)
            {
                if(PTPUI_Map_FastMotion[i].val == UI_PTP_UNSUPPORT_CMD)
            	{
	                continue;
            	}
                dat[cnt++] = PTPUI_Map_FastMotion[i].val;
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_WNR
		case APP_PTP_DPC_VIDEO_REC_WNR:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			dat[cnt++] = appPtpUiIndex2Switch(puiPara->VideoWindNoiseReduction, appSwitchWindReduce);
			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
			dat[cnt++] = APP_SWITCH_BUTTON_OFF;
			dat[cnt++] = APP_SWITCH_BUTTON_ON;
			break;
		#endif

		#ifdef APP_PTP_DPC_WIFI_MY_SSID
		case APP_PTP_DPC_WIFI_MY_SSID:
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			UINT8 *ssid = (UINT8*)&dat[cnt];
			appSSIDNameGet(ssid);
			cnt+= (1 + strlen((char *)ssid) + 3)/4;
			printf("wifi ssid : %s,  %d items\n", ssid, cnt);
			break;
		#endif

		#ifdef APP_PTP_DPC_WIFI_MY_PWD
		case APP_PTP_DPC_WIFI_MY_PWD:
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			char *pwd = (char *)&dat[cnt];
			strcpy(pwd, (char *)gpassword);
			cnt+= (1 + strlen(pwd) + 3)/4;
			printf("wifi password : %s,  %d items\n", pwd, cnt);
			break;
		#endif
		case USB_PIMA_DPC_TIME_ZONE:
			dat[cnt++] = LongStringForm;
			dat[cnt++] = PTP_PROPGETSET_GETSET;
			break;

		#ifdef APP_PTP_DPC_WIFI_SSID_TO_JOIN
		case APP_PTP_DPC_WIFI_SSID_TO_JOIN:
            dat[cnt++] = LongStringForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            UINT8 *strESSIDName= (UINT8*)&dat[cnt];
            sprintf((char *)strESSIDName, "%s",pNetCusData.STASsid);
            cnt+= (1 + strlen((char *)strESSIDName) + 3)/4;
            //printf("Essid name : %s,  %d items\n", strESSIDName, cnt);
            break;
		#endif

		#ifdef APP_PTP_DPC_WIFI_PWD_TO_JOIN
        case APP_PTP_DPC_WIFI_PWD_TO_JOIN:
            dat[cnt++] = LongStringForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            UINT8 *strESSIDpw= (UINT8*)&dat[cnt];
            sprintf((char *)strESSIDpw, "%s",pNetCusData.STAPW);
            cnt+= (1 + strlen((char *)strESSIDpw) + 3)/4;
            //printf("Essid Password : %s,  %d items\n", strESSIDpw, cnt);
            break;
		#endif

		#ifdef APP_PTP_DPC_WIFI_AP_STA_SWITCH
        case APP_PTP_DPC_WIFI_AP_STA_SWITCH:
            dat[cnt++] = EnumForm;
            dat[cnt++] = PTP_PROPGETSET_GETSET;
            dat[cnt++] = 0;
            dat[cnt++] = 0;
            dat[cnt++] = 0;	/* switch to ap */
            dat[cnt++] = 1;	/* switch to station */
            break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_NONBLOCKING
		case APP_PTP_DPC_STILL_SNAP_NONBLOCKING:
			dat[cnt++] = EnumForm;
			dat[cnt++] = PTP_PROPGETSET_GETONLY;
			dat[cnt++] = 1; /*1: non-blocking, 0: blocking*/
			dat[cnt++] = 1;
			dat[cnt++] = 1;
			dat[cnt++] = 0;
			break;
		#endif

		default:
			cnt = 0;
			/*printf("PIMA device property 0x%x isn't supported.\n", devPropCode);*/
			break;

	}
	dat[0] = cnt;
}

void appStill_PIMA_Apply_cb(UINT32 devPropCode, UINT32 *dat)
{
	#if 0
	UINT32 i;
	printf("\tdevprop=%x(%d):", devPropCode, dat[0]);
	for (i = 0 ; i < (dat[0]+3)/4 ; i++)
		printf("0x%x ", dat[i+1]);
	printf("\n");
	#endif

	uiPara_t* puiPara = appUiParaGet();
	if (puiPara->SleepTime != POWER_SAVE_OFF){
        appPowerSaveTimeOutReset();
    }

	#ifdef APP_PTP_DPC_VIDEO_SIZE
	if(devPropCode == APP_PTP_DPC_VIDEO_SIZE)/* prevent the time delay caused by ybfw msg send */
	{
		UINT8 i;
		UINT32 cnt = appVideoSizeGet(-1, 0, NULL);
		appVideoConfig_t *pVidData;

		#if PTP_SUPPORT_VIDEO_SIZE_FPS
		printf("\t-> (%d,%d,%d)\n", dat[1],dat[2],dat[3]);
		#else
		printf("\t-> (%d,%d)\n", dat[1],dat[2]);
		#endif

		for (i = 0 ; i < cnt ; i++)
		{
			appVideoSizeGet(i, 0, &pVidData);

			#if PTP_SUPPORT_VIDEO_SIZE_FPS
			if (pVidData->w == dat[1] && pVidData->h == dat[2] && pVidData->frameRate == dat[3])
			{
				puiPara->VideoSize = i;
				pViewParam->videoSize = i;
				break;
			}
			#else
			if (pVidData->w == dat[1] && pVidData->h == dat[2]) {
				puiPara->VideoSize = i;
				break;
			}
			#endif

		}
		if (pVidData->frameRate >= 240)
			pViewParam->videoStamp = puiPara->VideoStampMode = UI_DATESTAMP_OFF;

		if(appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == TRUE)
        {
            puiPara->VideoSlowMotion = UI_VIDEO_SLOW_MOTION_OFF;
        }
		if (appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == FALSE) {
			puiPara->VideImageStabilization = UI_VIDEO_IMAGE_STABILIZATION_OFF;
		}
		printf("0x%x : set param directly in apply cb (%d)\n",devPropCode, pViewParam->videoSize);

		return ;
	}
	#endif
	if (devPropCode == APP_PTP_DPC_DSC_OP_MODE) /* prevent the time delay caused by ybfw msg send */
	{
		UINT32 cur = app_PTP_Get_DscOpMode(MODE_ACTIVE);
		printf("\t-> %d\n", dat[1]);
		printf("0x%x : set param directly in apply cb (%d)\n",devPropCode, dat[1]);
		app_PTP_Set_DscOpMode(dat[1]);
		if (appActiveStateGet() == APP_STATE_BURST_CAPTURE
			&& appPtpOpModeGet() != YBFW_MODE_STILL_PREVIEW) {
			app_PTP_Set_DscOpMode(cur);
		}
		else {
			appPtpDscModeChange();
		}
	}
	else {
		UINT32 wait = 1;
		UINT32 size = dat[0] * sizeof(UINT32);
		UINT32 *buf = ybfwMallocCache(sizeof(UINT32) + size);
		memcpy(&buf[1], &dat[1], size);
		buf[0] = devPropCode;
		#if APP_RTP_DPC_FW_UPDATE_CHECK
		if (devPropCode == APP_RTP_DPC_FW_UPDATE)
			wait = 0;
		#endif
		appBackgroundFuncExec(appStill_PTPToUI_Set, (UINT32)buf, 16, wait);
	}
}

UINT16
appStill_PTPToUI_Map(
    UINT32 PTPCode,
    UINT32* PTPDat
)
{
    UINT8 i;
    UINT16 UIIdx = 0;

    printf("\t-> %d\n", PTPDat[1]);

    switch (PTPCode)
    {
    	#ifdef APP_PTP_DPC_STILL_IMG_SIZE
        case APP_PTP_DPC_STILL_IMG_SIZE:
            for (i = 1 ; i < (sizeof(PTPUI_Map_Size)/sizeof(PTPUI_Map_Size[0])) ; i++)
            {
                if (PTPUI_Map_Size[i].w == PTPDat[1] && PTPUI_Map_Size[i].h == PTPDat[2])
                {
                    UIIdx = PTPUI_Map_Size[i].UiIndex;
                    break;
                }
            }
            break;

		#endif

		#ifdef APP_PTP_DPC_STILL_IMG_QUALITY
        case APP_PTP_DPC_STILL_IMG_QUALITY:
            for (i = 1 ; i < (sizeof(PTPUI_Map_Quality)/sizeof(PTPUI_Map_Quality[0])) ; i++)
            {
                if (PTPUI_Map_Quality[i].quality== PTPDat[1])
                {
                    UIIdx = PTPUI_Map_Quality[i].UiIndex;
                    break;
                }
            }
            break;
		#endif
		#ifdef APP_PTP_DPC_WB_MODE
		case APP_PTP_DPC_WB_MODE:
        case PIMA_DPC_WHILE_BALANCE:
            UIIdx = PTPUI_Map_WB[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_APERTURE
        case APP_PTP_DPC_APERTURE:
            for (i = 1 ; i < (sizeof(PTPUI_Map_Iris)/sizeof(PTPUI_Map_Iris[0])) ; i++)
            {
                if (PTPUI_Map_Iris[i].iris == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_Iris[i].UiIndex;
                    break;
                }
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_AF_MODE
        case APP_PTP_DPC_AF_MODE:
            UIIdx = PTPUI_Map_AF[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_METERING
        case APP_PTP_DPC_AE_METERING:
            UIIdx = PTPUI_Map_Metering[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_FLASH_MODE
        case APP_PTP_DPC_AE_FLASH_MODE:
            UIIdx = PTPUI_Map_Flash[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_ISO
        case APP_PTP_DPC_AE_ISO:
            for (i = 1 ; i < (sizeof(PTPUI_Map_ISO)/sizeof(PTPUI_Map_ISO[0])) ; i++)
            {
                if (PTPUI_Map_ISO[i].iso == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_ISO[i].UiIndex;
                    break;
                }
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_DELAY
        case APP_PTP_DPC_STILL_SNAP_DELAY:
			UIIdx = UI_PTP_UNSUPPORT_CMD;
            for (i = 0 ; i < UI_PHOTO_INTERVAL_MAX ; i++) {
				if (PTPDat[1] == PTPUI_Map_Delay[i]) {
					UIIdx = i;
					break;
            	}
            }
            break;
		#endif

		#ifdef APP_PTP_DPC_AE_EV_OFFSET
        case APP_PTP_DPC_AE_EV_OFFSET:
            for (i = 1 ; i < (sizeof(PTPUI_Map_EV)/sizeof(PTPUI_Map_EV[0])) ; i++)
            {
                if (PTPUI_Map_EV[i].ev * 1000 == (SINT16)PTPDat[1])
                {
                    UIIdx = PTPUI_Map_EV[i].UiIndex;
                    break;
                }
            }
            break;
		case APP_PTP_DPC_AE_EV_OFFSET_ALTERNATIVE:
			for (i = 0 ; i < (sizeof(PTPUI_Map_EV)/sizeof(PTPUI_Map_EV[0])) ; i++)
			{
				if (appPtpEvVal2PtpVal(PTPUI_Map_EV[i].ev) == PTPDat[1])
				{
					UIIdx = PTPUI_Map_EV[i].UiIndex;
					break;
				}
			}
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_AUTO_START
        case APP_PTP_DPC_VIDEO_REC_AUTO_START:
            UIIdx = appSwitchAutoRec[PTPDat[1]];
            break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_WNR
		case APP_PTP_DPC_VIDEO_REC_WNR:
			UIIdx = appSwitchWindReduce[PTPDat[1]];
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_EIS
		case APP_PTP_DPC_VIDEO_EIS:
			UIIdx = appSwitchEis[PTPDat[1]];
			break;
		#endif

		#ifdef APP_PTP_DPC_STILL_SNAP_MODE
        case APP_PTP_DPC_STILL_SNAP_MODE:
            UIIdx = PTPUI_Map_Capture[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_SHARPNESS
        case APP_PTP_DPC_SHARPNESS:
            UIIdx = PTPUI_Map_Sharpness[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_DATESTAMP
        case APP_PTP_DPC_DATESTAMP:
            UIIdx = PTPUI_Map_Stamp[(PTPDat[1])];
            break;
		#endif

		#ifdef APP_PTP_DPC_POWER_SAVING_TIME
		case APP_PTP_DPC_POWER_SAVING_TIME:
			for (i = 0 ; i < (sizeof(PTPUI_Map_SaverScreen)/sizeof(PTPUI_Map_SaverScreen[0])) ; i++)
            {
                if (PTPUI_Map_SaverScreen[i].val == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_SaverScreen[i].UiIndex;
                    break;
                }
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_POWER_OFF_TIME
		case APP_PTP_DPC_POWER_OFF_TIME:
			for (i = 0 ; i < (sizeof(PTPUI_Map_AutoPowerOFF)/sizeof(PTPUI_Map_AutoPowerOFF[0])) ; i++)
            {
                if (PTPUI_Map_AutoPowerOFF[i].val == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_AutoPowerOFF[i].UiIndex;
                    break;
                }
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_FAST_MOTION
		case APP_PTP_DPC_VIDEO_REC_FAST_MOTION:
			for (i = 0 ; i < (sizeof(PTPUI_Map_FastMotion)/sizeof(PTPUI_Map_FastMotion[0])) ; i++)
            {
                if (PTPUI_Map_FastMotion[i].val == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_FastMotion[i].UiIndex;
                    break;
                }
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_REC_SLOW_MOTION
		case APP_PTP_DPC_VIDEO_REC_SLOW_MOTION :
			for (i = 0 ; i < (sizeof(PTPUI_Map_SlowMotion)/sizeof(PTPUI_Map_SlowMotion[0])) ; i++)
            {
                if (PTPUI_Map_SlowMotion[i].val == PTPDat[1])
                {
                    UIIdx = PTPUI_Map_SlowMotion[i].UiIndex;
                    break;
                }
            }
			break;
		#endif

		#ifdef APP_PTP_DPC_VIDEO_FILE_LENGTH
		case APP_PTP_DPC_VIDEO_FILE_LENGTH:

		for (i = 0 ; i < (sizeof(PTPUI_Map_VideoFileLength)/sizeof(PTPUI_Map_VideoFileLength[0])) ; i++)
            	{
	                if (PTPUI_Map_VideoFileLength[i] == PTPDat[1])
	                {
	                    UIIdx = i;
	                    break;
	                }
            	}
		break;
		#endif
		#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
		case APP_PTP_DPC_TIMELAPSE_INTERVAL:
			if(appPtpIsTimelapseCapMode()){
				UIIdx = appPtpVal2UiMap(PTPDat[1],PTPUI_Map_Cap_TimeLpsInt,ARRAY_SIZE(PTPUI_Map_Cap_TimeLpsInt));
			}else{
				for(i = 0 ; i < UI_TIME_LAPSE_VIDEO_INTERVAL_MAX ; i++) {
					if (PTPDat[1] == appVideoTimelapseIntvGet(i)) {
						UIIdx = i;
						break;
					}
				}
			}
			break;

		#ifdef APP_PTP_DPC_TIMELAPSE_LENGTH
		case PIMA_DPC_TIMELAPSE_NUMBER:
			if(appPtpIsTimelapseCapMode()){
				UIIdx = appPtpVal2UiMap(PTPDat[1],PTPUI_Map_TimeLps_Cap_Duration,ARRAY_SIZE(PTPUI_Map_TimeLps_Cap_Duration));
			}else{
				if (PTPDat[1] == 0xffff) {
					UIIdx = 0;
				}
				else {
					for(i = 0 ; i < UI_TIME_LAPSE_VIDEO_SHOOTING_TIME_MAX ; i++) {
						if (PTPDat[1] == appVideoTimelapseTimeGet(i)) {
							UIIdx = i;
							break;
						}
					}
				}
			}
			break;
		#endif
		#endif


		default:
			break;
    }

    printf("UIIdx = %d\n", UIIdx);

    return UIIdx;
}

static void
appStill_PTPToUI_Set(
    UINT32 *buf
)
{
    UINT32 i;
    UINT32 PTPCode = buf[0];
    UINT32 *PTPDat = buf;
    UINT16 UIIdx = 0;
    UINT32 mode5k;
    static UINT32 FWBinSize = 0;
    appDiskInfo_t* pDisk;
	uiPara_t* puiPara = appUiParaGet();
	UINT8 ssid[MAX_SSID_NAME_LEN]={0};
	appVideoConfig_t *pVidData;
	appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);

	printf("appStill_PTPToUI_Set:0x%x\n",PTPCode);

    UIIdx = appStill_PTPToUI_Map(PTPCode, PTPDat);
    if(UIIdx == UI_PTP_UNSUPPORT_CMD || UIIdx == UI_PTP_UNDEFINE_CMD)
    {
        printf("Unsupport Undefine cmd %d\n", PTPDat[1]);
    }
    else
    {
        switch (PTPCode)
        {
        	#ifdef APP_PTP_DPC_STILL_IMG_SIZE
            case APP_PTP_DPC_STILL_IMG_SIZE:
                pViewParam->stillSize = UIIdx;
                puiPara->ImageSize = pViewParam->stillSize;
                break;
			#endif

			#ifdef APP_PTP_DPC_STILL_IMG_QUALITY
            case APP_PTP_DPC_STILL_IMG_QUALITY:
                pViewParam->stillQuality= UIIdx;
                break;
			#endif

			#ifdef APP_PTP_DPC_WB_MODE
			case APP_PTP_DPC_WB_MODE:
            case PIMA_DPC_WHILE_BALANCE:
    			pViewParam->wb = UIIdx;
          		puiPara->WBMode = pViewParam->wb;
    			appStill_SetWB(pViewParam->wb);
                break;
			#endif

			#ifdef APP_PTP_DPC_AE_METERING
            case APP_PTP_DPC_AE_METERING:
                pViewParam->metering = UIIdx;
                appStill_SetMetering(pViewParam->metering);
                break;
			#endif

			#ifdef APP_PTP_DPC_AE_ISO
            case APP_PTP_DPC_AE_ISO:
                pViewParam->iso = UIIdx;
                appStill_SetISO(pViewParam->iso);
                break;
			#endif

			#ifdef APP_PTP_DPC_AE_EV_OFFSET
            case APP_PTP_DPC_AE_EV_OFFSET:
            case APP_PTP_DPC_AE_EV_OFFSET_ALTERNATIVE:
				if(opModeMap[app_PTP_Get_DscOpMode(MODE_ACTIVE)] == YBFW_MODE_STILL_PREVIEW){
					puiPara->PhotoAEMode = UIIdx;
					ybfwAeModeSet(0, YBFW_AE_MODE_INFO_ONLY);
					appStill_SetExposure(puiPara->PhotoAEMode);
				}else if(opModeMap[app_PTP_Get_DscOpMode(MODE_ACTIVE)] == YBFW_MODE_VIDEO_PREVIEW){
					puiPara->VideoAEMode = UIIdx;
					ybfwAeModeSet(0, YBFW_AE_MODE_INFO_ONLY);
					appStill_SetExposure(puiPara->VideoAEMode);
				}
				break;
			#endif

			#ifdef APP_PTP_DPC_STILL_SNAP_DELAY
            case APP_PTP_DPC_STILL_SNAP_DELAY:
                puiPara->PhotoDelayTimer = UIIdx;
				#if PTP_TIME_LAPSE_EN
				if(puiPara->PhotoDelayTimer != UI_PHOTO_DELAY_TIMER_OFF)
				{
					puiPara->PhotoTimeLapseInterval = UI_TIME_LAPSE_PHOTO_INTERVAL_OFF;
				}
				#endif

                break;
			#endif

			#ifdef APP_PTP_DPC_STILL_SNAP_MODE
            case APP_PTP_DPC_STILL_SNAP_MODE:
                pViewParam->stillDriverMode = UIIdx;
                break;
			#endif

			#ifdef APP_PTP_DPC_SHARPNESS
            case APP_PTP_DPC_SHARPNESS:
                pViewParam->edge = UIIdx;
                appStill_SetSharpness(pViewParam->edge);
                break;
			#endif

			#ifdef APP_PTP_DPC_DATESTAMP
            case PIMA_DPC_DATESTAMP:
				if (pVidData->frameRate >= 240)
					pViewParam->videoStamp = puiPara->VideoStampMode = UI_DATESTAMP_OFF;
				else
					pViewParam->videoStamp = puiPara->VideoStampMode = UIIdx;
				puiPara->StillStampMode = UIIdx;     /*fix jira BSP-1466*/
                break;
			#endif

			#ifdef APP_PTP_DPC_STILL_SNAP_BURST
            case APP_PTP_DPC_STILL_SNAP_BURST:
                printf("\t-> %d\n", (SINT16)PTPDat[1]);

                if(PTPDat[1] == PTP_DPC_BURST_3)
					pViewParam->stillDriverMode= UI_STILL_DRIVERMODE_BURST_3P_1S;
				else if(PTPDat[1] == PTP_DPC_BURST_7)
					pViewParam->stillDriverMode= UI_STILL_DRIVERMODE_BURST_7P_2S;
				else if(PTPDat[1] == PTP_DPC_BURST_15)
					pViewParam->stillDriverMode= UI_STILL_DRIVERMODE_BURST_15P_4S;
				else if(PTPDat[1] == PTP_DPC_BURST_30)
					pViewParam->stillDriverMode= UI_STILL_DRIVERMODE_BURST_30P_8S;
				else
					pViewParam->stillDriverMode = UI_STILL_DRIVERMODE_OFF;

				#if PTP_TIME_LAPSE_EN
				if(pViewParam->stillDriverMode != UI_STILL_DRIVERMODE_OFF)
				{
					puiPara->PhotoTimeLapseInterval = UI_TIME_LAPSE_PHOTO_INTERVAL_OFF;
				}
				#endif
				puiPara->PhotoBrustMode = pViewParam->stillDriverMode;
				DBG_PRINT("%s:%d  PTPDat[1] %d stillDriverMode %d\n",__FUNCTION__,__LINE__,PTPDat[1],pViewParam->stillDriverMode);

                break;
			#endif

			#ifdef APP_PTP_DPC_LIGHT_FREQUENCY
			case APP_PTP_DPC_LIGHT_FREQUENCY:
				printf("\t-> %d\n", PTPDat[1]);
				pUiSetting->lightFrequency = PTPDat[1];
				applightFrequencySetting(pUiSetting->lightFrequency);
				break;
			#endif

			#ifdef APP_PTP_DPC_WB_GAIN
            case APP_PTP_DPC_WB_GAIN:
                printf("\t-> %d, %d, %d\n", PTPDat[1], PTPDat[2], PTPDat[3]);
				ybfwWbGain_t wb;
                wb.rgain = PTPDat[1];
                wb.grgain = wb.gbgain = PTPDat[2];
                wb.bgain = PTPDat[2];
				ybfwModeGet(&mode5k);
				ybfwIqModeSet(YBFW_SEN_ID_0, mode5k);
				ybfwIqCfgSet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_R, wb.rgain );
				ybfwIqCfgSet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_GR, wb.grgain );
				ybfwIqCfgSet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_B, wb.bgain );
				ybfwIqCfgSet(YBFW_IQ_DO_FLOW_0, YBFW_IQ_WB_GAIN_GB, wb.gbgain );
                ybfwIqModeSetDone(YBFW_SEN_ID_0, mode5k);
                break;
			#endif

			#ifdef APP_PTP_DPC_DATE_TIME
            case APP_PTP_DPC_DATE_TIME:
                printf("PTPDateTime=%s\n", (UINT8 *)&PTPDat[1]);
                UINT8 *str = (UINT8 *)&PTPDat[1];

                /*setdatetime*/
                static UINT8 testSwRtc = 0;
                ybfwTmx_t dtime;

                dtime.tmx_year = 0;
                for (i = 0 ; i < 4 ; i++)
                    dtime.tmx_year = dtime.tmx_year * 10 + str[i] - '0';
                dtime.tmx_year -= 1900;
                dtime.tmx_mon = (str[4] - '0') * 10 + str[5] - '0';
                dtime.tmx_mday = (str[6] - '0') * 10 + str[7] - '0';
                dtime.tmx_hour = (str[9] - '0') * 10 + str[10] - '0';
                dtime.tmx_min = (str[11] - '0') * 10 + str[12] - '0';
                dtime.tmx_sec = (str[13] - '0') * 10 + str[14] - '0';
                /*printf("\t->%d/%d/%d %d:%d:%d\n", dtime.tmx_year+1900, dtime.tmx_mon, dtime.tmx_mday, dtime.tmx_hour, dtime.tmx_min, dtime.tmx_sec);*/
                if (!testSwRtc) {
                    testSwRtc = 1;
                }

                appRealRtcSet(&dtime);

                break;
			#endif
    		case APP_PTP_DPC_DSC_OP_MODE:
    			break;
            case PIMA_DPC_ICATCH_VARIABLE:
                break;

			#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
			case APP_PTP_DPC_TIMELAPSE_INTERVAL:
				printf("\t-> %d\n", PTPDat[1]);
				if(appPtpIsTimelapseCapMode()){
					puiPara->PhotoTimeLapseInterval = UIIdx;
					if(puiPara->PhotoTimeLapseInterval != UI_TIME_LAPSE_PHOTO_INTERVAL_OFF)
		            	puiPara->PhotoBrustMode = UI_STILL_DRIVERMODE_OFF;
  	              puiPara->PhotoDelayTimer = UI_PHOTO_DELAY_TIMER_OFF;
				}
				else {
					puiPara->VideoTimeLapseInterval = UIIdx;
					if(puiPara->VideoTimeLapseInterval != UI_TIME_LAPSE_VIDEO_INTERVAL_OFF){
						puiPara->VideoSlowMotion = UI_VIDEO_SLOW_MOTION_OFF;
						puiPara->VideoFastMotion = UI_VIDEO_FAST_MOTION_OFF;
						pViewParam->videoSeamless = puiPara->Seamless = UI_VIDEO_SEAMLESS_OFF;
					}
				}

				break;

			#ifdef APP_PTP_DPC_TIMELAPSE_LENGTH
			case PIMA_DPC_TIMELAPSE_NUMBER:
				printf("\t-> %d\n", PTPDat[1]);
				if(appPtpIsTimelapseCapMode())
					puiPara->TimeLapsePhotoShootingTime = UIIdx;
				else
					puiPara->TimeLapseVideoShootingTime = UIIdx;
				break;
			#endif
			#endif

			#ifdef APP_RTP_DPC_FW_UPDATE_CHECK
            case APP_RTP_DPC_FW_UPDATE_CHECK:

                pDisk = appCurDiskInfoGet();
                FWBinSize = PTPDat[1]/1000;  //KByte

                printf("Free disk=%u,FWBinSiz=%u",pDisk->freeSz, FWBinSize);



                if(!IS_CARD_EXIST)
                {
                    printf("\nNO card\n");
                    appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_FWUPDATE_CHECK, FW_CHECK_NO_CARD, 0);
                }
                else if(pDisk->freeSz  < FWBinSize)
                {
                    printf("\nNO space\n");
                    appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_FWUPDATE_CHECK, FW_CHECK_NO_SPACE, 0);
                }
				#if 0   //for adapter
                else if(!IS_USB_IN)
                {
                    printf("\nNO adapte\n");
                    appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_FWUPDATE_CHECK, FW_CHECK_ADAPTER_FAIL, 0);
                }
				#endif
                else
                {
                    printf("\nOK\n");
                    appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_FWUPDATE_CHECK, FW_CHECK_OK, 0);
                }

                break;

            case APP_RTP_DPC_FW_UPDATE:


                if(PTPDat[1] == FW_DOWNLOAD_OK)
                {
                    ybfwAeModeSet(0, YBFW_AE_MODE_OFF);
                    ybfwAwbModeSet(0, YBFW_AWB_MODE_OFF);
                    ybfwModeSet(YBFW_MODE_STANDBY);
                    appFirmwareUpdate();
                }
                break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_REC_SLOW_MOTION
			case APP_PTP_DPC_VIDEO_REC_SLOW_MOTION :
				printf("\t-> %d\n", UIIdx);

				puiPara->VideoSlowMotion = UIIdx;
				if(puiPara->VideoSlowMotion!= UI_VIDEO_SLOW_MOTION_OFF)
				{
					#if PTP_TIME_LAPSE_EN
					puiPara->VideoTimeLapseInterval = UI_TIME_LAPSE_VIDEO_INTERVAL_OFF;
				//	appStill_SetTimeLapse(puiPara->TimeLapse);
					#endif
					puiPara->Seamless = UI_VIDEO_SEAMLESS_OFF;
          puiPara->VideoFastMotion = UI_VIDEO_FAST_MOTION_OFF;
				}
				//appVideo_SetSlowMotion();
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_REC_CAR_MODE
			case APP_PTP_DPC_VIDEO_REC_CAR_MODE :

				printf("\t-> %d\n", PTPDat[1]);
				puiPara->InvertMode =  PTPDat[1];
				//appVideo_SetInvertMode(puiPara->InvertMode);
				break;
			#endif

			#ifdef APP_PTP_DPC_POWER_SAVING_TIME
			case APP_PTP_DPC_POWER_SAVING_TIME:
				printf("\t Save Screen set-> %d\n", UIIdx);
				puiPara->ScreenSaverTime =  UIIdx;
				appScreenSaveCfg(FALSE);
				#if 0
				if(puiPara->ScreenSaverTime < UI_GENERAL_SCREEN_SAVER_OFF){
			        if(!g_pScreenSaver){
                        g_pScreenSaver = screensaver_new();
                    }
			        if(g_pScreenSaver){
			            g_pScreenSaver->init(g_pScreenSaver, puiPara->ScreenSaverTime);
			        }
			    }else{
					if(g_pScreenSaver){
                        g_pScreenSaver->init(g_pScreenSaver, UIIdx);
                        g_pScreenSaver->disable(g_pScreenSaver);
                        g_pScreenSaver = 0;
                    }
				}
				#endif
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_REC_AUTO_START
			case USB_PIMA_DPC_POWER_ON_AUTO_RECORD:
				puiPara->powerOnRecord = UIIdx;
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_REC_FAST_MOTION
			case USB_PIMA_DPC_FAST_MOTION:
				printf("\t Fast Motion set-> %d\n", UIIdx);
				puiPara->VideoFastMotion= UIIdx;
				if(puiPara->VideoFastMotion!= UI_VIDEO_FAST_MOTION_OFF)
				{
					#if PTP_TIME_LAPSE_EN
					puiPara->VideoTimeLapseInterval = UI_TIME_LAPSE_VIDEO_INTERVAL_OFF;
					#endif
					puiPara->Seamless = UI_VIDEO_SEAMLESS_OFF;
          puiPara->VideoSlowMotion = UI_VIDEO_SLOW_MOTION_OFF;
				}
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_REC_WNR
			case APP_PTP_DPC_VIDEO_REC_WNR:
				printf("\t Wind Noise Reduction-> %d\n", PTPDat[1]);
				puiPara->VideoWindNoiseReduction=  UIIdx;
				break;
			#endif

			#ifdef APP_PTP_DPC_POWER_OFF_TIME
			case APP_PTP_DPC_POWER_OFF_TIME:
				printf("\t Auto Power OFF set-> %d\n", PTPDat[1]);
				puiPara->SleepTime = UIIdx;
				appPowerSaveTimeOutReset();
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_FILE_LENGTH
			case APP_PTP_DPC_VIDEO_FILE_LENGTH:
				printf("\t Video File Length set-> %d\n", PTPDat[1]);
				pViewParam->videoSeamless =  UIIdx;
		        	puiPara->Seamless = pViewParam->videoSeamless ;
				appVideoSeamlessTimeSet();
				break;
			#endif

			#ifdef APP_PTP_DPC_VIDEO_EIS
			case APP_PTP_DPC_VIDEO_EIS:
				if(appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == TRUE) {
					printf("\t Video Image Stabilization set-> %d\n", PTPDat[1]);
					puiPara->VideImageStabilization = UIIdx;
				}
				else {
					puiPara->VideImageStabilization = UI_VIDEO_IMAGE_STABILIZATION_OFF;
				}
				break;
			#endif

			#ifdef APP_PTP_DPC_WIFI_MY_SSID
			case APP_PTP_DPC_WIFI_MY_SSID:
				printf("ptp set SSID =%s\n", (UINT8 *)&PTPDat[1]);
				strcpy((char *)ssid,(char *)&PTPDat[1]);
				appSSIDNameSet(ssid,gpassword);
				break;
			#endif

			#ifdef APP_PTP_DPC_WIFI_MY_PWD
			case APP_PTP_DPC_WIFI_MY_PWD:
				printf("ptp set password =%s\n", (UINT8 *)&PTPDat[1]);
				appSSIDNameGet(ssid);
				strcpy((char *)gpassword,(char *)&PTPDat[1]);
				appSSIDNameSet(ssid,gpassword);
				break;
			#endif

			case USB_PIMA_DPC_TIME_ZONE:
                printf("ptp TimeZone = %s\n", (UINT8 *)&PTPDat[1]);
                UINT8 *tzstr = (UINT8 *)&PTPDat[1];

                appTZ_t tztime;
                memset(&tztime, 0, sizeof(tztime));

                if(tzstr[0]=='+')
                    tztime.tzsigned = 1; /* + */
                else
                    tztime.tzsigned = 2; /* - */
                tztime.hour = (tzstr[1] - '0') * 10 + tzstr[2] - '0';
                tztime.min = (tzstr[3] - '0') * 10 + tzstr[4] - '0';

                appUtcTimeZoneSet(&tztime);
                /*printf("\t->PIMA timezone : %d%2d%2d\n",tztime.tzsigned,tztime.hour,tztime.min);*/

                break;

			#ifdef APP_PTP_DPC_WIFI_AP_STA_SWITCH
			case APP_PTP_DPC_WIFI_AP_STA_SWITCH:
				if(PTPDat[1] && appWifiInitModeGet() == WIFI_MODE_AP){
					appAutoAP2STASWITCHCB();
				}
				else if(!PTPDat[1] && appWifiInitModeGet() == WIFI_MODE_STATION){
					appAutoSTA2APSWITCHCB();
				}
				break;
			#endif

			#ifdef APP_PTP_DPC_WIFI_SSID_TO_JOIN
			case APP_PTP_DPC_WIFI_SSID_TO_JOIN:
				memset( pNetCusData.STASsid, 0, sizeof(pNetCusData.STASsid) );
				strncpy(pNetCusData.STASsid ,(const char *) &PTPDat[1],PTPDat[0]);
				printf("USB_PIMA_DCP_ESSID_USER_NAME str=%s\n",pNetCusData.STASsid);
				break;
			#endif

			#ifdef APP_PTP_DPC_WIFI_PWD_TO_JOIN
			case APP_PTP_DPC_WIFI_PWD_TO_JOIN:
				memset( pNetCusData.STAPW, 0, sizeof(pNetCusData.STAPW) );
				strncpy(pNetCusData.STAPW , (const char *)&PTPDat[1],PTPDat[0]);
				printf("USB_PIMA_DCP_ESSID_USER_PASSWORD str=%s\n",pNetCusData.STAPW);
				break;
			#endif

            default:
                break;
        }
    }

    #if PIMA_DEBUG
    printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
        pViewParam->wb, pViewParam->af, pViewParam->metering,
        gStillCB.flashMode, pViewParam->effect, pViewParam->drive,
        pViewParam->quality, gStillCB.ae.iso, pViewParam->ev,
        gStillCB.remainImg, pViewParam->size, pViewParam->edge);
    #endif

	ybfwFree(buf);
}


UINT32 appPtp_PIMA_File_Filter(UINT32 dir, const UINT8 *name, const UINT8 *pname)
{
	UINT32 ret = FAIL;

    if (dir) {
        ret = SUCCESS;
    }
    else if (name) {
        UINT8 *str = (UINT8 *)&name[strlen((const char *)name)-3];
        if (strcmp((void *)str, "JPG") == 0) {
            ret = SUCCESS;
        }
        else if (strcmp((void *)str, "AVI") == 0) {
            ret = SUCCESS;
        }
        else if (strcmp((void *)str, "MOV") == 0) {
            ret = SUCCESS;
        }
        else if (strcmp((void *)str, "MP4") == 0) {
            ret = SUCCESS;
        }
    }

	return ret;
}

static UINT32 appPtpEvVal2PtpVal(SINT16 evVal)
{
	UINT8 flag = 0;
	UINT32 intVal = 0;
	if(evVal < 0){
		flag = 1;
		evVal = -evVal;
	}
	intVal = (UINT16)evVal;
	return (flag << 31)|intVal;
}

UINT32 appPtpCfgRtpSizeGet(UINT32 idx)
{
#ifdef APP_RTP_DPC_RTP_PV_CFG0

	if (idx == -1)
		idx = pViewParam->videoSize;
	return PTPUI_Map_VidPVFormatSizeBitrateVid[idx].vidPvData;
#else
	return (PTP_H264 << 31 ) |(1280 << 19) | (720  << 8) | 40;
#endif
}

UINT32 appPtpCfgRtpFrameRateGet(void)
{
	#ifndef APP_RTP_DPC_RTP_PV_CFG1
	appVideoConfig_t *pVidData;
	appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);
	return pVidData->frameRate;
	#else
	return PTPUI_Map_VidPVFormatFps[pViewParam->videoSize].vidPvData;
	#endif
}

UINT32
appPtpOpModeGet(
	void
)
{
	return opModeMap[app_PTP_Get_DscOpMode(MODE_ACTIVE)];
}


void
appPtpCmd(
	UINT32 argc,
	char *arg[]
)
{
	UINT32 i;
	printf("PTP argc=%d, arg=", argc);
	for (i = 0 ; i < argc ; i++) {
		printf("%s\n", arg[i]);
	}
	if (strcmp(arg[0], "get") == 0) {
		if (argc > 1) {
			UINT32 devprop = strtoul(arg[1], NULL, 0);
			UINT32 devpropVal[64];
			printf("device property=0x%x, ", devprop);
			appStill_PIMA_Get_cb(devprop, devpropVal);
			printf("type=%d r/w=%d, content=", devpropVal[1], devpropVal[2]);
			for (i = 3 ; i < devpropVal[0] ; i++) {
				printf("%x ", devpropVal[i]);
			}
			printf("\n");
		}
	}
	else if (strcmp(arg[0], "apply") == 0) {
		if (argc > 4) {
			UINT32 devprop = strtoul(arg[1], NULL, 0);
			UINT32 devpropVal[64];
			printf("device property=0x%x, set: ", devprop);
			devpropVal[0] = argc - 2;
			for (i = 1 ; i < argc - 2 ; i++) {
				devpropVal[i] = strtoul(arg[i+1], NULL, 0);
				if (i == 1)	printf("type=%d ", devpropVal[i]);
				else if (i == 2) printf("r/w=%d, content=", devpropVal[i]);
				else printf("%x ", devpropVal[i]);
			}
			printf("\n");
			appStill_PIMA_Apply_cb(devprop, devpropVal);
		}
	}
	else if (strcmp(arg[0], "cap") == 0) {
		if (argc > 1) {
			UINT32 trig = strtoul(arg[1], NULL, 0);
			if (app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_CAMERA) {
				if (trig)
					appStillCaptureTrigger(YBFW_PIMA_CAPTURE_TRIG_STILL);
				else
					appStillCaptureAbort(YBFW_PIMA_CAPTURE_TRIG_VIDEO);
			}
			else if (app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_VIDEO_OFF) {
				if (trig) {
					app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_ON);
					appPtpDscModeChange();
				}
			}
		}
	}
}

