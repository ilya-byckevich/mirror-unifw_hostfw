/**************************************************************************
 *
 *       Copyright (c) 20012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#define HOST_DBG 0

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "ybfw_global_api.h"
#include "ybfw_disk_api.h"
#include "ybfw_cdsp_api.h"
#include "ybfw_ptp_api.h"
#include "ybfw_dbg_api.h"
#include "ybfw_media_api.h"

#include "app_battery.h"
#include "app_icon_def.h"
#include "app_still.h"
#include "app_video.h"
#include "app_view_param.h"
#include "app_zoom_api.h"
#include "app_General.h"
#include "app_wifi_utility.h"
#include "app_wifi_connection.h"
#include "app_awbalg_api.h"
#include "app_setup_lib.h"
#include "../../app_ver.h"


#include "app_ptp.h"
#include "app_sensor.h"
#include "app_sys_cfg.h"
#include "app_util.h"

#include "app_ui_para.h"
#include "app_screen_saver.h"


/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#define PTP_WIFIOP_FUNC PTP_WIFI_FUNC_PREVIEW

static UINT8 ptpWiFiOpFunc = PTP_WIFIOP_FUNC ;
static PTPDscOpMode_e ptpDscOpMode = PTP_DSCOP_MODE_VIDEO_OFF;/* DSC power on : default */
static PTPDscOpMode_e ptpActOpMode = PTP_DSCOP_MODE_VIDEO_OFF;
PTPDscOpMode_e ptpPrevOpMode = PTP_DSCOP_MODE_VIDEO_OFF;


#if PTP_SUPPORT_CNT_VIDEOREC
static BOOL ptpContRecMode = TRUE;
#else
static BOOL ptpContRecMode = FALSE;
#endif


UINT8 *strFWPath;

extern struct NetCustomerDataDef pNetCusData;

static void appPtpPhotoTimelapseIntUISave(void);
static void appPtpPhotoTimelapseUiOff(void);

static ptpEventQueue_s sptpEventQueue={0};
UINT32 memoryFullEvtFlg=0;


static void app_PTP_EventQ_process(ptpEventQueue_s *ptpEventQueue);
static void app_PTP_Event2Queue(ptpEventQueue_s *ptpEventQueue,UINT32 iCatchEvt, UINT32 param1, UINT32 param2, UINT32 param3);
static void app_PTP_EventQ_clear(ptpEventQueue_s *ptpEventQueue);

void appStill_PIMA_Send_Resp(UINT32 iCatchResp, UINT32 param1, UINT32 param2)
{
	ybfwPtpiCatchCtl(YBFW_PTP_ICTL_PIMA_CUSTOM_RESP, iCatchResp, param1, param2, 0);

}

void appStill_PIMA_Send_iCatch_Event(UINT32 iCatchEvt, UINT32 param1, UINT32 param2)
{
	#if PTP_EVT_SEND_ENABLE
	if (appPtpStatGet()){
		printf("Host iCatch event sent : 0x%x,0x%x,0x%x\n",iCatchEvt,param1,param2);
	   	appStill_PIMA_Send_Event(PTP_EC_ICATCH_HOST_EVENT, iCatchEvt, param1, param2);
	}else{
		printf("WIFI Disconnect,Host iCatch event don't sent : 0x%x,0x%x,0x%x\n",iCatchEvt,param1,param2);
	}
	#else
	return;
	#endif
}

void appStill_PIMA_Send_Event(PTPEventCode_e EventCode, UINT32 param1, UINT32 param2, UINT32 param3)
{
	if(appWiFiConnection_UtilityStateGet() > WIFI_UTILITY_CLOSE) {
		#if PTP_EVT_SEND_ENABLE

		#if PTP_EVT_QUEUE_ENABLE
		app_PTP_Event2Queue(&sptpEventQueue,EventCode,param1,param2,param3);
		#else
		printf("Host event sent : 0x%x,0x%x,0x%x,0x%x\n",EventCode,param1,param2,param3);
		if (EventCode == PTP_EC_OBJECT_ADDED || EventCode == PTP_EC_OBJECT_REMOVED)
		{
			char *fname = (char *)param1;
			while ((fname = strchr(fname, '/')) != NULL)
				fname[0] = '\\';
			ybfwProfLogPrintf(0, "#P#PTP#N#%s:%s", (EventCode==PTP_EC_OBJECT_ADDED)?"add":"del", param1);
		}
		ybfwPtpiCatchCtl(YBFW_PTP_ICTL_PIMA_EVENT_SEND, param1,param2,param3,0);
		#endif

		#else
		printf("PTP_EVT_SEND_ENABLE==0\n");;
		#endif
	}
}

UINT32 appPtpCustomOperation(UINT32 op, UINT32 par1, UINT32 par2)
{

	ybfwProfLogPrintf(1,"Got iCatch Op:%x, %d, %d\n", op, par1, par2);
    switch (op)
    {
		case PTP_CUSOPCODE_S1_PRESS:
            /*ybfwPtpiCatchCtl(YBFW_PTP_ICTL_PIMA_EVENT_SEND, 0, 0, 0, 0);*/
          	printf("PtpCustomOperation PTP_CUSOPCODE_S1_PRESS\n");
			break;

		case PTP_CUSOPCODE_QUEUE_EVENT_POLLING:
			ybfwProfLogPrintf(1," Mobile APP is checking host event!\n");
			#if PTP_EVT_SEND_ENABLE&&PTP_EVT_QUEUE_ENABLE
			if(sptpEventQueue.TotalQNum){
				appStill_PIMA_Send_Resp(PTP_RESP_QUEUE_EVENT_OK, sptpEventQueue.TotalQNum, 0);
				app_PTP_EventQ_process(&sptpEventQueue);
			}
			else
			{
				appStill_PIMA_Send_Resp(PTP_RESP_QUEUE_EVENT_EMPTY,0,0);
			}
			#else
			printf("ptp evt send disabled!");
			#endif
			break;

		case PTP_CUSOPCODE_DZOOM_TELE_PRESS:
			printf("custom operation:0x%x\n",op);
			ybfwProfLogPrintf(0,"dzoom:tele");
			/* stop streaming before zoom */
			ybfwHostMsgSend(APP_UI_MSG_WIFI_DZOOM_TELE, 0, 0, 0);
			/* start streaming after zoom */
			appStill_PIMA_Send_Resp(PTP_RESP_OPERATION_OK,0,0);
			printf("dzoom:tele (%d)\n",appDZoomGetRatio() / 10);
			break;

		case PTP_CUSOPCODE_DZOOM_WIDE_PRESS:
			printf("custom operation:0x%x\n",op);
			ybfwProfLogPrintf(0,"dzoom:wide");
			/* stop streaming before zoom */
			ybfwHostMsgSend(APP_UI_MSG_WIFI_DZOOM_WIDE, 0, 0, 0);
			/* start streaming after zoom */
			appStill_PIMA_Send_Resp(PTP_RESP_OPERATION_OK,0,0);
			printf("dzoom:wide (%d)\n",appDZoomGetRatio() / 10);
			break;

		case PTP_CUSOPCODE_OZOOM_TELE_PRESS:
			printf("custom operation:0x%x\n",op);
			appStill_PIMA_Send_Resp(PTP_RESP_OPERATION_OK,0,0);
			break;

		case PTP_CUSOPCODE_OZOOM_WIDE_PRESS:
			printf("custom operation:0x%x\n",op);
			appStill_PIMA_Send_Resp(PTP_RESP_OPERATION_OK,0,0);
			break;

		default:
			printf("PtpCustomOperation property 0x%x isn't supported.\n", op);
			break;

    }

	return SUCCESS;
}


void appPtpDscModeChange(void)
{
	UINT32 curMode;
	appVideoConfig_t *pVidData;
	BOOL start_record = FALSE, stop_record = FALSE;
	UINT32 done = 0;

	ybfwModeGet(&curMode);

	 /* device property get */
	appVideoSizeGet(pViewParam->videoSize, 0, &pVidData);

	if (app_PTP_Get_DscOpMode(MODE_PREV) == PTP_DSCOP_MODE_SHARE) {
		if (app_PTP_Get_DscOpMode(MODE_PREV) != app_PTP_Get_DscOpMode(MODE_ACTIVE)) {
			/* wait for rtp pb closed.*/
			while (appCustMsrcConnGet()) {
				printf("Waiting RTP PB to be closed\n");
				ybfwOsThreadSleep(100);
			}
		}
	}

	switch(app_PTP_Get_DscOpMode(MODE_ACTIVE)){
		#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
		case PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF:
			printf("[DSC OP]  TIMELAPSE STILL mode OFF!\n");
			ybfwHostMsgSend(APP_KEY_SNAP, APP_KEY_SNAP_ABORT, 0, 0);
			break;

		case  PTP_DSCOP_MODE_TIMELAPSE_STILL:
			printf("[DSC OP] TIMELAPSE_STILL mode!\n");
			appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_VIDREC_TIME_CHANGE, 0, 0);
			ybfwHostMsgSend(APP_KEY_SNAP, APP_KEY_SNAP_START, 0, 0);
			break;
		#endif

		case PTP_DSCOP_MODE_CAMERA:
			printf("[DSC OP] CAMERA mode!\n");
			#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
			appUiParaGet()->PhotoTimeLapseInterval = UI_TIME_LAPSE_PHOTO_INTERVAL_OFF;
			ybfwHostMsgSend(APP_KEY_SNAP, APP_KEY_SNAP_ABORT, 0, 0);
			#endif

			break;

		#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
		case PTP_DSCOP_MODE_TIMELAPSE_VIDEO_OFF:
			printf("[DSC OP] VIDEO TIMELAPSE mode OFF!\n");
		#endif

		case PTP_DSCOP_MODE_VIDEO_OFF:
			printf("[DSC OP] VIDEO mode OFF!\n");
			stop_record = TRUE;
			appPowerSavingEnableSet(TRUE);
			appVideoFileRecord(0);
			break;

		case PTP_DSCOP_MODE_TIMELAPSE_VIDEO:
			printf("[DSC OP] TIMELAPSE VIDEO mode ON!\n");

		case PTP_DSCOP_MODE_VIDEO_ON:
			printf("[DSC OP] VIDEO mode ON!\n");
			#if YBFW_CDFS_OPT
			appCdfsFileFolderSet(CDFS_FILE_FOLDER_VIDEO);
			appCdfsFolderInit(CDFS_FILE_FOLDER_VIDEO);
			#endif

			if(IS_CARD_EXIST)
			{
				if(!(appCardSpeedGet(YBFW_DRIVE_SD)))
				{
					appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_SDCARD_ERROR, 0, 0);
				}
				else{
					if (appVideoFileRecord(1) == SUCCESS)
					start_record = TRUE;
					appPowerSavingEnableSet(FALSE);
				}
			}
			if (start_record != TRUE) {
				app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_OFF);
				appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_OPMODE_CHANGE,PTP_DSCOP_MODE_VIDEO_OFF,0);
			}
			break;

		case PTP_DSCOP_MODE_SHARE:
			printf("[DSC OP] SHARE mode!\n");
			if(appWiFiFileListUpdateFlagGet()) {
				ybfwPtpInfoUpdate();
				appWiFiFileListUpdateFlagSet(FALSE);
			}
			if(appRtpStreamStateGet()) {
				appStreamClose();
			}
			appVideoFileRecord(0);	/* make sure the video record is stopped. */
			if(curMode!=YBFW_MODE_STANDBY) {
				appModeSet(YBFW_MODE_STANDBY);
			}
			/* to prevent APP crashed in share mode, restart APP should be camera mode */
			if(app_PTP_ContRecMode_Get()){
				app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_CAMERA);
			}
			done = 1;
			break;

		case PTP_DSCOP_MODE_APP:
			printf("[DSC OP] APP mode!\n");
			if(appRtpStreamStateGet()) {
				if(app_PTP_Get_DscOpMode(MODE_PREV) == PTP_DSCOP_MODE_VIDEO_ON)
				{
					app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_ON);
				}
				else if(curMode==YBFW_MODE_VIDEO_PREVIEW && app_PTP_Get_DscOpMode(MODE_PREV)==PTP_DSCOP_MODE_CAMERA)
				{
					app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_CAMERA);
				}
				else if(curMode==YBFW_MODE_VIDEO_PREVIEW && app_PTP_Get_DscOpMode(MODE_PREV)==PTP_DSCOP_MODE_VIDEO_OFF)
				{
					app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_OFF);
				}
			}
			done = 1;
			break;

		default:
			HOST_PROF_LOG_PRINT(LEVEL_INFO, "WiFi: Line(%d), [PTP] you got the wrong camera mode:%d", __LINE__, app_PTP_Get_DscOpMode(MODE_ACTIVE));
			done = 1;
			break;
	}

	uiPara_t* puiPara = appUiParaGet();
	if (appPtpOpModeGet() == YBFW_MODE_STILL_PREVIEW)
	{
		appStill_SetExposure(puiPara->PhotoAEMode);
	}
	else if (appPtpOpModeGet() == YBFW_MODE_VIDEO_PREVIEW)
	{
		appStill_SetExposure(puiPara->VideoAEMode);
	}

	if (done == 0) {
		printf("****** RTP Streaming setting ******\n");
		printf("**	W(%d)\n**  H(%d)\n**  Video(%s)\n", pVidData->w, pVidData->h
			, start_record ? "rec+": (stop_record ? "rec-" : "n/a"));
		printf("***********************************\n");

		HOST_PROF_LOG_PRINT(LEVEL_INFO, "WiFi: Line(%d), StreamStateGet(%d), start_record(%d), stop_record(%d), curMode(0x%x)", __LINE__,  appRtpStreamStateGet() ,start_record, stop_record, curMode);
		 /* device property get */
		if((app_PTP_Get_DscOpMode(MODE_ACTIVE)==PTP_DSCOP_MODE_VIDEO_OFF
			||app_PTP_Get_DscOpMode(MODE_ACTIVE)==PTP_DSCOP_MODE_VIDEO_ON
			||app_PTP_Get_DscOpMode(MODE_ACTIVE)==PTP_DSCOP_MODE_CAMERA)
			&&!appRtpStreamStateGet())
		{
			appWiFiStartConnection(RTP_STREM);
		}
	}
}

#define QUICK_SORT_DBG  0

static SINT32
fileNameCmp(
    UINT8 *fname0,
    UINT8 *fname1,
    UINT32 len
)
{
    SINT32 ret = 0;

    while (len--) {
        ret = *fname0++ - *fname1++;
        if (ret) {
            break;
        }
    }

    return ret;
}

static UINT32
quickSort(
    UINT8 **fnameList,
    UINT32 *handleList,
    UINT32 cnt
)
{
    UINT32 *que = ybfwMallocCache(cnt*2);
    UINT32 queIdx = 0;
    UINT32 lo, hi;
    UINT8 *pivot;
    UINT32 i, j;
    UINT32 tmp;
    #define SWAP(x, y) \
    { \
        tmp = (UINT32)fnameList[x]; \
        fnameList[x] = fnameList[y]; \
        fnameList[y] = (UINT8 *)tmp; \
        tmp = handleList[x]; \
        handleList[x] = handleList[y]; \
        handleList[y] = tmp; \
    }

    ybfwProfLogPrintf(0, "#S#PTP#N#%s,%d", __func__, cnt);
    lo = 0;
    hi = cnt - 1;
    do {
        /*-----------------------------------*/
        /* partition between lo and hi                      */
        j = lo;	/* to swap */
        pivot = fnameList[hi];
        UINT32 len = strlen((char *)pivot);

		#if QUICK_SORT_DBG
        ybfwProfLogPrintf(0, "%d~%d, %s", lo, hi, pivot);
		#endif
        for (i = lo; i < hi; i++) {
            if (fileNameCmp(fnameList[i], pivot, len) > 0) {	/* [i] is newer than pivot */
                if (i != j) {
                    SWAP(i, j);
                }
                j++;
            }
        }
        SWAP(j, hi);
        /* now, lo ~ j-1 are all newer than j+1 ~ hi */
        /*-----------------------------------*/

        if (j == lo) {	/* hi is new */
            lo++;
            if (lo == hi && queIdx) {
                queIdx--;
                hi = que[queIdx];
                queIdx--;
                lo = que[queIdx];
            }
        }
        else if (j == hi) {	/* hi is old */
            hi--;
            if (lo == hi && queIdx) {
                queIdx--;
                hi = que[queIdx];
                queIdx--;
                lo = que[queIdx];
            }
        }
        else if (j - 1 == lo) {
            if (j + 1 != hi) {
                lo = j + 1;
            }
            else if (queIdx) {
                queIdx--;
                hi = que[queIdx];
                queIdx--;
                lo = que[queIdx];
            }
        }
        else {
            if (j + 1 != hi) {
                que[queIdx++] = j+1;
                que[queIdx++] = hi;
            }
            hi = j - 1;
        }
    } while (lo != hi || queIdx);

    ybfwFree(que);

    ybfwProfLogPrintf(0, "#E#PTP#N#%s", __func__);

    return SUCCESS;
}

UINT32
appPtp_MTP_File_Sort(
    UINT8 **fnameList,
    UINT32 *handleList,
    UINT32 count,
    UINT32 reserved
)
{
	#if QUICK_SORT_DBG
	UINT32 i;
    printf("before sorting:\n");
    for (i = 0; i < count; i++) {
        printf("[%d]%s, handle=%x\n", i, fnameList[i], handleList[i]);
    }
	#endif

    UINT32 t = ybfwMsTimeGet();

    if (count > 1) {
        quickSort(fnameList, handleList, count);
    }

    printf("q sort %d took %dms\n", count, ybfwMsTimeGet()-t);

	#if QUICK_SORT_DBG
    printf("after sorting:\n");
    for (i = 0; i < count; i++) {
        printf("[%d]%s, handle=%x\n", i, fnameList[i], handleList[i]);
    }
	#endif

    return SUCCESS;
}


void appStill_PIMA_DPC_Update(UINT8 nMenuItem)
{
	pimaDevicePropertyCode_t pimaDpcode = 0xFFFF;

	switch(nMenuItem)
	{
	case VIEWMENU_CHANGE:
		break;
	case VIEWMENU_STILL_RESOLUTION:
		#ifdef APP_PTP_DPC_STILL_SNAP_SIZE
		pimaDpcode = APP_PTP_DPC_STILL_SNAP_SIZE;
		#endif
		break;
	case VIEWMENU_STILL_QUALITY:
		#ifdef APP_PTP_DPC_STILL_SNAP_QUALITY
		pimaDpcode = APP_PTP_DPC_STILL_SNAP_QUALITY;
		#endif
		break;
	case VIEWMENU_DATESTAMP:
		#ifdef APP_PTP_DPC_DATESTAMP
		pimaDpcode = APP_PTP_DPC_DATESTAMP;
		#endif
		break;
	case VIEWMENU_VIDEO_SIZE:
		#ifdef APP_PTP_DPC_VIDEO_SIZE
		pimaDpcode = APP_PTP_DPC_VIDEO_SIZE;
		#endif
		break;
	case VIEWMENU_VIDEO_QUALITY:
	case VIEWMENU_VIDEO_SEAMLESS:
	case VIEWMENU_VIDEO_DATESTAMP:
		printf("Responder doesn't support this property!\n");
	default:
		break;
	}
	printf("PIMA DPC update:0x%x!\n",pimaDpcode);
	if(pimaDpcode!=0xFFFF)
		appStill_PIMA_Send_Event(PIMA_EC_DEVICE_PROP_CHANGED, pimaDpcode, 0, 0);
}

void app_PTP_Set_DscOpMode(PTPDscOpMode_e dscOpMode )
{
	ptpPrevOpMode = ptpDscOpMode;
	ptpActOpMode = dscOpMode;
	ptpDscOpMode = ptpActOpMode;
	ybfwProfLogPrintf(0, "#P#WIFI#N#OpMode0x%x", ptpActOpMode);
	printf("DSC Op mode set:0x%x,Prev Op mode:0x%x\n",ptpDscOpMode,ptpPrevOpMode);
}


UINT8 app_PTP_Get_DscOpMode(DscOpModeState_e opModeState)
{
	if(opModeState==MODE_PREV){
		DBG_PRINT("Prev Op mode get:0x%x\n",ptpPrevOpMode);
		return ptpPrevOpMode;
	}
	else if(opModeState==MODE_ACTIVE){
		DBG_PRINT("Active Op mode get:0x%x\n",ptpActOpMode);
		return ptpActOpMode;
	}
	else
		return ptpDscOpMode;
}


UINT8 app_PTP_ContRecMode_Get()
{
	return ptpContRecMode;
}

UINT8 app_PTP_Get_WiFiOpFunc()
{
	return ptpWiFiOpFunc;
}


static void app_PTP_Event2Queue(ptpEventQueue_s *ptpEventQueue,UINT32 EventCode, UINT32 param1, UINT32 param2, UINT32 param3)
{
    /* Check Queue is FULL ? */
    UINT32 WaitRetry = 6;

	while (ptpEventQueue->TotalQNum >= PTP_EVENT_MAX_Q_NUM)	{
        if ( WaitRetry-- ) {
            ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 100);
        }
		else {
	        HLPrintf1("[EvtQ] ERROR : total queue number exceed the MAX ADD 0x%x\n" , EventCode);
    	    appWiFiFileListUpdateFlagSet(TRUE);
			return;
		}
	}

	if (!ptpEventQueue->mutex) {
		ybfwOsMutexCreate(&ptpEventQueue->mutex, "hostPtpMut", TX_INHERIT);
		ybfwProfLogPrintf(0, "hostPtpMut=%x", ptpEventQueue->mutex);
	}
	if (ptpEventQueue->mutex)	ybfwOsMutexGet(&ptpEventQueue->mutex, TX_WAIT_FOREVER);

	ptpEventQueue->TotalQNum++;
	ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][0] = EventCode;

	switch (EventCode) {
		case PTP_EC_OBJECT_ADDED:
		case PTP_EC_OBJECT_REMOVED:
		case PTP_EC_OBJECT_INFO_CHANGED:
		case PTP_EC_REQUEST_OBJECT_TRANSFER:
			/* par1 is full path */
			ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][1] = (UINT32)ybfwMallocCache(strlen((char *)param1)+1);
			char *fname = (char *)ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][1];
            strcpy(fname, (const char *)param1);
			while ((fname = strchr(fname, '/')) != NULL)
				fname[0] = '\\';
			if (EventCode==PTP_EC_OBJECT_ADDED || EventCode==PTP_EC_OBJECT_REMOVED)
				ybfwProfLogPrintf(0, "PTP %s file: %s",
						(EventCode==PTP_EC_OBJECT_ADDED)?"add":"del",
						(char *)ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][1]);
			break;
		default:
			ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][1] = param1;
			ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][2] = param2;
			ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][3] = param3;
			break;
	}

	printf("[EvtQ] event 0x%x queue,total Q : %d\n",
		ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][0], ptpEventQueue->TotalQNum);
	ybfwProfLogPrintf(0, "#P#PTP#N#evtq%d:%x,%x", ptpEventQueue->EventQIndex
										, ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][0]
										, ptpEventQueue->EventQCode[ptpEventQueue->EventQIndex][1]);
	CIRCULAR_INCREASE(ptpEventQueue->EventQIndex,0,PTP_EVENT_MAX_Q_NUM-1); /* queue : [0] [1] ........ [8] [9] */
	if (ptpEventQueue->mutex)	ybfwOsMutexPut(&ptpEventQueue->mutex);
}

static void app_PTP_EventQ_process(ptpEventQueue_s *ptpEventQueue)
{
	UINT8 i = 0, processedQNum = 0;
	UINT8 curQindex;
	UINT8 totalQNum;

	/*printf("curQindex:%d,totalQNum:%d\n",curQindex,totalQNum);*/

	if (ptpEventQueue->mutex)	ybfwOsMutexGet(&ptpEventQueue->mutex, TX_WAIT_FOREVER);
	curQindex = ptpEventQueue->CurQ1stIndex;
	totalQNum = ptpEventQueue->TotalQNum;
	processedQNum = totalQNum;

	if(totalQNum){

		printf("[EvtQ] total Queue b4 process:%d\n", totalQNum);

		for(i=curQindex;i < PTP_EVENT_MAX_Q_NUM;i++) /* check 1st Q NO. in the ring buffer */
		{
			if(ptpEventQueue->EventQCode[i][0]!= 0)
			{
				curQindex = i;
				printf("[EvtQ] current Queue no:%d\n", curQindex);
				break;
			}
		}

		for(i = curQindex ;i < totalQNum + curQindex;i++)
		{
			UINT32 ret = ybfwPtpiCatchCtl(YBFW_PTP_ICTL_PIMA_EVENT_SEND,
									ptpEventQueue->EventQCode[i][0],
									ptpEventQueue->EventQCode[i][1],
									ptpEventQueue->EventQCode[i][2],
									ptpEventQueue->EventQCode[i][3]);

			ybfwProfLogPrintf(0, "#P#PTP#N#evts%d:%x,%X-%d", i
								, ptpEventQueue->EventQCode[i][0]
								, ptpEventQueue->EventQCode[i][1]
								, ret);
			printf("[EvtQ] event code = 0x%x,%d,%d\n",ptpEventQueue->EventQCode[i][0],i, ret);

			switch (ptpEventQueue->EventQCode[i][0]) {
				case PTP_EC_OBJECT_ADDED:
				case PTP_EC_OBJECT_REMOVED:
				case PTP_EC_OBJECT_INFO_CHANGED:
				case PTP_EC_REQUEST_OBJECT_TRANSFER:
					/* par1 is full path */
					if (ret != SUCCESS)
						printf("PTP handled %s failed\n", (char *)ptpEventQueue->EventQCode[i][1]);
					ybfwFree((void *)ptpEventQueue->EventQCode[i][1]);
				default:
					break;
			}
			memset(ptpEventQueue->EventQCode[i], 0, 4*sizeof(UINT32)); /* clear the processed event Q */
			CIRCULAR_DECREASE(processedQNum,0,PTP_EVENT_MAX_Q_NUM);
			/*processedQindex = i;*/
			/*printf("[%d,%d]\n",processedQNum,processedQindex);*/
		}

		ptpEventQueue->EventQIndex = 0;
		ptpEventQueue->CurQ1stIndex = 0 ;
		ptpEventQueue->TotalQNum = processedQNum;

		/*printf("[EvtQ] QIndex after process:%d\n", ptpEventQueue->CurQ1stIndex);*/
		printf("[EvtQ] total Queue after process:%d\n", ptpEventQueue->TotalQNum);
	}
	else{
		printf("[EvtQ] no event queue !\n");
	}
	if (ptpEventQueue->mutex)	ybfwOsMutexPut(&ptpEventQueue->mutex);
}
static void app_PTP_EventQ_clear(ptpEventQueue_s *ptpEventQueue)
{
	UINT8 i;
	for(i=0;i < PTP_EVENT_MAX_Q_NUM;i++){
		memset(ptpEventQueue->EventQCode[i], 0, 4*sizeof(UINT32));
	}
	ptpEventQueue->TotalQNum = 0;
	if (ptpEventQueue->mutex)	ybfwOsMutexDelete(&ptpEventQueue->mutex);
	ptpEventQueue->mutex = 0;
	printf("[EvtQ] clear event queue !\n");
}

UINT32 app_PTP_EventQNum_Get(void)
{
	return sptpEventQueue.TotalQNum;
}

void app_PTP_clear_EventQ(void)
{
	app_PTP_EventQ_clear(&sptpEventQueue);
}


void appPtpFileAddNotify(UINT8 *fname)
{
	appStill_PIMA_Send_Event(PTP_EC_OBJECT_ADDED, (UINT32)fname, 0, 0);
}

void appPtpFileDelNotify(UINT8 *fname)
{
	appStill_PIMA_Send_Event(PTP_EC_OBJECT_REMOVED, (UINT32)fname, 0, 0);
}

UINT32 appPtpDelFileCb(UINT8 *ppayload, UINT8 *pdata, UINT32 n)
{
	UINT32 ret = SUCCESS;

	printf("[PIMA]__delete file cb__\n");

	ret = ybfwDcfFsInit(appActiveDiskGet(),0,0);
	if(ret!=SUCCESS)
	{
		printf("[ERR] %s(%d)\n",__FUNCTION__,__LINE__);
		return ret;
	}

	ret = ybfwDcfFsActive(appActiveDiskGet());

	if(ret!=SUCCESS)
	{
		printf("[ERR] %s(%d)\n",__FUNCTION__,__LINE__);
		return ret;
	}

	if(IS_CARD_EXIST)
		appStill_PIMA_Send_Event(PTP_EC_STORAGE_INFO_CHANGED, /*YBFW_DRIVE_SD*/2, 0, 0);
	else
		appStill_PIMA_Send_Event(PTP_EC_STORAGE_INFO_CHANGED, /*YBFW_DRIVE_NAND*/1, 0, 0);

	return ret;
}

#ifdef APP_PTP_DPC_TIMELAPSE_INTERVAL
UINT8 appPtpIsTimelapseCapMode(void){
	if(app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_TIMELAPSE_STILL||
		app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF)
		return TRUE;
	else
		return FALSE;
}

static void appPtpPhotoTimelapseUiOff(void){

	uiPara_t* puiPara = appUiParaGet();
	puiPara->PhotoTimeLapseInterval = UI_TIME_LAPSE_VIDEO_INTERVAL_OFF;
}
#endif

#if (ICAT_WIFI || ICAT_ETHERNET)
/*-------------------------------------------------------------------------
 *  Function Name : appStillCaptureTrigger
 *  Description :
 *------------------------------------------------------------------------*/
UINT32 appStillCaptureTrigger(ybfwPtpTrigCaptureType_t cap_type)
{
	ybfwHostMsgSend(APP_KEY_SNAP, APP_KEY_SNAP_START, 0, 0);
    return SUCCESS;
}

/*-------------------------------------------------------------------------
 *  Function Name : appStillCaptureAbort
 *  Description :
 *------------------------------------------------------------------------*/
UINT32 appStillCaptureAbort(ybfwPtpTrigCaptureType_t cap_type)
{
	ybfwHostMsgSend(APP_KEY_SNAP, APP_KEY_SNAP_ABORT, 0, 0);
    return SUCCESS;
}
#endif

