/**************************************************************************
 *
 *       Copyright (c) 2014-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 *  Author:
 *
 **************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
//#include "ykn_bfw_api/ybfw_fs_api.h"
//#include "ykn_bfw_api/ybfw_sensor_api.h"

/*new rtsp server used*/
#include <ndk_stream_api.h>
#include <ndk_streaming.h>
#include <streamsrc_def.h>
#include "app_wifi_utility.h"

#include "app_ptp.h"

/*====RTP livestreaming start====*/
#define RTP_VLC_DEBUG 0
#define RTP_PTS_DEBUG 0

#if RTP_VLC_DEBUG
#define RtspLiveProfPrint(pattern, arg...)	ybfwProfLogPrintf(0, pattern, ##arg);
#else
#define RtspLiveProfPrint(pattern, arg...)
#endif

#if RTP_PTS_DEBUG
#define RtspPtsProfPrint(pattern, arg...)	ybfwProfLogPrintf(0, pattern, ##arg);
#else
#define RtspPtsProfPrint(pattern, arg...)
#endif

#define DBG_AUDIO_FILE	0
#define DBG_VIDEO_FILE 0

#define RTSP_YUV_IN_DEBUG 0

#if DBG_AUDIO_FILE || DBG_VIDEO_FILE
#include <ybfw_fs_api.h>
#endif

#define H264_PDR  0x1
#define H264_IDR  0x5
#define H264_SPS  0x7
#define H264_PPS  0x8

#define H265_TRAILN 0x0
#define H265_TSAN 0x02
#define H265_IDR  0x26
#define H265_VPS  0x40
#define H265_SPS  0x42
#define H265_PPS  0x44

#define MAX_YUV_BUF_NUM 2

#define RTP_DEBUG_COUNT(CAT) { \
	UINT32 t_ms = ybfwMsTimeGet();\
	debugInfo.CAT##Cnt++; \
	if (debugInfo.CAT##T0 == 0) \
		debugInfo.CAT##T0 = t_ms; \
	debugInfo.CAT##T1 = t_ms; \
}

typedef struct RtpStream_s {
	NDKStreamSrc ss;
	void *send_priv;
	NDKStreamSrcBufHandler send;
	UINT32 streamHandler;
	ybfwMediaRecCfg_t *pRtpCfg;
	UINT8 *yuv[MAX_YUV_BUF_NUM];/*for encode yuv*/
	UINT8 *sps;
	UINT8 *pps;
	UINT8 *vps;
	UINT32 hdr_len;
	UINT32 sps_len;
	UINT32 pps_len;
	UINT32 vps_len;
	UINT64 preFrmPts;/*pre frame pts*/
	UINT32 yuvCnt;
	UINT32 frameRate;
	UINT32 frameCnt;
	UINT32 startTime;
	UINT32 start;/*when open rtp, clean que*/
#if DBG_AUDIO_FILE
	UINT32 audFd;
#endif
#if DBG_VIDEO_FILE
	UINT32 vidFd;
#endif
} RtpStream_t;

typedef struct CusNdkBuf_s {
	UINT32 type; /* 0: video 1: audio */
	UINT8 *pbuf;
	UINT8 *data;
	UINT32 size;
	UINT32 key;
	UINT64 stamp;
	int refCnt;
} CusNdkBuf_s_t;

typedef struct RtpQueue_s {
	UINT8 *pbuf;
	UINT32 bufW;
	UINT32 bufH;
	UINT32 fmt;
	UINT64 pts;
	UINT32 fid;
} RtpQueue_t;

struct rtpdebug_s {
	UINT32 yuvCnt;
	UINT32 yuvT0;
	UINT32 yuvT1;
	UINT32 vlcCnt;
	UINT32 vlcT0;
	UINT32 vlcT1;
	UINT32 pushCnt;
	UINT32 pushT0;
	UINT32 pushT1;
};

YBFW_QUEUE RtpEncYuvQue;
UINT32 RtpConnectCnt = 0;
static UINT32 modeOpened = 0;

static struct rtpdebug_s debugInfo;	/* for debug only. Do not use it. */

static void appCustMsrcSwapAudData16(UINT8 *dst, UINT8 *src, UINT32 length);



UINT32 appRtpYuvGetUrCb( ybfwModePrevYuvInfo_t *pyuvInfo, UINT32 yuvNum) {

	ybfwModePrevYuvInfo_t *ppyuvInfo = pyuvInfo;
	UINT32 ret;

	RTP_DEBUG_COUNT(yuv);

	if( ppyuvInfo->pframe.pbuf && 	!(ppyuvInfo->attr & (YBFW_MODE_PV_CB_IMG_STOP_IN_PV_EXIT|YBFW_MODE_PV_CB_IMG_PAUSE_IN_PV))
#if SP5K_CVRCAM_MULTI_MODE
	&& pyuvInfo->streamId == YBFW_MODE_PREV_SENSOR_0_YUV_1
	&& pyuvInfo->fid > 2//When use multi-preview, Avoiding small frame is  not copy to main frame(sensor0yuv1)
#endif
	){
		RtspPtsProfPrint("#S#%s#N#%d_fid%d",__func__,(UINT32)((ppyuvInfo->timeSec * 1000) +  (ppyuvInfo->timeUsec / 1000)),ppyuvInfo->fid);
		RtpQueue_t *pQue;
		pQue = ybfwMallocCache(sizeof(RtpQueue_t));
		if (pQue) {
			pQue->pbuf = ppyuvInfo->pframe.pbuf;
			pQue->bufW = ppyuvInfo->pframe.width;
			pQue->bufH = ppyuvInfo->pframe.height;
			pQue->fmt  = ppyuvInfo->pframe.fmt;
			pQue->pts  = (ppyuvInfo->timeSec * 1000) +  (ppyuvInfo->timeUsec / 1000);
			pQue->fid = ppyuvInfo->fid;
			#if RTSP_YUV_IN_DEBUG
			UINT32 fd;
			UINT8 filename[64];
			sprintf((char *)filename, "D:\\APP.WIFI.IN.%d.YUV",ppyuvInfo->fid);
			fd = fsFileOpen(filename, FS_OPEN_CREATE);
			fsFileWrite(fd, ppyuvInfo->pframe.pbuf,  ppyuvInfo->pframe.width * ppyuvInfo->pframe.height *2);
			fsFileClose(fd);
			#endif

			if ((ret = ybfwOsQueueSend(&RtpEncYuvQue, (void*)&pQue, TX_NO_WAIT)) != YBFW_SUCCESS) {
				ybfwProfLogPrintf(0, "[%s]ybfwOsQueueSend error %x", __func__,ret);
				ybfwFree(pQue);
			}
		} else {
			printf("[%s]:%d ERR\n", __func__, __LINE__);
		}
		RtspPtsProfPrint("#E#%s#N#",__func__);
	}

	return SUCCESS;
}

void appRtpYuvUrCbSet(UINT32 enCb, UINT32 ybfwMode) {
	UINT32 rtpFps, senFps, targetFpsInterval,rtpInfo;
	UINT32 sizeIdx = pViewParam->videoSize;
	ybfwCapabilityPreview_t senprevCap;
	appVideoConfig_t *pVidData;

	rtpInfo = appPtpCfgRtpFrameRateGet();
	rtpFps = (rtpInfo & 0x7F);
	if(ybfwMode == YBFW_MODE_VIDEO_PREVIEW) {

		appVideoSizeGet(sizeIdx, 0, &pVidData);
		/*Note: YuvCb must enter preview mode or runtime update, and the sensor mode need to be set before set yuvCb*/
		ybfwSensorCapabilityGet(YBFW_SEN_ID_0, pVidData->sensormode, (UINT32 *)&senprevCap, sizeof(senprevCap));

		senFps = (senprevCap.fps_mhz+ 500000) / 1000000;

		targetFpsInterval = senFps/rtpFps;
		if(senFps < rtpFps) {
			targetFpsInterval = 1;
		}

	}
	else {
		senFps = rtpFps;
		targetFpsInterval = 1;
	}

	ybfwPrevUrgentCallback_t cb = {
		.ctrl = 0,
		.interval = targetFpsInterval,
		.fp = appRtpYuvGetUrCb,
    	};

	printf("enCb %d YUV interval %d senFps %d rtpFps %d\n",enCb,targetFpsInterval,senFps,rtpFps);
   	appYuvUrgentCallbackSet(enCb, ybfwMode, YBFW_MODE_PREV_SENSOR_0_YUV_1, &cb);
}

static UINT32
appRtpEncYuvGet(
	UINT32 handle,
	UINT8 **pptr,
	struct timeval *tv
)
{
	RtpQueue_t *pQue = NULL;
	ybfwMediaRecCfg_t *pcfg = *(ybfwMediaRecCfg_t **)handle;
	RtpStream_t *rtpStream = (RtpStream_t *)pcfg->pExt;
	UINT32 timeout;
	UINT32 ret;
	/*UINT64 encodePts = (UINT64)(rtpStream->frameCnt + 1) * 1000 / (UINT64)rtpStream->frameRate;*/
	UINT32 copy = 0;/*for DeBug*/

	if (rtpStream->startTime == 0) {
		timeout = TX_WAIT_FOREVER;
	}
	else {
		UINT32 sysClockPts = ybfwMsTimeGet();
		UINT32 sysClockPts2 = sysClockPts;/*sysClockPts without minus startTime*/
		sysClockPts = (sysClockPts > rtpStream->startTime) ? (sysClockPts - rtpStream->startTime) : (sysClockPts + (0xffffffff - rtpStream->startTime));
		SINT32 dev= sysClockPts2 - rtpStream->preFrmPts;
		if (dev > (SINT32)(1100 / rtpStream->frameRate)){/* over time, it include 10% acceptable error*/
			timeout = 0;
			//RtspPtsProfPrint("#P#timecompare#N#0sys%lld",sysClockPts);
			//RtspPtsProfPrint("#P#timecompare#N#0encode%lld",encodePts);
		}
		else{
			/*Calculate the timeout, if over the timeout we need send preframe immediately*/
			timeout =  (1200 / rtpStream->frameRate)-(sysClockPts2-rtpStream->preFrmPts);/*(frame period with 20% acceptable error)  minus (system clock - preframe pts)*/
			if(timeout > 1200 / rtpStream->frameRate)
				timeout = 1200 / rtpStream->frameRate;

			//RtspPtsProfPrint("#P#timecompare#N#1sys%lld",sysClockPts);
			//RtspPtsProfPrint("#P#timecompare#N#1encode%lld",encodePts);
			//RtspPtsProfPrint("#P#timecompare#N#timeout%d",timeout);
		}
	}

	while (!RtpEncYuvQue) {
		printf("%s: wait creating que:%d\n", __func__, RtpEncYuvQue);
		ybfwOsThreadSleep(100);
	}

	ret = ybfwOsQueueReceive(&RtpEncYuvQue, &pQue, timeout);
	RtspPtsProfPrint("#S#appRtpEncYuvGet#N#");
	UINT32 yuvBufSelector = rtpStream->yuvCnt % MAX_YUV_BUF_NUM;/*select yuv buffer*/
	if (ret == YBFW_SUCCESS && pQue) {
		RtspPtsProfPrint("#P#appRtpEncYuvGet#N#%lld",pQue->pts);
		if (rtpStream->startTime == 0) {
			if(rtpStream->start == 0) {/*clean que*/
				ybfwFree(pQue);
				while(ybfwOsQueueReceive(&RtpEncYuvQue, &pQue, TX_NO_WAIT) == YBFW_SUCCESS) {
					ybfwFree(pQue);
				}
				ybfwOsQueueReceive(&RtpEncYuvQue, &pQue, TX_WAIT_FOREVER);
				rtpStream->start = 1;
			}
			rtpStream->startTime = ybfwMsTimeGet();
			printf("_YUN_ rtp_starttime %d\n",rtpStream->startTime);
		}
		RtspPtsProfPrint("#P#appRtpEncYuvGet#N#fid%d_yuvs%d_%x",pQue->fid,yuvBufSelector,rtpStream->yuv[yuvBufSelector]);

		appYuvImgcpy(rtpStream->yuv[yuvBufSelector], pQue->pbuf, pQue->bufW, pQue->bufH, pQue->fmt);

		ybfwFree(pQue);
		rtpStream->yuvCnt++;
	} else /*Time out need send pre frame*/
	{
		copy = 1;/*for DeBug*/
		if(yuvBufSelector == 0)
			yuvBufSelector = MAX_YUV_BUF_NUM - 1;
		else
			yuvBufSelector = yuvBufSelector - 1;
		RtspPtsProfPrint("#P#appRtpEncYuvGet#N#yuvs%d",yuvBufSelector);
	}

	rtpStream->frameCnt++;

	UINT32 sysClock = ybfwMsTimeGet();	/* use sys clock for vlc player. */
	*pptr = rtpStream->yuv[yuvBufSelector];
	tv->tv_sec	= sysClock / 1000;
	tv->tv_usec = (sysClock % 1000) * 1000;
	rtpStream->preFrmPts = sysClock;

	if(!copy){
		RtspPtsProfPrint("#E#appRtpEncYuvGet#N#pts%lld",(UINT64)sysClock);
	}else{
		RtspPtsProfPrint("#E#appRtpEncYuvGet#N#copy%lld",(UINT64)sysClock);
	}
	/*for DeBug*/
	return YBFW_SUCCESS;

}

static void
appRtpEncYuvFree(
	UINT32 handle,
	void *ptr
)
{

/*The yuv buf was alloc by a big pool, no need free here.*/
/*
	if (ptr){
		ybfwProfLogPrintf(0,"#P#appRtpEncYuvFree#N#add%x",ptr);
		ybfwYuvBufferFree(ptr);
	}
*/
}

static void appRtpFreeAll(NDKStreamSrc *ss) {

	RtpStream_t *rtpStream = (RtpStream_t *)ss->ss_priv;
	ybfwMediaRecCfg_t *pRtpCfg = (ybfwMediaRecCfg_t *)rtpStream->pRtpCfg;
	UINT32 i;

	if(RtpEncYuvQue && !RtpConnectCnt) {
		RtpQueue_t *pQue = NULL;
		if (RtpEncYuvQue) {
			while(ybfwOsQueueReceive(&RtpEncYuvQue, &pQue, TX_NO_WAIT) == YBFW_SUCCESS) {
				ybfwFree(pQue);
			}
		}
		ybfwOsQueueDelete(&RtpEncYuvQue);
		RtpEncYuvQue = 0;
	}

	for(i = 0; i < MAX_YUV_BUF_NUM; i++){
		if(rtpStream->yuv[i]) {
			ybfwYuvBufferFree(rtpStream->yuv[i]);
			rtpStream->yuv[i] = NULL;
		}
	}
	if(rtpStream->pps)
		ybfwFree(rtpStream->pps);

	if(rtpStream->sps)
		ybfwFree(rtpStream->sps);

	if(rtpStream->vps)
		ybfwFree(rtpStream->vps);

#if YBFW_REC_DUAL_FILE_FOR_STREAMING
	if(rtpStream){
		appVideoStreamClrHdlCfg(rtpStream->streamHandler, pRtpCfg);
	}
#endif
	if (pRtpCfg) {
		appVideoConfigFree(pRtpCfg);
		rtpStream->pRtpCfg = NULL;
	}

	if(ss) {
		ss->ss_priv = NULL;
		ybfwFree(ss);
	}

}

static int host_ss_open(NDKStreamSrc *ss)
{
	UINT32 t = ybfwMsTimeGet();
	UINT32 err;
	UINT32 ret = 0;
	UINT32 i;
	RtpStream_t *rtpStream = (RtpStream_t *)ss->ss_priv;
	ybfwMediaRecCfg_t *pRtpCfg = (ybfwMediaRecCfg_t *)rtpStream->pRtpCfg;

	printf("[host_ss_open]\n");
	ybfwProfLogAdd(0,"#S#RTP#N#SS_OPEN");

	memset(&debugInfo, 0, sizeof(struct rtpdebug_s));

	/**Host can do extra works here.**/
	#if DBG_AUDIO_FILE
	rtpStream->audFd = ybfwFsFileOpen((UINT8 *)"D:\\dump.aac", YBFW_FS_OPEN_CREATE);
	#endif
	#if DBG_VIDEO_FILE
	rtpStream->vidFd = ybfwFsFileOpen((UINT8 *)"D:\\dump.h264", YBFW_FS_OPEN_CREATE);
	#endif
	appMediaInit();

	appNetMutex_Lock();
	appRtpActionLock();


	rtpStream->frameRate = pRtpCfg->vin[0].frameRateSave;
	for(i = 0; i < MAX_YUV_BUF_NUM; i++){
		rtpStream->yuv[i] = ybfwYuvBufferAlloc(pRtpCfg->vin[0].yuvInfo.width,pRtpCfg->vin[0].yuvInfo.height);
	}
	if(!rtpStream->yuv) {
		appRtpFreeAll(ss);
		return 1;
	}
#if !SP5K_REC_DUAL_FILE_FOR_STREAMING
	if(!RtpConnectCnt && !RtpEncYuvQue){
		if (ybfwOsQueueCreate(&RtpEncYuvQue, "rtpEncYuvQ", 1, NULL, 3*sizeof(UINT32)) != YBFW_SUCCESS) {
			printf("[%s]Create Queue fail :%d\n", __func__, __LINE__);
			appRtpFreeAll(ss);
			return 1;
		}
	}
#endif


#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	UINT32 yuvId;
	for (i = 0 ; i < pRtpCfg->vinNo ; i++) {
		/* set media flag for yuv */
		yuvId = 1 << ((pRtpCfg->vin[i].senId<<2) + pRtpCfg->vin[i].yuvId);
		appYuvEnSet(modeOpened, yuvId, YBFW_MODE_PV_CFG_STREAM_MEDIA);
	}
#endif


	uiPara_t* puiPara = appUiParaGet();
	if (app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_VIDEO_OFF &&
		puiPara->VideImageStabilization == UI_VIDEO_IMAGE_STABILIZATION_ON &&
		appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == TRUE) {
		appImgStableDbgNameSet((UINT8 *)"unkown.file");
		appImgStableEnable();
	}
	if ((err = appMediaPreviewEnter()) != SUCCESS) {
		printf("appMediaPreviewEnter() fail(%d)\n", err);
	}
	/* set stream flag to YUV */
	rtpStream->streamHandler = ybfwMediaRecCreate(rtpStream->pRtpCfg);

	if ((!rtpStream->streamHandler || ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, rtpStream->streamHandler) != YBFW_SUCCESS) && RtpConnectCnt == 0 ) {
		printf("[%s]Streaming start fail:%d\n", __func__, __LINE__);
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	for (i = 0 ; i < pRtpCfg->vinNo ; i++) {
			/* set media flag for yuv */
			yuvId = 1 << ((pRtpCfg->vin[i].senId<<2) + pRtpCfg->vin[i].yuvId);
			appYuvEnClear(modeOpened, yuvId, YBFW_MODE_PV_CFG_STREAM_MEDIA);
		}
#else
		appRtpYuvUrCbSet(0,modeOpened);
#endif
		appMediaPreviewExit();
		printf("YBFW_MEDIA_CTRL_START fail(%d)\n", err);

		appRtpFreeAll(ss);
		appMediaDeini();
	}
	else {
		RtpConnectCnt++;
		ybfwHostMsgSend(APP_UI_MSG_RTP_START, (UINT32)rtpStream->streamHandler, ybfwMsTimeGet() - t, 0);
	}

#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	appVideoStreamSetHdl(rtpStream->streamHandler);
	appStreamRtpStatusSet(1);
	if (appVideoStreamDestroyEnable(3)){
	/*if(appVideoStreamDestroyEnable(3) && !appStreamLrvCfgGet()){*/
		appStreamCfgExistSet(1<<1);
		appStreamLrvCfgBkup(pRtpCfg);
	}
#endif

	appNetMutex_UnLock();
	appRtpActionUnlock();

	ybfwProfLogAdd(0,"#E#RTP#N#SS_OPEN");

	return ret;
}

static void host_ss_start_receive(NDKStreamSrc *ss, NDKStreamSrcBufHandler handler, void *udata)
{
	RtpStream_t *rtpStream = (RtpStream_t *)ss->ss_priv;
	rtpStream->send = handler;
	rtpStream->send_priv = udata;
	ybfwProfLogAdd(0, "#P#host_ss_start_receive#N#");
}

static void host_ss_close(NDKStreamSrc *ss)
{
	RtpStream_t *rtpStream = (RtpStream_t *)ss->ss_priv;
	UINT32 t = ybfwMsTimeGet();
	UINT32 mode;
	printf("[host_ss_close]\n");

	appRtpActionLock();

	ybfwProfLogAdd(0,"#S#RTP#N#CLOSE");

	UINT32 lost = rtpStream->frameCnt - rtpStream->yuvCnt;
	UINT32 tt;
	tt = t > rtpStream->startTime ? t - rtpStream->startTime : t + 0xffffffff - rtpStream->startTime;
	tt /= 1000;
	ybfwProfLogPrintf(0, "process %d, %dfps", rtpStream->frameCnt, tt?rtpStream->frameCnt/tt:-1);
	ybfwProfLogPrintf(0, "lost %d,%d%% frame", lost, lost*100/rtpStream->frameCnt);

	ybfwModeGet(&mode);
	if (mode == YBFW_MODE_STILL_SNAP) {
		ybfwModeWait(YBFW_MODE_STILL_PREVIEW);
		mode = YBFW_MODE_STILL_PREVIEW;
	}

	ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, rtpStream->streamHandler);
	ybfwMediaRecDestroy(rtpStream->streamHandler);
	if (appMediaPreviewExit() != SUCCESS){
		printf("appMediaPreviewExit() fail\n");
	}

	RtpConnectCnt--;
	if(!RtpConnectCnt) {
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
		ybfwMediaRecCfg_t *pRtpCfg = (ybfwMediaRecCfg_t *)rtpStream->pRtpCfg;
		UINT32 i, yuvId;
		if(appVideoStreamDestroyEnable(1)){
			for (i = 0 ; i < pRtpCfg->vinNo ; i++) {
				/* set media flag for yuv */
				yuvId = 1 << ((pRtpCfg->vin[i].senId<<2) + pRtpCfg->vin[i].yuvId);
				appYuvEnClear(mode, yuvId, YBFW_MODE_PV_CFG_STREAM_MEDIA);
			}
		}
#else
		appRtpYuvUrCbSet(0,mode);
#endif
	}

	#if DBG_AUDIO_FILE
	if (rtpStream->audFd) {
		ybfwFsFileClose(rtpStream->audFd);
		rtpStream->audFd = 0;
	}
	#endif
	#if DBG_VIDEO_FILE
	if (rtpStream->vidFd) {
		ybfwFsFileClose(rtpStream->vidFd);
		rtpStream->vidFd = 0;
	}
	#endif
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	if(appVideoStreamDestroyEnable(1))
	{
		appStreamCfgExistSet(0);
		appStreamLrvCfgBkup(NULL);
	}
#endif
	ybfwHostMsgSend(APP_UI_MSG_RTP_STOP, (UINT32)rtpStream->streamHandler, ybfwMsTimeGet() - t, 0);
	ybfwProfLogAdd(0,"#E#RTP#N#CLOSE");

	if(!appVideoStreamHandleCheck(-1)) {
		uiPara_t* puiPara = appUiParaGet();
		/* only video pv mode has EIS on. */
		if (mode == YBFW_MODE_VIDEO_PREVIEW &&
			puiPara->VideImageStabilization == UI_VIDEO_IMAGE_STABILIZATION_ON &&
			(appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == TRUE))
			appImgStableDisable();
	}
	appRtpActionUnlock();
}

static void host_ss_free(NDKStreamSrc *ss)
{
	printf("[host_ss_free]\n");
	/**Host can do extra works here.**/
	appRtpFreeAll(ss);
	appMediaDeini();
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	appStreamRtpStatusSet(0);
#endif
}

static BOOL host_ss_is_open(NDKStreamSrc *ss)
{
	/**Host can do extra works here.**/
	/*no used in live streaming*/
	return 0;
}

static int  host_ss_get_attribute(NDKStreamSrc *ss, unsigned int attr, void *val)
{
	/**Host can do extra works here.**/
	RtpStream_t *rtpStream = (RtpStream_t *)ss->ss_priv;
	ybfwMediaRecCfg_t *pRtpCfg = (ybfwMediaRecCfg_t *)rtpStream->pRtpCfg;
	switch (attr) {
		case NDK_MEDIA_ATTR_VIDEO_CODEC:
			switch(pRtpCfg->vin[0].codecSel) {
				case YBFW_MEDIA_VIDEO_H264:
					*(UINT32 *)val = NDK_MEDIA_CODEC_H264;
				break;
				case YBFW_MEDIA_VIDEO_MJPG:
					*(UINT32 *)val = NDK_MEDIA_CODEC_MJPG;
				break;
				case YBFW_MEDIA_VIDEO_HEVC:
					*(UINT32 *)val = NDK_MEDIA_CODEC_HEVC;
				break;
				default:
					*(UINT32 *)val = NDK_MEDIA_CODEC_UNKNOWN;
				break;
			}
			break;
		case NDK_MEDIA_ATTR_DURATION:
			*(UINT32 *)val = 0;/*live mode*/
			break;
		case NDK_MEDIA_ATTR_WIDTH:
				*(UINT32 *)val = pRtpCfg->vin[0].yuvInfo.width ;
			break;
		case NDK_MEDIA_ATTR_HEIGHT:
				*(UINT32 *)val = pRtpCfg->vin[0].yuvInfo.height;
			break;
		case NDK_MEDIA_ATTR_VIDEO_BRC_TYPE:
			break;
		case NDK_MEDIA_ATTR_VIDEO_AVG_BITRATE:
			*(UINT32 *)val = pRtpCfg->vin[0].bitRate;
			break;
		case NDK_MEDIA_ATTR_VIDEO_FRAME_RATE:
			*(UINT32 *)val = pRtpCfg->vin[0].frameRateSave;

			break;
		case NDK_MEDIA_ATTR_H264_GOP_NO:
			*(UINT32 *)val =  pRtpCfg->vin[0].codec.h264.gopNo;
			break;
		case NDK_MEDIA_ATTR_AUDIO_CODEC:
			switch(pRtpCfg->ain[0].codecSel) {
				case YBFW_MEDIA_AUDIO_AAC:
					*(UINT32 *)val = NDK_MEDIA_CODEC_AAC;
				break;
				case YBFW_MEDIA_AUDIO_PCM:
					*(UINT32 *)val = NDK_MEDIA_CODEC_PCM;
				break;
				default:
				 	*(UINT32 *)val = NDK_MEDIA_CODEC_UNKNOWN;
				break;
			}
			break;
		case NDK_MEDIA_ATTR_AUDIO_SAMPLE_RATE:
			*(UINT32 *)val =  VIDEO_AUDIO_SAMPLE_RATE;
			break;
		case NDK_MEDIA_ATTR_AUDIO_SAMPLE_BITS:
			*(UINT32 *)val =  pRtpCfg->ain[0].sampleBits;
			break;
		case NDK_MEDIA_ATTR_AUDIO_CHANNELS:
			*(UINT32 *)val = VIDEO_AUDIO_CHANNEL;
			break;
		case NDK_MEDIA_ATTR_AUDIO_AVG_BITRATE:
			*(UINT32 *)val =  pRtpCfg->ain[0].bitrate;
			break;
		default:
			return -1;
	}

	return SUCCESS;
}

static BOOL host_ss_end_of_source(NDKStreamSrc *ss)
{
	/**Host can do extra works here.**/
	/*not used at live streaming*/
	return 0;
}

static int  host_ss_control(NDKStreamSrc *ss, NDKStreamSrcCtrlCode code, void *param)
{
	/**Host can do extra works here.**/
	/*no used in live streaming*/
	return 0;
}
#if 0
static void appRtpURLParameterParser(const char *urlstr)
{
	printf("[appRtpURLParameterParser] %s\n",urlstr);

    int len, nel;
    char *q, *name, *value;
    char *url = NULL;
    int i = 0 ;

    q = strchr(urlstr, '?');

    if (q) {
        q = q + 1 ;
        len = strlen(q);
    } else
        len = strlen(urlstr);

    if (len == 0 )  {
        return ;
    }

    url = ybfwMallocCache(len);

    if ( q ) {
        strcpy(url, q );
    } else {
        strcpy(url, urlstr );
    }

    /* to uppercase strupr() */
    for(i=0 ; i <= strlen(url) ; i++ ) {
        if( url[i] >= 97 && ( url[i] <= 122 ))
            url[i] = url[i] - 32 ;
    }

    q = url;
    nel = 1;
    while (strsep(&q, "&"))
        nel++;

    for (q = url; q < (url + len);) {
        value = name = q;
        printf("[string] : %s\n",q);
        for (q += strlen(q); q < (url + len) && !*q; q++);
        name = strsep(&value, "=");

        if (!strcmp((const char *)name , "W")) {
        	/*pcfg->vin[0].yuvInfo.width = atoi(value);
        	printf("[Set] : %s %d\n",name,pcfg->vin[0].yuvInfo.width);*/
        } else if (!strcmp((const char *)name , "H")) {
            /*pcfg->vin[0].yuvInfo.height = atoi(value);
            printf("[Set] : %s %d\n",name,pcfg->vin[0].yuvInfo.height);*/
        } else if (!strcmp((const char *)name , "BR")) {
            /*pcfg->vin[0].bitRate = atoi(value);
            printf("[Set] : %s %d\n",name,pcfg->vin[0].bitRate);*/
        } else if (!strcmp((const char *)name , "FPS")) {
            /*pcfg->vin[0].frameRateInput = atoi(value);
            printf("[Set] : %s %d\n",name,pcfg->vin[0].frameRateInput);*/
        }
    }

    ybfwFree(url);
}
#endif
void sbo_CusNdkBuf_ref(void *data)
{
	CusNdkBuf_s_t *p = (CusNdkBuf_s_t*)data;
	p->refCnt++;
 }

void sbo_CusNdkBuf_unref(void *data)
{
	CusNdkBuf_s_t *p = (CusNdkBuf_s_t*)data;

	RTP_DEBUG_COUNT(push);

	p->refCnt--;
	if(p->refCnt<0) HOST_ASSERT(0);
	else if(p->refCnt == 0) {
		ybfwFree(p->pbuf);
		ybfwFree(p);
	}
}

static void appRtpH265Parse(CusNdkBuf_s_t * ndkBuf, RtpStream_t *rtpStream) {

	UINT32 i;
	UINT8 *pps = NULL;
	UINT8 *sps = NULL;
	UINT8 *vps = NULL;
	rtpStream->hdr_len = 0;/*Must initialize*/
	ndkBuf->data = ndkBuf->pbuf;

	if(!rtpStream->vps || !rtpStream->sps || !rtpStream->pps) {
		for (i = 0 ; i < 128 ; i++) {
			//printf("%02x,%d %x\n",pbuf[4], i,pbuf);
			if (ndkBuf->data[0] == 0 && ndkBuf->data[1] == 0 && ndkBuf->data[2] == 0 && ndkBuf->data[3] == 1) {
				//printf("appCustH265Push-%d:NAL%02x,%d\n", __LINE__, ndkBuf->data[4], i);
				RtspLiveProfPrint("appCustH265Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				if ((ndkBuf->data[4] & 0x7f) == H265_IDR) {	/* end */
					if (pps && rtpStream->pps_len == 0) {
						rtpStream->pps_len = ndkBuf->data - pps;
						rtpStream->pps = ybfwMallocCache(rtpStream->pps_len);
						memcpy(rtpStream->pps, pps, rtpStream->pps_len);
					}
					if (sps && rtpStream->sps_len == 0) {
						rtpStream->sps_len = ndkBuf->data - sps;
						rtpStream->sps = ybfwMallocCache(rtpStream->sps_len);
						memcpy(rtpStream->sps, sps, rtpStream->sps_len);
 					}
					rtpStream->hdr_len = ndkBuf->data - ndkBuf->pbuf;
					ndkBuf->key = 1;
					RtspLiveProfPrint("end-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
					break;
				}
				else if ((ndkBuf->data[4] & 0x7f) == H265_VPS) {
					vps = ndkBuf->data;
					ndkBuf->data += 4;
					RtspLiveProfPrint("sps-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				}
				else if ((ndkBuf->data[4] & 0x7f) == H265_SPS) {
					sps = ndkBuf->data;
					if (vps && rtpStream->vps_len == 0) {
						rtpStream->vps_len = sps - vps;
						rtpStream->vps = ybfwMallocCache(rtpStream->vps_len);
						memcpy(rtpStream->vps, vps, rtpStream->vps_len);
 					}
					ndkBuf->data += 4;
					RtspLiveProfPrint("pps-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				}
				else if ((ndkBuf->data[4] & 0x7f) == H265_PPS) {
					pps = ndkBuf->data;
					if (sps && rtpStream->sps_len == 0) {
						rtpStream->sps_len = pps - sps;
						rtpStream->sps = ybfwMallocCache(rtpStream->sps_len);
						memcpy(rtpStream->sps, sps, rtpStream->sps_len);
 					}
					ndkBuf->data += 4;
					RtspLiveProfPrint("pps-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				}
				else if ((ndkBuf->data[4] & 0x7f) == H265_TRAILN || (ndkBuf->data[4] & 0x7f) == H265_TSAN) {
					RtspLiveProfPrint("break-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
					break;
				}
			}
			ndkBuf->data++;
		}
	}
	else {
		for (i = 0 ; i < 128 ; i++) {
		if (ndkBuf->data[0] == 0 && ndkBuf->data[1] == 0 && ndkBuf->data[2] == 0 && ndkBuf->data[3] == 1) {
			RtspLiveProfPrint("2appCustH265Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
			if ((ndkBuf->data[4] & 0x7f) == H265_IDR) {
				rtpStream->hdr_len = i;
				ndkBuf->key = 1;
				break;
			}
			else if ((ndkBuf->data[4] & 0x7f) == H265_TRAILN || (ndkBuf->data[4] & 0x7f) == H265_TSAN) {
				break;
			}
		}
		ndkBuf->data++;
		}
		if (!rtpStream->hdr_len) {
			ndkBuf->data = ndkBuf->pbuf;
		}
	}
}

static void appRtpH264Parse(CusNdkBuf_s_t * ndkBuf, RtpStream_t *rtpStream) {

	UINT32 i;
	UINT8 *pps = NULL;
	UINT8 *sps = NULL;

	rtpStream->hdr_len = 0;/*Must initialize*/
	ndkBuf->data = ndkBuf->pbuf;

	if(!rtpStream->sps || !rtpStream->pps) {
		for (i = 0 ; i < 64 ; i++) {
			//printf("%02x,%d %x\n",pbuf[4], i,pbuf);
			if (ndkBuf->data[0] == 0 && ndkBuf->data[1] == 0 && ndkBuf->data[2] == 0 && ndkBuf->data[3] == 1) {
				//printf("appCustH264Push-%d:NAL%02x,%d\n", __LINE__, ndkBuf->data[4], i);
				RtspLiveProfPrint("appCustH264Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				if ((ndkBuf->data[4] & 0x1f) == H264_IDR) {	/* end */
					if (pps && rtpStream->pps_len == 0) {
						rtpStream->pps_len = ndkBuf->data - pps;
						rtpStream->pps = ybfwMallocCache(rtpStream->pps_len);
						memcpy(rtpStream->pps, pps, rtpStream->pps_len);
					}
					if (sps && rtpStream->sps_len == 0) {
						rtpStream->sps_len = ndkBuf->data - sps;
						rtpStream->sps = ybfwMallocCache(rtpStream->sps_len);
						memcpy(rtpStream->sps, sps, rtpStream->sps_len);
 					}
					rtpStream->hdr_len = ndkBuf->data - ndkBuf->pbuf;
					ndkBuf->key = 1;
					RtspLiveProfPrint("end-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
					break;
				}
				else if ((ndkBuf->data[4] & 0x1f) == H264_SPS) {
					sps = ndkBuf->data;
					if (pps && rtpStream->pps_len == 0) {
						rtpStream->pps_len = sps - pps;
						rtpStream->pps = ybfwMallocCache(rtpStream->pps_len);
						memcpy(rtpStream->pps, pps, rtpStream->pps_len);
					}
					ndkBuf->data += 4;
					RtspLiveProfPrint("sps-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				}
				else if ((ndkBuf->data[4] & 0x1f) == H264_PPS) {
					pps = ndkBuf->data;
					if (sps && rtpStream->sps_len == 0) {
						rtpStream->sps_len = pps - sps;
						rtpStream->sps = ybfwMallocCache(rtpStream->sps_len);
						memcpy(rtpStream->sps, sps, rtpStream->sps_len);
 					}
					ndkBuf->data += 4;
					RtspLiveProfPrint("pps-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				}
				else if ((ndkBuf->data[4] & 0x1f) == H264_PDR) {
					RtspLiveProfPrint("break-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
					break;
				}
			}
			ndkBuf->data++;
		}
	}
	else {
		for (i = 0 ; i < 64 ; i++) {
		if (ndkBuf->data[0] == 0 && ndkBuf->data[1] == 0 && ndkBuf->data[2] == 0 && ndkBuf->data[3] == 1) {
			RtspLiveProfPrint("2appCustH264Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
			if ((ndkBuf->data[4] & 0x1f) == 0x5) {
				rtpStream->hdr_len = i;
				ndkBuf->key = 1;
				break;
			}
			else if ((ndkBuf->data[4] & 0x1f) == 0x1) {
				break;
			}
		}
		ndkBuf->data++;
		}
		if (!rtpStream->hdr_len) {
			ndkBuf->data = ndkBuf->pbuf;
		}
	}
}
static void appRtpParaFrameSend(RtpStream_t *rtpStream, UINT8 *pbuf, UINT32 len) {
	NDKStreamSrcBuf ndkStreamBuf = {
		{NULL, 0, 0},
		NDK_MEDIA_BUF_ES_VIDEO | NDK_MEDIA_BUF_PARAM_FRAME,
		pbuf, (unsigned int)len, 0,
		{0, 0}, {0, 0}
	};
	rtpStream->send(&ndkStreamBuf,rtpStream->send_priv);
}
static void appRtpFrameSend(CusNdkBuf_s_t *ndkBuf, RtpStream_t *rtpStream) {

	NDKStreamSrcBuf ssb = {{0,0,0},0};
	ssb.bufobj.sbo_data = ndkBuf;
	ssb.bufobj.sbo_ref = sbo_CusNdkBuf_ref;
	ssb.bufobj.sbo_unref = sbo_CusNdkBuf_unref;
	ssb.duration = 0;
	ssb.pts.tv_sec = ndkBuf->stamp/1000;
	ssb.pts.tv_usec = (ndkBuf->stamp%1000) * 1000;
	ssb.enctime = ssb.pts;
	ssb.data = ndkBuf->data;

	if(ndkBuf->type) {
		ssb.length = (unsigned int) ndkBuf->size;
		ssb.bufflags = NDK_MEDIA_BUF_ES_AUDIO | NDK_MEDIA_BUF_KEY_FRAME;
	}
	else {
		ssb.length = (unsigned int) ndkBuf->size - rtpStream->hdr_len;
		ssb.bufflags = NDK_MEDIA_BUF_ES_VIDEO;
		if(ndkBuf->key == 1)
			ssb.bufflags |= NDK_MEDIA_BUF_KEY_FRAME;
			}

	rtpStream->send(&ssb,rtpStream->send_priv);

}
static void
appRtpEncVlcToNdk(
	UINT32 handle,
	mediaMuxerCusInfo_t *pinfo
)
{
	#if RTP_PTS_DEBUG
	if(pinfo->type == 0) {
		RtspPtsProfPrint("#S#VlcToNdkV#N#%d",(UINT32)pinfo->stamp/1000);
		//RtspPtsProfPrint("#P#VlcToNdkV#N#Size%d",(UINT32)pinfo->size);
	}
	else {
		RtspPtsProfPrint("#S#VlcToNdkA#N#");
	}
	#endif
	ybfwMediaRecCfg_t *pcfg = *(ybfwMediaRecCfg_t **)handle;
	RtpStream_t *rtpStream = (RtpStream_t *)pcfg->pExt;

	RTP_DEBUG_COUNT(vlc);

	if(rtpStream->send_priv == NULL || rtpStream->send== NULL ) {
		ybfwProfLogPrintf(0,"#P#appRtpEncVlcToNdk#N#notReady");
		return;
	}


	CusNdkBuf_s_t *pClone;

	if ((pClone = ybfwMallocCache(sizeof(CusNdkBuf_s_t))) == NULL) {
		printf("[%s]alloc fail: %d\n", __func__, __LINE__);
	}
	else if ((pClone->pbuf = ybfwMallocCache(pinfo->size)) == NULL) {
		ybfwFree(pClone);
		printf("[%s]alloc fail: %d\n", __func__, __LINE__);
	}
	else {
		pClone->refCnt = 0;
		pClone->size  = pinfo->size;
		pClone->stamp = pinfo->stamp/1000;
		pClone->type = pinfo->type;
		pClone->key = 0;
		appDmaMemcpy(pClone->pbuf, pinfo->pbuf, pinfo->size);
		//memdump(pClone->pbuf,64);//for Debug
	}

	if(pClone->type == 0)/*Video*/
	{
		switch(pcfg->vin[0].codecSel)
		{
			case YBFW_MEDIA_VIDEO_H264:
				appRtpH264Parse(pClone, rtpStream);
 			break;
			case YBFW_MEDIA_VIDEO_HEVC:
				appRtpH265Parse(pClone, rtpStream);
			break;
			case YBFW_MEDIA_VIDEO_MJPG:

			break;
			default:
			break;
		}
		#if DBG_VIDEO_FILE
		if (rtpStream->vidFd){
			ybfwFsFileWrite(rtpStream->vidFd, pClone->pbuf, pClone->size);
		}
		#endif
		if(pcfg->vin[0].codecSel == YBFW_MEDIA_VIDEO_HEVC && rtpStream->vps && rtpStream->vps_len) {
			RtspLiveProfPrint("#P#appRtpEncVlcToNdk#N#vps-push");
			appRtpParaFrameSend(rtpStream,rtpStream->vps,rtpStream->vps_len);
		}
		if (rtpStream->sps && rtpStream->sps_len) {
			RtspLiveProfPrint("#P#appRtpEncVlcToNdk#N#sps-push");
			appRtpParaFrameSend(rtpStream,rtpStream->sps,rtpStream->sps_len);
		}
		if (rtpStream->pps && rtpStream->pps_len) {
			RtspLiveProfPrint("#P#appRtpEncVlcToNdk#N#pps-push");
			appRtpParaFrameSend(rtpStream,rtpStream->pps,rtpStream->pps_len);
		}
		appRtpFrameSend(pClone, rtpStream);
		RtspPtsProfPrint("#E#VlcToNdkV#N#");
	}
	else/*type = 1 :Audio*/
	{
		switch(pcfg->ain[0].codecSel)
		{
			case YBFW_MEDIA_AUDIO_AAC:
				pClone->data = pClone->pbuf;
			break;
			case YBFW_MEDIA_AUDIO_PCM:
				if (pcfg->ain[0].sampleBits == 16) {
					void *data = ybfwMallocCache(pClone->size);
					appCustMsrcSwapAudData16(data, pClone->pbuf, pClone->size);
					ybfwFree(pClone->pbuf);
					pClone->data = pClone->pbuf = data;
				}
			break;

			default:
			break;
		}

		#if DBG_AUDIO_FILE
		if (rtpStream->audFd)
			ybfwFsFileWrite(rtpStream->audFd, pClone->pbuf, pClone->size);
		#endif
		appRtpFrameSend(pClone, rtpStream);
		RtspPtsProfPrint("#E#VlcToNdkA#N#");
	}
}

void appRtpConfigStreaming(
	UINT32 rtpSize,
	UINT32 yuvId,
	UINT32 rtpInfo,
	void *priv
)
{
	UINT32 maxQp = 38;
	UINT32 minQp = 20;
	UINT32 initQp = 38;
	ybfwCapabilityPreview_t senprevCap;
	RtpStream_t *rtpStream = (RtpStream_t *) priv;
	UINT32 fps = (rtpInfo & 0x7F);

#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	appVideoConfig_t *pVidData;
#else
	appVideoConfig_t vidData = {
					.w = (rtpSize >> 19) & 0xfff,
					.h = (rtpSize >> 8) & 0x7ff,
					.video =  ((rtpInfo >> 8) & 0x3)? VIDEO_FORMAT_HEVC:((rtpSize >> 31) ? VIDEO_FORMAT_MJPG : VIDEO_FORMAT_H264),
					.audio = YBFW_MEDIA_AUDIO_UNKNOWN,
					.frameRate = fps,
				};
#endif

    ybfwModePreviewYuvCustCfg_t yuvCfg;
	ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_GET,
			modeOpened,
			YBFW_MODE_PREV_SENSOR_0_YUV_1,
			&yuvCfg);

#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	appVideoSizeGet(appVideoGetIdx(), 1, &pVidData);
	rtpStream->pRtpCfg = appVideoBasicConfigAlloc(pVidData);
#else
	rtpStream->pRtpCfg = appVideoBasicConfigAlloc(&vidData);
#endif
	ybfwMediaRecCfg_t *pRtpCfg = rtpStream->pRtpCfg;
	if(pRtpCfg->pExt) {
		ybfwFree(pRtpCfg->pExt);/*we change pext as rtpStream*/
	}
	ybfwSensorCapabilityGet(YBFW_SEN_ID_0, ybfwSensorModeCfgGet(YBFW_SEN_ID_0, appPtpOpModeGet()), (UINT32*)&senprevCap, sizeof(ybfwCapabilityPreview_t));

	pRtpCfg->vin[0].frameRateSensor = (senprevCap.fps_mhz+ 500000) / 1000000;
	pRtpCfg->muxSel = YBFW_MEDIA_FILE_TYPE_CUS;
	pRtpCfg->filename[0] = '\0';	/* no file name */
	pRtpCfg->vin[0].yuvId = yuvId;
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	pRtpCfg->vin[0].bufSize           = pVidData->w * pVidData->h * 2 * 40 / 100;	/* 40% */
#else
	pRtpCfg->vin[0].fifoDepth         = 6 * fps;	/* rtp need fifo? */
	pRtpCfg->vin[0].bufSize           = vidData.w * vidData.h * 2 * 40 / 100;	/* 40% */
#endif
	/* H264 IPPP */
	pRtpCfg->vin[0].bitRate           = pRtpCfg->vin[0].bitRateMax
	                               = pRtpCfg->vin[0].bitRateMin
	                               = (rtpSize & 0xff) * 100000;
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	if (pVidData->video == VIDEO_FORMAT_H264) {
		pRtpCfg->vin[0].codec.h264.vuiTag = 1<<4;
		/**< b[7:0] init, b[15:8] max, b[23:16] min */
	   	pRtpCfg->vin[0].codec.h264.qpCfgI  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	   	pRtpCfg->vin[0].codec.h264.qpCfgP  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	   	pRtpCfg->vin[0].codec.h264.qpCfgB  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	}
	else if(pVidData->video == VIDEO_FORMAT_HEVC) {
		pRtpCfg->vin[0].codec.hevc.vuiTag = 1<<4;
		pRtpCfg->vin[0].codec.hevc.qpInit = 30;
		pRtpCfg->vin[0].codec.hevc.qpMin = 20;
		pRtpCfg->vin[0].codec.hevc.qpMax = 38;
	}
#else
	if (vidData.video == VIDEO_FORMAT_H264) {
		pRtpCfg->vin[0].codec.h264.vuiTag = 1<<4;
		/**< b[7:0] init, b[15:8] max, b[23:16] min */
	   	pRtpCfg->vin[0].codec.h264.qpCfgI  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	   	pRtpCfg->vin[0].codec.h264.qpCfgP  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	   	pRtpCfg->vin[0].codec.h264.qpCfgB  = (initQp << 0) | (maxQp << 8) | (minQp << 16);
	}
	else if(vidData.video == VIDEO_FORMAT_HEVC) {
		pRtpCfg->vin[0].codec.hevc.vuiTag = 1<<4;
		pRtpCfg->vin[0].codec.hevc.qpInit = 30;
		pRtpCfg->vin[0].codec.hevc.qpMin = 20;
		pRtpCfg->vin[0].codec.hevc.qpMax = 38;
	}
#endif
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	pRtpCfg->vin[0].yuvInfo.width      = yuvCfg.yuv.width;
	pRtpCfg->vin[0].yuvInfo.height     = yuvCfg.yuv.height;
	pRtpCfg->vin[0].yuvInfo.roiX       = 0;
	pRtpCfg->vin[0].yuvInfo.roiY       = 0;
	pRtpCfg->vin[0].yuvInfo.roiW       = YUV_ROI_ALIGN(pVidData->w);
	pRtpCfg->vin[0].yuvInfo.roiH       = YUV_ROI_ALIGN(pVidData->h);
#else
	pRtpCfg->vin[0].yuvInfo.width      = yuvCfg.yuv.width;
	pRtpCfg->vin[0].yuvInfo.height     = yuvCfg.yuv.height;
	pRtpCfg->vin[0].yuvInfo.roiX       = 0;
	pRtpCfg->vin[0].yuvInfo.roiY       = 0;
	pRtpCfg->vin[0].yuvInfo.roiW       = YUV_ROI_ALIGN(vidData.w);
	pRtpCfg->vin[0].yuvInfo.roiH       = YUV_ROI_ALIGN(vidData.h);
#endif
	pRtpCfg->vin[0].yuvInfo.fmt        = YBFW_IMG_FMT_YUV420_V50;
	pRtpCfg->vin[0].stopMode = 0;
#if !SP5K_REC_DUAL_FILE_FOR_STREAMING
	pRtpCfg->vin[0].fpYuvPtrInputCb = appRtpEncYuvGet;
	pRtpCfg->vin[0].fpYuvPtrInputFreeCb = appRtpEncYuvFree;
#endif
	pRtpCfg->mux.muxer.cus.fpDataCb = appRtpEncVlcToNdk;
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	if(pVidData->audio == YBFW_MEDIA_AUDIO_AAC )
#else
	if(vidData.audio == YBFW_MEDIA_AUDIO_AAC )
#endif
		pRtpCfg->ain[0].codec.aac.adtsEn = 0;/*audio with adts header adtsEn= 1*/
	printf("[appRtpConfigStreaming] w %d h %d yuvId %d frameRate %d bitrate %d bufcnt %d\n", pRtpCfg->vin[0].yuvInfo.width,
												 pRtpCfg->vin[0].yuvInfo.height,
												 pRtpCfg->vin[0].yuvId,
												 fps ,
												 pRtpCfg->vin[0].bitRate,
												 pRtpCfg->vin[0].bufSize);
}

NDKStreamSrc* appRtpCreateStreamSrc(const char *stream_name, void *udata)
{
	UINT32 pauseBurst = -1;
	RtpStream_t *rtpStream = (RtpStream_t *)ybfwMallocCache(sizeof(RtpStream_t));

	ybfwProfLogAdd(0, "#P#RTP#N#Create");

	printf("[AppRtpSt] Create StreamSrc: %s\n", stream_name);
	if(rtpStream == NULL) {
		printf("[AppRtpSt] RtpStream alloc fail!\n");
		return 0;
	}
	else {
		memset(rtpStream, 0x0, sizeof(RtpStream_t));
	}

	/* In still state, 5k mode would be switch to 0x13 which will not allow to configure pv yuv. */
	while (appActiveStateGet() == APP_STATE_STILL_CAPTURE)
		ybfwOsThreadSleep(100);

	if (appActiveStateGet() == APP_STATE_BURST_CAPTURE) {
		pauseBurst = appStill_SetBurstPause(1);
		ybfwModeWait(YBFW_MODE_STILL_PREVIEW);
	}

	if (!appVideoStreamHandleCheck(-1)) {/*Check whether recording?*/
		modeOpened = appWifiYuvConfig();
	}
#if !SP5K_REC_DUAL_FILE_FOR_STREAMING
	else {
		appRtpYuvUrCbSet(1,modeOpened);
	}
#endif

	if (pauseBurst != -1)
		appStill_SetBurstPause(pauseBurst);

#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	if((!appVideoStreamDestroyEnable(1)) && appStreamLrvCfgGet())
	{
		rtpStream->pRtpCfg = appStreamLrvCfgGet();
		rtpStream->pRtpCfg->muxSel = YBFW_MEDIA_FILE_TYPE_CUS;
		rtpStream->pRtpCfg->mux.muxer.cus.fpDataCb = appRtpEncVlcToNdk;
		appVideoStreamSetCfg(rtpStream->pRtpCfg);
	}
	else
#endif
	{
		UINT32 rtpSize = appPtpCfgRtpSizeGet(appVideoGetIdx());
		UINT32 rtpInfo = appPtpCfgRtpFrameRateGet();
		appRtpConfigStreaming(rtpSize, 1, rtpInfo,rtpStream);
	}

	ybfwMediaRecCfg_t *pRtpCfg = (ybfwMediaRecCfg_t *)rtpStream->pRtpCfg;
	pRtpCfg->pExt = (void *)rtpStream;

	NDKStreamSrc *host_ss = (NDKStreamSrc *)ybfwMallocCache(sizeof(NDKStreamSrc));
	if(host_ss == NULL) {
		printf("[AppRtpSt] host_ss alloc fail!\n");
		return 0;
	}
	else {
		memset(host_ss, 0x0, sizeof(NDKStreamSrc));
		host_ss->ss_priv = (void *)rtpStream;
		host_ss->ss_open = host_ss_open;
		host_ss->ss_start_receive = host_ss_start_receive;
		host_ss->ss_close = host_ss_close;
		host_ss->ss_free = host_ss_free;
		host_ss->ss_is_open = host_ss_is_open;
		host_ss->ss_get_attribute = host_ss_get_attribute;
		host_ss->ss_end_of_source = host_ss_end_of_source;
		host_ss->ss_control = host_ss_control;
	}

	return host_ss;
}


UINT32 appRtpEventHandler(UINT32 event, UINT32 data)
{

	switch (event) {
	case NDK_ST_EVT_RTSP_REQUEST:
	{
		const char *stream_name = (const char *)data;
		UINT32 stfmt;
		printf("Streaming Request '%s' received\n", (const char *)stream_name);

		if(strstr(stream_name, "VIDEO") != NULL)
		{
			printf("[App playback] RTSP PLAYBACK\n");
			stfmt = NDK_ST_FMT_CUSTOM | NDK_ST_FMT_VID_ON | NDK_ST_FMT_AUD_ON;
		}
		else {
			printf("[App Rtsp] RTSP Live streaming\n");
			stfmt = NDK_ST_FMT_LIVE | NDK_ST_FMT_AUD_ON | NDK_ST_FMT_VID_ON;
		}
#if 0
		int len;
		char *p = strchr(stream_name, '?');
		if (p)
			len = p - stream_name;
		else
			len = strlen(stream_name);

       // appRtpURLParameterParser(stream_name);

 	    /* if video PB request streaming type must set default,can not set H264 OR MJPG */
		if((strstr(url, ".MOV") != NULL) || (strstr(url, ".AVI") != NULL)
				|| (strstr(url, ".MP4") != NULL))
		{
		    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_set_attribute");
			ndk_st_set_attribute(NDK_ST_ATTR_STREAMING_TYPE, NDK_ST_TYPE_DEFAULT);
			return TRUE;
		}
#endif

		return stfmt;
	}

	case NDK_ST_EVT_ON_STARTED:
	case NDK_ST_EVT_ON_STOPPED:
		/**Unused now.**/
		return TRUE;

	case NDK_ST_EVT_FRAME_DROPPED:
		return TRUE;

	default:
		break;
	}

	return FALSE;
}


/*remain for clients, can remove its*/

UINT32
appRtpStreamHandleGet(void)
{
	return 0;
}
void appRtpStop(void)
{
}

void appRtpStart(void)
{
}
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
void appRtpEncVlcToNdkTest(UINT32 handle, mediaMuxerCusInfo_t *pinfo)
{
	static UINT32 dataCnt = 0;
	if (pinfo && pinfo->type == 0) {
		dataCnt++;
	}
	if (dataCnt%100 ==0 && !pinfo->type) {
		/*printf("\n appRtpEncVlcToNdkTest received %d pkts ! \n", dataCnt);*/
		memdump(pinfo->pbuf, 16);
	}
}

void* appRtpStreamTestStart(void)
{
	printf("\n appRtpStreamTestStart \n");
	NDKStreamSrc * host_ss = appRtpCreateStreamSrc("rtp_test_stream", NULL);
	RtpStream_t *rtpStream = (RtpStream_t *)host_ss->ss_priv;
	rtpStream->pRtpCfg->mux.muxer.cus.fpDataCb = appRtpEncVlcToNdkTest;
	appMediaInit();

	if (!appVideoStreamHandleCheck(-1))	/* do not reset audio during recording */
		appStreamMediaAudInit(); /*Media audio Initialize*/
	host_ss_open(host_ss);
	return (void *)host_ss;
}

void appRtpStreamTestStop(void* ss)
{
	printf("\n appRtpStreamTestStop \n");
	if (ss){
		host_ss_close((NDKStreamSrc *)ss);
		host_ss_free((NDKStreamSrc *)ss);
		if(appVideoStreamDestroyEnable(1)){
			appStateChange(APP_STATE_NULL, APP_STATE_MSG_INIT,(UINT32)NULL, (UINT32)NULL);
			appStateChange(APP_STATE_VIDEO_PREVIEW, APP_STATE_MSG_INIT,(UINT32)NULL, (UINT32)NULL);
		}
	}
	else
		printf("\n Please start firstly! \n");
}
#endif

UINT32 appGetRtspState(void)
{
	return 0;
}
/*remain for clients, can remove its*/

/*====RTP livestreaming end====*/

/*====RTP playback start====*/
#define RTSP_PB_CODEC_MJPG	     1

#define RTSP_PB_TARGET_W         1280
#define RTSP_PB_TARGET_H         720

#if RTSP_PB_CODEC_MJPG
#define RTSP_PB_TARGET_FRAMERATE 15
#define RTSP_PB_TARGET_BITRATE   7000000
#else/*CODEC H264*/
#define RTSP_PB_TARGET_FRAMERATE 30
#define RTSP_PB_TARGET_BITRATE   6000000
#endif

#if RTSP_PB_CODEC_MJPG
#define RTSP_PB_BRC              1
#define RTSP_PB_TARGET_VLC_SIZE  (RTSP_PB_TARGET_BITRATE >> 3)
#else/*CODEC H264*/
#define RTSP_PB_QP_MAX           45
#define RTSP_PB_QP_MIN           10
#endif

#define RTSP_AV_SYNC             1
#define DBG_LOG_PTS              0
#define DBG_VLC_FILE             0


#define YUV_FRAME_SET(pYuv, w, h) { \
	pYuv->roiX = pYuv->roiY = 0; \
	pYuv->roiW = YUV_ROI_ALIGN(RTSP_PB_TARGET_W); \
	pYuv->roiH = YUV_ROI_ALIGN(RTSP_PB_TARGET_H); \
	w = YUV_BUF_W_ALIGN(pYuv->roiW); \
	h = YUV_BUF_H_ALIGN(pYuv->roiH); \
	pYuv->fmt  = YBFW_GFX_FMT_YUV420_V50; \
}

#define RTP_PLAY_PTS_DEBUG 1

#if RTP_PLAY_PTS_DEBUG
#define RtspPlayPtsProfPrint(pattern, arg...)	ybfwProfLogPrintf(0, pattern, ##arg);
#else
#define RtspPlayPtsProfPrint(pattern, arg...)
#endif

#define MULTIPLE_OF_4(l)       (!((UINT32)(l) & 0x03))
#define DSTWR16(p_dst, dst16)  *(UINT16*)(p_dst)=(dst16); p_dst+=2
#define DSTWR32(p_dst, dst32)  *(UINT32*)(p_dst)=(dst32); p_dst+=4
#define SRCRD16(p_src, src16)  src16=*(UINT16*)(p_src); p_src+=2
#define SRCRD32(p_src, src32)  src32=*(UINT32*)(p_src); p_src+=4
#define U8PTR(x)               ((UINT8*)&(x))

typedef enum {
	CMD_CUST_MSRC_YUV_CB,
	CMD_CUST_MSRC_VLC_CB,
	CMD_CUST_MSRC_PCM_CB,
	CMD_CUST_MSRC_PAUSE,
	CMD_CUST_MSRC_RESUME,
	CMD_CUST_MSRC_SEEK,
	CMD_CUST_MSRC_INIT,
	CMD_CUST_MSRC_CLOSE,
} custMsrcCmd_t;

typedef struct {
	NDKStMediaSrcPushBufferFunc push_func;
	void *push_param;
	char fname[256];

	UINT32 audCodec;
	SINT64 audStartPTS;
	UINT32 audBytesPerSec;
	UINT32 audSampleBits;
	UINT32 audChannels;
	UINT32 audSeeked;
	#if (DBG_LOG_PTS || RTSP_AV_SYNC)
	SINT64 audPushPts;
	#endif
	#if RTSP_AV_SYNC
	YBFW_QUEUE audQue;
	#endif

	UINT32 vidCodec;
	UINT32 vidDurationTime;
	SINT64 vidStartPTS;
	UINT32 vidFrmRate;
	UINT32 vidWidth;
	UINT32 vidHeight;
	UINT32 vidFrmCnt;
	#if RTSP_PB_CODEC_MJPG
	SINT32 vidTargetDiff;
	SINT32 vidJpegIdx;
	UINT8* jfifBuf;
	#else
	UINT32 vidEncHandle;
	UINT32 vidEncCntr;
	UINT32 vidPrevSysTm;
	#endif
	UINT32 vidSeeked;
	SINT64 vidPushPts;

	UINT32 vidPbHandle;
	YBFW_QUEUE CustMsrcQueue;
	YBFW_THREAD *CustMsrcThread;
	#if !RTSP_PB_CODEC_MJPG
	YBFW_QUEUE encYuvQue;
	#endif

	/* for debug log */
	UINT32 audPts;
	UINT32 audCnt;
	UINT32 vidSentByte;
	#if DBG_LOG_PTS
	UINT32 vidlag[5];
	UINT32 vidlagIn, vidlagOut;
	#endif
	#if DBG_VLC_FILE
	UINT32 fd;
	#endif
} appCustMsrcCtx_t;

typedef struct CustMsrcQueue_s {
	custMsrcCmd_t    cmd;
	union {
		UINT32       array[2];
    	ybfwGfxObj_t yuvObj;
	} param;
	SINT64           pts;
	appCustMsrcCtx_t *c;
	YBFW_SEMAPHORE sem;
} CustMsrcQueue_t;

static void appCustMsrcThread(ULONG param);

static volatile UINT32 rtpPlaybackOpened = 0;

#if RTSP_PB_CODEC_MJPG
/*Non*/
#else

static void appCustEncDone(UINT32 handle, mediaMuxerCusInfo_t *pinfo);
static UINT32 appCustEncYuvGet(UINT32 handle, UINT8 **pptr, struct timeval *tv);
static void appCustEncYuvFree(UINT32 handle, void *ptr);

#endif

UINT32 pbFrameCb(UINT32 handle, frameBufInfo_t *bufInfo, struct timeval *tv, void *rsvd)
{
	ybfwMediaPlayCfg_t *pcfg = *(ybfwMediaPlayCfg_t **)handle;
    appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)pcfg->pExt;
	UINT32 ret;

	SINT64 inputTime, dispTime;

	inputTime = tv->tv_sec * 1000 + tv->tv_usec / 1000;
	if (c->vidStartPTS == -1)	/* initial value. */
		c->vidStartPTS = inputTime;
	inputTime -= c->vidStartPTS;

	if (c->vidSeeked == 1) {
		return SUCCESS;
	}
	else if (c->vidSeeked == 2) {
		c->vidFrmCnt = (UINT32)((inputTime * c->vidFrmRate + 999) / 1000);
		#if !RTSP_PB_CODEC_MJPG
		c->vidEncCntr = c->vidFrmCnt;
		#endif
		c->vidSeeked = 0;
		ybfwProfLogPrintf(0,"#P#pbFrameCb#N#Seek%d/%d", c->vidFrmCnt, (UINT32)inputTime);
	}
	dispTime = c->vidFrmCnt * 1000 / c->vidFrmRate;

	if (inputTime >= dispTime) {
		CustMsrcQueue_t *pQue;

		#if DBG_LOG_PTS
		c->vidlag[c->vidlagIn++] = ybfwMsTimeGet();
		c->vidlagIn %= 5;
		#endif
		RtspPlayPtsProfPrint("#P#pbFrameCb#N#%d", (UINT32)dispTime);
		ybfwGfxObj_t srcObj = {
			.pbuf = bufInfo->pbuf,
			.bufW = bufInfo->width,
			.bufH = bufInfo->height,
			.roiX = bufInfo->roiX,
			.roiY = bufInfo->roiY,
			.roiW = bufInfo->roiW,
			.roiH = bufInfo->roiH,
			.fmt  = bufInfo->fmt,
		};


		c->vidFrmCnt++;

		pQue = ybfwMallocCache(sizeof(CustMsrcQueue_t));
		if (c->CustMsrcQueue && pQue) {
			pQue->cmd = CMD_CUST_MSRC_YUV_CB;
			pQue->c = c;
			pQue->pts = dispTime;

			ybfwGfxObj_t *yuv = &pQue->param.yuvObj;

			#if RTSP_PB_CODEC_MJPG
			/* for JPEG, align to 16 bigger. */
			yuv->roiX = yuv->roiY = 0;
			yuv->roiW = (RTSP_PB_TARGET_W+15) & (~0xf);
			yuv->roiH = (RTSP_PB_TARGET_H+15) & (~0xf);
			yuv->bufW = YUV_BUF_W_ALIGN(yuv->roiW);
			yuv->bufH = YUV_BUF_H_ALIGN(yuv->roiH);	/* for flexible, align to 16. */
			yuv->pbuf = ybfwYuvBufferAlloc(yuv->bufW, yuv->bufH);
			yuv->fmt  = YBFW_GFX_FMT_YUV420_V50;
			appYuvRoiScale(&srcObj, yuv);
			/* crop down to jpeg block. */
			yuv->roiW = YUV_ROI_ALIGN_JPEG(RTSP_PB_TARGET_W);
			yuv->roiH = YUV_ROI_ALIGN_JPEG(RTSP_PB_TARGET_H);
			if ((ret = ybfwOsQueueSend(&c->CustMsrcQueue, (void*)&pQue, TX_NO_WAIT)) != YBFW_SUCCESS) {
				ybfwProfLogPrintf(0, "ybfwOsQueueSend error %x",ret);
				ybfwYuvBufferFree(yuv->pbuf);
				ybfwFree(pQue);
			}

			#else
			YUV_FRAME_SET(yuv, yuv->bufW, yuv->bufH);
			yuv->pbuf = ybfwYuvBufferAlloc(yuv->bufW, yuv->bufH);
			appYuvRoiScale(&srcObj, yuv);
			pQue->pts += 1000;
			if ((ret = ybfwOsQueueSend(&c->encYuvQue, (void*)&pQue, TX_NO_WAIT)) != YBFW_SUCCESS) {
				ybfwProfLogPrintf(0,"ybfwOsQueueSend error %x",ret);
				ybfwYuvBufferFree(yuv->pbuf);
				ybfwFree(pQue);
			}
			#endif
		} else {
			ybfwProfLogPrintf(0,"%s:%d ERR\n", __func__, __LINE__);
		}
	}

    return 0;
}

UINT32 pbAudioCb(UINT32 handle, UINT8 *audAddr, UINT32 decodeSize, struct timeval *ptv, void *rsvd)
{
	ybfwMediaPlayCfg_t *pcfg = *(ybfwMediaPlayCfg_t **)handle;
        appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)pcfg->pExt;
	SINT64 pts;
	UINT8 *data;
	CustMsrcQueue_t *pQue;

	pts = ptv->tv_sec*1000+ptv->tv_usec/1000;
	if(c->audStartPTS == -1){/*record first frame pts*/
		c->audStartPTS = pts;
	}

	if (c->audSeeked == 1) {
		return SUCCESS;
	}
	else if (c->audSeeked == 2) {
		c->audPushPts = pts - 1;
		c->audSeeked = 0;
	}

	pts = pts - c->audStartPTS;
	c->audPts = (UINT32)pts;
	c->audCnt++;

	/*printf("pbAudioCb pts %lld time %d.%d decodeSize %x frm %d\n",(UINT64)ptss,ptv->tv_sec,ptv->tv_usec,decodeSize,frm );*/
	data = ybfwMallocCache(decodeSize);
	if(!data)
		printf("alloc data fail\n");
	pQue = ybfwMallocCache(sizeof(CustMsrcQueue_t));
	if(!pQue)
		printf("alloc pque fail\n");
	if (c->CustMsrcQueue && pQue && data) {
		RtspPlayPtsProfPrint("#P#pbAudioCb#N#%d", (UINT32)pts);
		memcpy(data, audAddr, decodeSize);
		pQue->cmd            = CMD_CUST_MSRC_PCM_CB;
		pQue->param.array[0] = (UINT32)data;
		pQue->param.array[1] = decodeSize;
		pQue->pts            = pts;
		pQue->c              = c;
		if (ybfwOsQueueSend(&c->CustMsrcQueue, (void*)&pQue, TX_NO_WAIT) != YBFW_SUCCESS) {
			if (pQue)
				ybfwFree(pQue);
			if (data)
				ybfwFree(data);
			ybfwProfLogPrintf(0,"%s:%d ERR\n", __func__, __LINE__);
		}
	}
	else {

		if (pQue)
			ybfwFree(pQue);
		if (data)
			ybfwFree(data);
		ybfwProfLogPrintf(0,"%s:%d ERR\n", __func__, __LINE__);
	}

	return 0;
}

static UINT32 appCustMsrcCtrl(appCustMsrcCtx_t *c, UINT32 cmd, UINT32 param)
{
	UINT32 ret = FAIL;
	CustMsrcQueue_t *pQue = ybfwMallocCache(sizeof(CustMsrcQueue_t));

	if (c->CustMsrcQueue && pQue) {
		pQue->cmd            = cmd;
		pQue->param.array[0] = param;
		pQue->c              = c;
		if (ybfwOsSemaphoreCreate(&pQue->sem, "RtpOp", 0) != SUCCESS) {
		}
		else {
			ybfwOsQueueSend(&c->CustMsrcQueue, (void *)&pQue, TX_WAIT_FOREVER);
			ybfwOsSemaphoreGet(&pQue->sem, TX_WAIT_FOREVER);
			ybfwOsSemaphoreDestroy(&pQue->sem);
			ret = SUCCESS;
		}
		ybfwFree(pQue);
	}

	return ret;
}

static void
appCustMsrcFreeAll(
	appCustMsrcCtx_t *c
)
{
	ybfwProfLogPrintf(0, "appCustMsrcFreeAll %p", c);
	if (c) {
		if (c->vidPbHandle) {
			ybfwMediaPlayCfg_t *pcfg = *(ybfwMediaPlayCfg_t **)c->vidPbHandle;
			ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, c->vidPbHandle);
			ybfwMediaPlayDestroy(c->vidPbHandle);
			c->vidPbHandle = 0;
			if (pcfg)
				ybfwFree(pcfg);
		}
		#if !RTSP_PB_CODEC_MJPG
		if (c->vidEncHandle) {
			ybfwMediaRecCfg_t *pcfg = *(ybfwMediaRecCfg_t **)c->vidEncHandle;
			void *nullptr = NULL;
			if (c->encYuvQue)
				ybfwOsQueueSend(&c->encYuvQue, &nullptr, TX_WAIT_FOREVER);
			ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, c->vidEncHandle);
			ybfwMediaRecDestroy(c->vidEncHandle);
			c->vidEncHandle = 0;
			if (pcfg) {
				pcfg->pExt = 0;
				appVideoConfigFree(pcfg);
			}
		}
		#if DBG_VLC_FILE
		if (c->fd) {
			ybfwFsFileClose(c->fd);
			c->fd = 0;
		}
		#endif
		if (c->CustMsrcQueue) {
			ybfwOsQueueDelete(&c->CustMsrcQueue);
			c->CustMsrcQueue = 0;
		}
		#if RTSP_AV_SYNC
		if (c->audQue) {
			ybfwOsQueueDelete(&c->audQue);
			c->audQue = 0;
		}
		#endif
		#if RTSP_PB_CODEC_MJPG
		if (c->jfifBuf) {
			ybfwFree(c->jfifBuf);
			c->jfifBuf = 0;
		}
		#endif
		if (c->encYuvQue) {
			ybfwOsQueueDelete(&c->encYuvQue);
			c->encYuvQue = 0;
		}
		#endif
		ybfwFree(c);
	}
}

static NDKStMediaSrcHandle appCustMsrcOpen(
	NDKStMediaSrcPushBufferFunc push_func,
	void *push_param,
	void *src_arg,
	...
)
{
	UINT32 pbHandler = 0;
	ybfwMediaPlayCfg_t *pcfg = NULL;
	#if !RTSP_PB_CODEC_MJPG
	ybfwMediaRecCfg_t *pRecCfg = NULL;
	#endif
    char suffixName[4] = {0};
    UINT8 len;

	printf("[App playback] open\n");
	ybfwProfLogAdd(0,"#S#PlayBack#N#OPEN");

	appCustMsrcCtx_t *c = ybfwMallocCache(sizeof(appCustMsrcCtx_t));
	if (!c)
		return NULL;

	memset(c, 0, sizeof(appCustMsrcCtx_t));
	c->push_func = push_func;
	c->push_param = push_param;
	strcpy(c->fname, src_arg);

	pcfg = ybfwMallocCache(sizeof(ybfwMediaPlayCfg_t));
	if (!pcfg) {
		ybfwFree(c);
		return NULL;
	}
	memset(pcfg, 0x0, sizeof(ybfwMediaPlayCfg_t));
	sprintf(pcfg->filename, c->fname);

    len = strlen((char *)pcfg->filename);
    strcpy(suffixName, (char *)pcfg->filename+len-3);

    if(strcmp(suffixName, "AVI") == 0){
        pcfg->demuxSel = MEDIA_FILE_TYPE_AVI;
        pcfg->demuxer.avi.plyVidStreamId = 0;// 0 or 1 for play diffent stream when using multi-track avi
    }
    else if(strcmp(suffixName, "MP4") == 0){
        pcfg->demuxSel = MEDIA_FILE_TYPE_MP4;
    }
    else{
        pcfg->demuxSel = MEDIA_FILE_TYPE_MOV;
    }

	appMediaInit();
	if ((pbHandler = ybfwMediaPlayCreate(pcfg)) == 0) {
		printf("file %s open failed!\n", c->fname);
		ybfwFree(pcfg);
		ybfwFree(c);
		c = NULL;
	}
	else if (ybfwMediaCtrl(YBFW_MEDIA_CTRL_PLAY_ATTR_PARSE, pbHandler) != YBFW_SUCCESS) {
		printf("file %s parsing failed!\n", c->fname);
		appCustMsrcFreeAll(c);
		c = NULL;
	}
    #if SP5K_2GB_MEMORY
	else if (pcfg->vattr[0].width > 3840 && pcfg->vattr[0].height > 2160) {
		printf("\n6K3k\n");
		appCustMsrcFreeAll(c);
		c = NULL;

	}
	else if (pcfg->vattr[0].width >= 3840 && pcfg->vattr[0].height >= 2160&& (pcfg->vattr[0].frameRate/pcfg->vattr[0].frameRateBase)>30) {
		printf("\n>4K2k\n");
		appCustMsrcFreeAll(c);
		c = NULL;

	}
    #endif
	else {
		frameBufInfo_t frameBuf = {.roiW = RTSP_PB_TARGET_W};

		/* fetch video config for RTP playback. */
		c->vidDurationTime = pcfg->duration;
		c->vidStartPTS     = -1;
		c->vidPushPts      = -1;
		c->vidFrmRate      = pcfg->vattr[0].frameRate/pcfg->vattr[0].frameRateBase;
		if (c->vidFrmRate > RTSP_PB_TARGET_FRAMERATE)
			c->vidFrmRate = RTSP_PB_TARGET_FRAMERATE;

		/* To apply decoder's scaler, use stream lib rule to decide output YUV size by assiged pframe. */
		appVideoStreamDecRoiSet(&frameBuf, &pcfg->vattr[0], &pcfg->vout[0]);
		pcfg->vout[0].maxResWxH = -1;

		#if RTSP_PB_CODEC_MJPG

		c->vidCodec        = YBFW_MEDIA_VIDEO_MJPG;
		c->vidWidth       = YUV_ROI_ALIGN_JPEG(RTSP_PB_TARGET_W);
		c->vidHeight      = YUV_ROI_ALIGN_JPEG(RTSP_PB_TARGET_H);
		c->vidJpegIdx     = 60;

		#else

		c->vidCodec        = YBFW_MEDIA_VIDEO_H264;
		c->vidWidth       = YUV_ROI_ALIGN(RTSP_PB_TARGET_W);
		c->vidHeight      = YUV_ROI_ALIGN(RTSP_PB_TARGET_H);
		appVideoConfig_t vidData = {
			.w = c->vidWidth,
			.h = c->vidHeight,
			.video = VIDEO_FORMAT_H264,
			.audio = YBFW_MEDIA_AUDIO_UNKNOWN,
			.frameRate = RTSP_PB_TARGET_FRAMERATE,
		};
		pRecCfg = appVideoBasicConfigAlloc(&vidData);
		if (pRecCfg) {
			pRecCfg->vin[0].codec.h264.vuiTag = 1<<4;
			pRecCfg->vin[0].stopMode = 1;
			pRecCfg->vin[0].fpYuvPtrInputCb = appCustEncYuvGet;
			pRecCfg->vin[0].fpYuvPtrInputFreeCb = appCustEncYuvFree;
			pRecCfg->vin[0].bitRate =
				pRecCfg->vin[0].bitRateMax =
				pRecCfg->vin[0].bitRateMin = RTSP_PB_TARGET_BITRATE;
			pRecCfg->vin[0].fifoDepth = RTSP_PB_TARGET_FRAMERATE;
			pRecCfg->vin[0].codec.h264.gopType = YBFW_MEDIA_GOP_TYPE_IPPP;
			pRecCfg->vin[0].codec.h264.gopNo	= 15;	/* 15, 45, 75, ... */
			pRecCfg->vin[0].codec.h264.qpCfgI	= (RTSP_PB_QP_MAX<<0) | (RTSP_PB_QP_MAX<<8) | (RTSP_PB_QP_MIN<<16);
			pRecCfg->vin[0].codec.h264.qpCfgP	= (RTSP_PB_QP_MAX<<0) | (RTSP_PB_QP_MAX<<8) | (RTSP_PB_QP_MIN<<16);

			printf("decode yuv size=%dx%d\n", pcfg->vout[0].targetW, pcfg->vout[0].targetH);
			frameBufInfo_t *yuv = &pRecCfg->vin[0].yuvInfo;
			YUV_FRAME_SET(yuv, yuv->width, yuv->height);
			pRecCfg->muxSel = YBFW_MEDIA_FILE_TYPE_CUS;
			pRecCfg->mux.muxer.cus.fpDataCb = appCustEncDone;
			c->vidEncHandle = ybfwMediaRecCreate(pRecCfg);
			if (pRecCfg->pExt)
				ybfwFree(pRecCfg->pExt);
			pRecCfg->pExt = (void *)c;
			#if DBG_VLC_FILE
			c->fd = ybfwFsFileOpen((UINT8 *)"D:\\dump.h264", YBFW_FS_OPEN_CREATE);
			#endif
		}
		if (!c->vidEncHandle || ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, c->vidEncHandle) != YBFW_SUCCESS) {
			printf("%s,%d\n", __func__, __LINE__);
			appCustMsrcFreeAll(c);
			appMediaDeini();
			return NULL;
		}
		#endif

		pcfg->vout[0].fpPipDrawCb = pbFrameCb;

		/* fetch video config for RTP playback. */
		if (pcfg->aoutNo) {
			c->audCodec       = YBFW_MEDIA_AUDIO_PCM;
			c->audStartPTS    = -1;
			c->audBytesPerSec = pcfg->aattr[0].sampleBits * pcfg->aattr[0].sampleRate * pcfg->aattr[0].channels / 8;
			c->audSampleBits  = pcfg->aattr[0].sampleBits;
			c->audChannels    = pcfg->aattr[0].channels;

			/* audio output */
			pcfg->aout[0].devName      = (UINT8*)"MIXER0";
			pcfg->aout[0].trackId      = YBFW_AUD_MIXER_PLAY_TRK_MEDIA;
			pcfg->aout[0].fpFramePcmCb = pbAudioCb;
			pcfg->audioDis = 1;	/* audio is not going to send to mixer. */
			#if RTSP_AV_SYNC
			if (ybfwOsQueueCreate(&c->audQue, "CustAudQ", 1, NULL, 100*sizeof(UINT32)) != YBFW_SUCCESS) {
				printf("%s,%d\n", __func__, __LINE__);
				appCustMsrcFreeAll(c);
				c = NULL;
			}
			#endif
		}

		c->vidPbHandle   = pbHandler;

		#if 0
		printf("[App playback] file V frameRate %d frbase %d  w %d h %d dur %d ms\n",pcfg->vattr[0].frameRate,pcfg->vattr[0].frameRateBase,pcfg->vattr[0].width,pcfg->vattr[0].height,pcfg->duration);
		printf("[App playback] file A codec %d  sampleBits %d sampleRate %d chan %d BytesPerSec %d\n",pcfg->aattr[0].codec,pcfg->aattr[0].sampleBits,pcfg->aattr[0].sampleRate,pcfg->aattr[0].channels,c->audBytesPerSec);
		printf("[App playback] out frameRate %d  w %d h %d\n",c->vidFrmRate,pcfg->vout[0].targetW,pcfg->vout[0].targetH);
		#endif

		/* to wait a few seconds at the beginning, 15fps video + 50packet/sec audio. FIXME: audio number is 50/s?*/
        if (ybfwOsQueueCreate(&c->CustMsrcQueue, "CustMsrcQueue", 1, NULL, (15+50)*sizeof(UINT32)) != YBFW_SUCCESS) {
            printf("%s,%d\n", __func__, __LINE__);
			appCustMsrcFreeAll(c);
			c = NULL;
        }
		else if ((c->CustMsrcThread = ybfwOsThreadCreate("CustMsrcThread", appCustMsrcThread, c->CustMsrcQueue,18,0,0,TX_AUTO_START)) == NULL) {
            printf("%s,%d\n", __func__, __LINE__);
			appCustMsrcFreeAll(c);
			c = NULL;
		}
		#if !RTSP_PB_CODEC_MJPG
		else if (ybfwOsQueueCreate(&c->encYuvQue, "CustYuvQ", 1, NULL, 6*sizeof(UINT32)) != YBFW_SUCCESS) {
            printf("%s,%d\n", __func__, __LINE__);
			appCustMsrcFreeAll(c);
			c = NULL;
		}
		#endif
		else {
			/* everything is set. ready to go. */
			appCustMsrcCtrl(c, CMD_CUST_MSRC_INIT, 0);
			pcfg->pExt = (void *)c;

			ybfwProfLogAdd(0,"#P#PlayBack#N#start");

			if (ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, pbHandler) != YBFW_SUCCESS) {
				printf("%s,%d\n", __func__, __LINE__);
				appCustMsrcFreeAll(c);
				c = NULL;
			}
		}
	}

	if (c) {
		rtpPlaybackOpened++;
		ybfwProfLogPrintf(0, "appCustMsrcOpen %p", c);
	}
	else {
		appMediaDeini();
	}

	ybfwProfLogAdd(0,"#E#PlayBack#N#OPEN");

	return (NDKStMediaSrcHandle)c;
}

static void appCustMsrcClose(NDKStMediaSrcHandle h)
{
	appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)h;

	printf("[playback] close %d %d %d %d\n",c->vidDurationTime,c->vidFrmCnt*1000/c->vidFrmRate,c->vidFrmCnt,c->vidFrmRate);
	ybfwProfLogPrintf(0,"#S#PlayBack#N#CLOSE %d %d,%dkbps",c->vidDurationTime,c->vidFrmCnt*1000/c->vidFrmRate, (c->vidSentByte/c->vidDurationTime)*8);

	if (c) {
		appCustMsrcCtrl(c, CMD_CUST_MSRC_CLOSE, 0);

		appCustMsrcFreeAll(c);

		appMediaDeini();

		rtpPlaybackOpened--;
	}

	ybfwProfLogPrintf(0,"#E#PlayBack#N#CLOSE");
}


static UINT32 appCustMsrcPause(NDKStMediaSrcHandle h)
{

	printf("[App playback] pause\n");
	ybfwProfLogAdd(0,"#P#PlayBack#N#PAUSE");

	return appCustMsrcCtrl((appCustMsrcCtx_t*)h, CMD_CUST_MSRC_PAUSE, 0);
}

static UINT32 appCustMsrcResume(NDKStMediaSrcHandle h)
{
	printf("[App playback] resume\n");
	ybfwProfLogAdd(0,"#P#PlayBack#N#RESUME");

	return appCustMsrcCtrl((appCustMsrcCtx_t*)h, CMD_CUST_MSRC_RESUME, 0);
}

static UINT32 appCustMsrcSeekTo(NDKStMediaSrcHandle h, UINT32 position)
{
	UINT32 ret;
	appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)h;

	printf("[App playback] seek position %d\n",position);
	ybfwProfLogPrintf(0,"#S#PlayBack#N#SEEK %d",position);

	c->audSeeked = c->vidSeeked = 1;
	ret = appCustMsrcCtrl(c, CMD_CUST_MSRC_SEEK, position);

	ybfwProfLogAdd(0,"#S#PlayBack#N#SEEKed");

	return ret;
}

/*return TRUE: at the end of source. FALSE: not.*/
static BOOL appCustMsrcEndOfSrc(NDKStMediaSrcHandle h)
{
	appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)h;
	UINT32 ret = FALSE;
	UINT32 truncateTime;
	UINT32 temp = c->vidDurationTime * c->vidFrmRate / 1000;

	if(temp<=0) {
		truncateTime = 0;
	}
	else {
		truncateTime = (temp - 1) * 1000 / c->vidFrmRate;
	}
	/* host's first frame PTS is 0. */
	if(c->vidPushPts >= truncateTime){
		printf("[App playback] EndOfSrc %d %d\n", (UINT32)c->vidPushPts, truncateTime);
		ybfwProfLogAdd(0,"#P#PlayBack#N#EndOfSrc");

		ret = TRUE;
	}

	return ret;
}

static UINT32 appCustMsrcGetAttr(NDKStMediaSrcHandle h, UINT32 attr, UINT32 *val)
{
	appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)h;

	UINT32 ret = SUCCESS;

	switch (attr) {
	case NDK_MEDIA_ATTR_WIDTH:
		*val = c->vidWidth;
		break;
	case NDK_MEDIA_ATTR_HEIGHT:
		*val = c->vidHeight;
		break;
	case NDK_MEDIA_ATTR_VIDEO_FRAME_RATE:
		*val = c->vidFrmRate;
		break;
	case NDK_MEDIA_ATTR_VIDEO_AVG_BITRATE:
		*val = RTSP_PB_TARGET_BITRATE;
		break;
	case NDK_MEDIA_ATTR_DURATION:
		*val = c->vidDurationTime;
		break;
	case NDK_MEDIA_ATTR_AUDIO_SAMPLE_BITS:
		*val = c->audSampleBits;
	break;
	case NDK_MEDIA_ATTR_AUDIO_SAMPLE_RATE:
		if (c->audSampleBits && c->audChannels)
			*val = c->audBytesPerSec/(c->audSampleBits/8)/c->audChannels;
		else
			*val = 0;
		break;
	case NDK_MEDIA_ATTR_AUDIO_CHANNELS:
		*val = c->audChannels;
		break;
	case NDK_MEDIA_ATTR_VIDEO_CODEC:
		if (c->vidCodec == YBFW_MEDIA_VIDEO_MJPG)
			*val = NDK_MEDIA_CODEC_MJPG;
		else if (c->vidCodec == YBFW_MEDIA_VIDEO_H264)
			*val = NDK_MEDIA_CODEC_H264;
		else if (c->vidCodec == YBFW_MEDIA_VIDEO_HEVC)
			*val = NDK_MEDIA_CODEC_HEVC;
		break;
	case NDK_MEDIA_ATTR_AUDIO_CODEC:
 		if (c->audCodec == YBFW_MEDIA_AUDIO_PCM)
			*val = NDK_MEDIA_CODEC_PCM;
		else
			*val = NDK_MEDIA_CODEC_UNKNOWN;
		break;
	default:
		printf("[App playback] unknown attr %x required\n", attr);
		ret = FAIL;
		break;
	}
	return  ret;
}

static void appCustMsrcFreeBufObj(NDKStMediaBufferObject obj)
{
	NDKStMediaBuffer *mb = (NDKStMediaBuffer *)obj;
	if (mb) {
		/* if (cbuf->bAudio) */
		/* Video and audio use the same variable */
		ybfwFree(mb->data);
		ybfwFree(mb);
	}
}


static NDKStMediaSrcCallback g_msrc_cb;

void appCustMsrcInit(void)
{
 	printf("[App playback] init\n");
	g_msrc_cb.open = appCustMsrcOpen;
	g_msrc_cb.close = appCustMsrcClose;
	g_msrc_cb.pause = appCustMsrcPause;
	g_msrc_cb.resume = appCustMsrcResume;
	g_msrc_cb.end_of_source = appCustMsrcEndOfSrc;
	g_msrc_cb.seek_to = appCustMsrcSeekTo;
	g_msrc_cb.get_attribute = appCustMsrcGetAttr;
	g_msrc_cb.free_buffer_object = appCustMsrcFreeBufObj;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_register_mediasrc");
	ndk_st_register_mediasrc(NDK_ST_MEDIASRC_FILE, &g_msrc_cb);
}

UINT32
appCustMsrcConnGet(
	void
)
{
	return rtpPlaybackOpened;
}

static void appCustMsrcSwapAudData16(UINT8 *dst, UINT8 *src, UINT32 length)
{
	/* Address */
	UINT8 *p_dst, *p_src, *p_src_end4;
	/* Data */
	UINT32 src32, dst32;
	UINT16 tmp16;

	p_dst = (UINT8*)dst;
	p_src = (UINT8*)src;
	p_src_end4 = (UINT8*)(((UINT32)src + length) & ~0x03);
	dst32 = 0;

	HOST_ASSERT(!(length & 0x01));
	HOST_ASSERT(!((UINT32)p_dst & 0x01));

	switch (((UINT32)p_src & 0x03) | (((UINT32)p_dst & 0x03) << 4 )) {
	case 0x00: /* All Align to 4 */
		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);
			DSTWR32(p_dst, ((src32 >> 8) & 0x00FF00FF) | ((src32 << 8) & 0xFF00FF00));
		}

		if (!MULTIPLE_OF_4(length)) {
			/* Two bytes left */
			SRCRD16(p_src, tmp16);
			DSTWR16(p_dst, ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00));
		}
		break;

	case 0x02: /* Align to 2 */
		/* [x][x][1][0] ==> [.][.][0][1] */
		SRCRD16(p_src, tmp16);
		dst32 = ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00);
		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);
			/* [3][2][1][0] ==> [0][1][.][.] */
			dst32 |= ((src32 << 8) & 0x00FF0000U) | ((src32 << 24) & 0xFF000000U);
			DSTWR32(p_dst, dst32);
			/* [3][2][.][.] ==> [.][.][2][3] */
			dst32 = ((src32 >> 24) & 0x000000FFU) | ((src32 >> 8) & 0x0000FF00U);
		}

		tmp16 = (UINT16)dst32;

		if (MULTIPLE_OF_4(length)) {
			/* Aligned to 4. Two bytes left*/
			DSTWR16(p_dst, tmp16);
			SRCRD16(p_src, tmp16);
			tmp16 = ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00);
		}

		DSTWR16(p_dst, tmp16);
		break;
	case 0x20: /* Align to 2 */
		/* [3][2][1][0] ==> [x][x][0][1] */
		SRCRD32(p_src, src32);
		tmp16 = ((src32 >> 8) & 0x000000FFU) | ((src32 << 8) & 0x0000FF00U);
		DSTWR16(p_dst, tmp16);
		dst32 = ((src32 >> 8) & 0x0000FF00U) | ((src32 >> 24) & 0x000000FFU);
		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);
			dst32 |= ((src32 << 8) & 0x00FF0000U) | ((src32 << 24) & 0xFF000000U);
			DSTWR32(p_dst, dst32);
			dst32 = ((src32 >> 8) & 0x0000FF00U) | ((src32 >> 24) & 0x000000FFU);
		}

		tmp16 = (UINT16)dst32;

		if (MULTIPLE_OF_4(length)) {
			/* Aligned to 4. Two bytes left*/
			DSTWR16(p_dst, tmp16);
			SRCRD16(p_src, tmp16);
			tmp16 = ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00);
		}

		DSTWR16(p_dst, tmp16);
		break;

	case 0x22: /* Align to 2 */
		/* [x][x][1][0] ==> [x][x][0][1] */
		SRCRD16(p_src, tmp16);
		DSTWR16(p_dst, ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00));

		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);
			DSTWR32(p_dst, ((src32 >> 8) & 0x00FF00FF) | ((src32 << 8) & 0xFF00FF00));
		}

		if (!MULTIPLE_OF_4(length)) {
			/* Two bytes left */
			SRCRD16(p_src, tmp16);
			DSTWR16(p_dst, ((tmp16 >> 8) & 0x00FF) | ((tmp16 << 8) & 0xFF00));
		}
		break;
	case 0x01:
		HOST_ASSERT(!((UINT32)p_dst & 0x03));
		/* [x][2][1][0] ==> [2][.][0][1] */
		U8PTR(dst32)[1] = *p_src++;
		U8PTR(dst32)[0] = *p_src++;
		U8PTR(dst32)[3] = *p_src++;

		/* TODO: i_buffer == 2 ? */

		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);

			/* [3][2][1][0] ==> [.][0][.][.] */
			dst32 |= (src32 << 16) & 0x00FF0000U;

			DSTWR32(p_dst, dst32);

			/* [3][2][1][.] ==> [3][.][1][2] */
			dst32 = (src32 & 0xFF00FF00) | ((src32 >> 16) & 0x000000FFU);
		}

		U8PTR(dst32)[2] = *p_src++;
		DSTWR32(p_dst, dst32);

		if (!MULTIPLE_OF_4(length)) {
			/* Such as 6. 2 bytes left. */
			p_dst[1] = *p_src++;
			p_dst[0] = *p_src++;
		}
		break;

	case 0x03:
		/* [x][x][x][0] ==> [.][.][0][.] */
		HOST_ASSERT(!((UINT32)p_dst & 0x03));
		U8PTR(dst32)[1] = *p_src++;

		while (p_src < p_src_end4) {
			SRCRD32(p_src, src32);

			/* [3][2][1][0] ==> [1][2][.][0] */
			dst32 |= (src32 & 0x00FF00FFU) | ((src32 << 16) & 0xFF000000U) ;

			DSTWR32(p_dst, dst32);

			/* [3][.][.][.] ==> [.][.][3][.] */
			dst32 = (src32 >> 16) & 0x0000FF00;
		}

		tmp16 = dst32;

		if (MULTIPLE_OF_4(length)) {
			/* eg. 4 bytes, 1 + 2 + 1 */
			U8PTR(tmp16)[0] = *p_src++;
			DSTWR16(p_dst, tmp16);

			U8PTR(tmp16)[1] = *p_src++;
		}

		U8PTR(tmp16)[0] = *p_src++;
		DSTWR16(p_dst, tmp16);
		break;

	default:
		printf("src=%p\n", p_src);
		HOST_ASSERT(0);
		break;
	}
}

static BOOL
appCustMsrcPushData(
	appCustMsrcCtx_t *c,
	UINT8 *data,
	UINT32 length,
	UINT32 key,
	SINT64 pts,
	BOOL bAudio
)
{
	NDKStMediaBuffer *mb = ybfwMallocCache(sizeof(NDKStMediaBuffer));

	mb->bufobj       = (NDKStMediaBufferObject)mb;
	mb->f_buftype    = bAudio ? NDK_ST_MEDIABUFFER_AUDIO : NDK_ST_MEDIABUFFER_VIDEO;
	mb->f_keyframe   = key;
	mb->f_paramframe = 0;
	mb->data         = data;
	mb->length       = length;

	if (bAudio)
		mb->duration = 1000 * length / c->audBytesPerSec;
	else
		mb->duration = 1000 / c->vidFrmRate;
	mb->pts.tv_sec   = pts/1000;
	mb->pts.tv_usec  = (pts % 1000) * 1000;

	#if RTSP_AV_SYNC
	if (bAudio) {
		if (c->audQue && pts > c->vidPushPts + 1000 / c->vidFrmRate) {
			#if DBG_LOG_PTS
			printf("Q t=%lld, sz=%d\n", pts, length);
			#endif
			ybfwOsQueueSend(&c->audQue, &mb, TX_WAIT_FOREVER);
		}
		else {
			NDKStMediaBuffer *pmb = NULL;
			while (c->audQue && ybfwOsQueueReceive(&c->audQue, &pmb, TX_NO_WAIT) == YBFW_SUCCESS && pmb) {
				SINT64 t = pmb->pts.tv_sec * 1000 + pmb->pts.tv_usec / 1000;
				//ybfwProfLogPrintf(0, "#P#A#N#%d", (UINT32)t);
				#if DBG_LOG_PTS
				printf("A t=%lld, sz=%d\n", t, pmb->length);
				#endif
				RtspPlayPtsProfPrint("#P#A#N#_%d", (UINT32)t);
				c->push_func(pmb, c->push_param);
				if (t > c->vidPushPts)
					break;
			}
			c->audPushPts = pts;
			//ybfwProfLogPrintf(0, "#P#A#N#%d", (UINT32)pts);
			#if DBG_LOG_PTS
			printf("A t=%lld, sz=%d\n", pts, length);
			#endif
			RtspPlayPtsProfPrint("#P#A#N#%d", (UINT32)pts);
			c->push_func(mb, c->push_param);
		}
	}
	else {
		#if DBG_LOG_PTS
		UINT32 lag = ybfwMsTimeGet() - c->vidlag[c->vidlagOut++];
		c->vidlagOut %= 5;
		printf("V t=%lld, sz=%d, lag=%d\n", pts, length, lag);
		ybfwProfLogPrintf(0, "t=%d,sz=%d,lag=%d,dif=%d", (UINT32)pts, length, lag, (UINT32)(pts-c->audPushPts));
		#endif
		RtspPlayPtsProfPrint("#P#V#N#%d", (UINT32)pts);
		c->push_func(mb, c->push_param);

		c->vidPushPts = pts;


		if (c->vidPushPts + 1000 / c->vidFrmRate >= c->audPushPts) {
			NDKStMediaBuffer *pmb = NULL;
			while (c->audQue && ybfwOsQueueReceive(&c->audQue, &pmb, TX_NO_WAIT) == YBFW_SUCCESS && pmb) {
				SINT64 t = pmb->pts.tv_sec * 1000 + pmb->pts.tv_usec / 1000;
				c->audPushPts = t;
				//ybfwProfLogPrintf(0, "#P#A#N#%d", (UINT32)t);
				#if DBG_LOG_PTS
				printf("A t=%lld, sz=%d\n", t, pmb->length);
				#endif
				RtspPlayPtsProfPrint("#P#A#N#__%d", (UINT32)t);
				c->push_func(pmb, c->push_param);
				if (t > c->vidPushPts)
					break;
			}
		}
	}
	#else	/* #elif !RTSP_AV_SYNC */

	#if DBG_LOG_PTS
	UINT32 lag = ybfwMsTimeGet() - c->vidlag[c->vidlagOut++];
	c->vidlagOut %= 5;
	printf("%c t=%lld, sz=%d, lag=%d\n", bAudio?'A':'V', pts, length, bAudio?0:lag);
	if (!bAudio)
		ybfwProfLogPrintf(0, "t=%d,sz=%d,lag=%d,dif=%d", (UINT32)pts, length, lag, (UINT32)(pts-c->audPushPts));
	#endif

	c->push_func(mb, c->push_param);
	if (!bAudio)
		c->vidPushPts = pts;

	#endif

	return TRUE;
}

#if RTSP_PB_CODEC_MJPG
#if RTSP_PB_BRC
static void
appCustMsrcMjpgBrcDo(
	appCustMsrcCtx_t *c,
	UINT32 vlcSize
)
{
	UINT32 targetVlcSize = RTSP_PB_TARGET_VLC_SIZE / c->vidFrmRate;

	c->vidSentByte += vlcSize;
	c->vidTargetDiff += vlcSize - targetVlcSize;

	if (c->vidTargetDiff > 0) { /* average is more than target */
		UINT32 lowerBound = targetVlcSize * 90 / 100;
		if (vlcSize > lowerBound) {
			UINT32 diff = vlcSize - lowerBound;
			c->vidJpegIdx -=  (diff + targetVlcSize - 1) / targetVlcSize;	/* round up */
		}
	}
	else if (c->vidTargetDiff < 0) { /* average is less than target */
		UINT32 upperBound = targetVlcSize * 110 / 100;
		if (vlcSize < upperBound) {
			UINT32 diff = upperBound - vlcSize;
			c->vidJpegIdx += (diff + targetVlcSize - 1) / (SINT32)targetVlcSize;
		}
	}

	if (c->vidJpegIdx > 95)
		c->vidJpegIdx = 95;
	else if (c->vidJpegIdx < 20)
		c->vidJpegIdx = 20;

	//printf("%d,%d=>%d\n", vlcSize, c->vidTargetDiff, c->vidJpegIdx);
}
#endif
#else	/* !RTSP_PB_CODEC_MJPG */

static BOOL
appCustMsrcPushH264(
	appCustMsrcCtx_t *c,
	UINT8 *data,
	UINT32 length,
	SINT64 pts,
	UINT8 clone,
	UINT8 key
)
{
	BOOL ret = FALSE;
	UINT8 *pbuf = NULL;

	if (clone) {
		pbuf = ybfwMallocCache(length);
		if (pbuf)
			memcpy(pbuf, data, length);
	}
	else {
		pbuf = data;
	}
	//printf("%d, %d, %d\n", c->vidEncCntr, length, key);
	if (pbuf) {
		ret = appCustMsrcPushData(c, pbuf, length, key, pts, FALSE);
		if (ret != TRUE) {
			ybfwFree(pbuf);
			printf("%s: %d\n", __func__, __LINE__);
		}
	}

	return ret;
}

static BOOL
appCustH264Push(
    appCustMsrcCtx_t *c,
    mediaMuxerCusInfo_t *pClone
)
{
	UINT32 i;
	UINT32 hdr_len = 0;
	BOOL ret = FALSE;
	UINT8 *pbuf = pClone->pbuf;
	SINT64 pts = c->vidEncCntr * 1000 / c->vidFrmRate;
	UINT32 clone = 0, key = 0;

	#if DBG_VLC_FILE
	if (c->fd)
		ybfwFsFileWrite(c->fd, pClone->pbuf, pClone->size);
	#endif

	if (c->vidEncCntr == 0) {
		UINT8 *sps = NULL, *pps = NULL;
		UINT32 sps_len = 0, pps_len = 0;

		for (i = 0 ; i < 64 ; i++) {
			if (pbuf[0] == 0 && pbuf[1] == 0 && pbuf[2] == 0 && pbuf[3] == 1) {
				//printf("appCustH264Push-%d:NAL%02x,%d\n", __LINE__, pbuf[4], i);
				//ybfwProfLogPrintf(0, "appCustH264Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				if ((pbuf[4] & 0x1f) == 0x5) {	/* end */
					if (pps && pps_len == 0)
						pps_len = pbuf - pps;
					if (sps && sps_len == 0)
						sps_len = pbuf - sps;
					hdr_len = pbuf - pClone->pbuf;
					key = 1;
					break;
				}
				else if ((pbuf[4] & 0x1f) == 0x7) {
					sps = pbuf;
					if (pps && pps_len == 0)
						pps_len = sps - pps;
					pbuf += 4;
					key = 1;
				}
				else if ((pbuf[4] & 0x1f) == 0x8) {
					pps = pbuf;
					if (sps && sps_len == 0)
						sps_len = pps - sps;
					pbuf += 4;
					key = 1;
				}
				else if ((pbuf[4] & 0x1f) == 0x1) {
					key = 0;
					break;
				}
			}
			pbuf++;
		}
		clone = 1;
		if (sps && sps_len)
			ret = appCustMsrcPushH264(c, sps, sps_len, pts, clone, key);
		if (pps && pps_len)
			ret = appCustMsrcPushH264(c, pps, pps_len, pts, clone, key);
		ret = appCustMsrcPushH264(c, pbuf, pClone->size - hdr_len, pts, clone, key);
	}
	else {
		for (i = 0 ; i < 64 ; i++) {
			if (pbuf[0] == 0 && pbuf[1] == 0 && pbuf[2] == 0 && pbuf[3] == 1) {
				//printf("appCustH264Push-%d:NAL%02x,%d\n", __LINE__, pbuf[4], i);
				//ybfwProfLogPrintf(0, "appCustH264Push-%d:NAL%02x,%d", __LINE__, pbuf[4], i);
				if ((pbuf[4] & 0x1f) == 0x5) {
					hdr_len = i;
					key = 1;
					break;
				}
				else if ((pbuf[4] & 0x1f) == 0x1) {
					key = 0;
					break;
				}
			}
			pbuf++;
		}
		if (!hdr_len) {
			pbuf = pClone->pbuf;
		}
		else {
			clone = 1;
		}
		ret = appCustMsrcPushH264(c, pbuf, pClone->size - hdr_len, pts, clone, key);
	}
	if (clone == 1)
		ybfwFree(pClone->pbuf);

	c->vidEncCntr++;

	return ret;
}
#endif

static void
appCustMsrcThread(
    ULONG param
)
{
    UINT32 err;
    appCustMsrcCtx_t *c;
	YBFW_QUEUE gCustMsrcQueue = param;
    CustMsrcQueue_t *CustMsrcQueueData;
    for (;;)
    {
        CustMsrcQueueData = 0;


        err = ybfwOsQueueReceive(&gCustMsrcQueue,
                            (void *)&CustMsrcQueueData,
                            TX_WAIT_FOREVER);

        if (err != YBFW_SUCCESS) {
			printf("[App playback] > ybfwOsQueueReceive ERROR %d\n", err);
			continue;
		}

        if ( CustMsrcQueueData ) {

            c = CustMsrcQueueData->c;

            if(c == NULL) {
				printf("%s:%d ERR\n", __func__, __LINE__);
				if (CustMsrcQueueData->param.yuvObj.pbuf)
					ybfwYuvBufferFree(CustMsrcQueueData->param.yuvObj.pbuf);
				continue; /*avoid access NULL pointer after close.*/
            }
			#if RTSP_PB_CODEC_MJPG
			if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_YUV_CB) {
				ybfwGfxObj_t *yuvObj = &CustMsrcQueueData->param.yuvObj;

				if (!c->jfifBuf) {	/* for temp vlc buffer which is not eactly encoded size. */
					c->jfifBuf = ybfwMallocCache(RTSP_PB_TARGET_W * RTSP_PB_TARGET_H);
				}
				if (!c->jfifBuf) {
					printf("[App playback] > error: jfif alloc\n");
				}
				else if (c->vidSeeked != 0) {	/* drop everythign while seeking. */
				}
				else {
					ybfwGfxObj_t jpgObj = {	.pbuf = c->jfifBuf,
						.bufW = yuvObj->bufW,	.bufH = yuvObj->bufH,
						.roiW = yuvObj->roiW,	.roiH = yuvObj->roiH,
						.roiY = RTSP_PB_TARGET_W * RTSP_PB_TARGET_H,	/* VLC buffer size. */
						.roiX = c->vidJpegIdx,	/* roiX is compress ration before convert. */
						.fmt  =YBFW_GFX_FMT_JFIF420,};

					if (ybfwGfxObjectConvert(yuvObj, &jpgObj) == YBFW_SUCCESS) {
						UINT32 vlcSize = jpgObj.roiX;	/* roiX is vlc size after convert. */
						void *pVlc = ybfwMallocCache(vlcSize);
						appDmaMemcpy(pVlc, jpgObj.pbuf, vlcSize);
						if (appCustMsrcPushData(c, pVlc, vlcSize, 1, CustMsrcQueueData->pts, FALSE) != TRUE)
							ybfwFree(pVlc);

						#if RTSP_PB_BRC /* brc */
						appCustMsrcMjpgBrcDo(c, vlcSize);
						#endif
					}
				}
				ybfwYuvBufferFree(yuvObj->pbuf);
				ybfwFree(CustMsrcQueueData);
			}
			#else
			if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_VLC_CB) {
				mediaMuxerCusInfo_t *pClone = (mediaMuxerCusInfo_t *)CustMsrcQueueData->param.array[0];
				if (c->vidPrevSysTm == 0)	/* wait for decoder performance dropping sometimes at encoder starting. */
					ybfwOsThreadSleep(1000/c->vidFrmRate);
				else {
					SINT32 t = ybfwMsTimeGet() - c->vidPrevSysTm;
					if (t < 1000 / c->vidFrmRate) {	/*don't push h264 too fast*/
						ybfwOsThreadSleep(1000/c->vidFrmRate-t);
					}
				}
				c->vidPrevSysTm = ybfwMsTimeGet();
				c->vidSentByte += pClone->size;
				if (appCustH264Push(c, pClone) != TRUE) {
					printf("%s: %d\n", __func__, __LINE__);
				}
				ybfwFree(CustMsrcQueueData);
				ybfwFree(pClone);
			}
			#endif /* #if RTSP_PB_CODEC_MJPG */
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_PCM_CB) {
				void *pbuf = (void *)CustMsrcQueueData->param.array[0];
				UINT32 size = CustMsrcQueueData->param.array[1];

				if (c->vidSeeked != 0) {	/* drop everythign while seeking. */
				}
				else {
					if (c->audSampleBits == 16) {
						void *data = ybfwMallocCache(size);
						if (!data) {
							printf("%s: %d\n", __func__, __LINE__);
							ybfwFree(pbuf);
							pbuf = NULL;
						}
						else {
							appCustMsrcSwapAudData16(data, pbuf, size);
							ybfwFree(pbuf);
							pbuf = data;
						}
					}

					if (pbuf) {
						if (!appCustMsrcPushData(c, pbuf, size, 1, CustMsrcQueueData->pts, TRUE)){
							printf("audio transport false!\ns");
							ybfwFree(pbuf);
						}
					}
				}
				ybfwFree(CustMsrcQueueData);
			}
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_PAUSE) {
				ybfwMediaCtrl(YBFW_MEDIA_CTRL_PAUSE, c->vidPbHandle);
				ybfwOsSemaphorePut(&CustMsrcQueueData->sem);
			}
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_RESUME) {
				ybfwMediaCtrl(YBFW_MEDIA_CTRL_RESUME, c->vidPbHandle);
				ybfwOsSemaphorePut(&CustMsrcQueueData->sem);
			}
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_SEEK) {
				ybfwMediaCtrl(YBFW_MEDIA_CTRL_PLAY_SEEK, c->vidPbHandle, CustMsrcQueueData->param.array[0]);
				c->audSeeked = c->vidSeeked = 2;

				#if RTSP_AV_SYNC
				NDKStMediaBuffer *pmb = NULL;
				while (c->audQue && ybfwOsQueueReceive(&c->audQue, &pmb, TX_NO_WAIT) == YBFW_SUCCESS && pmb);
				#endif

				ybfwOsSemaphorePut(&CustMsrcQueueData->sem);
			}
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_INIT) {
				ybfwOsSemaphorePut(&CustMsrcQueueData->sem);
			}
			else if (CustMsrcQueueData->cmd == CMD_CUST_MSRC_CLOSE) {
				ybfwOsSemaphorePut(&CustMsrcQueueData->sem);
				ybfwOsThreadDelete(0);
				break;
			}
        }

    }
}

#if !RTSP_PB_CODEC_MJPG
/* YUV encode for RTP PB */
static void
appCustEncDone(
	UINT32 handle,
	mediaMuxerCusInfo_t *pinfo
)
{
	ybfwMediaRecCfg_t *pcfg = *(ybfwMediaRecCfg_t **)handle;
    appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)pcfg->pExt;
	mediaMuxerCusInfo_t *pClone;

	if ((pClone = ybfwMallocCache(sizeof(mediaMuxerCusInfo_t))) == NULL) {
		printf("%s: %d\n", __func__, __LINE__);
	}
	else if ((pClone->pbuf = ybfwMallocCache(pinfo->size)) == NULL) {
		ybfwFree(pClone);
		printf("%s: %d\n", __func__, __LINE__);
	}
	else {
		UINT32 ret = YBFW_ERROR;

		pClone->size  = pinfo->size;
		pClone->stamp = pinfo->stamp / 1000;
		appDmaMemcpy(pClone->pbuf, pinfo->pbuf, pinfo->size);
		CustMsrcQueue_t *pQue = ybfwMallocCache(sizeof(CustMsrcQueue_t));

		if (c->CustMsrcQueue && pQue) {
			pQue->cmd			 = CMD_CUST_MSRC_VLC_CB;
			pQue->param.array[0] = (UINT32)pClone;
			pQue->c 			 = c;
			ret = ybfwOsQueueSend(&c->CustMsrcQueue, (void *)&pQue, TX_WAIT_FOREVER);
		}
		if (ret != YBFW_SUCCESS) {
			printf("%s: %d\n", __func__, __LINE__);
			if (pQue)
				ybfwFree(pQue);
			ybfwFree(pClone->pbuf);
			ybfwFree(pClone);
		}
	}
}

static UINT32
appCustEncYuvGet(
	UINT32 handle,
	UINT8 **pptr,
	struct timeval *tv
)
{
	UINT32 ret = YBFW_ERROR;
	ybfwMediaRecCfg_t *pcfg = *(ybfwMediaRecCfg_t **)handle;
    appCustMsrcCtx_t *c = (appCustMsrcCtx_t *)pcfg->pExt;
	CustMsrcQueue_t *pQue;

	if (c->encYuvQue && ybfwOsQueueReceive(&c->encYuvQue, &pQue, TX_WAIT_FOREVER) == YBFW_SUCCESS) {
		if (pQue) {
			*pptr       = pQue->param.yuvObj.pbuf;
			tv->tv_sec  = pQue->pts / 1000;
			tv->tv_usec = (pQue->pts % 1000) * 1000;
			ret = YBFW_SUCCESS;
		}
	}

	return ret;
}

static void
appCustEncYuvFree(
	UINT32 handle,
	void *ptr
)
{
	if (ptr)
		ybfwYuvBufferFree(ptr);
}
#endif

/*====RTP playback end====*/

void
appRtpCmd(
	UINT32 argc,
	char *arg[]
)
{
	UINT32 i;
	printf("RTP argc=%d, arg=", argc);
	for (i = 0 ; i < argc ; i++) {
		printf("%s\n", arg[i]);
	}
	if (strcmp(arg[0], "dump") == 0) {
		UINT32 t = ybfwMsTimeGet();
		if (debugInfo.yuvCnt) {
			printf("Rtp preview:\n");
			printf("\tYuvIn: %d, dt=%dms, %dfps\n", debugInfo.yuvCnt
												  , t-debugInfo.yuvT1
												  , (t>debugInfo.yuvT0)?debugInfo.yuvCnt*1000/(t-debugInfo.yuvT0):0);
			printf("\tEnc:   %d, dt=%dms, %dfps\n", debugInfo.vlcCnt
												  , t-debugInfo.vlcT1
												  , (t>debugInfo.vlcT0)?debugInfo.vlcCnt*1000/(t-debugInfo.vlcT0):0);
			printf("\tPush:  %d, dt=%dms, %dfps\n", debugInfo.pushCnt
												  , t-debugInfo.pushT1
												  , (t>debugInfo.pushT0)?debugInfo.pushCnt*1000/(t-debugInfo.pushT0):0);
		}
	}

}
