/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "app_bt_device.h"
#include "ndk_bluetooth.h"
#include "app_bt_utility.h"
#include "gpio_custom.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "ndk_types.h"
/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#if SP5K_BT_GATT

static appBTReadCb_t appBTDataInputCb = NULL ;
static YBFW_QUEUE btGATTTXQueue = 0;
static NDKBtGattServiceHandle	gatt_samp_serv;
static NDKBtGattChrcHandle		gatt_samp_chrc;

typedef struct btGATTSendData
{
	char*	data;
    unsigned int     len;
} btGATTSendData_t;


/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/
static void appBTGATTReadCB(void *buf, unsigned int *len, void *user_data)
{
    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    int ret = 0 ;
	btGATTSendData_t btdata ;
    static int sendlen = 0 ;
    static char* databuff = NULL;
	static char* pdatabuff = NULL;

    if ( sendlen == 0 ) {
		ret = ybfwOsQueueReceive(&btGATTTXQueue, (void *)&btdata, 20 /* wait 20 ms or TX_NO_WAIT */ );
        if (( ret == 0 ) && (btdata.data)) {
			sendlen = btdata.len ;
            databuff = btdata.data ;
			pdatabuff = databuff ;
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] sendlen(%d)",__FUNCTION__,__LINE__,(unsigned int)sendlen);
		} else {
            HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
            *len = 0 ;
        }
	}
	
	if ( sendlen ) {
		if ( sendlen > BT_GATT_MAX_LENGTH ) {
			*len = BT_GATT_MAX_LENGTH ;
		} else {
			*len = sendlen ;
		}
		sendlen = sendlen - *len ;
		memcpy(buf,pdatabuff,*len);
		pdatabuff = pdatabuff + *len ;
		if ( sendlen == 0 ) {
			ybfwFree(databuff);
			databuff = NULL ;
			pdatabuff = NULL ;
		}
	} else {
		*len = 0 ;
	}

    #if BT_DEBUG_MESSAGE
    	if ( *len ) {
    		memdump(buf,*len);
    	}
    #endif
}

static void appBTGATTWriteCB(const void *buf, unsigned int len, void *user_data)
{
    btData_t *btdata ;

    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    #if BT_DEBUG_MESSAGE
        memdump((void *)buf,(UINT32)len);
    #endif

    if (len > 0 ) {
        btdata = ybfwMallocCache(sizeof(btData_t));
        HOST_ASSERT(btdata);
        btdata->btProfile = APP_BT_GATT ;
        btdata->len = len ;
        btdata->data = ybfwMallocCache(len);
        memcpy(btdata->data,buf,len);
        appBTDataInputCb(btdata);
    }
}


UINT32 appBTGATTIndicate(
	char *pBuffer,
	UINT32 len
)
{
    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif


    int sendlen = len ;
	char* pbuff = pBuffer;
	int i = BT_GATT_MAX_LENGTH ;
	
	if ( len > 0) {

        #if BT_DEBUG_MESSAGE
            memdump(pBuffer,(UINT32)len);
        #endif

        pbuff = pBuffer ;
        do {
            if ( sendlen < BT_GATT_MAX_LENGTH ) {
				i = sendlen ;
			} 
            HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_gatt_chrc_indicate");
			ndk_bt_gatt_chrc_indicate(gatt_samp_serv, gatt_samp_chrc, pbuff, i);
			sendlen = sendlen - i ;
			pbuff = pbuff + i ;
		} while (sendlen > 0 );
	} else {
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
        return FAIL;
    }

	return SUCCESS;
}

UINT32 appBTGATTSend(
	char *pBuffer,
	UINT32 len
)
{
    btGATTSendData_t btdata ;

    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
        HOST_ASSERT(pBuffer);
        HOST_ASSERT(len);
        memdump(pBuffer,len);
    #endif

	if ( len > 0) {

        btdata.len = len ;
        btdata.data = ybfwMallocCache(len);
        memcpy(btdata.data,pBuffer,len);

        if (ybfwOsQueueSend(&btGATTTXQueue, (void *)&btdata, 20 /* wait 20 ms or TX_NO_WAIT */ ) != 0) {
			HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ybfwOsQueueSend ERROR",__FUNCTION__,__LINE__);
            ybfwFree(btdata.data);
            return FAIL;
		}

	}

    return SUCCESS;
}


int
appBTGATTInit(
	void
)
{

    if ( btGATTTXQueue == 0 ) {
        if (ybfwOsQueueCreate(&btGATTTXQueue, "btGATTTXQueue", 2, NULL, 16 * sizeof(btGATTSendData_t)) != 0) {
            HOST_ASSERT(0);
        }
    }


	if ( !gatt_samp_serv ) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_gatt_service_create");
		gatt_samp_serv = ndk_bt_gatt_service_create(
			NDK_BT_ARG_GATT_SERV_UUID128, BT_SERVICE_UUID,
			NDK_BT_ARG_GATT_SERV_CHRC_NUM, 1,
			NDK_BT_ARG_GATT_SERV_DESCRIPTION, "random number generator",
			NDK_BT_ARG_END);
		if ( gatt_samp_serv ) {
		    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_gatt_chrc_add");
			gatt_samp_chrc = ndk_bt_gatt_chrc_add(gatt_samp_serv,
				NDK_BT_ARG_GATT_CHRC_UUID128, BT_SERVICE_UUID,
				NDK_BT_ARG_GATT_CHRC_PROPERTIES, NDK_BT_GATT_CHRC_PROP_INDICATE | NDK_BT_GATT_CHRC_PROP_READ | NDK_BT_GATT_CHRC_PROP_WRITE,
				NDK_BT_ARG_GATT_CHRC_DATA_SIZE, sizeof(unsigned int),
				NDK_BT_ARG_GATT_CHRC_READ_CB, appBTGATTReadCB,
				NDK_BT_ARG_GATT_CHRC_WRITE_CB, appBTGATTWriteCB,
				NDK_BT_ARG_GATT_CHRC_USER_DATA, NULL,
				NDK_BT_ARG_END);
			if ( gatt_samp_chrc ) {
			    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_gatt_service_active");
				ndk_bt_gatt_service_active(gatt_samp_serv);
			} else {
			    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_gatt_service_destroy");
				/* free gatt_samp_serv */
				ndk_bt_gatt_service_destroy(gatt_samp_serv);
				gatt_samp_serv = 0;
				return -1 ;
			}
		} else {
			return -1 ;
		}
	}
	return 0 ;
}


void
appBTGATTStop(void)
{
    btData_t *btdata;
    int ret = 0 ;

	if ( gatt_samp_chrc ) {
		HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s(%d)] free gatt_samp_chrc",__FUNCTION__,__LINE__);
		gatt_samp_chrc = 0;
	}

	if ( gatt_samp_serv ) {
		HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s(%d)] free gatt_samp_serv",__FUNCTION__,__LINE__);
		ndk_bt_gatt_service_destroy(gatt_samp_serv);
		gatt_samp_serv = 0;
	}

    if( btGATTTXQueue ) {
		HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s(%d)] free btTXQueue\n",__FUNCTION__,__LINE__);
		do {
			btdata = 0 ;
			ret = ybfwOsQueueReceive(&btGATTTXQueue, (void*)btdata, 20 /* wait 20 ms or TX_NO_WAIT */ );
			if (( ret == 0 ) && (btdata)) {
                if ( btdata->data ) {
                    ybfwFree(btdata->data);
                }
				ybfwFree(btdata);
			}
		} while (ret == 0);
        ybfwOsQueueDelete(&btGATTTXQueue);
        btGATTTXQueue = 0 ;
    }
}


void
appBTGATTSetCB(void *pCB)
{
    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    HOST_ASSERT(pCB);
    appBTDataInputCb = (appBTReadCb_t) pCB ;
}


#endif

#endif /* BTON=YES in project defination */
