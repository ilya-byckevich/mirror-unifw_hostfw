/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/errno.h>
#include "common.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"
#include "app_bt_utility.h"
#include "app_bt_smartcfg.h"
#include "app_bt_device.h"
#include "ndk_types.h"
#include "linux/socktypes.h"
#include "app_bt_smartcfg.h"

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
UINT8 gBT_NAME[BT_NAME_SIZE+1] = {0};
UINT8 gBT_PWD[BT_PWD_SIZE+1] = {0};
UINT8 gBTMACAddr[IF_HWADDRSIZE]={0};
static UINT32 gbt_init_state = 0;

static YBFW_THREAD *thrappBTDoInit = NULL;
static BOOL bBTDoInit = FALSE;
#if ICAT_WIFI /* BT Smart Config Use */
extern void appSmartConfigReadCallBack(void *buf);
static appBTReadCb_t appBTDataCb = appSmartConfigReadCallBack ;
#else
static appBTReadCb_t appBTDataCb = appBTReadCallBack ;
#endif
/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/
void memdump(void *mem, UINT32 len);

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/

void appBTInitStateSet(UINT32 state)
{
	gbt_init_state |= state;
}

void appBTInitStateUnset(UINT32 state)
{
	gbt_init_state &= ~state;
}

UINT32 appBTInitStateGet(void)
{
	return gbt_init_state;
}


UINT32 appBTPreocessPowerOff(
    UINT8 param)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if ( !param ) {
        printf("Set Mode FAIL\n");
        return FAIL ;
    }

	if ( appBTPowerState (YBFW_POWER_OFF) == FAIL ) {
		return FAIL ;
	}

	if ( param == APP_BT_POWEROFF ) {
        /* goto Power Down Flow */
//		void appPsuedoPowerOff(UINT32 powerDown);
//        appPsuedoPowerOff(TRUE);

    }

	/* mw: temporary removed to pass compile */
	#if 0
    if ( param == APP_BT_HIBERATION ) {
        /* goto Hibernation Flow */
        extern void appPowerHibernateInit(void);
        appPowerHibernateInit();
    }
	#endif
    return SUCCESS ;
}


UINT32
appBTPowerState(
	UINT32 parm
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    UINT32 err = FAIL ;
    UINT8 bt_mode = 0 ;
    UINT32 pwrSRC = 0 ;

	err = ybfwRtcExtraGet(&bt_mode, RTC_EXTRA_ADDR_BT, 1);
	if( SUCCESS != err ) {
		bt_mode = 0 ;
		printf("ybfwRtcExtraGet failed!\n");
	}

	HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] bt_mode = 0x%x",__FUNCTION__,__LINE__,bt_mode);

    switch (parm) {
	case YBFW_POWER_ON:
		err = ybfwPwrDetectGet(YBFW_PWR_SRC_CUSTOM, &pwrSRC);
		if(SUCCESS != err) {
			pwrSRC = 0 ;
			printf("ybfwPwrDetectGet failed!\n");
		}

		bt_mode = bt_mode & RTC_EXTRA_BT_ENABLE_BIT ;

		/* check power on source definintion */
		if( (bt_mode) && (BT_POWER_SOURCE == pwrSRC) )
			return SUCCESS ; /* SUCCESS */
		break;
	case YBFW_POWER_OFF:
		if (!(bt_mode & RTC_EXTRA_BT_ENABLE_BIT)) {

			bt_mode = bt_mode | RTC_EXTRA_BT_ENABLE_BIT ;

			if ( FAIL == ybfwRtcExtraSet(&bt_mode, RTC_EXTRA_ADDR_BT, 1)) {
				printf("Set RTC FAIL\n");
				return FAIL ;
			}
			err = ybfwRtcExtraGet(&bt_mode, RTC_EXTRA_ADDR_BT, 1);
			if( SUCCESS != err ) {
				printf("ybfwRtcExtraGet failed!\n");
			}
		}
		return SUCCESS ;
		break;
	default:
		break;
	}
    return FAIL ;
}

/*----------------------------BT SETTING -------------------------------------------*/
/* Source Code in app_sd_log.c */
#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
UINT32 iniParseFile(UINT32* fd, UINT32 (*handler)(void*, const char*, const char*,const char*), void* user);
#define MAX_STRING_SIZE	50
static UINT32 appBTSettingParser(void* user, const char* section, const char* name,
                  const char* value)
{
/*
INI file content
[BTSETTING]
BT_NAME=iCatchBLE   ; BT Scan Device Name
BT_PWD=0000     	; BT PAIR Password
*/
	int length = 0 ;

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    static char prev_section[MAX_STRING_SIZE] = "";

    if (strcmp(section, prev_section)) {
        strncpy(prev_section, section, sizeof(prev_section));
        prev_section[sizeof(prev_section) - 1] = '\0';
    }

	printf("[%s]%d %s = %s\n",__FUNCTION__,__LINE__, name, value);

	if (MATCH(BT_SECTION, "BT_NAME")) {
		length = strlen((const char *)value) ;
		if ( length > BT_NAME_SIZE ) {
			length = BT_NAME_SIZE ;
		}
		sprintf((char *)gBT_NAME, value);
		printf("BT NAME = %s\n",gBT_NAME);
		memset(prev_section, 0x00 , MAX_STRING_SIZE);
	} else if (MATCH(BT_SECTION, "BT_PWD")) {
		length = strlen((const char *)value) ;
		if ( length > BT_PWD_SIZE ) {
			length = BT_PWD_SIZE ;
		}
		sprintf((char *)gBT_PWD, value);
		printf("BT PWD = %s\n",gBT_PWD);
		memset(prev_section, 0x00 , MAX_STRING_SIZE);
	} else {
        return 0;  /* unknown section/name, error */
    }
    return 1;

}


UINT32 appCreateAllDirs(UINT8* filename)
{
	char dir[128 + 1], *prev, *next;

	if (fsIsFileExists(filename) == SUCCESS)
		return SUCCESS;

	memset(dir, 0, 128 + 1);

	prev = strchr((const char *)filename, '/');
	if (!prev)
		return FAIL;

	strncat(dir, (const char *)filename, prev - (const char *)filename);

	while (prev) {
		next = strchr(prev + 1, '/');
		if (!next)
			break;

		if (next - prev == 1) {
			prev = next;
			continue;
		}

		strncat(dir, prev, next - prev);
		ybfwFsDirMake((const UINT8*)dir);
		prev = next;
	}

	return 0;
}


UINT32
appBTUpdateSetting(
	UINT8 *btname ,
	UINT8 *btpwd
)
{

	UINT32 fd;

	appCreateAllDirs((UINT8 *)BT_SETTINGS_FILE);

	fd = ybfwFsFileOpen((UINT8 *)BT_SETTINGS_FILE, YBFW_FS_OPEN_CREATE | YBFW_FS_OPEN_RDWR );

	if ( fd == 0 ) {
		printf("ybfwFsFileOpen FAIL\n");
		return FAIL ;
	}

	ybfwFsFileWrite(fd, (UINT8 *) ("[" BT_SECTION "]"), 2 + strlen((const char *)BT_SECTION));
	ybfwFsFileWrite(fd, (UINT8 *) "\r\nBT_NAME=", 10);
	ybfwFsFileWrite(fd, btname , strlen((const char *)btname));
	ybfwFsFileWrite(fd, (UINT8 *) "\r\nBT_PWD=", 9);
	ybfwFsFileWrite(fd, btpwd , strlen((const char *)btpwd));
	ybfwFsFileWrite(fd, (UINT8 *) "\r\n", 2);
	ybfwFsFileClose(fd);
	return SUCCESS ;
}


UINT32
appBTChangeName(
	UINT8 *btname
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_ioctl");
    if ( ndk_bt_ioctl(NDK_BT_IOCS_LOCAL_NAME, (const char *)btname) != 0 ) {
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "set btname FAIL");
        return FAIL ;
    }

	return SUCCESS ;
}


UINT32
appBTGetSetting(
	void
)
{
	UINT32 fd;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	memset(gBT_NAME,0,BT_NAME_SIZE+1);
	memset(gBT_PWD,0,BT_PWD_SIZE+1);

	if (fsIsFileExists( (UINT8 *) BT_SETTINGS_FILE ) != SUCCESS) {
		/* Use Default BT_NAME / BT_PWD */
		sprintf((char *)gBT_NAME, BT_DEVICE_NAME);
		sprintf((char *)gBT_PWD, BT_DEVICE_PWD);
		return appBTUpdateSetting( (UINT8 *) BT_DEVICE_NAME, (UINT8 *) BT_DEVICE_PWD);
	}

	fd = ybfwFsFileOpen((UINT8 *)BT_SETTINGS_FILE, YBFW_FS_OPEN_RDONLY);

	HOST_ASSERT(fd);

    iniParseFile(&fd, appBTSettingParser , NULL);

	ybfwFsFileClose(fd);
	return SUCCESS ;
}

/*---------------------------------    BT Utility -------------------------------------------*/

int
appBTGetPinCode(
    char *pbuf
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	int len = strlen((const char *)gBT_PWD) ;
	sprintf(pbuf, (const char *)gBT_PWD);
	printf("<<PIN: %s>>\n", pbuf);
	if ( BT_PWD_SIZE < len ) {
		return BT_PWD_SIZE ;
	}
	return len ;
}


UINT32
appBTGetMacAddr(
	UINT8 *btmacbuff
)
{

	char addr[18];
    int i = 0 ;
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	if ( gBTMACAddr[0] != 0 ) {
		memcpy(btmacbuff,gBTMACAddr,IF_HWADDRSIZE);
		return SUCCESS;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_ioctl");
	if (ndk_bt_ioctl(NDK_BT_IOCG_BDADDR, addr) == 0) {
		printf("%s\n",addr);
		/* to do */
        memset(gBTMACAddr , 0,IF_HWADDRSIZE);
        for ( i = 0 ; i < 6 ; i++ ) {
            gBTMACAddr[i*2] = addr[i*3];
            gBTMACAddr[i*2+1] = addr[i*3+1];
        }
        memcpy(btmacbuff , gBTMACAddr ,IF_HWADDRSIZE);
        printf("%s\n",btmacbuff);
		return SUCCESS;
	}
	return FAIL;
}


void appBTReadCallBack(void *buf)
{
	HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]\n",__FUNCTION__,__LINE__);
    btData_t *btdata = buf ;
    if (btdata->len ) {
        memdump(btdata->data, (UINT32)btdata->len);
    }
}


/*---------------------------------    BT init -------------------------------------------*/

static int
appBTSDPServerInit(
	const char * stor_dir
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	int err = 0 ;
	UINT32 state;
	AppBtGlobConf *conf = appBTGetGlobConf();

	state = appBTInitStateGet();
	if (state & BT_SDPSERVER_INIT) {
		return 0;
	}

	/* Start SDP server */
	err = ndk_bt_sdp_start_server(
			NDK_BT_ARG_SDP_SERVER_STORAGE_DIR, stor_dir,
			NDK_BT_ARG_SDP_SERVER_IO_CAPIBILITY, conf->io_capability,
			NDK_BT_ARG_SDP_SERVER_PINCODE_CB, appBTGetPinCode,
			NDK_BT_ARG_SDP_SERVER_CLASS_OF_DEVICE, BT_DEVICE_CLASS,
			NDK_BT_ARG_SDP_SERVER_BT_MODE, conf->bt_mode,
			NDK_BT_ARG_END);

	if (0 != err) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "ndk_bt_sdp_start_server init fail %d",err);
		return -1;
	}

	appBTInitStateSet(BT_SDPSERVER_INIT);
	return 0;
}

static void
appBTSDPServerStop(
	void
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_sdp_stop_server");
	ndk_bt_sdp_stop_server();
    appBTInitStateUnset(BT_SDPSERVER_INIT);
}

BOOL appBTStartStateGet(void)
{
    BOOL state_ret = FALSE;

    if ((BT_INT & BT_DO_ASYNC) == BT_DO_ASYNC){
        if(bBTDoInit)
            state_ret = TRUE;
    }
    return state_ret;
}

/*
 * Shadow init just control apater to act like power off
 * but BT module is still working, so we dont have to init again.
 * For ICOM-3668
 */
#define ENABLE_SHADOW_INIT 0
#if ENABLE_SHADOW_INIT
	int shadow_init = 0;
#endif

static void appBTDoInit(
	UINT32 btParm
)
{
	int ret = 0 ;
	UINT32 state;
	UINT8 mac[IF_HWADDRSIZE];
	int index = 0 ;
	int i = 0 ;

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");

#if ENABLE_SHADOW_INIT
	printf("BT shadow init [enable]\n");
	if (shadow_init == 1){
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_adapter_discoverable");
		ndk_bt_adapter_discoverable(1);
		return;
	}
#endif

	bBTDoInit = TRUE;

	#if 0
	/* work around */
	ybfwFsDirChange((const UINT8 *)"B:/UDF/");
	#endif

	appBTGetSetting();

    if ((btParm & BT_HCI_LOAD) == BT_HCI_LOAD){

        ret = appBTHCIEnable(0);
        if ( ret ) {
            HOST_PROF_LOG_PRINT(LEVEL_ERROR, "HCI init ERROR");
            goto ERROR_EXIT;
        }
    }


    if ((btParm & BT_SDPSERVER_INIT) == BT_SDPSERVER_INIT){
        ret = appBTSDPServerInit(BT_STOR_FOLDER);
        if ( ret ) {
            HOST_PROF_LOG_PRINT(LEVEL_ERROR, "SDP Server init ERROR");
            goto ERROR_EXIT;
        }
    }

    #if SP5K_BT_GATT
    if ((btParm & BT_GATT_INIT) == BT_GATT_INIT) {
        state = appBTInitStateGet();
        if (!(state & BT_GATT_INIT)) {
            appBTGATTSetCB(appBTDataCb);
            ret = appBTGATTInit();
            if ( ret ) {
                HOST_PROF_LOG_PRINT(LEVEL_ERROR, "HCI init ERROR");
                goto ERROR_EXIT;
            }
            appBTInitStateSet(BT_GATT_INIT);
        }
    }
    #endif

    #if SP5K_BT_SPP
    if ((btParm & BT_SPP_INIT) == BT_SPP_INIT){

        state = appBTInitStateGet();
        if (!(state & BT_SPP_INIT)) {
            appBTUartTTYDevSetCB(appBTDataCb);

            ret = appBTUartTTYDevInit();
            if ( ret ) {
                HOST_PROF_LOG_PRINT(LEVEL_ERROR, "SPP init ERROR");
                goto ERROR_EXIT;
            }
            appBTInitStateSet(BT_SPP_INIT);
        }
    }
    #endif

    if ( appBTGetMacAddr(mac) == SUCCESS ) {
        index = strlen(BT_DEVICE_NAME) ;

        if ( (!strcmp((const char *)gBT_NAME, BT_DEVICE_NAME )) &&
             (strlen((const char *)gBT_NAME) == index) ) {

            for ( i = 0 ; i < 6 ; i++ ) {
                gBT_NAME[i+index] = toupper(mac[i+6]) ;
            }
            appBTUpdateSetting( (UINT8 *) gBT_NAME, (UINT8 *) gBT_PWD);
            appBTChangeName((UINT8 *) gBT_NAME);
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "%s",gBT_NAME);
        }
    }


    #if 0
    {
        #include "ybfw_gfx_api.h"
        #include "app_osd_api.h"
        #include "app_com_api.h"
        /* Bad idle , put display in here */
        APP_OSD_REFRESH_OFF;
		CLEAR_OSD_SRC;
        appOsd_GfxFontIDSet(YBFW_GFX_FONT_ID_1);
        appOsdLib_TextDraw(2, 150, YBFW_GFX_ALIGN_LEFT, (UINT8 *)gBT_NAME);
        appOsdLib_TextDraw(2, 170, YBFW_GFX_ALIGN_LEFT, (UINT8 *)gBT_PWD);
        APP_OSD_REFRESH_ON;
    }
    #endif

    /* Init BT Smart Config */
	#if (ICAT_WIFI && SP5K_BTON)
    appSmartConfigInit();
	#endif

ERROR_EXIT:
    bBTDoInit = FALSE;
	if ( thrappBTDoInit) {
		ybfwOsThreadDelete(thrappBTDoInit);
        thrappBTDoInit = NULL ;
	}
}


static void appBTDoExit(
    UINT32 btParm
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	#if ENABLE_SHADOW_INIT
		shadow_init = 1;
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_adapter_discoverable");
		ndk_bt_adapter_discoverable(0);

	#else
	    /* Init BT Smart Config */
		#if ICAT_WIFI
	    appSmartConfigExit();
		#endif


	    #if SP5K_BT_GATT
	    if ((btParm & BT_GATT_INIT) == BT_GATT_INIT){
	        appBTGATTStop();
	        appBTInitStateUnset(BT_GATT_INIT);
	    }
	    #endif

	    #if SP5K_BT_SPP
	    if ((btParm & BT_SPP_INIT) == BT_SPP_INIT){
	        appBTUartTTYDevStop();
	        appBTInitStateUnset(BT_SPP_INIT);
	    }
	    #endif

	    appBTSDPServerStop();

	    if ((btParm & BT_HCI_LOAD) == BT_HCI_LOAD){
	        appBTHCIDisable(0);
	        appBTInitStateUnset(BT_HCI_LOAD);
	    }

	#endif
}


void appBTStartConnection(
	UINT32 btParm
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if(appBTStartStateGet()){
        printf("Please wait finishing starting connection\n");
        return;
    }

    if ((btParm & BT_DO_ASYNC) == BT_DO_ASYNC) {
        thrappBTDoInit = ybfwOsThreadCreate("BTStart", appBTDoInit, (UINT32)btParm
                           , 20, 0, 0
                           , YBFW_TX_AUTO_START);
        HOST_ASSERT(thrappBTDoInit);
	} else {
        appBTDoInit(btParm);
	}
}

void appBTExitConnection(
	UINT32 btParm
)
{
    appBTDoExit(btParm);
}

/*---------------------------------            -------------------------------------------*/

SINT32 appBlueToothOperation(APP_BT_OPERATION sel , ...)
{
	SINT32 ret = FAIL;
	UINT32 paraTot;
	UINT32 para1, para2, para3, para4;
	para4 = para3 = para2 = para1 = 0;
	va_list args;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	switch(sel)
	{
		case APP_BT_OPERATION_START:     	 		paraTot = 1; break;
		case APP_BT_OPERATION_STOP:     	 		paraTot = 1; break;
        case APP_BT_OPERATION_SEND:     	 		paraTot = 3; break;
        case APP_BT_OPERATION_SETCB:     	 		paraTot = 2; break;
		default: paraTot = 0; break;
	}

	if(paraTot) {
		va_start(args, sel);
		HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "sel=%d", sel);
		para1 = va_arg(args, UINT32);
		HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "para1=%d(0x%X)", para1, para1);
		if(paraTot >= 2) {
			para2 = va_arg(args, UINT32);
			HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "para2=%d(0x%X)", para2, para2);
			if(paraTot >= 3) {
				para3 = va_arg(args, UINT32);
				HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "para3=%d(0x%X)", para3, para3);
				if(paraTot >= 4) {
					para4 = va_arg(args, UINT32);
					HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "para4=%d(0x%X)", para4, para4);
				}
			}
		}
	}

	switch(sel)
	{
        case APP_BT_OPERATION_START:
            appBTStartConnection(para1);
            ret = SUCCESS;
            break;
        case APP_BT_OPERATION_STOP:
            appBTDoExit(para1);
			break;
        case APP_BT_OPERATION_SEND:
            if ( para3 == APP_BT_GATT ) {
                ret =  appBTGATTSend((void *) para1 , (UINT32) para2);
            } else if ( para3 == APP_BT_GATT_INDICATE ) {
                ret =  appBTGATTIndicate((void *) para1 , (UINT32) para2);
            } else if ( para3 == APP_BT_SPP ) {
                ret =  appBTUartTTYDevSend((void *) para1 , (UINT32) para2);
            }
			break;
        case APP_BT_OPERATION_SETCB:
            if ( (void *)para1 != NULL  ) {
                appBTDataCb = (void *)para1;
            }
            ret = SUCCESS;
            break;

		default:
			ret = FAIL;
			break;
	}

	if(paraTot) va_end(args);

	HOST_PROF_LOG_PRINT(LEVEL_DEBUG, "set sel:%d 0x%x",sel ,ret);

	return ret;
}

static AppBtGlobConf btGlobConf = { NULL };

AppBtGlobConf *appBTGetGlobConf()
{
	if (!btGlobConf.conf_dir) {
	#if defined(SP5K_BT_VENDOR_BRCM)
		btGlobConf.conf_dir = "A:/RO_RES/BLUETOOTH/BRCM";
		btGlobConf.hcia_type = "brcm_h4";
		btGlobConf.hcia_extra_opts = "fw=A:/RO_RES/BLUETOOTH/BRCM/BCM4345C0.hcd;tosleep=200";
		btGlobConf.bt_mode = NDK_BT_MODE_LE;
	#elif defined(SP5K_BT_VENDOR_REALTEK)
		btGlobConf.conf_dir = "A:/RO_RES/BLUETOOTH/RTK";
		btGlobConf.hcia_type = "rtk_h5";
		btGlobConf.hcia_extra_opts = NULL;
		btGlobConf.bt_mode = NDK_BT_MODE_DUAL;
	#else
		#error "Unknow BT Vendor"
	#endif

        #if SP5K_BT_GATT
		btGlobConf.io_capability = NDK_BT_IOCAP_NOINPUTNOOUTPUT;
        #else
	        btGlobConf.io_capability = NDK_BT_IOCAP_KEYBOARDONLY;
        #endif
	}

	return &btGlobConf;
}

/*---------------------------------------------------------------------------*/
#define BT_TESTCMD	1 /* Test Code Only */

#if BT_TESTCMD
#include "ykn_bfw_api/ybfw_cmd.h"

#if 0
extern UINT32 halRtcRegWrite(UINT32 addr, UINT32 data);
extern UINT32 halRtcRegRead(UINT32 addr);

static void _cmd_rtc(
   int argc,
   char *argv[]
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	printf("[%s(%d)]\n",__FUNCTION__,__LINE__);

	UINT32 mode = 0xFF ;
	if ( argc > 0 ) {
		mode = strtoul(argv[0], NULL, 0);
	}
	switch(mode) {
	case 0:
		{
			#if defined(SPCA6350)
				#define RTC_EXTREA_MAX_SIZE 26
			#elif defined(SPCA6330)
				#define RTC_EXTREA_MAX_SIZE 6
			#endif
			int i = 0 ;
			for ( i = 0 ; i < RTC_EXTREA_MAX_SIZE ; i++) {
				UINT8 val = 0 ;
				UINT32 err = FAIL ;
				err = ybfwRtcExtraGet(&val, i , 1);
				if( SUCCESS != err ) {
					val = 0 ;
					HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ybfwRtcExtraGet index = 0x%x",__FUNCTION__,__LINE__,i);
				} else
					HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] index(%d) val = 0x%x",__FUNCTION__,__LINE__,i,val);
			}
		}
		break;
	case 1:
		 appBTPowerState(YBFW_POWER_OFF);
		break;
	case 2:
		 appBTPowerState(YBFW_POWER_ON);
		break;
	case 3:
		halRtcRegWrite(0x53, 0x00);
		int i = 0 ;
		for ( i = 0x40 ; i < 0x60 ; i++) {
			HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] index(0x%x) val = 0x%x",__FUNCTION__,__LINE__,i,halRtcRegRead(i));
		}
		break;
	case 4:
		/* reset Hibernation */
		/* halRtcRegWrite(0x59, 0); */
		break;
	default:
		break;
	}


}
#endif
/**
 *  @brief	UART Test
 *  @param[in]    argc
 *  @param[in]    *argv
 *  @return       void
 */
static void _cmd_bttest(
   int argc,
   char *argv[]
)
{
    int index = 0 ;
    int mode = 0 ;
    void* test = 0 ;

    #if 1

    char *testCMD[] = {
        /* 00 */ "{\"mode\":\"wifi\",\"action\":\"connect\",\"essid\":\"SZ-Dorm21\",\"pwd\":\"075526984588\"}" ,
        /* 01 */ "{\"mode\":\"wifi\",\"action\":\"disable\"}" ,
        /* 02 */ "{\"mode\":\"wifi\",\"action\":\"enable\",\"type\":\"ap\"}" ,
        /* 03 */ "{\"mode\":\"wifi\",\"action\":\"enable\",\"type\":\"sta\"}" ,
        /* 04 */ "{\"mode\":\"wifi\",\"action\":\"info\",\"essid\":\"\",\"pwd\":\"\",\"ipaddr\":\"\"}" ,
        /* 05 */ "{\"mode\":\"wifi\",\"action\":\"info\"}", /* send essid,pwd back */
        /* 06 */ "{\"mode\":\"wifi\",\"action\":\"set\",\"essid\":\"test\",\"pwd\":\"1111111111\"}" ,
        /* 07 */ "{\"mode\":\"wifi\",\"action\":\"info\",\"essid\":\"\",\"pwd\":\"\",\"auth_algs\":\"\"}" ,
        /* 08 */ "{\"mode\":\"system\",\"action\":\"power\",\"type\":\"down\"}" ,
        /* 09 */ "{\"mode\":\"system\",\"action\":\"power\",\"type\":\"hiber\"}" ,
        /* 10 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"s1\"}" ,
        /* 11 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"s2\"}" ,
        /* 12 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"wide\"}" ,
        /* 13 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"tele\"}" ,
        /* 14 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"up\"}" ,
        /* 15 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"down\"}" ,
        /* 16 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"left\"}" ,
        /* 17 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"right\"}" ,
        /* 18 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"menu\"}" ,
        /* 19 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"del\"}" ,
        /* 20 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"set\"}" ,
        /* 21 */ "{\"mode\":\"event\",\"action\":\"key\",\"type\":\"mode\"}" ,
        /* 22 */ "{\"mode\":\"bt\",\"action\":\"info\",\"name\":\"\",\"pwd\":\"\"}" ,
        /* 23 */ "{\"mode\":\"bt\",\"action\":\"info\"}" , /* send name,pwd back */
        /* 24 */ "{\"mode\":\"bt\",\"action\":\"set\",\"name\":\"test\",\"pwd\":\"1111\"}" ,
        /* 25 */ "{\"mode\":\"bt\",\"action\":\"restart\"}" ,
        /* 26 */ "{\"mode\":\"bt\",\"action\":\"disable\"}" ,
    };

    #else
    char *testCMD[] = {
        /* 00 */ "bt wifi connect essid=SZ-Dorm21,pwd=075526984588" ,
        /* 01 */ "bt wifi disable" ,
        /* 02 */ "bt wifi enable ap" ,
        /* 03 */ "bt wifi info essid,pwd,ipaddr" ,
        /* 04 */ "bt wifi info essid,pwd" ,
        /* 05 */ "bt wifi info ipaddr,essid,pwd" ,
        /* 06 */ "bt wifi set essid=test,pwd=1111111111" ,
        /* 07 */ "bt system power down" ,
        /* 08 */ "bt system power hiber" ,
        /* 09 */ "bt ptp data" ,
        /* 10 */ "bt bin data" ,
        /* 11 */ "bt wifi info" ,
        /* 12 */ "bt event key s1" ,
        /* 13 */ "bt event key s2" ,
        /* 14 */ "bt event key wide" ,
        /* 15 */ "bt event key tele" ,
        /* 16 */ "bt event key up" ,
        /* 17 */ "bt event key down" ,
        /* 18 */ "bt event key left" ,
        /* 19 */ "bt event key right" ,
        /* 20 */ "bt event key menu" ,
        /* 21 */ "bt event key del" ,
        /* 22 */ "bt event key set" ,
        /* 23 */ "bt event key mode" ,
        /* 24 */ "bt bt info name,pwd" ,
        /* 25 */ "bt bt set name=test,pwd=1111" ,
        /* 26 */ "bt bt restart" ,
        /* 27 */ "bt bt disable" ,
        /* 28 */ "bt wifi info essid,pwd,auth_algs" ,
        /* 29 */ "bt bt info uuid" ,
        /* 30 */ "bt bt info name,pwd,uuid" ,
    };
    #endif

    if ( argc == 2 ) {
        mode = strtoul(argv[1], NULL, 0) ;
    }

    if ((argc == 1) || (argc == 2)) {
        index = strtoul(argv[0], NULL, 0) ;
        printf("index %d\n",index);
        printf("mode %d\n",mode);
        test = ybfwMallocCache(sizeof(btData_t));
        printf("test 0x%x\n",(unsigned int) test);
        ybfwFree(test);
        if ( index < (sizeof(testCMD)/sizeof(testCMD[0])) ) {
            btData_t *btdata ;
            btdata = ybfwMallocCache(sizeof(btData_t));
            HOST_ASSERT(btdata);
            if ( mode ) {
                btdata->btProfile = APP_BT_SPP;
                btdata->msg  = NDK_BT_TTYDEV_MSG_DATA ;
            } else {
                btdata->btProfile = APP_BT_GATT;
            }
            btdata->len = strlen(testCMD[index]) ;
            btdata->data = ybfwMallocCache(btdata->len);
            memcpy(btdata->data,testCMD[index],btdata->len);
            appBTDataCb(btdata);
        }
    }
}

static void _cmd_bton(
   int argc,
   char *argv[]
)
{
	UINT32 mode = 0xFF ;
	if ( argc > 0 ) {
		mode = strtoul(argv[0], NULL, 0);
	}
	switch(mode) {
	case 0:
		bt_system_init();
		break;
	default:
		appBTStartConnection(BT_INT);
		break;
	}
}

static void _cmd_btoff(
   int argc,
   char *argv[]
)
{
	UINT32 mode = 0xFF ;
	if ( argc > 0 ) {
		mode = strtoul(argv[0], NULL, 0);
	}
	switch(mode) {
	case 0:
	default:
		appBTExitConnection(BT_INT);
		break;
	}
}

static void _cmd_bt_hidprint(
	int argc,
	char *argv[])
{
	UINT32 printkey_en = 0 ;
	if ( argc > 0 ) {
		printkey_en = strtoul(argv[0], NULL, 0);
	}
	else return;

	switch(printkey_en) {
	case 1:
		printf("[BT][HID] enable key print");
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_hidp_keychar_print_en");
		ndk_bt_hidp_keychar_print_en(1);
		break;
	case 0:
	default:
		printf("[BT][HID] disable key print");
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_hidp_keychar_print_en");
		ndk_bt_hidp_keychar_print_en(0);
		break;
	}
}

void _bt_hid_keyevent(unsigned char k, unsigned int status){
	printf("[BT][HID] %s key = %d\n", status? "Press": "Release",k);

	/* Add host application here, the following code is a sample */
	#if 0
	if(status==1){
		/* trigger on press key */

		switch(k){
			case 40: /* key board "Enter" */
				/* do snapshot */
				break;
			default:
				break;
		}
	}
	#endif
}

static void _cmd_bt_hid_installCB(
	int argc,
	char *argv[])
{
	printf("[BT][HID] install HID key trigger callback\n");
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_hidp_keyevent_register");
	ndk_bt_hidp_keyevent_register(_bt_hid_keyevent);
}

static cmd_t testsubBTcmds[] = {
    #if 0
	{"rtc", _cmd_rtc, "rtc test.",NULL},
    #endif
    {"cmd", _cmd_bttest, "bt cmd test.(0:GATT,1:SPP)",NULL},
	{"on", _cmd_bton, "bt on test.",NULL},
    {"off", _cmd_btoff, "bt off test.",NULL},
    {"hid", _cmd_bt_hidprint, "bt HID print",NULL},
    {"hidevent", _cmd_bt_hid_installCB, "bt HID install callback", NULL},
	{NULL   ,  NULL, NULL, NULL}
};

static void _bt_cmd_not_found(
   int argc,
   char *argv[]
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	printf("available commands:\n");

	cmd_t *pcmd;
	const char *spaces = " : ";

	for (pcmd = testsubBTcmds; pcmd->cmd; ++pcmd) {
		printf(" %s %s %s\n", pcmd->cmd, spaces , pcmd->phelp);
	}
	printf("\n");
}

void _bt_cmd_main(
   int argc,
   char *argv[]
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	static cmd_t *psubcmds = NULL;

    if (!psubcmds) {
		psubcmds = cmdSubBatchRegister(0, &testsubBTcmds[0]);
	}
    cmdSubProcess(argc, argv, psubcmds, _bt_cmd_not_found);
}

#endif /* BT_TESTCMD */
#endif /* BTON=YES in project defination */
