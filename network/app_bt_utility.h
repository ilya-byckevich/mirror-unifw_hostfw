/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_BT_UTILITY_H_
#define _APP_BT_UTILITY_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#include "common.h"
#if SP5K_BTON
#include <ndk_bluetooth.h>
#include <ndk_bluetooth_hidp.h>
#endif
/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define BT_STOR_FOLDER   "B:/UDF/BLUEZ"
#define BT_SETTINGS_FILE "B:/UDF/BLUEZ/BTDEVICE.CFG"

#define BT_DEVICE_NAME "SBC-BLE"
#define BT_DEVICE_PWD  "0000"
#define BT_SECTION	   "BTSETTING"

#define BT_NAME_SIZE	20
#define BT_PWD_SIZE		4

#define RTC_EXTRA_ADDR_BT	25
#define RTC_EXTRA_BT_ENABLE_BIT	(1<<7)
#define BT_POWER_SOURCE	YBFW_PWR_SRC_CUSTOM

#define BT_HCI_LOAD			0x01
#define BT_SDPSERVER_INIT   0x02
#define BT_GATT_INIT		0x04
#define BT_SPP_INIT			0x08
#define BT_DO_ASYNC			0x10


#define SP5K_BT_GATT    1
#define SP5K_BT_SPP     1

#define BT_INT			(BT_DO_ASYNC|BT_HCI_LOAD|BT_SDPSERVER_INIT|BT_GATT_INIT|BT_SPP_INIT)


#if SP5K_BT_GATT
#define BT_DEVICE_CLASS	0x700400
#define BT_SERVICE_UUID	"e44b82fb-f3a6-4c72-ab3f-bf94abfd9930"
#define BT_GATT_MAX_LENGTH	18
#endif


#define BT_DEBUG_MESSAGE    1

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
typedef enum {
	APP_BT_OPERATION_START = 0,
	APP_BT_OPERATION_STOP ,
	APP_BT_OPERATION_SEND ,
    APP_BT_OPERATION_SETCB ,
} 	APP_BT_OPERATION ;

enum {
	APP_BT_GATT = 0,
    APP_BT_GATT_INDICATE,
	APP_BT_SPP ,
};

enum {
    APP_BT_DISABLE       = 0x00,
    APP_BT_HIBERATION       ,
	APP_BT_POWEROFF         ,
};

typedef struct btData
{
	int 	msg;
    char*	data;
    unsigned int     len;
    int     btProfile;
} btData_t;

typedef struct {
	const char *conf_dir;
	const char *hcia_type;
	const char *hcia_extra_opts;
	unsigned int bt_mode;
	unsigned int io_capability;
} AppBtGlobConf;

typedef void (*appBTReadCb_t)(void *buf);

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
AppBtGlobConf *appBTGetGlobConf();
UINT32 appBTPreocessPowerOff(UINT8 param);
int appBTGetPinCode(char *pbuf);
UINT32 appBTGetSetting(void);
UINT32 appBTUpdateSetting(UINT8 *btname ,UINT8 *btpwd);
UINT32 appBTPowerState(UINT32 parm);
UINT32 appBTGetMacAddr(UINT8 *btmacbuff);
void appBTReadCallBack(void *buf);
SINT32 appBlueToothOperation(APP_BT_OPERATION sel , ...);

#if SP5K_BT_GATT
int appBTGATTInit(void);
void appBTGATTStop(void);
UINT32 appBTGATTSend(char *pBuffer,UINT32 len);
UINT32 appBTGATTIndicate(char *pBuffer,UINT32 len);
void appBTGATTSetCB(void *pCB);
#endif


#if SP5K_BT_SPP
int appBTUartTTYDevInit(void);
void appBTUartTTYDevStop(void);
UINT32 appBTUartTTYDevSend(char *pBuffer,UINT32 len);
void appBTUartTTYDevSetCB(void *pCB);
#endif

void appBTInitStateSet(UINT32 state);
void appBTInitStateUnset(UINT32 state);
UINT32 appBTInitStateGet(void);
void appBTStartConnection(UINT32 btParm);
#endif  /* _APP_BT_UTILITY_H_ */


