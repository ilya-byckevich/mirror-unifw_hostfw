/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_BT_CMD_H_
#define _APP_BT_CMD_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#include "common.h"

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define CMD_ARGC_MAX        24
#define CMD_PARM_ARGC_MAX   5
#define CMD_BUFFER_SIZE     256
#define RSP_STR_WIFI_INFO   "%s%s%s%d"
#define RSP_STR_BT_INFO   	"%s%s%s%d"

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
typedef UINT32 (*btCmdFunc)(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
struct btCmd_s;
typedef struct btCmd_s btCmd_t;

struct btCmd_s {
	const char *name;
	btCmdFunc  func;
	btCmd_t    *next;
};

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/
enum {
	APP_BT_DISABLE          = 0x00,
	APP_BT_HIBERATION       = 0x01,
	APP_BT_POWEROFF         = 0x02,
};

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
UINT32 appBTCmdWiFi(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdSystem(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdEvent(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdPtp(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdBin(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdBT(int argc, char *argv[], char *btCmd, UINT32(*appBTSend)(char *btCmd,UINT32 len));
UINT32 appBTCmdParser(char *btCmd,UINT32(*appBTSend)(char *btCmd , UINT32 len));

#endif  /* _APP_BT_CMD_H_ */


