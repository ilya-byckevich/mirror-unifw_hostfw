/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 *  Author:
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gpio_custom.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_disk_api.h"
#include "ndk_global_api.h"
#include "ndk_eth.h"


extern int sdhc_initialize(char second_sdio,UINT32 f_max);
extern int ndk_tcpip_global_init(const char *options);
extern int ndk_utils_global_init(const char *options);
extern int wireless_extension_global_init(const char *options);
extern int ndk_hapd_global_init(const char *options);
extern int ndk_wpas_global_init(const char *options);
extern int ndk_st_global_init(const char *options);

#if defined(SP5K_WIFI_MRVL_SD8801)
	extern int mrvl_wlan_global_init(const char *fw_dir, const char *options);
#elif defined(SP5K_WIFI_BRCM)
	extern int brcm_dhd_wlan_global_init(const char *fw, const char *nvram, const char *cfg, const char *extra_options);
	extern int brcm_dhd_utils_global_init(const char *options);
	extern void brcm_wl_register_cmds();
#else
	extern int rtk_wlan_global_init(const char *options);
#endif

#if SP5K_BTON
#include "app_bt_device.h"
#endif

static int net_glob_inited = FALSE;
static int net_sdio_inited = FALSE;

extern void ndk_getopt_reset(char first_arg_is_cmd_name);

#define OPT_ARRAY_SIZE(a)       (sizeof(a)/sizeof((a)[0]))
#define WLAN_RES_DIR		"A:/RO_RES/WLAN/"

int net_device_status(void)
{
	return net_sdio_inited ;
}

void net_device_poweroff(void)
{
    #if 0
	if (GRP_SDIO_PWR_EN != YBFW_GPIO_GRP_NO_USE) {
		ybfwGpioCfgSet( GRP_SDIO_PWR_EN, 1 << CFG_SDIO_PWR_EN, 1 << CFG_SDIO_PWR_EN);
		SET_SDIO_PWR_EN(0);
		printf("power off %d for WiFi\n",CFG_SDIO_PWR_EN);
	}
	if ( GRP_SDIO_SHUTDOWN != YBFW_GPIO_GRP_NO_USE ) {
		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN, 1 << CFG_SDIO_SHUTDOWN, 1 << CFG_SDIO_SHUTDOWN);
		SET_SDIO_SHUTDOWN(1);
		printf("disable %d for WiFi\n",CFG_SDIO_SHUTDOWN);
	}
    #endif
    if (GRP_WIFI_SHUTDOWN != YBFW_GPIO_GRP_NO_USE) {
        ybfwGpioCfgSet(GRP_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN);
        SET_WIFI_SHUTDOWN(0);
        printf("disable %d for WIFI\n", CFG_WIFI_SHUTDOWN);
    }
}

void net_device_poweron(void)
{
    if (GRP_WIFI_SHUTDOWN != YBFW_GPIO_GRP_NO_USE) {
        ybfwGpioCfgSet(GRP_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN, 1 << CFG_WIFI_SHUTDOWN);
        SET_WIFI_SHUTDOWN(0);
        ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 100);/* 50 ms */
        SET_WIFI_SHUTDOWN(1);
        printf("enable %d for WIFI\n", CFG_WIFI_SHUTDOWN);
    }
    #if 0
	if (GRP_SDIO_PWR_EN != YBFW_GPIO_GRP_NO_USE) {
		ybfwGpioCfgSet( GRP_SDIO_PWR_EN, 1 << CFG_SDIO_PWR_EN, 1 << CFG_SDIO_PWR_EN);
		SET_SDIO_PWR_EN(0);
		ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 50); /* 50 ms */
		SET_SDIO_PWR_EN(1);
		printf("power enable %d for WiFi\n",CFG_SDIO_PWR_EN);
	}
	if ( GRP_SDIO_SHUTDOWN != YBFW_GPIO_GRP_NO_USE ) {
		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN, 1 << CFG_SDIO_SHUTDOWN, 1 << CFG_SDIO_SHUTDOWN);
		SET_SDIO_SHUTDOWN(1);
		ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 10); /* 10 ms */
		SET_SDIO_SHUTDOWN(0);
		printf("enable %d for WiFi\n",CFG_SDIO_SHUTDOWN);
	}
    #endif
}


void net_device_deinit(
    void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s]",__FUNCTION__);
	if ( net_sdio_inited == TRUE) {
#if 0 // NAV disable
		#if SP5K_BTON
			/* check bt on ? , because bt/wifi cs is the same */
			if ( bt_device_status() == FALSE )
		#endif
#endif
		net_device_poweroff();

		/* todo: reset SDIO Bus */
		sdhc_deinitialize();
		SET_SDIO_PWR_SEL(0);

		net_sdio_inited = FALSE ;
	}
}

static void net_sdio_phase_latch_config(int clk,
	char clk_delay, char clk_rvs,
	char data_delay, char pos_latch)
{
	ybfwDiskCfg_SdioPhase_t psdioCfg = {
	    .clk_freq = clk,
	    .mmcmode = 0,
	    .clk_delay = clk_delay,
	    .clk_rvs = clk_rvs,
	    .pos_latch = pos_latch,
	    .data_delay = data_delay,
	};

	ybfwDiskCfgSet(YBFW_SDIO_CLK_PHASE_CFG, &psdioCfg);
}

void net_device_init(
    void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
#if ICAT_WIFI
	HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s]",__FUNCTION__);
	if ( net_sdio_inited == FALSE) {
		net_sdio_inited = TRUE ;


		/*ndk_getopt_reset(FALSE);*//*for test*/
		printf("power enable for WiFi\n");

#if defined(HW_360)
		#ifdef CFG_SDIO_SHUTDOWN_BT
		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN_BT, 1 << CFG_SDIO_SHUTDOWN_BT, OUTPUT);
		SET_SDIO_SHUTDOWN_BT(1);
		#endif
		#ifdef CFG_SDIO_SHUTDOWN_WL
		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN_WL, 1 << CFG_SDIO_SHUTDOWN_WL, OUTPUT);
		SET_SDIO_SHUTDOWN_WL(1);
		#endif
#endif


#if defined HW_360
		#ifdef CFG_SDIO_SHUTDOWN_BT

		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN_BT, 1 << CFG_SDIO_SHUTDOWN_BT, OUTPUT);
		SET_SDIO_SHUTDOWN_BT(1);
		#endif
		#ifdef CFG_SDIO_SHUTDOWN_WL
		ybfwGpioCfgSet(GRP_SDIO_SHUTDOWN_WL, 1 << CFG_SDIO_SHUTDOWN_WL, OUTPUT);
		SET_SDIO_SHUTDOWN_WL(1);
		#endif
#endif

#ifndef CFG_SDIO_PWR_SEL
		ybfwRtcCfgSet(YBFW_RTC_CFG_WIFI_PWR_EN, 0);
		ybfwOsThreadSleep(50); /* 50 ms */
		ybfwRtcCfgSet(YBFW_RTC_CFG_WIFI_PWR_EN, 1);
#else
		SET_SDIO_PWR_SEL(1);
#endif

		/*SDIO port select*/
		ybfwDiskCfgSet(YBFW_SDIO_SEL_PIN_MUX_GRP, 1);
		net_sdio_phase_latch_config(48000, 0, 0, 0, 1);

#ifdef SP5K_WIFI_SDIO30
#if defined(SP5K_WIFI_VENDOR_BRCM) && defined(SP5K_WIFI_CHIP_BCM43455)
		net_sdio_phase_latch_config(96000, 1, 1, 0, 1);
#endif
		ybfwDiskCfgSet(YBFW_DISK_SD18V_CTRL_PIN_CFG, YBFW_DRIVE_SDIO, GPIO_GRP_NO_USE, 0, 0);
		sdhc_initialize(1, 96 * 1000000);
#else
		sdhc_initialize(1, 48 * 1000000);
#endif


		printf("WiFi system init done.\n");
	}
#endif


}

static void
net_driver_init(
	void
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
#if ICAT_WIFI
#if defined(SP5K_WIFI_VENDOR_MRVL) // marvell
#if defined(SP5K_WIFI_CHIP_SD8801)
	mrvl_wlan_global_init(WLAN_RES_DIR "MrvlSD8801", NULL);
#else
	#error "Unsupported Marvell Wi-Fi"
#endif
	mrvl_wlan_register_cmds();
#elif defined(SP5K_WIFI_VENDOR_BRCM) // broadcom
	const char *fw, *nvram, *cfg;
#if defined(SP5K_WIFI_CHIP_BCM43438)
	fw    = WLAN_RES_DIR "BCM43438/fw_bcm43438a1.bin";
	nvram = WLAN_RES_DIR "BCM43438/nvram_ap6212a.txt";
	cfg   = WLAN_RES_DIR "BCM43438/config.txt";
#elif defined(SP5K_WIFI_CHIP_BCM43455)
	fw    = WLAN_RES_DIR "BCM43455/fw_bcm43455c0_ag_apsta.bin";
	//fw    = WLAN_RES_DIR "BCM43455/fw_bcm43455c0_ag.bin";
	nvram = WLAN_RES_DIR "BCM43455/nvram_ap6255.txt";
	cfg   = WLAN_RES_DIR "BCM43455/config.txt";
#elif defined(SP5K_WIFI_CHIP_BCM43455MFG)
	fw    = WLAN_RES_DIR "BCM43455MFG/fw_bcm43455c0_ag_mfg.bin";
	nvram = WLAN_RES_DIR "BCM43455MFG/nvram_ap6255.txt";
	cfg   = WLAN_RES_DIR "BCM43455MFG/config.txt";
#else
	#error "Unsupported Broadcom Wi-Fi"
#endif
	brcm_dhd_wlan_global_init(fw, nvram, cfg, NULL);
	brcm_dhd_utils_global_init(NULL);
	brcm_wl_register_cmds();
#else // realtek
	rtk_wlan_global_init(NULL);
#endif
#endif // ICAT_WIFI

#if ICAT_ETHERNET
	int err;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_ethdrv_global_init");
	err = ndk_ethdrv_global_init(NULL); /*Initialize ethernet driver*/
	if (err < 0){
		printf("ethernet install fail\n");
	}
	ndk_ethdrv_register_cmds();
#endif // ICAT_ETHERNET
}

void net_system_init(
    void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s]",__FUNCTION__);
	if ( net_glob_inited == FALSE ) {
		NDKInitOpt opts[] = {
			{NDK_INIT_OPT_PRIORITY, 20},
			{NDK_INIT_OPT_MEMPOOL_SIZE, 512*1024},
		};
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK net_device_init");
		net_device_init();
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_global_init");
		ndk_global_init(OPT_ARRAY_SIZE(opts), opts);

		/* LWIP */
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_tcpip_global_init");
		ndk_tcpip_global_init(NULL);

		/* Driver */
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK net_driver_init");
		net_driver_init();

		/* Applications */
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_utils_global_init");
		ndk_utils_global_init(NULL);
		//ndk_httpd_global_init(NULL);
		#if ICAT_WIFI
		wireless_extension_global_init(NULL);
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_hapd_global_init ndk_wpas_global_init");
		ndk_hapd_global_init(NULL);
		ndk_wpas_global_init(NULL);
		#endif

        #ifdef YBFW_IPERF
        extern void iperf_register_cmds(void);
        iperf_register_cmds();
        #endif

		printf("\nOPT_ARRAY_SIZE %d\n",OPT_ARRAY_SIZE(opts));

		net_glob_inited = TRUE;
	}
}
