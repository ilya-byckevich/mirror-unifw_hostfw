/**************************************************************************
 *
 *       Copyright (c) 2006-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_WIFI_CONNECTION_H_
#define _APP_WIFI_CONNECTION_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#if ICAT_WIFI
#include "ndk_global_api.h"
#endif
/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
#define WIFI_SEARCH_TIMEOUT 60

enum {
	WIFI_UTILITY_NULL,
	WIFI_UTILITY_CLOSE,
	WIFI_AP_DISASSOC,
	WIFI_STA_SEARCH_TIMEOUT,
	WIFI_UTILITY_OPEN,
	WIFI_UTILITY_OPEN_AND_ASSOC
};

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
void appWiFiConnectionState(UINT32 msg, UINT32 param1, UINT32 param2, UINT32 param3);
#if ICAT_WIFI
void appNetSysEventHandler(NDKSysEvt evt, unsigned long param, unsigned long udata);
#endif
void appWiFiConnection_UtilityStateSet(UINT8 nWiFiUtility);
UINT8 appWiFiConnection_UtilityStateGet();

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/


#endif  /* _APP_WIFI_CONNECTION_H_ */


