/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "app_bt_device.h"
#include "ndk_bluetooth.h"
#include "app_bt_utility.h"
#include "gpio_custom.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "ndk_types.h"
/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#if SP5K_BT_SPP

static struct TTYDevInf *pttyDevInf = NULL;
static appBTReadCb_t appBTDataInputCb = NULL ;

/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/

UINT32
appBTUartTTYDevSend(
	char *pBuffer,
	UINT32 len
)
{

    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

	if(NULL == pBuffer) return FAIL;

	if (pttyDevInf->dev) {
        #if BT_DEBUG_MESSAGE
            memdump(pBuffer,len+10);
        #endif
            HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_ttydev_write");
        ndk_bt_ttydev_write((NDKBtTtyDev)pttyDevInf->dev, (void *)pBuffer, len);
        return SUCCESS;
    }
    return FAIL;
}

/**
 *  @brief	      handler for dealing with MSG from OS here
 *  @param[in]    ttydev
 *  @param[in]    user_data
 *  @param[in]    ms
 *  [in]   NULL
 *  @return       void
 */
static void
appTTYDevMsgHandler(
	NDKBtTtyDev dev,
	void *user_data,
	int msg,
	void *param
)
{

    struct TTYDevInf *pttyDevInf = (struct TTYDevInf *)user_data;
    btData_t *btdata ;
    NDKBtTtyData *data = (NDKBtTtyData *) param ;
    int len ;

    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    if ( pttyDevInf->uart_channel == 0 ) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] UART is free",__FUNCTION__,__LINE__);
        return ;
    }

    switch (msg)
    {
        case NDK_BT_TTYDEV_MSG_OPENED:
            #if BT_DEBUG_MESSAGE
				HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] NDK_BT_TTYDEV_MSG_OPENED",__FUNCTION__,__LINE__);
                HOST_ASSERT(appBTDataInputCb);
			#endif
            pttyDevInf->wpos = 0;
            pttyDevInf->connected = TRUE;

            btdata = ybfwMallocCache(sizeof(btData_t));
            HOST_ASSERT(btdata);
            btdata->btProfile = APP_BT_SPP ;
            btdata->msg  = msg ;
            btdata->len = 0 ;
            appBTDataInputCb(btdata);
            break;
        case NDK_BT_TTYDEV_MSG_DATA:
            #if BT_DEBUG_MESSAGE
				HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] NDK_BT_TTYDEV_MSG_DATA",__FUNCTION__,__LINE__);
                HOST_ASSERT(appBTDataInputCb);
			#endif

            len = data->len;

            #if BT_DEBUG_MESSAGE
				memdump(data->buf,(UINT32)len);
			#endif

            if (len > 0 ) {
                btdata = ybfwMallocCache(sizeof(btData_t));
                HOST_ASSERT(btdata);
                btdata->btProfile = APP_BT_SPP ;
                btdata->msg  = msg ;
                btdata->len = len ;
                btdata->data = ybfwMallocCache(len);
                memcpy(btdata->data,data->buf,len);
                appBTDataInputCb(btdata);
            }
            break;
        case NDK_BT_TTYDEV_MSG_CLOSED:
            #if BT_DEBUG_MESSAGE
				HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] NDK_BT_TTYDEV_MSG_CLOSED",__FUNCTION__,__LINE__);
                HOST_ASSERT(appBTDataInputCb);
            #endif
            btdata = ybfwMallocCache(sizeof(btData_t));
            HOST_ASSERT(btdata);
            btdata->btProfile = APP_BT_SPP ;
            btdata->msg  = msg ;
            btdata->len = 0 ;
            appBTDataInputCb(btdata);
            break;
        default:
            break;
    }

}

#define APP_BT_TTY_CHANNEL 1

int
appBTUartTTYDevInit(void)
{
    int err;

    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    pttyDevInf = appUartDevGetInf();

    HOST_ASSERT(pttyDevInf);
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_sdp_add_service");
    err = ndk_bt_sdp_add_service(NDK_BT_SDP_SVC_SP, pttyDevInf->dev_id );
    if (err < 0){
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ndk_bt_sdp_add_service %d fail",__FUNCTION__,__LINE__,err);
        return -1;
    }
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_ttydev_open");
    err = ndk_bt_ttydev_open((NDKBtTtyDev *) &pttyDevInf->dev , APP_BT_TTY_CHANNEL, appTTYDevMsgHandler, pttyDevInf);
    if( err ) {
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ndk_bt_ttydev_open %d fail",__FUNCTION__,__LINE__,pttyDevInf->uart_channel);
        return -1 ;
	}
    return 0 ;
}


void
appBTUartTTYDevStop(void)
{
    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_ttydev_close");
    /* Free Resource */
    ndk_bt_ttydev_close((NDKBtTtyDev)pttyDevInf->dev);
}


void
appBTUartTTYDevSetCB(void *pCB)
{
    #if BT_DEBUG_MESSAGE
        HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)]",__FUNCTION__,__LINE__);
    #endif

    HOST_ASSERT(pCB);
    appBTDataInputCb = (appBTReadCb_t) pCB ;
}


#endif

#endif /* BTON=YES in project defination */
