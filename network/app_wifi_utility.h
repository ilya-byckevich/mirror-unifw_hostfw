/**************************************************************************
 *
 *       Copyright (c) 2006-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_WIFI_H_
#define _APP_WIFI_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#include "ndk_global_api.h"
#include "ndk_stream_api.h"
/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define WIFI_LOAD			0x01
#define DHCP_SERVER			0x02
#define HOST_AP				0x04
#define PTP_IP				0x08
#define RTP_STREM			0x10
#define FTP_SRV				0x20
#define BTN_LCK				0x40
#define DO_ASYNC			0x80
#define STATION_MODE		0x100
#define WIFI_SUSPEND		0x200
#define DHCP_CLIENT			0x400

#define MAX_SSID_NAME_LEN 	(30)
#define MAX_PASSWORD_LEN 	(63)
#if SP5K_360CAM_MODE
#define PTPIP_ALIVE_TIME 	(30)
#define RTP_ALIVE_TIME 		(45)
#else
#define PTPIP_ALIVE_TIME 	(15)
#define RTP_ALIVE_TIME 		(15)
#endif
#define WIFI_INIT			(DO_ASYNC|WIFI_LOAD|DHCP_SERVER|HOST_AP|PTP_IP|RTP_STREM|FTP_SRV)

#define STA_INIT			(DO_ASYNC|WIFI_LOAD|STATION_MODE|PTP_IP|RTP_STREM|FTP_SRV)

#define WIFI_AP_CONFIG_FILE "B:/UDF/SSID_PW.CFG"
#define WIFI_AP_CONFIG_FILE_BACKUP "B:/UDF/SSID_PW_BACKUP.CFG"


enum {
	NDK_SYSEVT_CUST_WIFI_INIT_DONE     = NDK_SYSEVT_NR+1,
};

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
typedef struct {
	UINT32 file_type;

	UINT32 h264_width;
	UINT32 h264_height;
	UINT32 h264_bitrate;
	UINT32 h264_frmrate;

	UINT32 jpeg_width;
	UINT32 jpeg_height;
	UINT32 jpeg_q_factor;
	UINT32 jpeg_bitrate;
} NetStParams;

struct H264ResolDef {
	const char *resol_str;
	UINT32 width, height;
	UINT32 bitrates[3]; /* bit-rate maps */
};

struct UrlAttrDef
{
	const char *name;
	int id;
};

typedef struct YuvCfg {
	UINT32 w;
	UINT32 h;
} YuvCfg_t;

typedef struct videoCfg {
	UINT32 w,h;
	UINT32 sensormode;
	UINT32 audio;
	UINT32 frameRate;
	UINT32 br;
} videoCfg_t;

enum {
	URL_H264_BITRATE,
	URL_H264_FRMRATE,

	URL_MJPG_WIDTH,
	URL_MJPG_HEIGHT,
	URL_MJPG_Q_FACTOR,
	URL_MJPG_BITRATE
};

enum {
	RTP_MJPG_Q_FINE = 45,
	RTP_MJPG_Q_NORMAL = 30,
	RTP_MJPG_Q_ECONOMY = 15,

	RTP_MJPG_BR_FINE = 8000000,
	RTP_MJPG_BR_NORMAL = 6000000,
	RTP_MJPG_BR_ECONOMY = 4000000
};

enum {
	WIFI_MODE_STATION,
	WIFI_MODE_AP,
	WIFI_MODE_CONCURRENT,

	ETHERNET_MODE ,
	WIFI_MODE_TOTAL,
};



/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/
#define MYINFO(args...)     do {printf("<<<%s:%d>>> ", __func__, __LINE__); printf(args); }while(0)

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
UINT32 appSTANumGet(void);
void appPtpStateSet(BOOL en);
BOOL appPtpStatGet(void);
/*
UINT32 appRtpStartStreaming(UINT32 stream_bits);
void appRtpStopStreaming(UINT32 stream_bits);
*/
void appRtpStreamStateSet(BOOL en);
BOOL appRtpStreamStateGet(void);
BOOL appIsStreamingActive(void);
BOOL appWiFiStartStateGet(void);
void appWiFiStartConnection(UINT32 wifiParm);
void appWiFiStopConnection(UINT32 wifiParm);
void appExceptionHandle(UINT32 state);
void appWiFiVideoStop(BOOL opModeSw);
void app_station_mode(void);
void appSTAServerStop(void);
BOOL appSSIDNameGet(UINT8 *ssidname);
BOOL appSSIDNameSet(UINT8 *ssidname,UINT8 *wifipwd);
void appWiFiFileListUpdateFlagSet(UINT8 flag);
UINT8 appWiFiFileListUpdateFlagGet(void);

void net_system_init(void);
void net_device_poweron(void);
void net_device_poweroff(void);

BOOL appStreamCheckIsRTPStream(UINT32 stream_bit);
void appMediaSrvStart(UINT32 reserved);
void appStreamClose(void);
void appCustMsrcInit(void);
void appMediaSrvStop(UINT32 reserved);
void appFtpSrvStart(void);
void appFtpSrvStop(void);
void appPtpIpStart(void);
void appPtpIpStop(void);
int appScanBestChannel(UINT8 *ssidname);
BOOL appWiFiGetAuth_ALGS(int *auth_algs);
BOOL appWiFiAPReady(void);
void appNetDoInit(UINT32 parm);
void appNetDoExit(UINT32 parm);

void appNetMutex_Lock(void);
void appNetMutex_UnLock(void);
void appRtpActionLock(void);
void appRtpActionUnlock(void);
void appRtpStreamStop(UINT32 param);

UINT32 appCustMsrcConnGet(void);


NDKStreamSrc* appRtpCreateStreamSrc(const char *stream_name, void *udata);
UINT32 appRtpEventHandler(UINT32 event, UINT32 data);
UINT32 appWifiYuvConfig(void);
UINT32 appVideoFileRecord(UINT32 on);
void appWifiInitModeSet(UINT8 mode);
UINT8 appWifiInitModeGet(void);

UINT32 appRtpStreamHandleGet(void);
void appRtpStop(void);
void appRtpStart(void);
void appSTAZombieCheckerStart(UINT8 *macaddr, int timeoutInMs);
void appSTAZombieCheckerStop();

void appRtpYuvUrCbSet(UINT32 enCb, UINT32 ybfwMode);


#endif  /* _APP_WIFI_H_ */


