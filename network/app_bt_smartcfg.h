/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_BT_SMARTCONFIG_H_
#define _APP_BT_SMARTCONFIG_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#include "common.h"

#if defined(SP5K_NDK2) || defined(SP5K_BTON)
#include "jansson.h"
#endif

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define SMART_DEBUG_MESSAGE 1
#define SMART_DATA_TIMEOUT 5000 /* 5000 ms */
#define SMART_BUFFER_SIZE   64
/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
typedef int (*btSmartConfigFunc)(json_t *jroot, const char *action, int btProfile);
struct btSmartConfig_s;
typedef struct btSmartConfig_s btSmartConfig_t;

struct btSmartConfig_s {
	const char *name;
	btSmartConfigFunc  func;
	btSmartConfig_t    *next;
};

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/
#ifndef __TODO__
#define __TODO__
#define TODO_STRINGIFY(S) #S
#define TODO_DEFER_STRINGIFY(S) TODO_STRINGIFY(S)
#define PRAGMA_MESSAGE(MSG) _Pragma(TODO_STRINGIFY(message(MSG)))
#define FORMATTED_MESSAGE(MSG) "[TODO-" TODO_DEFER_STRINGIFY(__COUNTER__) "] " MSG " " \
		TODO_DEFER_STRINGIFY(__FILE__) " line " TODO_DEFER_STRINGIFY(__LINE__)

#if 0
#define KEYWORDIFY try {} @catch (...) {}
#else
#define KEYWORDIFY
#endif

#define _TODO_(MSG) KEYWORDIFY PRAGMA_MESSAGE(FORMATTED_MESSAGE(MSG))
#endif
/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
void memdump(void *mem, UINT32 len);
int appSmartConfigWiFi(json_t *jroot,const char *action,int btProfile);
int appSmartConfigSystem(json_t *jroot,const char *action,int btProfile);
int appSmartConfigEvent(json_t *jroot,const char *action,int btProfile);
int appSmartConfigBT(json_t *jroot,const char *action,int btProfile);

void appSmartConfigExit(void);
void appSmartConfigInit(void);

#endif  /* _APP_BT_SMARTCFG_H_ */


