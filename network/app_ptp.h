/**************************************************************************
 *
 *       Copyright (c) 2006-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef APP_PTP_H
#define APP_PTP_H

//#include "ykn_bfw_api/ybfw_usb_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ykn_bfw_api/ybfw_ptp_api.h"

#include "middleware/pima_code.h"

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/

#define APP_KEY_SNAP_START	1
#define APP_KEY_SNAP_ABORT	2

typedef enum PTPiCatchEventCode_enum {
	PTP_ICAT_EVENT_DAILSW					= 0x3001,
	PTP_ICAT_EVENT_SHUTTER_S1_OFF		= 0x3101,
	PTP_ICAT_EVENT_SHUTTER_S1_ON		= 0x3102,
	PTP_ICAT_EVENT_SHUTTER_S2_OFF		= 0x3201,
	PTP_ICAT_EVENT_SHUTTER_S2_ON		= 0x3202,
	PTP_ICAT_EVENT_ZOOM_VECTOR			= 0x3301,
	PTP_ICAT_EVENT_VIDEMODE_ON			= 0x3401,
	PTP_ICAT_EVENT_VIDEMODE_OFF			= 0x3402,
	PTP_ICAT_EVENT_FLASHRAISE_OFF		= 0x3501,
	PTP_ICAT_EVENT_FLASHRAISE_ON		= 0x3502,
	PTP_ICAT_EVENT_LENSOPEN_OFF			= 0x3601,
	PTP_ICAT_EVENT_LENSOPEN_ON			= 0x3602,
	PTP_ICAT_EVENT_SDCARD_IN				= 0x3701,
	PTP_ICAT_EVENT_SDCARD_OUT				= 0x3702,
	PTP_ICAT_EVENT_SDCARD_ERROR			= 0x370f,
	PTP_ICAT_EVENT_BATTLVL_CHANGE		= 0x3801,
	PTP_ICAT_EVENT_AF_OK					= 0x4001,
	PTP_ICAT_EVENT_AF_NG					= 0x4002,
	PTP_ICAT_EVENT_VIDREC_TIME_CHANGE		= 0x5001,
	PTP_ICAT_EVENT_HOST_CAPTURING		= 0xC001,
	PTP_ICAT_EVENT_OPMODE_CHANGE		= 0xD001,
	PTP_ICAT_EVENT_TIMELAPSE_STOP		= 0xD200,
	PTP_ICAT_EVENT_EXCEPTION			= 0xE001,
    PTP_ICAT_EVENT_FWUPDATE_CHECK		= 0xE300,
    PTP_ICAT_EVENT_FWUPDATE_COMPLETE	= 0xE301,
    PTP_ICAT_EVENT_FWUPDATE_POWEROFF	= 0xE302,
} PTPiCatchEventCode_e;


typedef enum PTPEventCode_enum {
	PTP_EC_UNDEFINED 					= PIMA_EC_UNDEFINED,
	PTP_EC_CANCEL_TRANSACTION 	= PIMA_EC_CANCEL_TRANSACTION,
	PTP_EC_OBJECT_ADDED 			= PIMA_EC_OBJECT_ADDED,
	PTP_EC_OBJECT_REMOVED 			= PIMA_EC_OBJECT_REMOVED,
	PTP_EC_STORE_ADDED 				= PIMA_EC_STORE_ADDED,
	PTP_EC_STORE_REMOVED 			= PIMA_EC_STORE_REMOVED,
	PTP_EC_DEVICE_PROP_CHANGED 	= PIMA_EC_DEVICE_PROP_CHANGED,
	PTP_EC_OBJECT_INFO_CHANGED 	= PIMA_EC_OBJECT_INFO_CHANGED,
	PTP_EC_DEVICE_INFO_CHANGED 	= PIMA_EC_DEVICE_INFO_CHANGED,
	PTP_EC_REQUEST_OBJECT_TRANSFER = PIMA_EC_REQUEST_OBJECT_TRANSFER ,
	PTP_EC_STORE_FULL 					= PIMA_EC_STORE_FULL,
	PTP_EC_DEVICE_RESET 				= PIMA_EC_DEVICE_RESET,
	PTP_EC_STORAGE_INFO_CHANGED 		= PIMA_EC_STORAGE_INFO_CHANGED,
	PTP_EC_CAPTURE_COMPLETE 			= PIMA_EC_CAPTURE_COMPLETE,
	PTP_EC_UNREPORTED_STATUS 			= PIMA_EC_UNREPORTED_STATUS,
	/* iCatch vendor event code */
	PTP_EC_ICATCH_HOST_EVENT 			= PIMA_EC_ICATCH_HOST_EVENT,
	PTP_EC_ICATCH_HOST_RESPONSE			= PIMA_RC_ICATCH_HOST_RESPONSE,
	/* end */
	PTP_EC_NUM_MAX 						= PIMA_EC_NUM_MAX
} PTPEventCode_e;

typedef enum PTPCusOpCodeParam_enum {
	PTP_CUSOPCODE_S1_PRESS				= 0xC002,
	PTP_CUSOPCODE_DZOOM_TELE_PRESS = 0xC005,
	PTP_CUSOPCODE_DZOOM_WIDE_PRESS = 0xC006,
	PTP_CUSOPCODE_OZOOM_TELE_PRESS = 0xC007,
	PTP_CUSOPCODE_OZOOM_WIDE_PRESS = 0xC008,
	PTP_CUSOPCODE_QUEUE_EVENT_POLLING  = 0xD001,
} PTPCusOpCodeParam_e;

typedef enum PTPCusRespCodeParam_enum {
	PTP_RESP_OPERATION_OK = 0xC100,
	PTP_RESP_OPERATION_NG = 0xC200,
	PTP_RESP_QUEUE_EVENT_OK		= 0xD002,
	PTP_RESP_QUEUE_EVENT_EMPTY  = 0xD003,
} PTPCusRespCodeParam_e;

typedef enum PTPDscOpMode_enum
{
 	PTP_DSCOP_MODE_VIDEO_OFF				= 0x0001,
	PTP_DSCOP_MODE_SHARE					= 0X0002,
 	PTP_DSCOP_MODE_CAMERA					= 0X0003,
 	PTP_DSCOP_MODE_APP						= 0X0004,
 	PTP_DSCOP_MODE_EDIT						= 0X0005,
 	PTP_DSCOP_MODE_BACKWARD				= 0X0006,
 	PTP_DSCOP_MODE_TIMELAPSE_STILL     = 0X0007,
 	PTP_DSCOP_MODE_TIMELAPSE_VIDEO     = 0x0008,
 	PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF   = 0x0009,
    PTP_DSCOP_MODE_TIMELAPSE_VIDEO_OFF   = 0x000A,
	PTP_DSCOP_MODE_VIDEO_ON				= 0x0011,
	PTP_DSCOP_MODE_MAX
}PTPDscOpMode_e;

typedef enum PTPFWCHECKOpMode_enum
{

    FW_CHECK_OK		            = 0x0000,
    FW_CHECK_NO_CARD            = 0x0001,
    FW_CHECK_NO_SPACE           = 0x0002,
    FW_CHECK_ADAPTER_FAIL       = 0x0003,

}PTPFWCHECKOpMode_e;

typedef enum PTPFWDOWNLOADOpMode_enum
{
    FW_DOWNLOAD_FAIL                = 0x0000,
    FW_DOWNLOAD_OK                  = 0x0001,


}PTPFWDOWNLOADOpMode_e;

typedef enum PTPUISupportWiFiFunc_enum
{
	PTP_WIFI_FUNC_BROWSER          = 0x0001, /* only browser */
 	PTP_WIFI_FUNC_CAPTURE			= 0x0002, /* capture + browser */
	PTP_WIFI_FUNC_VIDEO				= 0X0003, /* video record + browser */
 	PTP_WIFI_FUNC_PREVIEW			= 0X0004, /* capture + video record + browser */
}PTPUISupportWiFiFunc_e;

typedef enum DscOpModeState_enum
{
	MODE_PREV = 0x00,
 	MODE_ACTIVE = 0x01,
}DscOpModeState_e;


typedef enum PTPEventDialModeParam_enum
{
	PTP_EVENT_DIAL_MODE_AUTO				= 0x01,
	PTP_EVENT_DIAL_MODE_PROGRAM			= 0x02,
	PTP_EVENT_DIAL_MODE_SHUTTER			= 0x03,
	PTP_EVENT_DIAL_MODE_APERTURE		= 0x04,
	PTP_EVENT_DIAL_MODE_MANUAL			= 0x05,
	PTP_EVENT_DIAL_MODE_CONT_SHOOT		= 0x06,
	PTP_EVENT_DIAL_MODE_SCENE				= 0x07
}PTPEventDialModeParam_e;

typedef enum PTPDpcDialModeValue_enum
{
	PTP_DPC_DIAL_MODE_AUTO 			= 0x0002,
	PTP_DPC_DIAL_MODE_PROGRAM			= 0xC001,
	PTP_DPC_DIAL_MODE_SHUTTER			= 0x0004,
	PTP_DPC_DIAL_MODE_APERTURE		= 0x0003,
	PTP_DPC_DIAL_MODE_MANUAL			= 0x0001,
	PTP_DPC_DIAL_MODE_CONT_SHOOT	= 0xC002,
	PTP_DPC_DIAL_MODE_SCENE			= 0xC003,
	PTP_DPC_DIAL_MODE_FACEDETECT    = 0x0007
}PTPDpcDialModeValue_e;

typedef enum PTPDpcBandSelectValue_enum
{
	PTP_DPC_BAND_SELECT_50HZ			= 0x0000,
	PTP_DPC_BAND_SELECT_60HZ			= 0x0001,
	PTP_DPC_BAND_SELECT_AUTO 			= 0x0002,
}PTPDpcBandSelectValue_e;

typedef enum PTPDpcBurstValue_enum
{
	PTP_DPC_BURST_OFF			= 0x0001,
	PTP_DPC_BURST_3				= 0x0002,
	PTP_DPC_BURST_5 				= 0x0003,
	PTP_DPC_BURST_10 				= 0x0004,
	PTP_DPC_BURST_7 				= 0x0005,
	PTP_DPC_BURST_15 				= 0x0006,
	PTP_DPC_BURST_30 				= 0x0007,
	PTP_DPC_BURST_HS				= 0x0000, /* it should be 0xFFFF, wait for Client APP modification */
}PTPDpcBurstValue_e;

typedef enum PTPDpcCapTimeLpsIntervalValue_enum
{
	PTP_DPC_CAP_TIMELPS_INT_OFF			= 0x0001,
	PTP_DPC_CAP_TIMELPS_INT_1S			= 0x0002,
	PTP_DPC_CAP_TIMELPS_INT_5S			= 0x0003,
	PTP_DPC_CAP_TIMELPS_INT_10S			= 0x0004,
	PTP_DPC_CAP_TIMELPS_INT_20S			= 0x0005,
	PTP_DPC_CAP_TIMELPS_INT_30S			= 0x0006,
	PTP_DPC_CAP_TIMELPS_INT_1M			= 0x0007,
	PTP_DPC_CAP_TIMELPS_INT_5M			= 0x0008,
	PTP_DPC_CAP_TIMELPS_INT_10M			= 0x0009,
	PTP_DPC_CAP_TIMELPS_INT_30M			= 0x000A,
	PTP_DPC_CAP_TIMELPS_INT_1HR			= 0x000B,
}PTPDpcCapTimeLpsIntervalValue_e;

typedef enum PTPDpcCapTimeLpsDuraValue_enum
{
	PTP_DPC_CAP_TIMELPS_DUR_OFF			= 0x0001,
	PTP_DPC_CAP_TIMELPS_DUR_5M			= 0x0002,
	PTP_DPC_CAP_TIMELPS_DUR_10M			= 0x0003,
	PTP_DPC_CAP_TIMELPS_DUR_15M			= 0x0004,
	PTP_DPC_CAP_TIMELPS_DUR_20M			= 0x0005,
	PTP_DPC_CAP_TIMELPS_DUR_30M			= 0x0006,
	PTP_DPC_CAP_TIMELPS_DUR_60M			= 0x0007,
	PTP_DPC_CAP_TIMELPS_DUR_UNLMT      = 0xFFFF,
}PTPDpcCapTimeLpsDuraValue_e;

typedef enum PTPDpcVidTimeLpsIntervalValue_enum
{
	PTP_DPC_VID_TIMELPS_INT_OFF			= 0x0001,
	PTP_DPC_VID_TIMELPS_INT_1M			= 0x0002,
	PTP_DPC_VID_TIMELPS_INT_5M			= 0x0003,
	PTP_DPC_VID_TIMELPS_INT_10M			= 0x0004,
	PTP_DPC_VID_TIMELPS_INT_EVT			= 0xFFFF
}PTPDpcVidTimeLpsIntervalValue_e;

typedef enum PTPDpcVidTimeLpsDuraValue_enum
{
	PTP_DPC_VID_TIMELPS_DUR_OFF			= 0x0001,
	PTP_DPC_VID_TIMELPS_DUR_5S			= 0x0002,
	PTP_DPC_VID_TIMELPS_DUR_10S			= 0x0003,
	PTP_DPC_VID_TIMELPS_DUR_15S			= 0x0004,
	PTP_DPC_VID_TIMELPS_DUR_20S			= 0x0005,
	PTP_DPC_VID_TIMELPS_DUR_30S			= 0x0006,
	PTP_DPC_VID_TIMELPS_DUR_1M			= 0x0007,
	PTP_DPC_VID_TIMELPS_DUR_5M			= 0x0008,
	PTP_DPC_VID_TIMELPS_DUR_10M			= 0x0009,
}PTPDpcVidTimeLpsDuraValue_e;

typedef enum PTPEventZoomParam_enum
{
	PTP_ZOOM_RATIO_1 = 0x01,
	PTP_ZOOM_RATIO_2 = 0x02,
	PTP_ZOOM_RATIO_3 = 0x03,
	PTP_ZOOM_RATIO_4 = 0x04,
	PTP_ZOOM_RATIO_5 = 0x05,
	PTP_ZOOM_RATIO_6 = 0x06,
	PTP_ZOOM_RATIO_7 = 0x07,
	PTP_ZOOM_RATIO_8 = 0x08,
	PTP_ZOOM_RATIO_9 = 0x09,
	PTP_ZOOM_RATIO_10 = 0x0a,
}PTPEventZoomParam_e;

typedef enum ptpPIMADevicePropCustomeCode_enum
{
	USB_PIMA_DPC_APP_SUPPORT_MODE           = 0XD700,
	USB_PIMA_DCP_STILL_WITHOUT_CLOSE_RTP	= 0xd704,
	USB_PIMA_DPC_SAVE_SCREEN                = 0XD720,
	USB_PIMA_DPC_AUTO_POWER_OFF             = 0XD721,
	USB_PIMA_DPC_POWER_ON_AUTO_RECORD       = 0XD722,
	USB_PIMA_DPC_EXPOSURE_COMPENSATION      = 0XD723,
	USB_PIMA_DPC_IMAGE_STABILIZATION        = 0XD724,
	USB_PIMA_DPC_VIDEO_FILE_LENGTH          = 0XD725,
	USB_PIMA_DPC_FAST_MOTION                = 0XD726,
	USB_PIMA_DPC_WIND_NOISE_REDUCTION       = 0XD727,
	USB_PIMA_DPC_CUSTOME_VIDEO_PB_SUPPORT   = 0xD75F,

    /* 0xD7A0~0xD7FF for iCatch Host use only*/
    USB_PIMA_DCP_VIDEO_PREVIEW_INFO        = 0XD7AB,
    USB_PIMA_DCP_FWUPDATE_CHECK            = 0XD7AC,
    USB_PIMA_DCP_FWUPDATE_START            = 0XD7AD,
    USB_PIMA_DCP_VIDEO_PREVIEW_INFO2       = 0XD7AE,
    USB_PIMA_DPC_NONBLOCKING_CAPTURE	   = 0XD7F0,
    USB_PIMA_DCP_AP_STA_CHANGE             = 0xD7FB,  /* app hotspot mode to station mode*/
	ICAT_PIMA_DPC_APP_VIDEO_REC_TIME       = 0xD7FD,
    USB_PIMA_DPC_APP_COMPATIBILY           = 0xD7FC,
    USB_PIMA_DPC_APP_CACHE_TIME            = 0XD7FE,
    USB_PIMA_DPC_APP_CAN_STREAMING		   = 0xD7FF,

	USB_PIMA_DCP_ESSID_USER_NAME           = 0xD834,  /* wifi station mode ESSID */
    USB_PIMA_DCP_ESSID_USER_PASSWORD       = 0xD835,  /* wifi station mode password*/

	USB_PIMA_DPC_APP_WIFI_SSID             = 0xD83C,
	USB_PIMA_DPC_APP_WIFI_PWD              = 0xD83D,
	USB_PIMA_DPC_TIME_ZONE                 = 0xD83E,
	USB_PIMA_DCP_GET_FWUPDATE_PATH         = 0XD801,
} ptpPIMADevicePropCustomeCode_e;

#if SP5K_360CAM_MODE
#define APP_PTP_DPC_AE_METERING         PIMA_DPC_EXPOSURE_METERING_MODE
#define APP_PTP_DPC_STILL_IMG_SIZE      PIMA_DPC_IMAGE_SIZE
#define APP_PTP_DPC_AE_ISO			    PIMA_DPC_EXPOSURE_INDEX
#define APP_PTP_DPC_AE_EV_OFFSET	    PIMA_DPC_EXPOSURE_BAIS_COMPENSATION
#define APP_PTP_DPC_DATE_TIME		    PIMA_DPC_DATE_TIME
#define APP_PTP_DPC_STILL_SNAP_DELAY    PIMA_DPC_CAPTURE_DELAY
#define APP_PTP_DPC_STILL_SNAP_MODE     PIMA_DPC_STILL_CAPTURE_MODE
#define APP_PTP_DPC_SHARPNESS           PIMA_DPC_SHARPNESS
#define APP_PTP_DPC_TIMELAPSE_LENGTH    PIMA_DPC_TIMELAPSE_NUMBER
#define APP_PTP_DPC_TIMELAPSE_INTERVAL  PIMA_DPC_TIMELAPSE_INTERVAL
#define APP_PTP_DPC_PRODUCT_NAME	    PIMA_DPC_ARTIST
#define APP_PTP_DPC_FW_VERSION		    PIMA_DPC_COPYRIGHT_INFO
#define APP_PTP_DPC_WB_MODE             PIMA_DPC_OPTIC_RATIO
#define APP_PTP_DPC_DSC_OP_MODE 	    PIMA_DPC_DSC_OP_MODE
#define APP_PTP_DPC_VIDEO_SIZE		    PIMA_DPC_VIDEO_SIZE
#define APP_PTP_DPC_DATESTAMP           PIMA_DPC_DATESTAMP
#define APP_PTP_DPC_STILL_SNAP_SPACE    PIMA_DPC_FREE_IN_IMAGE
#define APP_PTP_DPC_WIFI_FEATURES	    PIMA_DPC_UI_SUPPORT_WIFI_OP_FUNC
#define APP_PTP_DPC_VIDEO_REC_SPACE     PIMA_DPC_REMAIN_CAPACITY_IN_SECS
#define APP_PTP_DPC_CUSTOMER_NAME	    PIMA_DPC_CUSTOMER_NAME
#define APP_PTP_DPC_SYS_FEATURES	    USB_PIMA_DPC_APP_SUPPORT_MODE
#define APP_PTP_DPC_RTP_CONDITION1	    USB_PIMA_DCP_STILL_WITHOUT_CLOSE_RTP /* set 1 for keeping streaming before taking a snap. */
#define APP_PTP_DPC_VIDEO_REC_AUTO_START  USB_PIMA_DPC_POWER_ON_AUTO_RECORD /* auto start reocrding when power is turned on . */
#define APP_PTP_DPC_AE_EV_OFFSET_ALTERNATIVE  USB_PIMA_DPC_EXPOSURE_COMPENSATION
#define APP_PTP_DPC_VIDEO_FILE_LENGTH   USB_PIMA_DPC_VIDEO_FILE_LENGTH /* aka seamless */
#define APP_PTP_DPC_VIDEO_REC_WNR	    USB_PIMA_DPC_WIND_NOISE_REDUCTION /* to reduce wind sound(noise) or not */
#define APP_PTP_DPC_VIDEO_CUSTOM_PB     USB_PIMA_DPC_CUSTOME_VIDEO_PB_SUPPORT
#define APP_RTP_DPC_RTP_PV_CFG0 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO /* configure codec, size, bitrate */
#define APP_RTP_DPC_RTP_PV_CFG1 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO2 /* configure frame rate */
#define APP_PTP_DPC_VIDEO_REC_ELAPSE    ICAT_PIMA_DPC_APP_VIDEO_REC_TIME
#define APP_PTP_DPC_RTP_PV_CACHE_TIME   USB_PIMA_DPC_APP_CACHE_TIME
#define APP_PTP_DPC_RTP_CONDITION0      USB_PIMA_DPC_APP_COMPATIBILY /* set 1 for disconnecting before changing video rec size. */
#define APP_PTP_DPC_RTP_SERVICE         USB_PIMA_DPC_APP_CAN_STREAMING
#define APP_PTP_DPC_TIME_ZONE			USB_PIMA_DPC_TIME_ZONE
#elif SP5K_CVRCAM_MULTI_MODE
#define APP_PTP_DPC_STILL_IMG_SIZE      PIMA_DPC_IMAGE_SIZE
#define APP_PTP_DPC_DATE_TIME		    PIMA_DPC_DATE_TIME
#define APP_PTP_DPC_PRODUCT_NAME	    PIMA_DPC_ARTIST
#define APP_PTP_DPC_FW_VERSION		    PIMA_DPC_COPYRIGHT_INFO
#define APP_PTP_DPC_DSC_OP_MODE 	    PIMA_DPC_DSC_OP_MODE
#define APP_PTP_DPC_VIDEO_SIZE		    PIMA_DPC_VIDEO_SIZE
#define APP_PTP_DPC_DATESTAMP           PIMA_DPC_DATESTAMP
#define APP_PTP_DPC_STILL_SNAP_SPACE    PIMA_DPC_FREE_IN_IMAGE
#define APP_PTP_DPC_WIFI_FEATURES	    PIMA_DPC_UI_SUPPORT_WIFI_OP_FUNC
#define APP_PTP_DPC_VIDEO_REC_SPACE     PIMA_DPC_REMAIN_CAPACITY_IN_SECS
#define APP_PTP_DPC_CUSTOMER_NAME	    PIMA_DPC_CUSTOMER_NAME
/*#define APP_PTP_DPC_VIDEO_REC_CAR_MODE  PIMA_DPC_CAR_MODE*/ /* when camera is mounted on ceiling, both video record and rtp streaming are going to flip in vertical*/
#define APP_PTP_DPC_SYS_FEATURES	    USB_PIMA_DPC_APP_SUPPORT_MODE
#define APP_PTP_DPC_RTP_CONDITION1	    USB_PIMA_DCP_STILL_WITHOUT_CLOSE_RTP /* set 1 for keeping streaming before taking a snap. */
#define APP_PTP_DPC_POWER_SAVING_TIME   USB_PIMA_DPC_SAVE_SCREEN /* for power saving, turn off panel at least. */
#define APP_PTP_DPC_VIDEO_REC_AUTO_START  USB_PIMA_DPC_POWER_ON_AUTO_RECORD /* auto start reocrding when power is turned on . */
#define APP_PTP_DPC_VIDEO_FILE_LENGTH   USB_PIMA_DPC_VIDEO_FILE_LENGTH /* aka seamless */
#define APP_PTP_DPC_VIDEO_CUSTOM_PB     USB_PIMA_DPC_CUSTOME_VIDEO_PB_SUPPORT
#define APP_RTP_DPC_RTP_PV_CFG0 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO /* configure codec, size, bitrate */
#define APP_RTP_DPC_RTP_PV_CFG1 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO2 /* configure frame rate */
#define APP_PTP_DPC_VIDEO_REC_ELAPSE    ICAT_PIMA_DPC_APP_VIDEO_REC_TIME
#define APP_PTP_DPC_RTP_PV_CACHE_TIME   USB_PIMA_DPC_APP_CACHE_TIME
#define APP_PTP_DPC_RTP_CONDITION0      USB_PIMA_DPC_APP_COMPATIBILY /* set 1 for disconnecting before changing video rec size. */
#define APP_PTP_DPC_RTP_SERVICE         USB_PIMA_DPC_APP_CAN_STREAMING
#define APP_PTP_DPC_TIME_ZONE			USB_PIMA_DPC_TIME_ZONE
#else
#define APP_PTP_DPC_BATTERY_LEVEL       PIMA_DPC_BATTERY_LEVEL
#define APP_PTP_DPC_STILL_IMG_SIZE      PIMA_DPC_IMAGE_SIZE
#define APP_PTP_DPC_STILL_IMG_QUALITY   PIMA_DPC_COMPRESSION_SETTING
/*#define APP_PTP_DPC_WB_GAIN 		    PIMA_DPC_RGB_GAIN*/
#define APP_PTP_DPC_APERTURE            PIMA_DPC_F_NUMBER
#define APP_PTP_DPC_AF_MODE 		    PIMA_DPC_FOCUS_MODE
#define APP_PTP_DPC_AE_METERING         PIMA_DPC_EXPOSURE_METERING_MODE
#define APP_PTP_DPC_AE_FLASH_MODE       PIMA_DPC_FLASH_MODE
#define APP_PTP_DPC_AE_EXPOSURE         PIMA_DPC_EXPOSURE_TIME
#define APP_PTP_DPC_STILL_SNAP_PROGRAM  PIMA_DPC_EXPOSURE_PROGRAM_MODE
#define APP_PTP_DPC_AE_ISO			    PIMA_DPC_EXPOSURE_INDEX
#define APP_PTP_DPC_AE_EV_OFFSET	    PIMA_DPC_EXPOSURE_BAIS_COMPENSATION
#define APP_PTP_DPC_DATE_TIME		    PIMA_DPC_DATE_TIME
#define APP_PTP_DPC_STILL_SNAP_DELAY    PIMA_DPC_CAPTURE_DELAY
#define APP_PTP_DPC_STILL_SNAP_MODE     PIMA_DPC_STILL_CAPTURE_MODE
#define APP_PTP_DPC_SHARPNESS           PIMA_DPC_SHARPNESS
#define APP_PTP_DPC_DIGITAL_ZOOM	    PIMA_DPC_DIGITAL_ZOOM
#define APP_PTP_DPC_STILL_SNAP_BURST    PIMA_DPC_BURST_NUMBER
#define APP_PTP_DPC_TIMELAPSE_LENGTH    PIMA_DPC_TIMELAPSE_NUMBER
#define APP_PTP_DPC_TIMELAPSE_INTERVAL  PIMA_DPC_TIMELAPSE_INTERVAL
#define APP_PTP_DPC_PRODUCT_NAME	    PIMA_DPC_ARTIST
#define APP_PTP_DPC_FW_VERSION		    PIMA_DPC_COPYRIGHT_INFO
/*#define APP_PTP_DPC_SCENE_MODE          PIMA_DPC_SCENE_MODE*/
#define APP_PTP_DPC_WB_MODE             PIMA_DPC_OPTIC_RATIO
#define APP_PTP_DPC_DSC_OP_MODE 	    PIMA_DPC_DSC_OP_MODE
#define APP_PTP_DPC_VIDEO_SIZE		    PIMA_DPC_VIDEO_SIZE
#define APP_PTP_DPC_LIGHT_FREQUENCY     PIMA_DPC_BAND_SELECTION
#define APP_PTP_DPC_DATESTAMP           PIMA_DPC_DATESTAMP
#define APP_PTP_DPC_STILL_SNAP_SPACE    PIMA_DPC_FREE_IN_IMAGE
#define APP_PTP_DPC_WIFI_FEATURES	    PIMA_DPC_UI_SUPPORT_WIFI_OP_FUNC
#define APP_PTP_DPC_VIDEO_REC_SPACE     PIMA_DPC_REMAIN_CAPACITY_IN_SECS
#define APP_PTP_DPC_CUSTOMER_NAME	    PIMA_DPC_CUSTOMER_NAME
#define APP_PTP_DPC_VIDEO_REC_SLOW_MOTION PIMA_DPC_SLOW_MOTION
#define APP_PTP_DPC_SYS_FEATURES	    USB_PIMA_DPC_APP_SUPPORT_MODE
#define APP_PTP_DPC_RTP_CONDITION1	    USB_PIMA_DCP_STILL_WITHOUT_CLOSE_RTP /* set 1 for keeping streaming before taking a snap. */
#define APP_PTP_DPC_POWER_SAVING_TIME   USB_PIMA_DPC_SAVE_SCREEN /* for power saving, turn off panel at least. */
#define APP_PTP_DPC_POWER_OFF_TIME	    USB_PIMA_DPC_AUTO_POWER_OFF
#define APP_PTP_DPC_VIDEO_REC_AUTO_START  USB_PIMA_DPC_POWER_ON_AUTO_RECORD /* auto start reocrding when power is turned on . */
#define APP_PTP_DPC_AE_EV_OFFSET_ALTERNATIVE  USB_PIMA_DPC_EXPOSURE_COMPENSATION
#define APP_PTP_DPC_VIDEO_EIS		    USB_PIMA_DPC_IMAGE_STABILIZATION
#define APP_PTP_DPC_VIDEO_FILE_LENGTH   USB_PIMA_DPC_VIDEO_FILE_LENGTH /* aka seamless */
#define APP_PTP_DPC_VIDEO_REC_FAST_MOTION  USB_PIMA_DPC_FAST_MOTION
#define APP_PTP_DPC_VIDEO_REC_WNR	    USB_PIMA_DPC_WIND_NOISE_REDUCTION /* to reduce wind sound(noise) or not */
#define APP_PTP_DPC_VIDEO_CUSTOM_PB     USB_PIMA_DPC_CUSTOME_VIDEO_PB_SUPPORT
#define APP_RTP_DPC_RTP_PV_CFG0 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO /* configure codec, size, bitrate */
#define APP_RTP_DPC_FW_UPDATE_CHECK     USB_PIMA_DCP_FWUPDATE_CHECK /* set fw size to check if this device accept fw update now? */
#define APP_RTP_DPC_FW_UPDATE		    USB_PIMA_DCP_FWUPDATE_START /* take action */
#define APP_RTP_DPC_RTP_PV_CFG1 	    USB_PIMA_DCP_VIDEO_PREVIEW_INFO2 /* configure frame rate */
#define APP_PTP_DPC_VIDEO_REC_ELAPSE    ICAT_PIMA_DPC_APP_VIDEO_REC_TIME
#define APP_PTP_DPC_RTP_PV_CACHE_TIME   USB_PIMA_DPC_APP_CACHE_TIME
#define APP_PTP_DPC_RTP_CONDITION0      USB_PIMA_DPC_APP_COMPATIBILY /* set 1 for disconnecting before changing video rec size. */
#define APP_PTP_DPC_RTP_SERVICE         USB_PIMA_DPC_APP_CAN_STREAMING
#define APP_PTP_DPC_FW_STORE_PATH       USB_PIMA_DCP_GET_FWUPDATE_PATH /* APP should store new fw in here by FTP. */
#if ICAT_WIFI /* no ssid if operate ptp through either USB or ethernet interface. */
#define APP_PTP_DPC_WIFI_MY_SSID        USB_PIMA_DPC_APP_WIFI_SSID /* this is an AP. ssid is the name. */
#define APP_PTP_DPC_WIFI_MY_PWD         USB_PIMA_DPC_APP_WIFI_PWD
#if 0 /* todo */
#define APP_PTP_DPC_WIFI_SSID_TO_JOIN   USB_PIMA_DCP_ESSID_USER_NAME /* after changed from AP to station mode, essid is station mode should connect to.*/
#define APP_PTP_DPC_WIFI_PWD_TO_JOIN    USB_PIMA_DCP_ESSID_USER_PASSWORD
#define APP_PTP_DPC_WIFI_AP_STA_SWITCH  USB_PIMA_DCP_AP_STA_CHANGE
#endif
#endif
#define APP_PTP_DPC_STILL_SNAP_NONBLOCKING USB_PIMA_DPC_NONBLOCKING_CAPTURE
#define APP_PTP_DPC_TIME_ZONE			USB_PIMA_DPC_TIME_ZONE
#endif

typedef enum PTPDpcSlowMotionValue_enum
{
	PTP_DPC_SLOW_MOTION_OFF = 0x0000,
	PTP_DPC_SLOW_MOTION_ON	  = 0x0001,
}PTPDpcSlowMotionValue_e;


typedef enum PTPDpcPowerOnAutoRecordValue_enum
{
    PTP_DPC_POWER_ON_AUTO_RECORD_OFF = 0x0000,
    PTP_DPC_POWER_ON_AUTO_RECORD_ON	  = 0x0001,
}PTPDpcPowerOnAutoRecordValue_e;


typedef enum PTPDpcCarModeValue_enum
{
	PTP_DPC_CAR_MODE_OFF  = 0x0000,
	PTP_DPC_CAR_MODE_ON	   = 0x0001,
}PTPDpcCarModeValue_e;



#define PTP_EVENT_MAX_Q_NUM (20)

typedef struct PTPEventQ_S{
	UINT8  CurQ1stIndex;
	UINT8  EventQIndex;
	UINT8  TotalQNum;
	YBFW_MUTEX mutex;
	UINT32 EventQCode[PTP_EVENT_MAX_Q_NUM][4]; /* ex, PTP_ICAT_EVENT_EXCEPTION*/
}ptpEventQueue_s;

typedef struct
{
    UINT16 val;
    UINT16 UiIndex;
}PtpValMap_t;


/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
void ptpip_reg_n_start(void);
void ptpip_reg_n_start_ex(BOOL bMode);
void ptpip_timeout_set(UINT32 timeout_sec);
void ptpip_connection_state_cb_reg(void (*cb)(UINT32));

UINT32 appPtpCustomOperation(UINT32 op, UINT32 par1, UINT32 par2);
void appStill_PIMA_Get_cb(UINT32 devPropCode, UINT32 *dat);
void appStill_PIMA_Apply_cb(UINT32 devPropCode, UINT32 *dat);
UINT32 appPtp_PIMA_File_Filter(UINT32 dir, const UINT8 *name, const UINT8 *pname);
UINT32
appPtp_MTP_File_Sort(
    UINT8 **fnameList,
    UINT32 *handleList,
    UINT32 count,
    UINT32 reserved
);
UINT32 appStillCaptureTrigger(ybfwPtpTrigCaptureType_t cap_type);
UINT32 appStillCaptureAbort(ybfwPtpTrigCaptureType_t cap_type);
void appStill_PIMA_Send_Resp(UINT32 iCatchResp, UINT32 param1, UINT32 param2);
void appStill_PIMA_Send_Event(PTPEventCode_e EventCode, UINT32 param1, UINT32 param2, UINT32 param3);
void appStill_PIMA_Send_iCatch_Event(UINT32 iCatchEvt, UINT32 param1, UINT32 param2);
void appStill_PIMA_DPC_Update(UINT8 nMenuItem);
void app_PTP_Set_DscOpMode(PTPDscOpMode_e dscOpMode );
void app_PTP_clear_EventQ(void);
UINT32 app_PTP_EventQNum_Get(void);
UINT8 app_PTP_Get_DscOpMode(DscOpModeState_e opModeState);
UINT8 app_PTP_ContRecMode_Get();
UINT8 app_PTP_Get_WiFiOpFunc();
UINT16 app_PTP_Get_CustomName();
void app_PTP_Set_TimeLapse_param(UINT16 dpc,UINT32 param);
UINT32 app_PTP_Get_TimeLapse_param(UINT16 dpc);
UINT32 appPtpDelFileCb(UINT8 *ppayload, UINT8 *pdata, UINT32 n);
UINT32 appPtpCfgRtpSizeGet(UINT32 idx);
UINT32 appPtpCfgRtpFrameRateGet(void);
void appPtpDscModeChange(void);

void appPtpFileAddNotify(UINT8 *fname);
void appPtpFileDelNotify(UINT8 *fname);

UINT8 appPtpIsTimelapseVideoMode(void);
UINT8 appPtpIsTimelapseCapMode(void);
void appPtpVideoTimelapseIntUISave(void);
void appPtpVideoTimelapseUiOff(void);

UINT32
appPtpOpModeGet(
	void
);

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/
#define APP_NO_TEAR_DOWN 		(1)
#define PTP_EVT_SEND_ENABLE 	(1)
#define PTP_EVT_QUEUE_ENABLE 	(1)
#define PTP_SUPPORT_CNT_VIDEOREC (1)
#define PTP_SUPPORT_VIDEO_SIZE_FPS (1)
#define PTP_TIME_LAPSE_EN (1)
#define PTP_BURST_INTERVAL_EN	(1)
#define DHCP_HOST_NAME   (1)
#define UPSIDE_DOWN (0)


#endif  /* APP_PTP_H*/

