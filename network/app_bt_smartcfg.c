/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "common.h"
#include "app_bt_utility.h"

#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"

#include "app_bt_smartcfg.h"
#include "app_wifi_utility.h"
#include "app_wifi_connection.h"

#include "linux/socktypes.h"
#include "ndk_global_api.h"
#include "ndk/netif.h"
#include "ndk_socutils.h"


/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#if defined(SPCA6330) || defined(SPCA6350)
	#if defined(SPCA6350)
		#if !defined(appRealloc)
			#define appRealloc realloc
		#endif
	#elif defined(SPCA6330)
		void *appRealloc(void *p, size_t n);
	#endif
#endif

#if defined(SPCA6330) && !defined(SPCA6350)
	void*  osMemRealloc(void *pori, UINT32 size);
	void *
	appRealloc(void *p, size_t n)
	{
		if (!p) return ybfwMallocCache(n);
		return osMemRealloc(p, n);
	}
#endif

static btSmartConfig_t btSmartConfigList[] =
{
    { "wifi"	, appSmartConfigWiFi		, NULL },
    { "system"	, appSmartConfigSystem	    , NULL },
    { "event"	, appSmartConfigEvent		, NULL },
    { "bt"		, appSmartConfigBT		    , NULL },
    /*, {NULL, NULL, NULL}*/
};

enum ACTIONS { ACTS_INFO, ACTS_SET, ACTS_ENABLE, ACTS_KEY, ACTS_POWER, ACTS_DISABLE, ACTS_RESTART, ACTS_CONNECT};

struct actionitems
{
  char *name;
  enum ACTIONS id;
} actionitems_list[] = {
  { "info", ACTS_INFO },
  { "set", ACTS_SET },
  { "enable", ACTS_ENABLE },
  { "key", ACTS_KEY },
  { "power", ACTS_POWER },
  { "disable", ACTS_DISABLE },
  { "restart", ACTS_RESTART },
  { "connect", ACTS_CONNECT },
};

#define ACTIONS_ARRAY_SIZE(a)       (sizeof(a)/sizeof((a)[0]))


static YBFW_QUEUE btSmartConfigQueue = 0;
static YBFW_THREAD *thrappSmartConfig = NULL;

static YBFW_EVENT_FLAGS_GROUP  evtSmartConfig = 0 ;

#define SMART_EVENT_PRECESS_DATA  0x0001
#define SMART_EVENT_STOP          0x0002
#define SMART_EVENT_INIT          0x0004


static btData_t *btGATTdata = NULL;
static btData_t *btSPPdata = NULL;
static UINT32   btGATTLastTime = 0 ;
static UINT32   btSPPLastTime = 0 ;
static UINT32 preState;

extern UINT8 gpassword[];
extern UINT8 ssidname[];
extern UINT8 gBT_NAME[];
extern UINT8 gBT_PWD[];
/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/
int
appSmartConfigActionIndex(
    const char *action)
{
    int i;
    enum ACTIONS index;
    struct actionitems *choice = NULL;
    int counter= ACTIONS_ARRAY_SIZE(actionitems_list) ;

    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] items %d",__FUNCTION__,__LINE__,counter);
    #endif

    for(i = 0, choice = NULL; i < counter; i++)
    {
        if (strcmp(action, actionitems_list[i].name) == 0)
        {
            choice = actionitems_list + i;
            break;
        }
    }

    index = choice ? choice->id : -1;

    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] index %d",__FUNCTION__,__LINE__,index);
    #endif
    return index ;
}


int appSmartConfigDoFeedbacks(json_t *jroot , int btProfile)
{
    int ret = 0 ;
    char *JsonDump = NULL ;

     JsonDump = json_dumps(jroot,JSON_ENCODE_ANY);

    #if SMART_DEBUG_MESSAGE
        memdump(JsonDump,strlen(JsonDump));
    #endif

    switch (btProfile) {
    case APP_BT_GATT:
        ret = appBlueToothOperation(APP_BT_OPERATION_SEND,JsonDump,strlen(JsonDump),APP_BT_GATT_INDICATE);
        break;
    case APP_BT_GATT_INDICATE:
        ret = appBlueToothOperation(APP_BT_OPERATION_SEND,JsonDump,strlen(JsonDump),APP_BT_GATT_INDICATE);
        break;
    case APP_BT_SPP:
        ret = appBlueToothOperation(APP_BT_OPERATION_SEND,JsonDump,strlen(JsonDump),APP_BT_SPP);
        break;
    default:
        HOST_ASSERT(0);
        break;
    }
    /* free JSON */
    if ( JsonDump) {
        ybfwFree(JsonDump);
    }
    json_decref(jroot);
	return ret;
}


#if 1
appSmartConfigWiFi(
    json_t *jroot,
    const char *action,
    int btProfile )
{

}
#else
int
appSmartConfigWiFi(
	json_t *jroot,
    const char *action,
	int btProfile )
{
    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] action %s",__FUNCTION__,__LINE__,action);
    #endif
    int loop_error = 0 ;
    UINT8 ssid_temp[MAX_SSID_NAME_LEN]={0};

    UINT8 ifname[IF_NAMESIZE];
    char resp_msg[SMART_BUFFER_SIZE];
    struct in_addr addr;
    int timout = 0 ;
    const char *type = NULL;
    const char *essid = NULL;
    const char *pwd = NULL;
    int ret = -1 ;
    json_t *jitem;
    json_t *jerror;
    int action_index = appSmartConfigActionIndex(action);

    switch (action_index) {
    case ACTS_INFO:
        if (NULL == (jitem = json_object_get(jroot, "essid"))) {
            json_object_set_new(jroot, "essid", json_string(""));
        }

        jitem = json_object_get(jroot, "essid");
        memset(ssid_temp,0,MAX_SSID_NAME_LEN);
        if (appSSIDNameGet(ssid_temp)) {
            HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ssid name get failed!",__FUNCTION__,__LINE__);
            loop_error = loop_error + 1 ;
        } else {
           json_string_set(jitem,(const char *)ssid_temp);
        }

        if ( loop_error == 0 ) {
            if (NULL == (jitem = json_object_get(jroot, "pwd"))) {
                json_object_set_new(jroot, "pwd", json_string(""));
            }
            jitem = json_object_get(jroot, "pwd");
            json_string_set(jitem,(const char *)gpassword);
        }

        if (NULL != (jitem = json_object_get(jroot, "ipaddr"))) {
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] ipaddr",__FUNCTION__,__LINE__);
            HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK dk_netif_ioctl(NDK_IOCG_IF_INDEXTONAM NDK_IOCG_IF_ADDR");
            if (ndk_netif_ioctl(NDK_IOCG_IF_INDEXTONAME, 1, (long *)ifname) != 0) {
                HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ERROR:indextoname",__FUNCTION__,__LINE__);
                #if 0
                loop_error = loop_error + 1 ;
                #else
                json_string_set(jitem,"0.0.0.0");
                #endif
            } else if (ndk_netif_ioctl(NDK_IOCG_IF_ADDR, (long)ifname, (long *)(void *)&addr) != 0 ) {
               memset(resp_msg,0,SMART_BUFFER_SIZE);
               sprintf(resp_msg, "%s", inet_ntoa(addr));
               json_string_set(jitem,resp_msg);
            } else {
                #if 0
                loop_error = loop_error + 1 ;
                #else
                json_string_set(jitem,"0.0.0.0");
                #endif
            }
        }

        if (NULL != (jitem = json_object_get(jroot, "auth_algs"))) {
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] auth_algs",__FUNCTION__,__LINE__);
            json_integer_set(jitem,2);
        }

        break;
    case ACTS_SET:
        if (NULL != (jitem = json_object_get(jroot, "essid"))) {
            essid = json_string_value(jitem);
        }
        if (NULL != (jitem = json_object_get(jroot, "pwd"))) {
            pwd = json_string_value(jitem);
        }
        if (( essid != NULL ) && ( pwd != NULL )) {
            memset(ssidname, 0, MAX_SSID_NAME_LEN);
            memcpy(ssidname,essid,MAX_SSID_NAME_LEN);
            memset(gpassword, 0, MAX_PASSWORD_LEN);
            memcpy(gpassword,pwd,MAX_PASSWORD_LEN);
            appSSIDNameSet((UINT8 *)essid, (UINT8 *)pwd);
            memset(ssidname, 0, MAX_SSID_NAME_LEN);
            memset(gpassword, 0, MAX_PASSWORD_LEN);
            ret = 0;
        }

        break;
    case ACTS_ENABLE:
        jitem = json_object_get(jroot, "type");
        if (NULL != (jitem = json_object_get(jroot, "type"))) {
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] type",__FUNCTION__,__LINE__);
            type = json_string_value(jitem);
            if (!strcmp(type, "ap")) {
                /* enable AP mode connection */
    			#if ICAT_WIFI
//    			if (pViewParam->stillDriverMode != UI_STILL_DRIVERMODE_OFF) {
//    				pViewParam->stillDriverMode = UI_STILL_DRIVERMODE_OFF;
//    				appStill_SetDriveMode(pViewParam->stillDriverMode);
//    			}
    			HOST_PROF_LOG_ADD(LEVEL_INFO, "vpv: WiFi connection, entering WiFi state");
				preState = appPreviousStateGet();
//				appStateChange(APP_STATE_WiFi_CONNECTION, STATE_PARAM_NORMAL_INIT, 0, 0);
    			timout = 20 ;
    			while( appWiFiAPReady() && timout ){
    				ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 1000);
    				timout--;
    			}

    			if ( !appWiFiAPReady() ){
                     ret = 0;
                }
                #endif
            }
        }
        break;
    case ACTS_DISABLE:
//		appStateChange(preState, STATE_PARAM_NORMAL_INIT, 0, 0);
		ret = 0 ;
        break;
    case ACTS_CONNECT:
        #if 0
        jitem = json_object_get(jroot, "essid");
        jitem = json_object_get(jroot, "pwd");
        #endif
        break;
    default:
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
        break;
    }

    if ( loop_error == 0 ) {
        ret = 0 ;
    }

    jerror = json_object_get(jroot, "err");
    json_integer_set(jerror,ret);
    appSmartConfigDoFeedbacks(jroot,btProfile);

    return ret ;
}
#endif
int
appSmartConfigSystem(
	json_t *jroot,
    const char *action,
	int btProfile )
{
    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] action %s",__FUNCTION__,__LINE__,action);
    #endif

    const char *type;
    UINT8 param = APP_BT_DISABLE;
    int ret = -1 ;
    json_t *jerror;
    json_t *jitem;
    int action_index = appSmartConfigActionIndex(action);

    switch (action_index) {
    case ACTS_POWER:
        if (NULL != (jitem = json_object_get(jroot, "type"))) {
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] type",__FUNCTION__,__LINE__);
            type = json_string_value(jitem);
            if (!strcmp(type, "down")) {
                param = APP_BT_POWEROFF;
            } else if (!strcmp(type, "hiber")) {
				param = APP_BT_HIBERATION;
			}

            if (param != APP_BT_DISABLE) {
				appSmartConfigDoFeedbacks(jroot,btProfile);
				ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 1000);
				appBTPreocessPowerOff(APP_BT_POWEROFF);
				return ret;
			}
         }
        break;
    default:
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
        break;
    }
    jerror = json_object_get(jroot, "err");
    json_integer_set(jerror,ret);
    appSmartConfigDoFeedbacks(jroot,btProfile);
    return ret ;
}


int
appSmartConfigEvent(
	json_t *jroot,
    const char *action,
	int btProfile )
{
    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] action %s",__FUNCTION__,__LINE__,action);
    #endif

    int ret = -1 ;
    json_t *jitem;
    json_t *jerror;
    const char *type;
    int action_index = appSmartConfigActionIndex(action);
    UINT32 KeyEvent = 0xFFFFFFFF;

    switch (action_index) {
    case ACTS_KEY:
        jitem = json_object_get(jroot, "type");
        HOST_ASSERT(jitem);
        type = json_string_value(jitem);
        KeyEvent = 0;//appKeyLookup((char *)type);
        if (KeyEvent != 0xFFFFFFFF) {
            appSmartConfigDoFeedbacks(jroot,btProfile);
//            ybfwHostMsgSend(KeyEvent, 0, 0, 0);
            return 0 ;
        }
        break;
    default:
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
        break;
    }
    jerror = json_object_get(jroot, "err");
    json_integer_set(jerror,ret);
    appSmartConfigDoFeedbacks(jroot,btProfile);
    return ret ;
}


int
appSmartConfigBT(
	json_t *jroot,
    const char *action,
	int btProfile )
{
    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] action %s",__FUNCTION__,__LINE__,action);
    #endif

    const char *name = NULL;
    const char *pwd = NULL;
    int ret = -1 ;
    json_t *jerror;
    json_t *jitem;
    int action_index = appSmartConfigActionIndex(action);

    switch (action_index) {
    case ACTS_INFO:
        if (NULL == (jitem = json_object_get(jroot, "name"))) {
            json_object_set_new(jroot, "name", json_string(""));
        }

        jitem = json_object_get(jroot, "name");
        json_string_set(jitem,(const char *)gBT_NAME);

        if (NULL == (jitem = json_object_get(jroot, "pwd"))) {
            json_object_set_new(jroot, "pwd", json_string(""));
        }

        jitem = json_object_get(jroot, "pwd");
        json_string_set(jitem,(const char *)gBT_PWD);

        ret = 0 ;

        break;
    case ACTS_SET:
        if (NULL != (jitem = json_object_get(jroot, "name"))) {
            name = json_string_value(jitem);
        }
        if (NULL != (jitem = json_object_get(jroot, "pwd"))) {
            pwd = json_string_value(jitem);
        }
        if (( name != NULL ) && ( pwd != NULL )) {
            memset(gBT_NAME, 0, BT_NAME_SIZE+1);
            memset(gBT_PWD, 0, BT_PWD_SIZE+1);
            memcpy(gBT_NAME,name,BT_NAME_SIZE+1);
            memcpy(gBT_PWD,pwd,BT_PWD_SIZE+1);
            appBTUpdateSetting( (UINT8 *) gBT_NAME, (UINT8 *) gBT_PWD);
            ret = 0;
        }
        break;
    case ACTS_DISABLE:

        break;
    case ACTS_RESTART:

        break;
    default:
        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
        break;
    }
    jerror = json_object_get(jroot, "err");
    json_integer_set(jerror,ret);
    appSmartConfigDoFeedbacks(jroot,btProfile);
    return ret ;
}


static btSmartConfig_t*
appSmartConfigGetInfo(btSmartConfig_t *List, const char *mode)
{
	btSmartConfig_t *p;
	for (p = List; p; p++) {
		if (p->func)
			if (!strcmp(mode, (const char *)p->name))
				return p;
	}
	return NULL;
}

static void
appSmartConfigEventSet(UINT32 event)
{
	if (!evtSmartConfig) {
		ybfwOsEventFlagsCreate(&evtSmartConfig, "btSmartConfigEvent");
	}
	ybfwOsEventFlagsSet(&evtSmartConfig,event,YBFW_TX_OR);
}


static int appSmartConfigDataParser(void *buf, unsigned int len,int btProfile)
{

	#if SMART_DEBUG_MESSAGE
		memdump(buf,(UINT32)len);
	#endif
    json_t *jroot = 0 ;
	json_t *jitem;
    json_error_t jerror;
    jroot = json_loadb(buf , (UINT32)len , JSON_DECODE_ANY | JSON_DISABLE_EOF_CHECK , &jerror);
	if(jroot)
	{
        if (NULL != (jitem = json_object_get(jroot, "mode"))) {

            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] jroot(0x%x)",__FUNCTION__,__LINE__,(unsigned int)jroot);
            json_object_set_new(jroot, "profile", json_integer(btProfile));
            if (ybfwOsQueueSend(&btSmartConfigQueue, (void *)&jroot, 20 /* wait 20 ms or TX_NO_WAIT */ ) != 0) {
                HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] ybfwOsQueueSend ERROR",__FUNCTION__,__LINE__);
                json_decref(jroot);
                _TODO_("send back and free resource");
                HOST_ASSERT(0);
                return -1;
            }
            appSmartConfigEventSet(SMART_EVENT_PRECESS_DATA);
		} else {
			HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] JSON format ERROR",__FUNCTION__,__LINE__);
		}
	} else {
		_TODO_("Wait sometime for next packet");

		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] line(%d),column(%d),position(%d),text(%s)",__FUNCTION__,__LINE__,jerror.line,jerror.column ,jerror.position,jerror.text);
        return 1 ;
	}
    return 0 ;
}

void appSmartConfigReadCallBack(void *buf)
{
	int ret = 0 ;
    btData_t *btdata = (btData_t *)buf;
    btData_t **btSaved = 0 ;
    UINT32 *LastTime = 0 ;
    UINT32 CurrentTime = 0;


    #if SMART_DEBUG_MESSAGE
    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] len(%d)",__FUNCTION__,__LINE__,btdata->len);
    #endif

    if ( btdata->len ) {
        #if SMART_DEBUG_MESSAGE
        memdump(btdata->data, (UINT32)btdata->len);
        #endif

        if ( btdata->btProfile ==  APP_BT_GATT ) {
            btSaved = &btGATTdata ;
            LastTime = &btGATTLastTime;
        } else if ( btdata->btProfile ==  APP_BT_SPP ) {
            btSaved = &btSPPdata ;
            LastTime = &btSPPLastTime;
        } else {
            HOST_ASSERT(0);
        }

        /* Check Saved Data */
        CurrentTime = ybfwMsTimeGet();
        if ( *LastTime ) {
            if ( (CurrentTime - *LastTime) > SMART_DATA_TIMEOUT ) {
                /* Free Last Data Saved */
                *LastTime = 0 ;
                if ( (*btSaved)->data ) {
                    ybfwFree((*btSaved)->data);
                }
                ybfwFree(*btSaved);
                *btSaved = NULL ;
                ret = appSmartConfigDataParser(btdata->data,btdata->len,btdata->btProfile);
            } else {
                (*btSaved)->data = appRealloc((*btSaved)->data,(*btSaved)->len + btdata->len);
                memcpy((*btSaved)->data + (*btSaved)->len , btdata->data , btdata->len);
                (*btSaved)->len = (*btSaved)->len + btdata->len ;
                #if SMART_DEBUG_MESSAGE
                memdump((*btSaved)->data, (UINT32)(*btSaved)->len);
                #endif
                ret = appSmartConfigDataParser((*btSaved)->data,(*btSaved)->len,btdata->btProfile);
            }
        } else {
            ret = appSmartConfigDataParser(btdata->data,btdata->len,btdata->btProfile);
        }

        if (ret) {
            /* Keep data in memory and wait timeout */
            HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] Need more data",__FUNCTION__,__LINE__);
            *LastTime = ybfwMsTimeGet();
            if ( *btSaved == NULL) {
                *btSaved = btdata;
            }
            return ;
        } else if ( *LastTime ) {
            *LastTime = 0 ;
            if ( *btSaved ) {
                if ((*btSaved)->data) {
                    ybfwFree((*btSaved)->data);
                }
                ybfwFree(*btSaved);
                *btSaved = NULL ;
            }
        }

        if ( btdata->data ) {
            ybfwFree(btdata->data);
        }
    }

    ybfwFree(btdata);
}

static void appSmartConfigThread(UINT32 param)
{
    ULONG ulActualEvents;
    UINT32 ret ;
    json_t *jroot;
    const char *mode;
    const char *action;
    int result = 0 ;
    json_t *jitem;
    int btProfile = 0 ;

    if ( btSmartConfigQueue == 0 ) {
        if (ybfwOsQueueCreate(&btSmartConfigQueue, "btSmartConfigQueue", 1, NULL, 16 * sizeof(unsigned int)) != 0) {
            HOST_ASSERT(0);
        }
    }

    while(1) {
        ybfwOsEventFlagsGet(&evtSmartConfig, SMART_EVENT_PRECESS_DATA|SMART_EVENT_STOP ,YBFW_TX_OR_CLEAR,
            &ulActualEvents,YBFW_TX_WAIT_FOREVER);
        if (ulActualEvents & SMART_EVENT_PRECESS_DATA) {
            #if SMART_DEBUG_MESSAGE
                HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] SMART_EVENT_PRECESS_DATA",__FUNCTION__,__LINE__);
            #endif
            do {
                jroot = 0 ;
                ret = ybfwOsQueueReceive(&btSmartConfigQueue, (void *)&jroot, 20 /* wait 20 ms or TX_NO_WAIT */ );
                if (( ret == SUCCESS ) && (jroot)) {
                    HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] jroot(0x%x)",__FUNCTION__,__LINE__,(unsigned int)jroot);

                    jitem = json_object_get(jroot, "profile");
                    HOST_ASSERT(jitem);
                    btProfile = json_integer_value(jitem);
                    json_object_del(jroot, "profile");

                    jitem = json_object_get(jroot, "mode");
                    mode = json_string_value(jitem);
                    jitem = json_object_get(jroot, "action");
                    HOST_ASSERT(jitem);
                    action = json_string_value(jitem);
                    json_object_set_new(jroot, "err", json_integer(0));
                    btSmartConfig_t *pfun = appSmartConfigGetInfo(btSmartConfigList, mode);
                    HOST_ASSERT(pfun->func);
                    result = pfun->func(jroot, action ,btProfile);
                    if ( result ) {
                        HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)]",__FUNCTION__,__LINE__);
                    }
                }
            } while(ret == SUCCESS);
        } else if (ulActualEvents & SMART_EVENT_STOP) {
            #if SMART_DEBUG_MESSAGE
                HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] SMART_EVENT_STOP",__FUNCTION__,__LINE__);
            #endif
            break;
        } else if (ulActualEvents & SMART_EVENT_INIT) {
            #if SMART_DEBUG_MESSAGE
                HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] SMART_EVENT_STOP",__FUNCTION__,__LINE__);
            #endif
            break;
        }
    }
    /* free Resource */


    if( btSmartConfigQueue ) {
        HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s(%d)] free btSmartConfigQueue\n",__FUNCTION__,__LINE__);
        do {
            jroot = 0 ;
            ret = ybfwOsQueueReceive(&btSmartConfigQueue, (void*)&jroot, 20 /* wait 20 ms or TX_NO_WAIT */ );
            if (( ret == SUCCESS ) && (jroot)) {
                HOST_PROF_LOG_PRINT(LEVEL_INFO, "[%s(%d)] jroot(0x%x)",__FUNCTION__,__LINE__,(unsigned int)jroot);
            }
        } while (ret == SUCCESS);
        ybfwOsQueueDelete(&btSmartConfigQueue);
        btSmartConfigQueue = 0 ;
    }



    ret = ybfwOsEventFlagsDelete(&evtSmartConfig);
    HOST_ASSERT(ret==0);
    evtSmartConfig = 0 ;
    ybfwOsThreadDelete(thrappSmartConfig);
    thrappSmartConfig = NULL ;
}


void appSmartConfigInit(
    void)
{

    appSmartConfigEventSet(SMART_EVENT_INIT);

    if ( thrappSmartConfig == NULL ) {
        thrappSmartConfig = ybfwOsThreadCreate("thrSmartConfig", appSmartConfigThread, 0
                                                 , 27, 0, 0
                                                 , YBFW_TX_AUTO_START);
        HOST_ASSERT(thrappSmartConfig);
    }
}

void appSmartConfigExit(
    void)
{
    /* Send Event to Thread & free Resource */
    if ( thrappSmartConfig ) {
        appSmartConfigEventSet(SMART_EVENT_STOP);
    }
}

#endif /* BTON=YES in project defination */
