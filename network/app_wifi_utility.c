/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 *  Author:
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ndk_types.h>
#include <ndk_global_api.h>

#include <ndk_tcpip_api.h>
#include <ndk_wpas.h>
#include <ndk_hostap.h>
#include <ndk_dhcpd.h>
#include <ndk_ftpd.h>
#include <ndk_socutils.h>
#include <rtk_netdev.h>
#include "linux/if_ether.h"
#include "ndk_eth.h"

#include <ctype.h>
//#include <api/cmd.h>
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ybfw_utility_api.h"
//#include "ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_ptp_api.h"
//#include "ybfw_aud_api.h"
//#include "ybfw_media_api.h"


#include <app_wifi_utility.h>
#include <app_wifi_connection.h>
#include <gpio_custom.h>
#include "app_ptp.h"
#if PTP_TIME_LAPSE_EN
#endif

#include <ndk_stream_api.h>
#include <ndk_streaming.h>


/**************************************************************************
 *                          M A C R O S                                   *
 **************************************************************************/
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#endif


/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static const char *WIFI_IFNAME = "wlan0"; /* default ifname */
static char WIFI_DRVNAME[32] = { 0 };

UINT8 gssidname[MAX_SSID_NAME_LEN]={0};
UINT8 gpassword[MAX_PASSWORD_LEN]={0};
UINT8 gStaSsidName[MAX_SSID_NAME_LEN]={0};
UINT8 gStaPW[MAX_PASSWORD_LEN]={0};

#if ICAT_WIFI
#define YBFW_USE_STA_ZOMBIE_CHECKER 1
#endif

#define YBFW_USE_BITRATE_TEST   0
#define APP_2ND_GOP_NUM         30

#if YBFW_USE_STA_ZOMBIE_CHECKER
extern UINT8 current_sta[]; /*from app_WiFi_connection.c*/
extern int staConnected;
#endif

static UINT8 ghapMode = 0;
static UINT32 g_init_state = 0;
static BOOL bPtpStatus = 0;
static BOOL bRtpStatus = 0;
/*
static UINT32 Streaming_width = 720;
static UINT32 Streaming_height = 400;
static UINT32 Streaming_bitrate = 6000000;
static UINT32 Streaming_fps = 30;
*/
static UINT8 sFileListUpdateFlag = 0;

static UINT8 WIFIInitMode = WIFI_MODE_AP;
UINT8 isStaConnect =0;

#if YBFW_USE_BITRATE_TEST
static UINT8 ADV_VBR_I_MinQ = 40;
static UINT8 ADV_VBR_I_MaxQ = 50;
static UINT8 ADV_VBR_P_MinQ = 30;
static UINT8 ADV_VBR_P_MaxQ = 50;
static UINT8 ADV_VBR_P_IniQ = 31;
#endif

/*static NetStParams st_params;*/
extern PTPDscOpMode_e ptpPrevOpMode;


YBFW_EVENT_FLAGS_GROUP gnet_sysevt = 0;


#ifdef APP_MEDIACLI_RTP
static NDKMediaCliHandle gmediacli_handle = NULL;
#endif

static BOOL bNetDoInit = FALSE;

extern UINT32 memoryFullEvtFlg;


static YBFW_MUTEX appNetMutex;
static YBFW_SEMAPHORE appRtpActionSem = 0;


/*======================*/

#if ICAT_WIFI
UINT8 ssidname[MAX_SSID_NAME_LEN]={0};
#endif

#ifdef VIDEO_REC_STAMP
extern UINT8 appVideoRecStampInitStateGet(void);
extern void appVideoRecStampInitStateSet(UINT8 val);
#endif
extern UINT8 u8Seamless_backup;

/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/

#if ICAT_WIFI
static int appNdkNetDrvInit(const char* drvname);
static BOOL appWiFiInit(BOOL bStation/* TRUE: station mode; FALSE: hostap mode */);
static BOOL appWiFiSuspend(void);
static BOOL appWiFiResume(void);
static void appHostAPStart(const char* cfg_file, const char* ssid_name, const char* passphrase,const char* channel_scan);
static void appHostAPStop(void);
static BOOL appAPModeGet(UINT8 *hapMode);
#endif
#if DHCPC
static void appDhcpClientStart(void);
static void appDhcpClientStop(void);
#else
static void appDhcpServerStart(void);
static void appDhcpServerStop(void);
#endif
static void appPtpipConnectcb(UINT32 sessnum);
/*static void appPtpIpStop(void);*/
void appInitStateUnset(UINT32 state);
static void appInitStateSet(UINT32 state);
static void appNetStateReset(UINT32 wifiParm);
UINT32 appInitStateGet(void);
static void appPTP_PIMA_Num(UINT32 num);

/**************************************************************************
 *                  F U N C T I O N     D E F I N I T I O N               *
 **************************************************************************/

void appWiFiFileListUpdateFlagSet(UINT8 flag){
	sFileListUpdateFlag = flag;
}

UINT8 appWiFiFileListUpdateFlagGet(void){
	return sFileListUpdateFlag;
}

void appNetMutex_Lock(void)
{
	ybfwOsMutexGet(&appNetMutex, YBFW_TX_WAIT_FOREVER);
}

void appNetMutex_UnLock(void)
{
	ybfwOsMutexPut(&appNetMutex);
}

void appRtpActionLock(void)
{
	ybfwOsSemaphoreGet(&appRtpActionSem, YBFW_TX_WAIT_FOREVER);
}

void appRtpActionUnlock(void)
{
	ybfwOsSemaphorePut(&appRtpActionSem);
}

#if DHCPC
static YBFW_EVENT_FLAGS_GROUP dhcpcEvt = 0;
static void appDhcpClientStart(void)
{
	UINT32 state;

	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & DHCP_CLIENT) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] DHCP Server Started",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return;
	}
	if (dhcpcEvt == 0)
		ybfwOsEventFlagsCreate(&dhcpcEvt, "dhcpcEvt");

	ndk_netif_set_address_s(WIFI_IFNAME, "0.0.0.0", "255.255.255.0", NULL);
	ndk_dhcp_start(WIFI_IFNAME);

	appInitStateSet(DHCP_CLIENT);

	appNetMutex_UnLock();
}

static void appDhcpClientStop(
	void
)
{
	UINT32 state;

	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & DHCP_CLIENT) {
		ndk_dhcp_stop(WIFI_IFNAME);
		appInitStateUnset(DHCP_CLIENT);
	}

	if (dhcpcEvt) {
		ybfwOsEventFlagsDelete(&dhcpcEvt);
		dhcpcEvt = 0;
	}

	appNetMutex_UnLock();
}

void
appDhcpBoundSet(
	void
)
{
	if (dhcpcEvt)
		ybfwOsEventFlagsSet(&dhcpcEvt, 1, TX_OR);
}

void
appDhcpBoundWait(
	void
)
{
	UINT32 flags;

	if (dhcpcEvt)
		ybfwOsEventFlagsGet(&dhcpcEvt, 1, TX_OR, &flags, TX_WAIT_FOREVER);
}

#else
static void appDhcpServerStart(void)
{
	struct in_addr local, start, netmask;
	//printf("DHCP server start....\n");
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & DHCP_SERVER) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] DHCP Server Started",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return;
	}

	inet_aton("192.168.1.1", &local);
	inet_aton("192.168.1.10", &start);
	inet_aton("255.255.255.0", &netmask);
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_set_address");
	ndk_netif_set_address(WIFI_IFNAME, &local, &netmask, &local);
    ndk_dhcpd_start();
    ndk_dhcpd_add_interface(WIFI_IFNAME, &local, &start, &netmask, &local, 10);
	appInitStateSet(DHCP_SERVER);

	appNetMutex_UnLock();
}

static void appDhcpServerStop(
	void
)
{
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & DHCP_SERVER) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_dhcpd_stop");
		ndk_dhcpd_stop();
		appInitStateUnset(DHCP_SERVER);
	}

	appNetMutex_UnLock();
}
#endif


#if ICAT_WIFI
static BOOL appWiFiInit(BOOL bStation)
{
	if (appNetMutex==0) {
		if (ybfwOsMutexCreate(&appNetMutex, "appNetMutex", 0) != 0) {
			HOST_ASSERT(0);
			return -1;
		}
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	if (!WIFI_DRVNAME[0]) {
		const char *drvname = ndk_netdev_get_wlan_driver_name(0);
		HOST_ASSERT(drvname);
		strcpy(WIFI_DRVNAME, drvname);
	}

	if (appNdkNetDrvInit(WIFI_DRVNAME) != 0) { /* Loading Realtek WiFi driver */
		HOST_ASSERT(0);
		appNetMutex_UnLock();
		return -1;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_get_station ndk_netif_get_hostap");
	WIFI_IFNAME = bStation ? ndk_netif_get_station() : ndk_netif_get_hostap();
	HOST_ASSERT(WIFI_IFNAME);

#ifndef SP5K_WIFI_VENDOR_BRCM
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl");
	ndk_netif_ioctl(NDK_IOCS_IF_UP, (long)WIFI_IFNAME, NULL);
#endif
	ndk_netif_ioctl(NDK_IOCS_IF_DEFAULT, (long)WIFI_IFNAME, NULL);

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_set_address_s");
	if(ndk_netif_set_address_s(WIFI_IFNAME, "192.168.0.1", "255.255.255.0", "192.168.0.1")!=0){
		printf("Net address set failed!\n");
		HOST_ASSERT(0);

		appNetMutex_UnLock();
		return -1;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(RTK_IOCS_WIFI_COUNTRY_US");
	ndk_netif_ioctl(RTK_IOCS_WIFI_COUNTRY_US, (long)WIFI_IFNAME, NULL);
	printf("Loading WiFi module succeed\n");
	appInitStateSet(WIFI_LOAD);

//	ybfwHostMsgSend(APP_UI_MSG_NET_EVENT, NDK_SYSEVT_CUST_WIFI_INIT_DONE, 0, 0);

	appNetMutex_UnLock();

	return 0; /* succeed */
}

static BOOL appWiFiSuspend(void)
{
	UINT32 state ;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();
	if (state & WIFI_SUSPEND) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] WiFi Suspend",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return -1;
	}

    #ifdef WIFI_MODULE_RTL8723BS
    /* Need Fixed ICOM-3406 */
    #else
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(RTK_IOCS_WIFI_SUSPEND");
	ndk_netif_ioctl(RTK_IOCS_WIFI_SUSPEND, (long)WIFI_IFNAME, NULL);
    #endif /* end of #ifdef WIFI_MODULE_RTL8723BS */
	appInitStateSet(WIFI_SUSPEND);

	appNetMutex_UnLock();

	return 0;
}

static BOOL appWiFiResume(void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

    #ifdef WIFI_MODULE_RTL8723BS
    /* Need Fixed ICOM-3406 */
    #else
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(RTK_IOCS_WIFI_RESUME");
	ndk_netif_ioctl(RTK_IOCS_WIFI_RESUME, (long)WIFI_IFNAME, NULL);
    #endif /* end of #ifdef WIFI_MODULE_RTL8723BS */
	appInitStateUnset(WIFI_SUSPEND);

//	ybfwHostMsgSend(APP_UI_MSG_NET_EVENT, NDK_SYSEVT_CUST_WIFI_INIT_DONE, 0, 0);
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"NDK_SYSEVT_CUST_WIFI_INIT_DONE");
	appNetMutex_UnLock();

	return 0;
}

extern void net_device_init(void);

static int appNdkNetDrvInit(
	const char* drvname
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace drivername = %s",drvname? drvname: "null");
	if (!drvname) {
		printf("Need driver name\n");
		HOST_ASSERT(0);
		return -1;
	}

	int ret;
	printf("Loading WI-Fi driver %s\n", drvname);

	net_device_init(); //NAV Why ?
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netdev_load_driver");
	ret = ndk_netdev_load_driver(drvname, NULL);
	if (ret != 0)
		printf("[%s(#%d)] Net Driver init Error, [\"%s\"] ret = (%d)\n", __FUNCTION__, __LINE__, drvname, ret);

    return ret;
}

static void appHostAPStart(
	const char* cfg_file,
	const char* ssid_name,
	const char* passphrase,
	const char* channel_scan
)
{
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();
	if (state & HOST_AP) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] HOSTAPD Started",__FUNCTION__,__LINE__);
		HOST_ASSERT(0);

		appNetMutex_UnLock();

		return ;
	}
	printf("Host AP start(%s,%s)\n",cfg_file,ssid_name);
	if(passphrase)
		printf("pwd:%s\n",passphrase);
	NDKHapdOpt opts[] = {
		{"ssid", ssid_name},
		{"wpa_passphrase", passphrase},
		{"channel", channel_scan},
		{"wpa", "2"},
		{NULL, NULL}
	};
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_hapd_start");
	ndk_hapd_start(cfg_file, opts);
	appInitStateSet(HOST_AP);

	appNetMutex_UnLock();
}

static void appHostAPStop(void)
{
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & HOST_AP) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_hapd_stop");
		ndk_hapd_stop();
		appInitStateUnset(HOST_AP);
	}

	appNetMutex_UnLock();
}

BOOL appWiFiGetMACAddr(UINT8 *macbuff , UINT8 isUnload)
{
    UINT8 mac[IF_HWADDRSIZE];
    int ret = 0 ;
	int loaddriver = 0 ;
	UINT32 state = appInitStateGet();

	memset(mac,0,IF_HWADDRSIZE);
	memset(macbuff,0,IF_HWADDRSIZE);
	if (!(state & WIFI_LOAD)) {
		ret = appWiFiInit(TRUE);
		if ( ret ) {
			printf("Load WiFi Driver FAIL\n");
			return FAIL;
		}
		loaddriver = 1 ;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCG_IF_HWADDR");
	if (ndk_netif_ioctl(NDK_IOCG_IF_HWADDR,(long)WIFI_IFNAME, (long *)mac) == 0) {
		printf("hw-addr:%s\n", eth_addr_ntoa(mac));
		snprintf((char *)macbuff, IF_HWADDRSIZE , "%02x%02x%02x", mac[3], mac[4], mac[5]);
		printf("%s\n",macbuff);
	}

	if (( isUnload ) && ( loaddriver )) {
		HOST_ASSERT(WIFI_DRVNAME[0]);
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netdev_unload_driver");
		ret = ndk_netdev_unload_driver(WIFI_DRVNAME);
		appInitStateUnset(WIFI_LOAD);
	}

	return SUCCESS;
}


BOOL appSSIDNameSet(UINT8 *wifissidname,UINT8 *wifipwd)
{
	UINT32 fd;

	printf("appSSIDNameSet  --> ssid:%s  pwd:%s\n",wifissidname,wifipwd);

	fd = ybfwFsFileOpen((UINT8 *)WIFI_AP_CONFIG_FILE, FS_OPEN_CREATE | FS_OPEN_RDWR );

	if ( fd == 0 ) {
		printf("ybfwFsFileOpen FAIL\n");
		return FAIL ;
	}

	ybfwFsFileWrite(fd, wifissidname , strlen((const char *)wifissidname));
	ybfwFsFileWrite(fd, (UINT8 *) "\r\n", 2);
	ybfwFsFileWrite(fd, wifipwd , strlen((const char *)wifipwd));
	ybfwFsFileWrite(fd, (UINT8 *) "\r\n", 2);
	ybfwFsFileClose(fd);
	return SUCCESS ;

}

static BOOL appSsidEndWithMacCheck(const UINT8 *ssid){

	/*  SSID foramt: SSID_XXXXXX  */

	char *pMacStart = NULL;
	char *pCharCheck = NULL;
	UINT8 macBuffer[IF_HWADDRSIZE] = {0};
	UINT32 len = 0;
	UINT32 i = 0;

	if(ssid == NULL){
		return FALSE;
	}

	pCharCheck = strchr((char *)ssid, '_');
	if(pCharCheck == NULL){
		return FALSE;
	}
	pCharCheck++;
	pMacStart = pCharCheck;

	/* find last string start with _  */
	while(pCharCheck != NULL){
		pCharCheck = strchr(pMacStart, '_');
		if(pCharCheck != NULL){
			pMacStart = pCharCheck+1;
		}
	}

	appWiFiGetMACAddr(macBuffer,0);
	len = strlen((char *)macBuffer);
	if(strlen(pMacStart) != len){
		return FALSE;
	}

	/* MAC addr check [0-9a-eA-E]{6} */
	for(i = 0; i < len; i++){
		if(((pMacStart[i] >= '0') && (pMacStart[i] <= '9')) ||
		    ((pMacStart[i] >= 'a') && (pMacStart[i] <= 'f')) ||
			((pMacStart[i] >= 'A') && (pMacStart[i] <= 'F'))){
			continue;
		}else{
			return FALSE;
		}
	}

	return TRUE;
}

BOOL appSSIDNameGet(UINT8 *ssidname)
{
    UINT32 j=0;

    UINT8 macbuff[IF_HWADDRSIZE];

    memset(ssidname,0,MAX_SSID_NAME_LEN);
	memset(gpassword,0,MAX_PASSWORD_LEN);
	/* read MAC Address */
	appWiFiGetMACAddr(macbuff,0);
	macbuff[6]=0;
	for ( j=0 ; j < 6 ; j++ ) {
	    macbuff[j] = toupper(macbuff[j]);
	}

	snprintf(ssidname,MAX_SSID_NAME_LEN-1,"SBC_%s",macbuff);
	snprintf(gpassword,MAX_PASSWORD_LEN-1,"12345678");

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WIFI_CFG ssidname = %s Password=%s \n", ssidname,gpassword);

    return SUCCESS;
}

BOOL appWiFiGetAuth_ALGS(int *auth_algs)
{
    *auth_algs = 2 ;
    return SUCCESS;
}

BOOL appWiFiAPReady(void)
{

	if ( appInitStateGet() & HOST_AP) {
		return 0 ;
	}
	return -1 ;
}

static BOOL appAPModeGet(UINT8 *hapMode)
{
	UINT32 fd;
	UINT32 size;
	UINT8 mode = 0;
	char smode[1]={0};
	fd = ybfwFsFileOpen((const UINT8 *)"B:/UDF/APMODE.CFG",YBFW_FS_OPEN_RDONLY|YBFW_FS_OPEN_RDWR);

	if(!fd)
	{
		printf("apmode cfg file open fail!\n");
		ybfwFsFileClose(fd);
		return FAIL;
	}

	if(ghapMode) /* user assign custom ap mode */
	{
		memcpy(&mode, &ghapMode, 1*sizeof(UINT8));
	}
	else /* read default or ssid name saved by user  */
	{
		size = ybfwFsFileSizeGet(fd);
		/*printf("[deb2] size = %d\n",size);*/
		ybfwFsFileRead(fd, &mode, size*sizeof(UINT8));
		mode = atoi((char*)&mode);
		printf("[deb2] mode = %d\n",mode);
	}

	*hapMode = mode;
	printf("hapMode = %d\n", *hapMode);

	if(ghapMode){/* if no change, won't write the ap mode to file again */
		sprintf(smode,"%d",*hapMode);
		ybfwFsFileWrite( fd, (UINT8 *)smode, 1*sizeof(UINT8) );
	}

	ybfwFsFileClose(fd);
	return SUCCESS;
}

UINT32 appSTANumGet(void)
{
	static long sta_nr = 0;
	long n;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCG_WIFI_STA_NR");
	if (ndk_netif_ioctl(NDK_IOCG_WIFI_STA_NR, (long)WIFI_IFNAME, &n) == 0) {
		if (n != sta_nr) {
			sta_nr = n;
		}
	}
	else
		return 0;

	return n;
}
#endif

#if 0 //NAV disable
static volatile UINT32 ptpConnNum = 0;
void appPtpIpStart(void)
{
	UINT32 state ;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();

	if (state & PTP_IP) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] PTP Server Started",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return;
	}

	if (!appVideoStreamHandleCheck(-1))	/* do not enter idle mode during recording */
		appModeSet(YBFW_MODE_STANDBY);	/* free all pv buffers before allocating for PTP */

	ybfwPtpCfgSet(YBFW_PTP_OP_FUNC_CB, PIMA_OC_DELETE_OBJECT, 0, NULL, appPtpDelFileCb);
	ybfwPtpCfgSet(YBFW_PTP_PIMA_IMGFORM_CTRL, YBFW_PIMA_OFC_MP4, 1);
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_GET_INFO, (UINT32)appStill_PIMA_Get_cb, 0, 0, 0);
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_APPLY_INFO, (UINT32)appStill_PIMA_Apply_cb, 0, 0, 0);
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_CUSTOM_OP, (UINT32)appPtpCustomOperation, 0, 0 , 0);

    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_TRIG_CAPTURE, (UINT32)appStillCaptureTrigger, 0, 0, 0);
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_ABORT_CAPTURE, (UINT32)appStillCaptureAbort, 0, 0, 0);
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_CB_FILTER_FILES, (UINT32)appPtp_PIMA_File_Filter, 0, 0, 0);

	//ybfwPtpiCatchCtl(YBFW_PTP_ICTL_MTP_BK_BUFFER_SIZE, 2*512000, 0, 0, 0);

	#if SP5K_CDFS_OPT
	appCdfsCbReg(APP_CDFS_ADD_FILE_CB, appPtpFileAddNotify);
	appCdfsCbReg(APP_CDFS_DELETE_FILE_CB, appPtpFileDelNotify);
	#else
	#ERROR DCF ADD/REMOVE file
	#endif

	appMediaInit();	/* MTP needs media to decode video files. */

	printf("ptp-ip start....\n");
    appPTP_PIMA_Num(128);
    ptpip_timeout_set(PTPIP_ALIVE_TIME);
    ptpip_reg_n_start_ex(FALSE);

    appPtpStateSet(1);
    ptpip_connection_state_cb_reg(appPtpipConnectcb);
	appInitStateSet(PTP_IP);

	appNetMutex_UnLock();
}

void appPtpIpStop(void)
{
	UINT32 state ;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	extern void ptpip_unreg_n_stop(void);

	appNetMutex_Lock();

	state = appInitStateGet();
	if (state & PTP_IP) {
		UINT32 t0 = ybfwMsTimeGet(), t1 = 0;
		while (ptpConnNum) {
			t1 = ybfwMsTimeGet() - t0;
			if (app_PTP_EventQNum_Get() == 0 || t1 > 500)
				break;
		}
		if (t1 > 500) {
			printf("ptp event final sending was timeout. Clear it.\n");
			app_PTP_clear_EventQ();
		}
		#if SP5K_CDFS_OPT
		appCdfsCbReg(APP_CDFS_ADD_FILE_CB, NULL);
		appCdfsCbReg(APP_CDFS_DELETE_FILE_CB, NULL);
		#else
		#ERROR DCF ADD/REMOVE file
		#endif
		ptpip_unreg_n_stop();
		appInitStateUnset(PTP_IP);
		appMediaDeini();
	}

	appNetMutex_UnLock();
}

static void appPtpipConnectcb(UINT32 sessnum)
{
	printf("[PTP-IP] session num:%d\n",sessnum);
	ptpConnNum = sessnum;
}

UINT32
appVideoFileRecord(
	UINT32 on
)
{

	UINT32 ret = SUCCESS;
	UINT32 anyStream;
	UINT32 i,mode;
	appVideoConfig_t *pVidData;

	appNetMutex_Lock();

	anyStream = appVideoStreamHandleCheck(-1);
#if SP5K_REC_DUAL_FILE_FOR_STREAMING
	if (on)
		anyStream = 0;
#endif

	if (on && !anyStream && appVideoCheckReady(CAN_I_START_RECORD)!=VIDEO_STATE_NO) {
 		UINT32 timelapse = 0;

        //enable timer
        if(gVideoCB.CapTimer == TIMER_NULL){
            gVideoCB.CapTimer = appTimerSet(100,"gVideoCB.CapTimer");
			gVideoCB.CapSec = 0;
        }
		appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_VIDREC_TIME_CHANGE, 0, 0);

		#ifdef VIDEO_REC_STAMP
		if (pViewParam->videoStamp !=  UI_DATESTAMP_OFF)	{
			appVideoRecStampInit(16 ,16, 0, 0);
		}
		#endif
		if  (app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_TIMELAPSE_VIDEO) {
			if (appUiParaGet()->VideoTimeLapseInterval != UI_TIME_LAPSE_VIDEO_INTERVAL_OFF)
				timelapse = 1;
		}

		gVideoCB.aviKbPerSec = 0;
		for (i = 0 ; i < MAX_STREAM_REC_NUM ; i++) {
			if (appVideoRecEnGet(i) == ENABLE) {
				appVideoSizeGet(pViewParam->videoSize, i, &pVidData);
				gVideoCB.streamBytesPerSec[i] = pVidData->VideoBitrate[pViewParam->videoQuality]*1000000>>3;
				gVideoCB.aviKbPerSec += (gVideoCB.streamBytesPerSec[i]	/* video data */
							+ (pVidData->frameRate)/**vid_unit*/ /* video hdr */
							+ VIDEO_AUDIO_SAMPLE_RATE*(VIDEO_AUDIO_SAMPLE_BIT/8)*(VIDEO_AUDIO_CHANNEL) /* audio data */
							+ 1024)>>10;
			}
		}


		appMediaInit();
		ybfwModeGet(&mode);
		if(mode != YBFW_MODE_VIDEO_PREVIEW) {
			appModeSet(YBFW_MODE_VIDEO_PREVIEW);
		}
		appStartRecVideo(timelapse, 1);
	}
	else if (!on && anyStream) {
        	gVideoCB.CapSec = 0;

		appVideoImmediateStop();
		appMediaDeini();
	}
	else {
		ret = FAIL;
	}

	appNetMutex_UnLock();


	return ret;
}



UINT32
appWifiYuvConfig()
{

	UINT32 sizeIdx = pViewParam->videoSize;
	appVideoConfig_t *pVidData;
	appVideoConfig_t VidData;
	UINT32 rtpSize, w, h;
	UINT32 mode;

	appVideoSizeGet(sizeIdx, 0, &pVidData);	/* get max YUV size to config */
	rtpSize = appPtpCfgRtpSizeGet(sizeIdx);
	w = (rtpSize >> 19) & 0xfff;
	h = (rtpSize >> 8) & 0x7ff;

	UINT32 dscmode = app_PTP_Get_DscOpMode(MODE_ACTIVE);
	switch(dscmode) {
		case PTP_DSCOP_MODE_TIMELAPSE_VIDEO_OFF:
		case PTP_DSCOP_MODE_TIMELAPSE_VIDEO:
		case PTP_DSCOP_MODE_VIDEO_OFF:
			mode = YBFW_MODE_VIDEO_PREVIEW;
		break;
		case PTP_DSCOP_MODE_TIMELAPSE_STILL:
		case PTP_DSCOP_MODE_TIMELAPSE_STILL_OFF:
		case PTP_DSCOP_MODE_CAMERA:
			mode = YBFW_MODE_STILL_PREVIEW;
			pVidData = &VidData;
			pVidData->w = YUV_ROI_ALIGN_CDSP(w / 3);
			pVidData->h = YUV_ROI_ALIGN_CDSP(h / 3);
			pVidData->sensormode = DEFAULT_PREVIEW_MODE;
			pVidData->frameRate = 30;
			pVidData->FovSupport = FOV_SUPPORT_NONE;
		break;
		default:
			mode = YBFW_MODE_VIDEO_PREVIEW;
		break;

	}

	UINT32 curMode;
	ybfwModeGet(&curMode);
	if (curMode != YBFW_MODE_STANDBY)
		appModeSet(YBFW_MODE_STANDBY);


#if PV_YUV_BUF_CFG_BY_CUSTOM
	UINT32 i;
	UINT32 bufId[4]={0};
	if(mode == YBFW_MODE_STILL_PREVIEW){
	    for(i=0; i<YBFW_SENSOR_DEV_NUM; i++){
		    bufId[i] = PV_BUF_CFG_WIFI_STILL;
		}
		/* initial as still pv state. */
		appStillPvStateInit(APP_STATE_WiFi_CONNECTION, 0, 0, 0);
	}
	else{
		switch(sizeIdx)
    	{
    		case UI_VIDEO_SIZE_QHD_QHD:
    			bufId[0] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			bufId[1] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			bufId[2] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			bufId[3] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			break;
    		case UI_VIDEO_SIZE_QHD_FHD:
    			bufId[0] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			bufId[1] = PV_BUF_CFG_WIFI_VIDEO_SIZE2;
    			bufId[2] = PV_BUF_CFG_WIFI_VIDEO_SIZE2;
    			bufId[3] = PV_BUF_CFG_WIFI_VIDEO_SIZE2;
    			break;
    		case UI_VIDEO_SIZE_QHD_HD:
    			bufId[0] = PV_BUF_CFG_WIFI_VIDEO_SIZE1;
    			bufId[1] = PV_BUF_CFG_WIFI_VIDEO_SIZE3;
    			bufId[2] = PV_BUF_CFG_WIFI_VIDEO_SIZE3;
    			bufId[3] = PV_BUF_CFG_WIFI_VIDEO_SIZE3;
    			break;
    		case UI_VIDEO_SIZE_FHD_FHD:
    			bufId[0] = PV_BUF_CFG_WIFI_VIDEO_SIZE4;
    			bufId[1] = PV_BUF_CFG_WIFI_VIDEO_SIZE4;
    			bufId[2] = PV_BUF_CFG_WIFI_VIDEO_SIZE4;
    			bufId[3] = PV_BUF_CFG_WIFI_VIDEO_SIZE4;
    			break;

    	}
	}
	for(i=0; i<YBFW_SENSOR_DEV_NUM; i++){
		appPreviewYuvCfgApply(i, bufId[i]);
	}
#elif YBFW_360CAM_MODE
	if (mode == YBFW_MODE_STILL_PREVIEW) {
		/* initial as still pv state. */
		appStillPvStateInit(APP_STATE_WiFi_CONNECTION, 0, 0, 0);
	}
	appYuv360CfgSet(mode, pVidData, w, h, 0);
#else /*Single Cam*/

    uiPara_t* puiPara = appUiParaGet();

    if( (UI_VIDEO_HDR_ON == puiPara->VideoHDR) && (pVidData->HdrSupport) && mode == YBFW_MODE_VIDEO_PREVIEW ){  //Video HDR
        appOpenPvHdrPvcfg(1, mode);
    }
    else{
        /* set max video size */
        UINT32 flag, yuvNum = 8;
        UINT32 isCompensateRange = appImgStableRangeGet();
        UINT32 yuvSrcCrop;

	#if CDSP_RAW_FLIP
        flag = YBFW_MODE_PV_CDSP_V_FLIP | YBFW_MODE_PV_CDSP_H_FLIP_IN_RAW;
	#else
        flag = 0;
	#endif

        if (mode == YBFW_MODE_VIDEO_PREVIEW &&
            puiPara->VideImageStabilization == UI_VIDEO_IMAGE_STABILIZATION_ON &&
            (appVideoSizeSupportAttrGet(EIS_SUPPORT_S) == TRUE || pVidData->frameRate)) {
            yuvSrcCrop = isCompensateRange;
        }
        else {
            if (puiPara->VideoFOV == UI_FOV_Narrow && (pVidData->FovSupport & FOV_SUPPORT_NARROW))
                yuvSrcCrop = YUV_SRC_ROI_CROP_ONLY;
            else if (puiPara->VideoFOV == UI_FOV_Medium && (pVidData->FovSupport & FOV_SUPPORT_MEDIUM))
                yuvSrcCrop = YUV_SRC_ROI_CROP_MORE;
            else if (pVidData->FovSupport == FOV_SUPPORT_SUPERVIEW)
                yuvSrcCrop = YUV_SRC_ROI_FULL;
            else
                yuvSrcCrop = YUV_SRC_ROI_RATIO;
            if (puiPara->VideoTimeLapseInterval == UI_TIME_LAPSE_VIDEO_INTERVAL_OFF) {
                /* What is reasonable buffer count? */
                yuvNum += pVidData->frameRate / 60;
            }
            else {
            	yuvNum = 4;/*save memory usage*/
            }
        }

        if (mode == YBFW_MODE_STILL_PREVIEW) {
            /* initial as still pv state. */
            appStillPvStateInit(APP_STATE_WiFi_CONNECTION, 0, 0, 0);
        }
        appYuvCfgSet(mode, pVidData, flag, 0, w, h, yuvNum, 0, yuvSrcCrop);
    }

#endif
	appVideo3AInit(1);
	appModeSet(mode);
	#if !SP5K_REC_DUAL_FILE_FOR_STREAMING || SP5K_360CAM_MODE
	/*Note: YuvCb must enter preview mode or runtime update, and the sensor mode need to be set before set yuvCb*/
	appRtpYuvUrCbSet(1,mode);
	ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_RUNTIME_UPD,
				mode,
				YBFW_MODE_PV_CFG_CDSP_YUV);
	#endif
	return mode;
}


//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"

#ifdef LIVE555_STREAM
#define ndk_st_global_init(v)
#define ndk_st_start_server(v)
#endif

void appStreamMediaAudInit()
{
	ybfwAudDevEnable(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, 1);
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC | YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_REC_IO_SRC);
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL  | YBFW_AUD_CFG_MASK_CH_ALL, 32);
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN | YBFW_AUD_CFG_MASK_CH_ALL, 3);
	ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, VIDEO_AUDIO_SAMPLE_RATE);
	ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, VIDEO_AUDIO_CHANNEL);
}

void appMediaSrvStart(
	UINT32 reserved
)
{

	UINT32 state ;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	if (appRtpActionSem == 0)
		ybfwOsSemaphoreCreate(&appRtpActionSem, "rtpAction", 1);

	state = appInitStateGet();

	if (state & RTP_STREM) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] RTP Server Started",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return;
	}

	printf(">>> start media server...........\n");

	NDKStCfg stcfg;
	memset(&stcfg, 0, sizeof(stcfg));
	stcfg.root_dir       = "D:";
	stcfg.port           = 554;
	stcfg.keepalive_secs = RTP_ALIVE_TIME;
	/*stcfg.flags          = 0;*/
	stcfg.evt_handler    = appRtpEventHandler;
	stcfg.sscreator.udata = NULL;
	stcfg.sscreator.create = appRtpCreateStreamSrc;

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_global_init");
	ndk_st_global_init(NULL);

	appMediaInit();/*a pair with appMediaDeini*/

	if (!appVideoStreamHandleCheck(-1))	/* do not reset audio during recording */
		appStreamMediaAudInit(); /*Media audio Initialize*/
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_start_server");
	if (ndk_st_start_server(&stcfg) != 0) {
		printf("\nstart server failed...\n");

		appNetMutex_UnLock();

		return;
	}
	appInitStateSet(RTP_STREM);

	appNetMutex_UnLock();
}

void appMediaSrvStop(
	UINT32 reserved
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	UINT32 state ;
	appNetMutex_Lock();

	if (appRtpActionSem) {
		appRtpActionLock();
		ybfwOsSemaphoreDestroy(&appRtpActionSem);
		appRtpActionSem = 0;
	}

	state = appInitStateGet();

	if (state & RTP_STREM) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_stop_server");
		ndk_st_stop_server();
		appInitStateUnset(RTP_STREM);
		appMediaDeini();/*a pair with appMediaInit*/
	}

	appNetMutex_UnLock();

}
#endif
void appStreamClose(void)
{
	printf("<<< ndk streaming closed...........\n");
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_close_streaming");
	ndk_st_close_streaming();
	appRtpStreamStateSet(0);

}


void appFtpSrvStart(void)
{
	UINT32 state ;
	UINT8 *drv;
	drv = ybfwMalloc(32*sizeof(UINT8));
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();

	state = appInitStateGet();
	if (state & FTP_SRV) {
		HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] FTP Server Started",__FUNCTION__,__LINE__);

		appNetMutex_UnLock();

		return;
	}

	sprintf((char *)drv,"%s","D:/");
	HOST_PROF_LOG_PRINT(LEVEL_INFO, " FTP Server use drive %s",drv);
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_ftpd_start");
	ndk_ftpd_start((const char*)drv, 21, NULL);
	ybfwFree(drv);
	appInitStateSet(FTP_SRV);

	appNetMutex_UnLock();
}

void appFtpSrvStop(void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	appNetMutex_Lock();
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_ftpd_stop");
	ndk_ftpd_stop();
	appInitStateUnset(FTP_SRV);

	appNetMutex_UnLock();
}

static void appPTP_PIMA_Num(UINT32 num)
{
    ybfwPtpiCatchCtl(YBFW_PTP_ICTL_PIMA_DEV_PROP_NUM, num,0,0,0) ;
}
static void appInitStateSet(UINT32 state)
{
	g_init_state |= state;
}

void appInitStateUnset(UINT32 state)
{
	g_init_state &= ~state;
}

UINT32 appInitStateGet(void)
{
	return g_init_state;
}

static void appNetStateReset(UINT32 wifiParm)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	if(appPtpStatGet()&&((wifiParm & PTP_IP)== PTP_IP))
		appPtpStateSet(0);
	if(appRtpStreamStateGet()&&((wifiParm & RTP_STREM)== RTP_STREM))
		appRtpStreamStateSet(0);
}

static void appNetSysEventCb(
	NDKSysEvt evt,
	unsigned long param,
	unsigned long udata
)
{
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
//	ybfwHostMsgSend(APP_UI_MSG_NET_EVENT, evt, param, udata);
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"APP_UI_MSG_NET_EVENT %d ",evt);
}

static YBFW_SEMAPHORE semaNetDo = 0;
void appNetDoInit(UINT32 parm)
{
	UINT32 wifiParm = (UINT32)parm;
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace  param %d",parm);
	if (semaNetDo == 0) {
		ybfwOsSemaphoreCreate(&semaNetDo, "netDo", 1);
	}

	ybfwOsSemaphoreGet(&semaNetDo, YBFW_TX_WAIT_FOREVER);

    	bNetDoInit = TRUE;

	/* [47844] key is not allowed till net init procedure is done, YBFW_MSG_USB_PTP_OP_END |0x80009805 */
	if ((wifiParm & BTN_LCK) == BTN_LCK){
		#if 0
		appBtnDisable(BTN_ALL);
		callStack();
		#endif
	}

	wifiParm &= ~BTN_LCK;
	if (wifiParm == WIFI_INIT || wifiParm == STA_INIT){
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_sysevt_handler_set");
		ndk_sysevt_handler_set(appNetSysEventCb, 0);
	}

#if ICAT_WIFI
    if ((wifiParm & WIFI_LOAD) == WIFI_LOAD) {
        state = appInitStateGet();
        if (state & WIFI_LOAD) {
            /* WIFI LOADED */
            if (appWiFiResume() != 0) {
                printf("WiFi resume failed!\n");
                HOST_ASSERT(0);
                goto INIT_EXIT;
            }
        }
        else {
            if(appWiFiInit(FALSE)!=0){
                printf("Loading WiFi module failed!\n");
                HOST_ASSERT(0);
                goto INIT_EXIT;
            }
        }
    }

	if ((wifiParm & STATION_MODE) == STATION_MODE) {
		UINT16 staTimeout = 0;
		app_station_mode();
		while(!isStaConnect){
            	ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 200);
                staTimeout ++ ;
				if(staTimeout>100){
					printf("---->sta mode fail ..exit connection\n");
					goto INIT_EXIT;
				}
	    }

	}
#endif


	if ((wifiParm & DHCP_SERVER) == DHCP_SERVER) {
        appDhcpServerStart();
	}

#if ICAT_WIFI
	if ((wifiParm & HOST_AP) == HOST_AP) {
		UINT8 hapMode = 0;
        int bestChannel = 0;
        char channelscan[8] = "auto";
        const char* passphrase = "1234567890";
        int lenght = sizeof(channelscan);
        const char *cfg_file =
    #if defined(SP5K_WIFI_VENDOR_MRVL)
    		"B:/UDF/HAP_MRVL.CFG"
    #elif defined(SP5K_WIFI_VENDOR_BRCM)
    		"B:/UDF/HAP_BRCM.CFG"
    #else
	    	"B:/UDF/HAPD0.CFG"
    #endif
    	;

#ifdef SP5K_WIFI_REALTEK
	#if 0
        bestChannel = appScanBestChannel(0);
		#endif
#else // Marvell, BRCM
		bestChannel = 1; // No ACS for Marvell and BRCM.
#endif

        if (bestChannel > 0) {
            snprintf(channelscan, lenght , "%d", bestChannel);
            printf("channelscan %s\n",channelscan);
        }

        if (bestChannel > 0) {
            snprintf(channelscan, lenght , "%d", bestChannel);
            printf("channelscan %s\n",channelscan);
        }

        if (appSSIDNameGet(ssidname)) {
			printf("ssid name get failed!\n");
            goto INIT_EXIT;
		}


		if(appAPModeGet(&hapMode)){
			printf("ap mode get failed!\n");
            goto INIT_EXIT;
		}

		if(hapMode==2)
		{
			if (gpassword[0])
                appHostAPStart(cfg_file, (char *)ssidname, (char *)gpassword, channelscan);
			else
				appHostAPStart(cfg_file, (char *)ssidname, passphrase, channelscan);
		}
		else
		{
			if(gpassword[0])
				appHostAPStart(cfg_file, (char *)ssidname, (char *)gpassword, channelscan);
            else
				appHostAPStart(cfg_file, (char *)ssidname, NULL, channelscan);
		}

	}
#endif
#if 0  //NAV disable
	if ((wifiParm & PTP_IP) == PTP_IP) {
		state = appInitStateGet();

		if (!(state & PTP_IP)){
			appPtpIpStart();
			ybfwPtpiCatchCtl(YBFW_PTP_ICTL_MTP_BK_UPDATE_TYPE, YBFW_MTP_BK_AUTO|PIMA_MTP_BK_AUTO_QUICK, 0, 0, 0);
		}
	}
#endif
	if ((wifiParm & FTP_SRV) == FTP_SRV) {
		state = appInitStateGet();
		if (!(state & FTP_SRV)){/* ftpd won't be stopped after WiFi off  */
			appFtpSrvStart();
		}
	}
#if 0  //NAV disable
	if ((wifiParm & RTP_STREM) == RTP_STREM) {
		state = appInitStateGet();
		if (!(state & RTP_STREM)){
			appRtpStreamStateSet(1);
			appMediaSrvStart(0);
			appCustMsrcInit();
		}
	}

	if(!IS_CARD_EXIST)
		appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_SDCARD_OUT, 0, 0);
#endif
#if 0 //NAV disable
	UINT32 ret = appVideoStreamHandleCheck(-1);
	if(!ret && (app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_VIDEO_ON ||
		app_PTP_Get_DscOpMode(MODE_ACTIVE) == PTP_DSCOP_MODE_VIDEO_OFF)){/*Chenck and back to VIDEO OFF mode*/
	    app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_OFF);
	    appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_OPMODE_CHANGE,PTP_DSCOP_MODE_VIDEO_OFF,0);
	}
#endif
INIT_EXIT:
    bNetDoInit = FALSE;
    if ((wifiParm & DO_ASYNC) == DO_ASYNC) {
       ;
    }
	ybfwOsSemaphorePut(&semaNetDo);

}


void appNetDoExit(UINT32 parm)
{
	UINT32 wifiParm = (UINT32)parm;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT param %d",parm);
	if (semaNetDo)
		ybfwOsSemaphoreGet(&semaNetDo, YBFW_TX_WAIT_FOREVER);

	/* [47844] key is not allowed till net init procedure is done, YBFW_MSG_USB_PTP_OP_END |0x80009805 */
//	if ((wifiParm & BTN_LCK) == BTN_LCK){
//		appBtnEnable(BTN_ALL);
//		callStack();
//	}
	#if ICAT_WIFI
	if ((wifiParm & STATION_MODE)== STATION_MODE){		//Add Station mode stop case in appDoExit
			printf("exit:0x%x\n",(wifiParm & STATION_MODE));
			appSTAServerStop();
	}
	#endif
	if ((wifiParm & FTP_SRV)== FTP_SRV){
		printf("exit:0x%x\n",(wifiParm & FTP_SRV));
		appFtpSrvStop();
	}
#if 0
	if ((wifiParm & RTP_STREM)== RTP_STREM){
		printf("exit:0x%x\n",(wifiParm & RTP_STREM));
		appMediaSrvStop(0);
	}

	if ((wifiParm & PTP_IP)== PTP_IP){

        printf("exit:0x%x\n",(wifiParm & PTP_IP));
		/*when wifi off before power off ,ptp close maybe case crash*/
//		if((appActiveStateGet() != APP_STATE_POWER_OFF) &&
//		(appNextStateGet() != APP_STATE_POWER_OFF)){
//			appPtpIpStop();
//		}
	}
#endif
	#if ICAT_WIFI
	if ((wifiParm & HOST_AP)== HOST_AP){
		printf("exit:0x%x\n",(wifiParm & HOST_AP));
		appHostAPStop();
	}
	#endif

	if ((wifiParm & DHCP_SERVER)== DHCP_SERVER){
		printf("exit:0x%x\n",(wifiParm & DHCP_SERVER));
		appDhcpServerStop();
	}

	#if ICAT_WIFI
	if((wifiParm & WIFI_LOAD)== WIFI_LOAD){
		printf("exit:0x%x\n",(wifiParm & WIFI_LOAD));
		if (appWiFiSuspend() != 0) {
			printf("WiFi suspend failed!\n");
			return;
		}
	}
	#endif

	wifiParm &= ~BTN_LCK;
	if (wifiParm == WIFI_INIT || wifiParm == STA_INIT){
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_sysevt_handler_set");
		ndk_sysevt_handler_set(NULL, 0);
		appNetStateReset(wifiParm);
		appSTAZombieCheckerStop();
	}
	if (semaNetDo)
		ybfwOsSemaphorePut(&semaNetDo);
}

/* External using API */
void appPtpStateSet(BOOL en)
{
	bPtpStatus = en;
}

BOOL appPtpStatGet(void)
{
	return bPtpStatus;
}

void appRtpStreamStateSet(BOOL en)
{
	bRtpStatus = en;
}


BOOL appRtpStreamStateGet(void)
{
	return bRtpStatus;
}

BOOL appIsStreamingActive(void)
{HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_st_is_streaming_started");
	return ndk_st_is_streaming_started();
}

BOOL appWiFiStartStateGet(void)
{
    BOOL state_ret = FALSE;

    if ((WIFI_INIT & DO_ASYNC) == DO_ASYNC){
        if(bNetDoInit)
            state_ret = TRUE;
    }
    return state_ret;
}

void appWiFiStartConnection(UINT32 wifiParm)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	ybfwProfLogPrintf(0, "%s:%x", __func__, wifiParm);
//	if ((wifiParm & DO_ASYNC) == DO_ASYNC) {
//		UINT32 priority = 20;
//
//		appBackgroundFuncExec(appNetDoInit, (UINT32)wifiParm, priority, 0);
//	}
//	else {
		appNetDoInit((UINT32)wifiParm);
//	}
}

/* zombie station checker */
#if SP5K_USE_STA_ZOMBIE_CHECKER
extern int hostapd_cli_main(int argc, char *argv[]);

#define ZOMBIE_PING_TIMEOUT 3000

static UINT8 zombieStaMacAddr[ETH_ALEN];
static int   zombieStaTimeoutMs = 7000;
static int   zombieStaRunCounter = 0;

static void
appSTAZombieDisassoc (UINT8 *macaddr)
{
	char * av[4] = {"hapc" , "-iwlan0", "disassociate", NULL};
	av[3] = (char *)eth_addr_ntoa(macaddr);
	hostapd_cli_main(4, av);
}

static void
appSTAZombieKickThread (ULONG param)
{
	UINT8 sta_addr[ETH_ALEN];
	char ifname[IF_NAMESIZE] = { 0 };
	UINT64 rx_bytes, last_rx_bytes = 0;
	struct in_addr ipaddr = { 0 };
	int ping_timeout = ZOMBIE_PING_TIMEOUT;
	int sta_timeout  = zombieStaTimeoutMs;
	int live_timeout = zombieStaTimeoutMs;
	int run_counter  = zombieStaRunCounter, err;
	UINT32 sleep_ms  = 500;
	BOOL kick_done   = FALSE;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	memcpy(sta_addr, zombieStaMacAddr, ETH_ALEN);

	HOST_PROF_LOG_PRINT(LEVEL_INFO, "[ZombieKick] Start RCnt:%d MAC:%s", run_counter, eth_addr_ntoa(sta_addr));

	while (run_counter == zombieStaRunCounter) {
		if (kick_done) {
			ybfwOsThreadSleep(500);
			continue;
		}

		if (!ifname[0]) {
		    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_get_hostap");
			const char *s = ndk_netif_get_hostap();
			if (s) {
				strncpy(ifname, s, IF_NAMESIZE);
				ifname[IF_NAMESIZE-1] = 0;
			}
			else {
				ybfwOsThreadSleep(500);
				continue;
			}
		}
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_dhcpd_find_inaddr");
		if (ipaddr.s_addr == 0 && !ndk_dhcpd_find_inaddr(sta_addr, &ipaddr)) {
			ybfwOsThreadSleep(500);
			continue;
		}

		if (sleep_ms > 0) {
			ybfwOsThreadSleep(sleep_ms);
			live_timeout -= sleep_ms;
			ping_timeout -= sleep_ms;
		}
		sleep_ms = 500;
		HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netmon_get_remote_host_info");
		err = ndk_netmon_get_remote_host_info(sta_addr, NDK_NETMON_HOST_RX_BYTES, &rx_bytes);
///		printf("[ZombieKick] rx bytes: err %d, %llu %llu\n", err, rx_bytes, last_rx_bytes);

 		if (!err && (rx_bytes != last_rx_bytes)) {
			last_rx_bytes = rx_bytes;
			live_timeout = sta_timeout;
			ping_timeout = ZOMBIE_PING_TIMEOUT;
			continue;
		}

		if (live_timeout <= 0) {
			// keep alive timeout, kick-out zombie station
			if (appRtpStreamStateGet())
				appStreamClose(); // prevent the liveStreaming from sending data

			appSTAZombieDisassoc(sta_addr);
			HOST_PROF_LOG_PRINT(LEVEL_WARN, "[ZombieKick] Kick-Out IP:%s, MAC:%s",
					inet_ntoa(ipaddr), eth_addr_ntoa(sta_addr));
			kick_done = TRUE;
			continue;
		}

		if (ping_timeout <= 0) {
			SINT64 t = ybfwOsTimeGet();

///			printf("ping TimeLeft %d\n", live_timeout);
			HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_ping ");
			err = ndk_ping(&ipaddr,
					live_timeout > 2000 ? 2000 : live_timeout,
					NULL);
			t = (ybfwOsTimeGet() - t) * 10;

			if (err) {
				HOST_PROF_LOG_PRINT(LEVEL_WARN, "[ZombieKick] Ping Fail IP:%s, MAC:%s, TimeLeft:%d",
						inet_ntoa(ipaddr), eth_addr_ntoa(sta_addr), live_timeout);
				sleep_ms = 0;
				live_timeout -= t > 2000 ? 2000 : t;
			}
			else {
				live_timeout = sta_timeout;
				ping_timeout = ZOMBIE_PING_TIMEOUT;
			}
		}
	}

	HOST_PROF_LOG_PRINT(LEVEL_INFO, "[ZombieKick] Exit RCnt:%d MAC:%s", run_counter, eth_addr_ntoa(sta_addr));
	ybfwOsThreadDelete(NULL);
}

void
appSTAZombieCheckerStart(UINT8 *macaddr, int timeoutInMs)
{
	zombieStaRunCounter++;
	memcpy(zombieStaMacAddr, macaddr, ETH_ALEN);

	zombieStaTimeoutMs = timeoutInMs;
	if (zombieStaTimeoutMs < 5000)
		zombieStaTimeoutMs = 5000;

	YBFW_THREAD *thrid = ybfwOsThreadCreate("appSTAZombieKickThread",
			appSTAZombieKickThread, 0,
			30, 0, 0, YBFW_TX_AUTO_START);
	HOST_ASSERT(thrid);
}

void
appSTAZombieCheckerStop()
{
	zombieStaRunCounter++;
}

#else /* YBFW_USE_STA_ZOMBIE_CHECKER */
void
appSTAZombieCheckerStart(UINT8 *macaddr, int timeoutInMs)
{
}

void
appSTAZombieCheckerStop()
{
}
#endif /* YBFW_USE_STA_ZOMBIE_CHECKER */

void appWiFiStopConnection(UINT32 wifiParm)
{
#if 0
	appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_EXCEPTION, 0, 0); /* notify the APP to do connection close */
#endif
	appNetDoExit(wifiParm);
}

#if 0  //NAV disable
void appWiFiVideoStop(BOOL opModeSw)
{
	appVideoFileRecord(0);
	if(opModeSw) {
		app_PTP_Set_DscOpMode(PTP_DSCOP_MODE_VIDEO_OFF);
		appStill_PIMA_Send_iCatch_Event(PTP_ICAT_EVENT_OPMODE_CHANGE,PTP_DSCOP_MODE_VIDEO_OFF,0);
		if(appVideoRemainSecGet()==0){
			memoryFullEvtFlg=1;
			appStill_PIMA_Send_Event(PTP_EC_STORE_FULL, YBFW_DRIVE_SD, 0, 0);
			HOST_PROF_LOG_PRINT(LEVEL_WARN, "appWiFi: Line(%d):[RTP] PTP_EC_STORE_FULL\n", __LINE__);
		}else if(memoryFullEvtFlg ){
			memoryFullEvtFlg=0;
			appStill_PIMA_Send_Event(PTP_EC_STORE_FULL, YBFW_DRIVE_SD, 1, 0);
			HOST_PROF_LOG_PRINT(LEVEL_WARN, "appWiFi: Line(%d):[RTP] PTP_EC_STORE_FULL\n", __LINE__);
		}
	}
}
#endif
/*--------------------------------- RTP Media Client -------------------------------------------*/

#if 0  //NAV //#ifdef APP_MEDIACLI_RTP
static void appMediaCliRtpMsgHandler(
	NDKMediaCliHandle handle,
	NDKMediaCliRtpMsg msg,
	long param)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	switch (msg) {
	case NDK_MEDIACLI_RTP_MSG_STARTED:
	{
		unsigned int w = 0xFFFF, h = 0xFFFF;
		ndk_mediacli_get_attribute(handle, YBFW_MEDIA_ATTR_WIDTH, &w);
		ndk_mediacli_get_attribute(handle, YBFW_MEDIA_ATTR_HEIGHT, &h);
		printf("START RTP Play: %u %u\n", w, h);

		ybfwDispAttrSet(YBFW_DISP_CHNL_1, YBFW_DISP_IMG_WINDOW, 0, 0, w, h);
		ybfwDispAttrSet(YBFW_DISP_CHNL_1, YBFW_DISP_OSD_WINDOW,0, 0, w, h);
		break;
	}
	case NDK_MEDIACLI_RTP_MSG_STOPPED:
	case NDK_MEDIACLI_RTP_MSG_EGENERIC:
	case NDK_MEDIACLI_RTP_MSG_TIMEOUT:
	case NDK_MEDIACLI_RTP_MSG_ECONN:
		if (handle) {
			ndk_mediacli_destroy(handle);
			gmediacli_handle = NULL;
		}
		break;
	default:
		break;
	}
}

void appMediaCliRtpInit(void)
{
	double starttime = 0.0;
	long opt_tcp = 0, opt_timeout = 10*10000;
	const char *url = "rtsp://192.168.1.1/h264";
	int c;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	if (gmediacli_handle) {
		printf("rtpcli is already started\n");
		return;
	}

	NDKMediaCliRtpOpt opts[] = {
		{NDK_MEDIACLI_RTP_OPT_TCP, opt_tcp},
		{NDK_MEDIACLI_RTP_OPT_TIMEOUT, opt_timeout},
	};

	printf("URL=%s\n", url);
	int err = ndk_mediacli_rtp_init(&gmediacli_handle, url, appMediaCliRtpMsgHandler, NDK_ARRAY_SIZE(opts), opts);

	if (err == 0) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_mediacli_start");
		ndk_mediacli_start(gmediacli_handle, starttime);
	}
	else {
		printf("init error %d", err);
	}

	return;

lUsage:
	printf("rtp [options or controls] -u RTSP-URL\n");
	printf("Controls:\n");
	printf("  -h : help\n");
	printf("  -C : enter control mode\n");
	printf("  -t : stop\n");
	printf("  -d : destroy\n");
	printf("Options for RTP Client:\n");
	printf("  -Otcp: Use TCP protocol.\n");
	printf("  -sm.m: start play at time m.m seconds\n");
	printf("  -idemux|null: sink type\n");
}
#endif

 /*---------------------------------STA function-------------------------------------------*/
#if 0
 static void app_sysevt_handler(NDKSysEvt evt, unsigned long param, unsigned long udata)
 {
	 switch (evt) {
	 case NDK_SYSEVT_HAPD_ASSOC:
		 printf("ASSOC: %s\n", eth_addr_ntoa((unsigned char*)param));
		 memcpy(gsta_hwaddr, (void *)param, 6);
		 break;

	 case NDK_SYSEVT_HAPD_DISASSOC:
		 isStaConnect = 0 ;

		 printf("DIS-ASSOC: %s\n", eth_addr_ntoa((unsigned char*)param));
		 break;

	 case NDK_SYSEVT_DHCPD_ALLOC_ADDR:
		 //printf("dhcpd alloc: %s\n", ipaddr_ntoa((ip_addr_t*)param));
		 break;

	 case NDK_SYSEVT_STA_CONNECTED:
		 printf("Sta: connected %s\n", param ? (const char*)param : "");
		 break;

	 case NDK_SYSEVT_STA_DISCONNECTED:
		 printf("Station mode: disconnected %s\n", param ? (const char*)param : "");
		 isStaConnect = 0 ;

		 printf("Sta: disconnected %s\n", param ? (const char*)param : "");
		 break;

	 case NDK_SYSEVT_DHCP_BOUND: {
		 extern void appWifiStationModeConnected(void);
		 printf(" Station mode DHCP: bound %s\n", param ? (const char*)param : "");
		 isStaConnect = 1 ;
		 appWifiStationModeConnected();

		 break; }

	 default:
		 printf("event %u, %lu\n", evt, param);
		 break;
	 }

	 if (gnet_sysevt)
		 ybfwOsEventFlagsSet(&gnet_sysevt, 1 << evt, TX_OR);
 }

 static void app_sysevt_register_handler(unsigned long udata)
 {
	 if (!gnet_sysevt) {
		 ybfwOsEventFlagsCreate(&gnet_sysevt, "netsysevt");
	 }

	 ndk_sysevt_handler_set(app_sysevt_handler, udata);
 }
#endif
 static void app_sysevt_clear(UINT32 evt)
 {
	 ybfwOsEventFlagsSet(&gnet_sysevt, ~(1 << evt), YBFW_TX_AND);
 }

 static BOOL app_sysevt_wait(UINT32 evt, UINT32 secs)
 {
	 if (gnet_sysevt) {
		 UINT32 cur = 0;
		 if (ybfwOsEventFlagsGet(&gnet_sysevt, 1 << evt, YBFW_TX_OR_CLEAR, &cur, 1000 * secs) == SUCCESS) {
			 if (cur & (1 << evt))
				 return TRUE;
		 }
	 }

	 return FALSE;
 }


#if ICAT_WIFI
 static BOOL appSTASSIDNameGet(char  *ssidname, char *password)
 {
	 UINT32 fd;
	 UINT32 Len,i,j=0,size=0;
	 UINT8	buffer[MAX_SSID_NAME_LEN+MAX_PASSWORD_LEN];
	 /* ssid name cfg file for ssid name save assigned by user */
	 fd = ybfwFsFileOpen((const UINT8 *)"B:/UDF/SSID_PW_STA.CFG",YBFW_FS_OPEN_RDONLY);

	 if(!fd)
	 {
		 printf("ssid cfg file open fail!\n");
		 ybfwFsFileClose(fd);
		 return FAIL;
	 }

	 if(ssidname[0]) /* user assign custom ssid name */
	 {
		 memcpy(ssidname, ssidname, MAX_SSID_NAME_LEN*sizeof(UINT8));
		 printf("[custom]ssidname = %s\n", ssidname);
	 }
	 else /* read default or user save ssid name */
	 {
		 size = ybfwFsFileSizeGet(fd);
		 Len = ybfwFsFileRead(fd, buffer, size);

		 for(i=0 ;i<Len ;i++ ){
			 if(buffer[i]==0x0d && buffer[i+1]==0x0A ){
				 memcpy(ssidname, buffer, i);
				 ssidname[i]='\0';
				 for(j=i+2 ;j<Len ;j++ )
					 if((buffer[j]==0x0d && buffer[j+1]==0x0A) ||(j==Len-1)  ){

						 memcpy(password, buffer+i+2, Len-i-2);
						 if(buffer[j]==0x0d){;
						  password[j-i-2]='\0';
						 }
						break;
				 }
				 break;
			 }
		 }
		 printf("[cfg]ssidname = %s Password=%s,Len = %d i=%d j=%d\n", ssidname,password ,Len ,i ,j);

	 }

	 if(ssidname[0]){/* if no change, won't write the ssid name to file again */
		 ybfwFsFileWrite( fd, buffer, (MAX_SSID_NAME_LEN+MAX_PASSWORD_LEN)*sizeof(UINT8) );
	 }

	 ybfwFsFileClose(fd);
	 return SUCCESS;
 }

 void app_station_mode(void)
 {
	 char ifname[32] = { 0 };
	 char ssid[32]	 = { 0 };
	 char psk[32]	 = { 0 };
	 int res;

	 char tssid[32]   = { 0 };
	 char tpsk[32]	  = { 0 };
	 appSTASSIDNameGet(tssid, tpsk);

	 strcpy(ifname,  WIFI_IFNAME);
	 strcpy(ssid,	 tssid);
	 strcpy(psk,	 tpsk);
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	 if (!ssid[0]) {
		 printf("No SSID is provided\n");
		 return;
	 }

	 printf("\n= STA Mode on %s. Join '%s'\n", ifname, ssid);
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCS_IF_UP");
	 ndk_netif_ioctl(NDK_IOCS_IF_UP, (long)ifname, NULL);

	 char *av[] = { "-Cwpas_conf", "-i", (char*)ifname
#ifdef SP5K_WIFI_MAC80211
		, "-Dnl80211"
#else
		, "-Dwext"
#endif
	};
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_wpas_start_daemon");
	 res = ndk_wpas_start_daemon(ARRAY_SIZE(av), av);


	 app_sysevt_clear(NDK_SYSEVT_STA_CONNECTED);
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
	 ndk_netif_ioctl(NDK_IOCS_IF_DEFAULT, (long)WIFI_IFNAME, NULL);

	 NDKWpasOpt opts_psk[] = {
		 {"proto", "WPA RSN"},
		 {"key_mgmt", "WPA-PSK"},
		 {"pairwise", "CCMP TKIP"},
		 {"group", "CCMP TKIP WEP104 WEP40"},
		 {"*psk", psk},
		 {NULL, NULL}
	 };

	 NDKWpasOpt opts_none[] = {
		 {"key_mgmt", "NONE"},
		 {NULL, NULL}
	 };

	 NDKWpasOpt *opts = psk[0] ? opts_psk : opts_none;
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
	 res = ndk_wpas_add_network(ssid, opts);
	 if (res < 0) {
		 printf("add network err(%d)\n", res);
		 return;
	 }
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
	 res = ndk_wpas_enable_network(ssid, TRUE);
	 if (res < 0) {
		 printf("enable network err = %d\n", res);
		 return;
	 }

	 if (!app_sysevt_wait(NDK_SYSEVT_STA_CONNECTED, 10)) {
		 printf("Connect failed\n");
		 return;
	 }

	 app_sysevt_clear(NDK_SYSEVT_DHCP_BOUND);
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_set_address_s");
 //  ndk_netif_set_address_s(ifname, "0.0.0.0", "255.255.255.0", NULL);
	 ndk_netif_set_address_s(ifname, "0.0.0.0", "0.0.0.0", "0.0.0.0");
	 ndk_dhcp_start(ifname);

	 if (!app_sysevt_wait(NDK_SYSEVT_DHCP_BOUND, 10)) {
		 printf("DHCP bound failed\n");
	 }

 }

 void appSTAServerStop(void)
 {
	 char ifname[32] = { 0 };
	 strcpy((char *)ifname,  WIFI_IFNAME);
	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_dhcp_stop");
	 ndk_dhcp_stop(ifname);
	 ndk_wpas_stop_daemon();
 }


int appScanBestChannel(
    UINT8 *ssidname)
{

    /*
       if ssidname != 0 , check essid exit or not for STA mode
          ret = 1 exit , others not exit
       if ssidname == 0 , find the best channel for AP mode
         ret = -1 not find , others is channel
    */
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    int ret = -1 ;
    int bestchannel = 11 ;
    UINT8 ifname[IF_NAMESIZE];

    if ( ssidname == 0 ) {
        NDKWifiChanArray24G ca;
        void *pca = (void *)&ca ;
        long ch = -1 ;
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCG_IF_INDEXTONAME");
        if (ndk_netif_ioctl(NDK_IOCG_IF_INDEXTONAME, 1, (long *)ifname) != 0) {
            printf("ERROR:indextoname\n");
        } else {
            memset(ifname,0,IF_NAMESIZE);
            snprintf((char *) ifname, IF_NAMESIZE , "%s", WIFI_IFNAME);
        }
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCG_WIFI_ACS_24G");
        ndk_netif_ioctl(NDK_IOCG_WIFI_ACS_24G, (long)ifname, &ch);
        printf("ch = %d\n", (int ) ch);
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCG_WIFI_BEST_CHAN_24G");
        if (ndk_netif_ioctl(NDK_IOCG_WIFI_BEST_CHAN_24G, (long)ifname, (long *)pca) == 0) {
            ret = ca.channels[0];
            /* debug print */
            #if 0
            /* command: net acs list */
            int i;
            printf("==============>>>> bestchannel:%d\n", bestchannel);
            printf("==============>>>> ca.num:%d\n", ca.num);
            for (i = 0; i < ca.num && i < ARRAY_SIZE(ca.channels); ++i) {
                printf("==============>>>> ca->channels[%d]:%d\n", i , ca.channels[i]);
            }
            #endif
        } else {
            ret = ch ;
        }
        printf("BEST CHANNEL = %d  , (pre=%d)\n", ret, bestchannel);
    } else {
        /* toDO: */
        printf("\n\nSOMETHING MISTAKE!!   no return best channel\n");
    }
    return ret ;
}

#endif

UINT8 appWifiInitModeGet(void){
	return WIFIInitMode;

}
void appWifiInitModeSet(UINT8 mode){
	WIFIInitMode = mode ;
}


