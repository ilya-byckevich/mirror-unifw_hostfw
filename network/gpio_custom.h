/**************************************************************************
 *
 *       Copyright (c) 2006-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef GPIO_CUSTOM_H
#define GPIO_CUSTOM_H

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/

#include "customization/dev_init.h"
#include "gpio_def.h"


#include GPIO_INC

#if defined(HW_EVB)
#include "gpio_custom_evb.h"
#elif defined(HW_WDV4K)
#include "gpio_custom_wdv4k.h"
#elif defined(HW_PRJ)
#include "gpio_custom_prj.h"
#elif defined(HW_SBC)
#include "gpio_custom_sbc.h"
#elif defined(HW_YKN)
#include "gpio_custom_ykn.h"
#elif defined(HW_360)
#include "gpio_custom_360.h"
#elif defined(HW_CVR)
#include "gpio_custom_cvr.h"
#elif defined(HW_RND610MMC)
#include "gpio_custom_rnd610mmc.h"
#else
#error Unsupported gpio Configured !
#endif


#endif  /* GPIO_CUSTOM_H */


