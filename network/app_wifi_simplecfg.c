/**************************************************************************
 *
 *       Copyright (c) 2012 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch Technology,
 *  Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan, R.O.C.
 *
 *  Author:
 *
 **************************************************************************/
#if 0
#define SP5K_SIMPLECONFIG
#define SP5K_SIMPLECONFIG_RECV
#define SP5K_SIMPLECONFIG_SEND
#define SP5K_SIMPLECONFIG_DEMOAPP_FLOW
#endif

#if defined(SP5K_SIMPLECONFIG) 

#define SIMPLECFG_DEBUG 1

#if defined(SPCA6350)
    #if !SP5K_NDK2
        #error "NEED NDK2"
    #endif
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ndk_global_api.h>
#if SP5K_NDK2
#include <ndk_tcpip_api.h>
#include <linux/wireless.h>
#include "ndk_socutils.h"
#include "ndk_wpas.h"
extern int ndk_netif_standard_ioctl(int cmd, void *arg);
#define netif_ioctl ndk_netif_standard_ioctl
#else
#include "app_simplecfg_wireless.h"
extern int netif_ioctl(int cmd, void *arg);
#endif

#include "ybfw_global_api.h"

typedef void (* aes_callback) (char* result, int result_len);  
extern int ndk_socket_create(int family, int type, int protocol);
extern int ndk_socket_destroy(int skfd);
extern int simpleconfig_recv_start(char *keydata, int scan_timeout, int prog_timeout, int priorty, aes_callback in);
extern int simpleconfig_send(char *keydata, char *data, int send_round);
extern int simpleconfig_recv_stop();

extern void appWiFiStartConnection(UINT8 wifiParm);

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#if SIMPLECFG_DEBUG
#define SIMPLECFG_MESSAGE(fmt, ...) do { printf("[%s, %d]"fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__);} while(0)
#else
#define SIMPLECFG_MESSAGE(fmt, ...) do {} while (0)
#endif


/**************************************************************************
 *                         F U N C T I O N S                              *
 **************************************************************************/
/**
 * @brief simple configure demo flow function. 
 *  This is a demo flow for simple configure. All source code is duplicated from
 *  app_net_broadcast.c and wifi station connection sample code.
 */

#if defined(SP5K_SIMPLECONFIG_DEMOAPP_FLOW) 
#include <jansson.h>

/* ducpcated from app_wifi_utility.h */
#define PTP_IP			0x08
#define RTP_STREM		0x10
#define DO_ASYNC		0x80

/**
 * @brief simple configure demo flow function. 
 *  This is a demo flow for simple configure. All source code is
 *  duplicated from wifi station connection sample code.
 */
static UINT8 gSTAip[20]={'\0'};
static UINT8 gSTAssidname[30]={'\0'};
static UINT8 gSTApassword[60]={'\0'};
static YBFW_EVENT_FLAGS_GROUP gNetEvent = 0;

static void appSimpleConfigNetEventHandler(NDKSysEvt evt, unsigned long param, unsigned long udata)
{
	switch (evt) {
		case NDK_SYSEVT_STA_CONNECTED:
			printf("NDK_SYSEVT_STA_CONNECTED %s\n", param ? (const char*)param : "");
			break;
		case NDK_SYSEVT_STA_DISCONNECTED:
			printf("NDK_SYSEVT_STA_DISCONNECTED %s\n", param ? (const char*)param : "");
			break;
		case NDK_SYSEVT_DHCP_BOUND:
			printf("NDK_SYSEVT_DHCP_BOUND %s\n", param ? (const char*)param : "");
			break;
		default:
			printf("event %u, %lu\n", evt, param);
			break;
	}
	if ( evt < NDK_SYSEVT_NR) {
		if (gNetEvent)
			ybfwOsEventFlagsSet(&gNetEvent, 1 << evt, TX_OR);
	}
}

static void appSimpleCfgNetEventRegisterHandler(unsigned long udata)
{
	if (!gNetEvent) {
		ybfwOsEventFlagsCreate(&gNetEvent, "NetEvent");
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_sysevt_handler_set");
	ndk_sysevt_handler_set(appSimpleConfigNetEventHandler, udata);
}

static void appSimpleCfgNetEventClear(UINT32 evt)
{
	ybfwOsEventFlagsSet(&gNetEvent, ~(1 << evt), TX_AND);
}

static BOOL appSimpleCfgNetEventWait(UINT32 evt, UINT32 secs)
{
	if (gNetEvent) {
		UINT32 cur = 0;
		if (ybfwOsEventFlagsGet(&gNetEvent, 1 << evt, TX_OR_CLEAR, &cur, 1000 * secs) == SUCCESS) {
			if (cur & (1 << evt))
				return TRUE;
			}
		}
	return FALSE;
}


static BOOL appSimpleCfgWiFiStationMode(void)
{
    char ifname[32] = { 0 };
    int res ;
	strcpy((char *)ifname,  "wlan0");

	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if (!gSTAssidname[0]) {
		printf("[HOST] No SSID is provided [%s, %d] err return\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

	// register a flag
	appSimpleCfgNetEventRegisterHandler(0);
	
    printf("STA Mode on %s. Join '%s'\n", ifname, gSTAssidname);
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCS_IF_UP");
    ndk_netif_ioctl(NDK_IOCS_IF_UP, (long)ifname, NULL);
    char *av[] = { "-Cwpas_conf", "-Dwext", "-i", ifname };
    res = ndk_wpas_start_daemon(4, av);
	
	NDKWpasOpt opts_psk[] = {
			{"proto", "WPA RSN"},
			{"key_mgmt", "WPA-PSK"},
			{"pairwise", "CCMP TKIP"},
			{"group", "CCMP TKIP WEP104 WEP40"},
			{"*psk", (char *)gSTApassword},
			{NULL, NULL}
	};
    NDKWpasOpt opts_none[] = {
        {"key_mgmt", "NONE"},
        {NULL, NULL}
    };

    NDKWpasOpt *opts = gSTApassword[0] ? opts_psk : opts_none;
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_wpas_add_network");
	res = ndk_wpas_add_network((const char *)gSTAssidname, opts);
    if (res < 0) {
		printf("[HOST] wpa add network err(%d) [%s, %d] err return\n", res, __FUNCTION__, __LINE__);
        return FAIL;
    }

    appSimpleCfgNetEventClear(NDK_SYSEVT_STA_CONNECTED);
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_wpas_enable_network");
    res = ndk_wpas_enable_network((const char *)gSTAssidname, TRUE);
    if (res < 0) {
		printf("[HOST] wpa enable network err(%d) [%s, %d] err return\n", res, __FUNCTION__, __LINE__);
        return FAIL;
    }

    if ( appSimpleCfgNetEventWait(NDK_SYSEVT_STA_CONNECTED, 100) == FALSE) {
		printf("[HOST] Connect failed [%s, %d] err return\n", __FUNCTION__, __LINE__);
        return FAIL;
    }

    appSimpleCfgNetEventClear(NDK_SYSEVT_DHCP_BOUND);
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_set_address_s");
	ndk_netif_set_address_s(ifname, "0.0.0.0", "0.0.0.0", "0.0.0.0");
	ndk_dhcp_start(ifname);
    if (appSimpleCfgNetEventWait(NDK_SYSEVT_DHCP_BOUND, 100) == FALSE) {
		printf("[HOST] DHCP bound failed [%s, %d] err return\n", __FUNCTION__, __LINE__);
		return FAIL;
    }
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCS_IF_DEFAULT");
	ndk_netif_ioctl(NDK_IOCS_IF_DEFAULT, (long)ifname, NULL);

	return SUCCESS;
}


#define EINPROGRESS 119  /* Connection already in progress */


#define TCP_SERVER_PORT 5001
#define MULTICAST_SERVER_PORT 5002
#define MULTICAST_ADDR "234.168.168.168"
#define MULTICAST_SEND_COUNTER 100
#define CHECK_PATTERN "{\"key\":\"ICATCHTEK\",\"id\":\"%s\"}"
/* {"key":"ICATCHTEK","id":"00000000000000000000"} */
int appSimpleCfgSendMessageSocket(void)
{

    struct sockaddr_in sDestAddr;
    int socketfd = -1 ;
    int flags;
    int ret = -1 ;
    char content[128];
    int size_send = 0 ;
    int total_size = 0 ;
    int sock_err = 0 ;
    char *pbuff ;
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");

    if (gSTAip[0]=='\0'){
        printf("[%s(%d)]\n",__FUNCTION__,__LINE__);
        return ret;
    }

    printf("[%s(%d)] connect to %s\n",__FUNCTION__,__LINE__,gSTAip);

    memset(&sDestAddr, 0, sizeof(sDestAddr));
    sDestAddr.sin_family = AF_INET;
    sDestAddr.sin_addr.s_addr = inet_addr((char *)gSTAip);
    sDestAddr.sin_port =  htons(TCP_SERVER_PORT);
    
	socketfd = socket(AF_INET, SOCK_STREAM, 0);

	if (socketfd < 0) {
        printf("[%s(%d)]\n",__FUNCTION__,__LINE__);
		return ret;
	}

	setsockopt_int(socketfd, SOL_SOCKET, SO_REUSEADDR, 1);
	setsockopt_int(socketfd, SOL_SOCKET, SO_REUSEPORT, 1);

	flags = lwip_fcntl(socketfd, F_GETFL, 0);

	if (lwip_fcntl(socketfd , F_SETFL, flags | O_NONBLOCK) != 0) {
        goto ERROR_EXIT;
	}

	if (connect(socketfd, (struct sockaddr *) &sDestAddr , sizeof(sDestAddr)) < 0 ) {
		int err = lwip_geterr(socketfd);
		if (err != EINPROGRESS) {
            printf("[%s(%d)] err %d\n",__FUNCTION__,__LINE__,err);
			goto ERROR_EXIT;
		} else {
			struct timeval tv; 
			fd_set wfds;
			socklen_t lon;
			int valopt;
			lon = sizeof(int); 

			tv.tv_sec = 5; 
			tv.tv_usec = 0; 

			FD_ZERO(&wfds); 
			FD_SET(socketfd, &wfds); 

			if (select(socketfd+1, NULL, &wfds, NULL, &tv) > 0) { 
				getsockopt(socketfd, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon); 
				if (valopt) {
                    printf("[%s(%d)] valopt %d\n",__FUNCTION__,__LINE__,valopt);
                    goto ERROR_EXIT;
				} 
			} else {
                printf("[%s(%d)]\n",__FUNCTION__,__LINE__);
                goto ERROR_EXIT;
			}
		}
	}


    /* Send Message */
    memset(content, 0, sizeof(content));
    sprintf(content,CHECK_PATTERN  ,"00000000000000000000" );
    printf("[%s] content %s\n",__FUNCTION__,content);
    total_size = strlen((const char *)content) + 1 ;
    pbuff = content ;
    size_send = send(socketfd, pbuff, total_size, 0);
    if (size_send < 0) {
        sock_err = lwip_geterr(socketfd);
        printf("[%s(%d)] sock_err %d",__FUNCTION__,__LINE__,sock_err);
        goto ERROR_EXIT;
    } 

    ret = 0 ;

ERROR_EXIT:
    if ( socketfd != -1 ) {
        closesocket(socketfd);
    }
    return ret ;
}


static int appSimpleCfgSendMessageMultiCast(void)
{

    struct sockaddr_in sDestAddr;
    struct sockaddr_in sLocalAddr;
    int total_size = 0 ;
    int socketfd = -1 ;
    int ret = -1 ;
    int val = 1;
    int i = 0 ;
    char content[128];
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if (gSTAip[0]=='\0'){
        printf("[%s(%d)]\n",__FUNCTION__,__LINE__);
        return ret;
    }

    printf("[%s(%d)] connect to %s\n",__FUNCTION__,__LINE__,MULTICAST_ADDR);


    socketfd = socket(PF_INET, SOCK_DGRAM, 0);

    if(socketfd < 0) {
        printf("socket ERROR\n");
        return ret;
    }

	if (setsockopt (socketfd, SOL_SOCKET, SO_REUSEADDR, (char *) & val, sizeof (val)) < 0) {
		printf("setsockopt SO_REUSEADDR error\n");
		goto ERROR_EXIT;
	}
    
	if (setsockopt (socketfd, SOL_SOCKET, SO_BROADCAST, (char *) & val, sizeof (val)) < 0) {
		printf("setsockopt SO_BROADCAST error\n");
		goto ERROR_EXIT;
	}

    memset((char *)&sLocalAddr, 0, sizeof(sLocalAddr));
    memset((char *)&sDestAddr, 0, sizeof(sDestAddr));

    sDestAddr.sin_family = AF_INET;
    sDestAddr.sin_addr.s_addr = inet_addr(MULTICAST_ADDR) ;
    sDestAddr.sin_port = htons(MULTICAST_SERVER_PORT) ;

    sLocalAddr.sin_family = AF_INET;
    sLocalAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if( bind(socketfd, (struct sockaddr *)&sLocalAddr, sizeof(sLocalAddr)) < 0) {
        printf("bind ERROR\n");
        goto ERROR_EXIT;
    }

    memset(content, 0, sizeof(content));
    sprintf(content,CHECK_PATTERN  ,"00000000000000000000" );
    total_size = strlen((const char *)content) + 1 ;
    printf("[%s] content %s\n",__FUNCTION__,content);

    i = 0 ;
    while (1) {
        sendto(socketfd, content, total_size, 0, (struct sockaddr *)&sDestAddr, sizeof(sDestAddr));
        ybfwTimeDelay(YBFW_TIME_DELAY_1MS,100);
        i++ ;
        if (i > MULTICAST_SEND_COUNTER ) {
            break;
        }
    }
    ret = 0 ;


ERROR_EXIT:
    if ( socketfd != -1 ) {
        closesocket(socketfd);
    }
    return ret ;

}

#endif


/**************************************************************************
 *                         F U N C T I O N S                              *
 **************************************************************************/
/**
 * @brief simple configure recv function. 
 *  
 */
#if defined(SP5K_SIMPLECONFIG_RECV)
volatile static int simplecfgdone= 0 ;
UINT32 current_mode = 0;
#define IW_MONITOR_MODE 6
#define KEY_LEN_LIMIT 16
#define SIMPLECFG_SINGLECHANSCAN_TIMEOUT 100 // ms
#define SIMPLECFG_PROGRAM_TIMEOUT 	60 //actual ~20 sec
#define SIMPLECFG_PRIORITY			19

static BOOL appModeSwitch(UINT32 tmode)
{
    if(tmode > 8)
        return FALSE;   
    int skfd;
    struct iwreq iwr;
    memset(&iwr, 0, sizeof(iwr));
    strcpy(iwr.ifr_ifrn.ifrn_name, "wlan0");
    iwr.u.mode = tmode; /*iw operation mode*/
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_create");
    if ((skfd = ndk_socket_create(2, 0, 0)) < 0) { //NDK_AF_NETLINK
        printf("socket err @ %s:%d\n", __func__, __LINE__);
        return FALSE;
    }

    if ( netif_ioctl(SIOCSIWMODE, &iwr) <0 ){
        printf("ioctl SIOCSIWMODE err @ %s:%d\n", __func__, __LINE__);
        return FALSE;
    }
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_destroy");
    ndk_socket_destroy(skfd);
    return TRUE;
}  /*eof appModeSwitch*/


static UINT32 appModeget()
{
    int skfd;	
    struct iwreq iwr;
    memset(&iwr, 0, sizeof(iwr));
    strcpy(iwr.ifr_ifrn.ifrn_name, "wlan0");
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_create");
    if ((skfd = ndk_socket_create(2, 0, 0)) < 0) { //NDK_AF_NETLINK
        printf("socket err @ %s:%d\n", __func__, __LINE__);
        return FALSE;
    }
	if ( netif_ioctl(SIOCGIWMODE, &iwr) <0 ){
		printf("[HOST] ioctl SIOCGIWMODE fail [%s, %d] err return\n", __func__, __LINE__);
        return FALSE;
    }
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_destroy");
    ndk_socket_destroy(skfd);
    return iwr.u.mode;
}  /*eof appModeSwitch*/



/**
 * @brief callback function.
 * result string will hold in json format like {"id":"LinkIt_Smart_7688_1B24F0","pwd":"","ip":"192.168.100.111","mac":"001f1fb439f8"} if sender answer
 * parse result string and fill up 'gSTAssidname' and 'gSTApassword
 * @param[in] pointer of result
 * @param[in] length of result
 * @return s
 */
static void app_aesresult(char *result, int result_len){

    simplecfgdone = 1 ;

    if(result == NULL){
		printf("[Host]err return at [%s, %d]\n", __func__, __LINE__);
		return;
	}
	
    #if defined(SP5K_SIMPLECONFIG_DEMOAPP_FLOW) 
    	/*--- decode json fromat ---*/ 
    	json_t * jroot= NULL;
    	json_error_t jerror;
    	json_t * jitem_item=NULL;
    	const char *id = NULL;
    	const char *pwd =NULL;
        const char *ip =NULL;
    	jroot = json_loadb(result , strlen(result) , JSON_DECODE_ANY | JSON_DISABLE_EOF_CHECK , &jerror);
    	if(jroot)
    	{
    		if( NULL != (jitem_item = json_object_get(jroot, "id")) ){
    			id = json_string_value(jitem_item);
                strcpy((char *)gSTAssidname, id);
            }
    		if( NULL != (jitem_item= json_object_get(jroot, "pwd")) ){
    			pwd = json_string_value(jitem_item);
                strcpy((char *)gSTApassword, pwd);
            }
            if( NULL != (jitem_item= json_object_get(jroot, "ip")) ){
                ip = json_string_value(jitem_item);
                strcpy((char *)gSTAip, ip);
            }
                		
    		SIMPLECFG_MESSAGE("set gSTAssidname : %s @ %s:%d\n", gSTAssidname, __func__, __LINE__);
    		SIMPLECFG_MESSAGE("set gSTApassword : %s @ %s:%d\n", gSTApassword, __func__, __LINE__);
            SIMPLECFG_MESSAGE("set gSTAip       : %s @ %s:%d\n", gSTAip      , __func__, __LINE__);
            json_decref(jroot);
    	}else{
    		printf("[HOST]No SSID is provided [%s, %d] err return\n", __FUNCTION__, __LINE__);
    		return ;
    	}
    #else
        printf("aes check result_len:%d\n", result_len);
        printf("aes check result:%s\n", result);
    #endif
}


void simplecfg_recv(int argc, char *argv[]) {
    int scan_timeout = SIMPLECFG_SINGLECHANSCAN_TIMEOUT;/*ms*/
    int program_timeout = SIMPLECFG_PROGRAM_TIMEOUT; 	/*sec*/
    int priority = SIMPLECFG_PRIORITY;
	char keydata[KEY_LEN_LIMIT+1] = {'\0'}; 
	
     
    if (argc == 2 && argv[1]){
        strcpy(keydata, argv[1]);
        printf("keyget:%s\n", keydata);
    }

    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    simplecfgdone = 0 ;

    /*--- load driver ---*/
    #if SP5K_NDK2
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netdev_load_driver ");
    if (ndk_netdev_load_driver("rtkwlan", NULL) != 0){
    #else
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netdrv_init");
	if (ndk_netdrv_init("rtk") != 0){
    #endif
        printf("[HOST]Driver load fail [%s, %d] err return\n", __FUNCTION__, __LINE__);
        return;
    }
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK  ndk_netif_ioctl(NDK_IOCS_IF_UP");
    ndk_netif_ioctl(NDK_IOCS_IF_UP, (long)"wlan0", NULL);
    ndk_netif_set_address_s("wlan0", "192.168.1.1", "255.255.255.0", "0.0.0.0");

    /*--- get mode ---*/
    current_mode = appModeget();

    /*--- set monitor mode ---*/
    appModeSwitch(IW_MONITOR_MODE);

    /*--- aesdecode ---*/
    simpleconfig_recv_start(keydata, scan_timeout, program_timeout, priority, app_aesresult); // app_aesresult is the callback

    while ( simplecfgdone == 0  ) {
        ybfwTimeDelay(YBFW_TIME_DELAY_1MS,500);
    }
    simpleconfig_recv_stop();

    /* Check Stop */
    appModeSwitch(current_mode);

    #if defined(YBFW_SIMPLECONFIG_DEMOAPP_FLOW) 
    	
    	/*--- blocking until callback fill-up password and ssid ---*/
    	if (gSTAssidname[0]=='\0'){
            return;
    	}

        /*--- prepare and start to response broadcast ---*/
    	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCS_IF_DOWN");
        ndk_netif_ioctl(NDK_IOCS_IF_DOWN, (long)"wlan0", NULL);

        /*--- then trigger into station mode ---*/
    	appSimpleCfgWiFiStationMode();

        appWiFiStartConnection(DO_ASYNC|PTP_IP|RTP_STREM);

        if ( appSimpleCfgSendMessageSocket() != 0) {
            appSimpleCfgSendMessageMultiCast();
        }

    #else
        /* Set wlan0 interface down */
        HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl(NDK_IOCS_IF_DOWN");
        ndk_netif_ioctl(NDK_IOCS_IF_DOWN, (long)"wlan0", NULL);
    #endif
}
#endif /*YBFW_SIMPLECONFIG_RECV*/


/**************************************************************************
 *                         F U N C T I O N S                              *
 **************************************************************************/
/**
 * @brief simple configure send function. 
 *  
 */

#if defined(SP5K_SIMPLECONFIG_SEND)
#define SSID_NAME_LIMIT	128
static UINT8  gAPssidname[SSID_NAME_LIMIT]={'\0'};
static UINT8  host[30];
static UINT8  mac[14];
#define DEFAULT_INTERFACE "wlan0"
#define TEST_PATTERN "{\"id\":\"%s\",\"pwd\":\"%s\",\"ip\":\"%s\",\"mac\":\"%s\"}"


static int appWirelessSsidGetSet(void){
	int ret = 0 ;
	int skfd = 0;
	struct iwreq iwr;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	if ((skfd = ndk_socket_create(2, 0, 0)) < 0) { //NDK_AF_NETLINK
   		printf("[HOST]socket create [%s, %d] err return\n", __func__, __LINE__);
       	return -1;
	}
	
	memset(&iwr, 0, sizeof(iwr));
	strcpy(iwr.ifr_ifrn.ifrn_name, DEFAULT_INTERFACE);
   	iwr.u.essid.pointer = (caddr_t)gAPssidname;
   	iwr.u.essid.length = SSID_NAME_LIMIT; //30
	
#if SP5K_NDK2
   	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_standard_ioctl(SIOCGIWESSID");
	if (ndk_netif_standard_ioctl(SIOCGIWESSID, &iwr) < 0)
#else
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK netif_ioctl( SIOCGIWESSI");
	if (netif_ioctl( SIOCGIWESSID, &iwr) < 0)
#endif
	{
		printf("[HOST]ioctl SIOCGIWESSID fail [%s, %d] err return\n", __func__, __LINE__);
		ret = -1;
	}

   	if (skfd != -1){
   	 HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_destroy");
        ndk_socket_destroy(skfd);
		skfd = -1;
   	}
   return ret;

}


static int appIFAddrGetSet(void){
	int ret = 0 ;
	int skfd = 0;
	struct in_addr addr;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_socket_create");
	if ((skfd = ndk_socket_create(2, 0, 0)) < 0) { //NDK_AF_NETLINK
   		printf("[HOST]socket create [%s, %d] err return\n", __func__, __LINE__);
       	return -1;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_netif_ioctl");
	if (ndk_netif_ioctl(NDK_IOCG_IF_ADDR, (long)DEFAULT_INTERFACE, (long *)(void *)&addr) < 0) 
	{	
		printf("[HOST]ioctl NDK_IOCG_IF_ADDR fail [%s, %d] err return\n", __func__, __LINE__);
		ret = -1;
		goto IF_ADDR_GET_SET_EXIT;
	}
	sprintf((char *)&host, "%s", inet_ntoa(addr));

IF_ADDR_GET_SET_EXIT:
	if (skfd != -1){
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
		ndk_socket_destroy(skfd);
		skfd = -1;
	}
	return ret;
}


static int appIFHwaddrGetSet(void){
	int skfd =0;
	int ret = 0;
	UINT8 macaddr[IF_HWADDRSIZE];
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
	if ((skfd = ndk_socket_create(2, 0, 0)) < 0) { //NDK_AF_NETLINK
   		printf("[HOST]socket create [%s,%d] err return\n", __func__, __LINE__);
       	return -1;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
    if (ndk_netif_ioctl(NDK_IOCG_IF_HWADDR, (long)DEFAULT_INTERFACE, (long *)macaddr) < 0) {
    	ret = -1;
        goto IF_HWADDR_GET_SET_EXIT;;
    }
    snprintf((char *)mac, IF_HWADDRSIZE , "%02x%02x%02x%02x%02x%02x", macaddr[0], macaddr[1], macaddr[2] ,macaddr[3], macaddr[4], macaddr[5]);

IF_HWADDR_GET_SET_EXIT:
	if (skfd != -1){
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ");
		ndk_socket_destroy(skfd);
		skfd = -1;
	}
	return ret;
}


void simplecfg_send(int argc, char *argv[])
{
	char keydata[KEY_LEN_LIMIT+1] = {'\0'};
    char data[512] = {'\0'};
    int send_round = 1000;
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");

    /*--- access current ssid, ip addr, mac addr for creating content data string ---*/
	if(appWirelessSsidGetSet() < 0){
		printf("[Host]Un-connected to an AP\n");
		return;
	}
	if(appIFAddrGetSet() < 0){
		printf("[Host]Cannot get IP addr\n");
		return;
	}
	if(appIFHwaddrGetSet() < 0){
		printf("[Host]Cannot get MAC addr\n");
		return;
	}
	
	if( (argc != 2) || (argv[1]==NULL)){
		printf("[Host]cmd> simplesend [AP's psk]\n"); 
		return;
	}

    sprintf(data,TEST_PATTERN,gAPssidname,argv[1], host , mac);	

	/********************************************************************/
	/*															    	*/
	/* 	Define Your optional key or use the default one  in the NDK     */
	/* memcpy(keydata, USER_DEF_OPT_KEY , KEY_LEN_LIMIT*sizeof(char)); 	*/
	/*															   		*/
	/********************************************************************/
	
	/*--- pass (keydata) , content data, send round to NDK ---*/
	simpleconfig_send(keydata, data, send_round);
	
	
}
#endif /*YBFW_SIMPLECONFIG_SEND*/
#endif /*YBFW_SIMPLECONFIG*/

#if 0
   /* Command Sample */

   For V33:
    else if (strcmp(*argv, "simplerecv")==0){
        void simplecfg_recv(int argc, char *argv[]);
        simplecfg_recv(0 , NULL);
	}
    else if (strcmp(*argv, "simplesend") == 0){
        void simplecfg_send(int argc, char *argv[]);
        simplecfg_send(argc, argv);
	}
   For V35:
    else if (strcmp(param, "simplerecv")==0){
        extern void simplecfg_recv(int argc, char *argv[]);
        simplecfg_recv(0 , NULL);
        }
    else if (strcmp(param, "simplesend") == 0){
        extern void simplecfg_send(int argc, char *argv[]);
	simplecfg_send(argc, arg);
        }

    /* simplesend LinkIt_Smart_7688_1B24F0  */

#endif

