/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <api/cmd.h>
#include "app_bt_cmd.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "app_view_param.h"
#include "app_still.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "app_wifi_connection.h"
#include "ndk_global_api.h"
#include "app_wifi_utility.h"
#include "ndk/netif.h"
#include "ndk_socutils.h"
#include "app_bt_utility.h"
#include "linux/socktypes.h"
#include "app_key_def.h"

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static btCmd_t btCmdsList[] =
{
	{ "wifi"	, appBTCmdWiFi		, NULL },
	{ "system"	, appBTCmdSystem	, NULL },
	{ "event"	, appBTCmdEvent		, NULL },
	{ "ptp"		, appBTCmdPtp		, NULL },
	{ "bin"		, appBTCmdBin		, NULL },
	{ "bt"		, appBTCmdBT		, NULL },
	/*, {NULL, NULL, NULL}*/
};
static UINT32 preState;

extern UINT8 gpassword[];
extern UINT8 ssidname[];

/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/

static void
parmParseToArgv(
	char *cmd,
	int  *pargc,
	char *argv[]
	)
{
	int argc;
	int stringmark;
	char *c;
	unsigned d;

	HOST_ASSERT(cmd);
	c = cmd;

	/* skipping heading white spaces */
	while (d = *(unsigned char *)c, isspace(d))
		++c;

	argc = 0;
	while (d != '\0') {
		if (++argc >= CMD_PARM_ARGC_MAX) {
			printf("cmd argc too many\n");
			--argc;
			break;
		}
		/* the leading token (cmd name) not supporting " or ' sign */
		if (argc && (d == '"' || d == '\'')) {
			stringmark = d;
			++c;
			*argv++ = c;
			while (d = *(unsigned char *)c, d && d != stringmark)
				++c;
			if (d != '\0')
				*c++ = '\0';
			else {
				printf("tailing >%c< expected\n", stringmark);
				break;
			}
		} else {
			*argv++ = c;
			while (d && !isspace(d) && (d != ','))
				d = *(unsigned char *)++c;
			if (d != '\0')
				*c++ = '\0';
		}
		while (d = *(unsigned char *)c, isspace(d))
			++c;
	}
	*argv = NULL; /* filling a null terminator at the end of argv array */
	*pargc = argc;
}


static btCmd_t*
appBTGetCmdInfo(btCmd_t *cmdList, const char *cmd)
{
	btCmd_t *p;
	for (p = cmdList; p; p++) {
		if (p->func)
			if (!strcmp(cmd, (const char *)p->name))
				return p;
	}
	return NULL;
}

#if 0
UINT32
appBTCmdWiFi(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;
	int i = -1;
	int pargc;
	int parm_msg_len = 0;
	int loop_count = 0 ;
	char parm_msg[CMD_BUFFER_SIZE];
	char *pbuffer = parm_msg;
	char *pargv[CMD_PARM_ARGC_MAX + 1];
	int loop_err = 0 ;
	UINT8 ssidname_temp[MAX_SSID_NAME_LEN]={0};
	UINT8 gpassword_temp[MAX_PASSWORD_LEN]={0};
	char parm_buf[64];
	struct in_addr addr;
	UINT8 ifname[IF_NAMESIZE];
	int timout = 0 ;
	int auth_algs = 2 ;

	SINT32 len = strlen(btCmd);

	memset(parm_msg, 0, CMD_BUFFER_SIZE);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);
	if (!strcmp(argv[0], "enable")) {
		if (!strcmp(argv[1], "ap")) {
			/* enable AP mode connection */
			#if ICAT_WIFI
			if (pViewParam->stillDriverMode != UI_STILL_DRIVERMODE_OFF) {
				pViewParam->stillDriverMode = UI_STILL_DRIVERMODE_OFF;
				appStill_SetDriveMode(pViewParam->stillDriverMode);
			}
			HOST_PROF_LOG_ADD(LEVEL_INFO, "vpv: WiFi connection, entering WiFi state");
			preState = appPreviousStateGet();
			appStateChange(APP_STATE_WiFi_CONNECTION, STATE_PARAM_NORMAL_INIT, 0, 0);
			timout = 20 ;
			while( appWiFiAPReady() && timout ){
			    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 1000);
				timout--;
			}

			if ( appWiFiAPReady() )
				err = 1 ;
			else
				err = 0 ;
			#endif
		}
	}

	if (!strcmp(argv[0], "disable")) {
		/* disable wifi */
		appStateChange(preState, STATE_PARAM_WIFI_OFF, 0, 0);
		err = 0 ;
	}

	if (!strcmp(argv[0], "connect")) {
		/* connect AP wifi */
		/* set wifi */
		if (argc == 2) {
			parmParseToArgv(argv[1], &pargc, pargv);

			if (pargc > 0) {
				i = -1;
				while (i < pargc - 1) {

					i++;
					if (!strncmp(pargv[i], "essid" , 5)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "essid=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)ssidname_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
					if (!strncmp(pargv[i], "pwd",3)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "pwd=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)gpassword_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
				}

				if (( loop_count ) && ( !loop_err ) ){
					if (( ssidname_temp[0] != 0 ) && ( gpassword_temp[0] != 0 )) {

						HOST_PROF_LOG_PRINT(LEVEL_ERROR, "[%s(%d)] connect t0 %s:%s",__FUNCTION__,__LINE__,ssidname_temp,gpassword_temp);

						err = 0 ;
					} else
						err = 1 ;
				}
			}
		}
	}

	if (!strcmp(argv[0], "set")) {
		/* set wifi */
		if (argc == 2) {
			parmParseToArgv(argv[1], &pargc, pargv);

			if (pargc > 0) {
				i = -1;
				while (i < pargc - 1) {

					i++;
					if (!strncmp(pargv[i], "essid" , 5)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "essid=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)ssidname_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
					if (!strncmp(pargv[i], "pwd",3)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "pwd=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)gpassword_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
					/*
					if (!strncmp(pargv[i], "auth_algs",9)) {
						int auth_algs = 4;
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "auth_algs=%s", parm_buf);
						if (strlen(parm_buf)) {
							auth_algs =
							sprintf((char *)buffer, "%d", auth_algs);
							if ( auth_algs > 3 ) {
								auth_algs = 2 ;
							}
							loop_count = loop_count + 1;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
					*/
				}

				if (( loop_count ) && ( !loop_err ) ){
					if ( ssidname_temp[0] != 0 ) {
						memset(ssidname, 0, MAX_SSID_NAME_LEN);
						memcpy(ssidname,ssidname_temp,MAX_SSID_NAME_LEN);
					}
					if ( gpassword_temp[0] != 0 ) {
						memset(gpassword, 0, MAX_PASSWORD_LEN);
						memcpy(gpassword,gpassword_temp,MAX_PASSWORD_LEN);
					}
					appSSIDNameSet(ssidname, gpassword);
					memset(ssidname, 0, MAX_SSID_NAME_LEN);
					memset(gpassword, 0, MAX_PASSWORD_LEN);
					err = 0 ;
				}
			}
		}
	}

	if (!strcmp(argv[0], "info")) {
		if (argc == 2) {

			parmParseToArgv(argv[1], &pargc, pargv);
			if (pargc > 0) {
				i = -1;
				while (i < pargc - 1) {
					i++;
					if (!strcmp(pargv[i], "ipaddr")) {
					    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK dk_netif_ioctl(NDK_IOCG_IF_INDEXTONAME");
						if (ndk_netif_ioctl(NDK_IOCG_IF_INDEXTONAME, 1, (long *)ifname) != 0) {
							printf("ERROR:indextoname\n");
							loop_err = loop_err + 1 ;
							continue;
						} else {
						    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK dk_netif_ioctl(NDK_IOCG_IF_ADDR");
							if (ndk_netif_ioctl(NDK_IOCG_IF_ADDR, (long)ifname, (long *)(void *)&addr) != 0 ) {
								loop_err = loop_err + 1 ;
							}
						}
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "ipaddr=%s", inet_ntoa(addr));
						pbuffer = pbuffer + parm_msg_len;
					} else if (!strcmp(pargv[i], "essid")) {
						if (appSSIDNameGet(ssidname_temp)) {
							printf("essid -> ssid name get failed!\n");
							loop_err = loop_err + 1 ;
							continue;
						}
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "essid=%s", ssidname_temp);
						pbuffer = pbuffer + parm_msg_len;
					} else if (!strcmp(pargv[i], "pwd")) {
						if (appSSIDNameGet(ssidname_temp)) {
							printf("pwd -> ssid name get failed!\n");
							loop_err = loop_err + 1 ;
							continue;
						}
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "pwd=%s", gpassword);
						pbuffer = pbuffer + parm_msg_len;
					} else if (!strcmp(pargv[i], "auth_algs")) {
						if (appWiFiGetAuth_ALGS(&auth_algs)) {
							printf("auth_algs get failed!\n");
							loop_err = loop_err + 1 ;
							auth_algs = 2 ;
							continue;
						}
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "auth_algs=%d", auth_algs);
						pbuffer = pbuffer + parm_msg_len;
					} else
						loop_err = loop_err + 1 ;
				}
				if (( parm_msg_len ) && ( !loop_err) ) {
					err = 0 ;
					*pbuffer = '\0';
					snprintf(resp_msg, strlen(parm_msg) + 20, RSP_STR_WIFI_INFO, "bt wifi info ", parm_msg, " err=", err);
					return appBTSend(resp_msg,strlen(resp_msg));
				}

			}
		}
	}

	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}
#endif

UINT32
appBTCmdSystem(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;
	SINT32 len = strlen(btCmd);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);
	if (!strcmp(argv[0], "power")) {
		if (argc == 2) {
			UINT8 param = APP_BT_DISABLE;
			if (!strcmp(argv[1], "down")) {
				param = APP_BT_POWEROFF;
			} else if (!strcmp(argv[1], "hiber")) {
				param = APP_BT_HIBERATION;
			}

			if (param != APP_BT_DISABLE) {
				err = 0 ;
				snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
				appBTSend(resp_msg,strlen(resp_msg));
				ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 1000);
				appBTPreocessPowerOff(APP_BT_POWEROFF);
				return SUCCESS;
			}
		}
	}
	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}


UINT32
appBTCmdEvent(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;
	SINT32 len = strlen(btCmd);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);

	if (!strcmp(argv[0], "key")) {
		UINT32 KeyEvent = 0xFFFFFFFF;
		if (argc == 2) {
			KeyEvent = appKeyLookup(argv[1]);
		}
		if (KeyEvent != 0xFFFFFFFF) {
			err = 0 ;
			ybfwHostMsgSend(KeyEvent, 0, 0, 0);
		}
	}
	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}


UINT32
appBTCmdPtp(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;
	SINT32 len = strlen(btCmd);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);
	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}


UINT32
appBTCmdBin(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;
	SINT32 len = strlen(btCmd);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);
	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}

UINT32
appBTCmdBT(
	int argc,
	char *argv[],
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd,UINT32 len))
{
	char resp_msg[CMD_BUFFER_SIZE];
	UINT8 err = 1;

	int pargc;
	int i = -1;
	int loop_err = 0 ;
	int loop_count = 0 ;
	char parm_buf[64];
	char *pargv[CMD_PARM_ARGC_MAX + 1];

	int parm_msg_len = 0;
	char parm_msg[CMD_BUFFER_SIZE];
	char *pbuffer = parm_msg;

	memset(parm_msg, 0, CMD_BUFFER_SIZE);

	SINT32 len = strlen(btCmd);
	memset(resp_msg, 0, CMD_BUFFER_SIZE);
	extern UINT8 gBT_NAME[];
	extern UINT8 gBT_PWD[];
	UINT8 gBT_NAME_temp[BT_NAME_SIZE+1] = {0};
	UINT8 gBT_PWD_temp[BT_PWD_SIZE+1] = {0};

	if (!strcmp(argv[0], "set")) {
		if (argc == 2) {

			parmParseToArgv(argv[1], &pargc, pargv);

			if (pargc > 0) {
				i = -1;
				while (i < pargc - 1) {
					i++;
					if (!strncmp(pargv[i], "name" , 4)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "name=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)gBT_NAME_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
					if (!strncmp(pargv[i], "pwd",3)) {
						memset(parm_buf, 0, 64);
						sscanf(pargv[i], "pwd=%s", parm_buf);
						if (strlen(parm_buf)) {
							sprintf((char *)gBT_PWD_temp, parm_buf);
							loop_count = loop_count + 1 ;
						} else {
							loop_err = loop_err + 1 ;
						}
					}
				}
				if (( loop_count ) && ( !loop_err ) ){

					if ( gBT_NAME_temp[0] != 0 ) {
						memcpy(gBT_NAME,gBT_NAME_temp,BT_NAME_SIZE+1);
					}
					if ( gBT_PWD_temp[0] != 0 ) {
						memcpy(gBT_PWD,gBT_PWD_temp,BT_PWD_SIZE+1);
					}
					appBTUpdateSetting( (UINT8 *) gBT_NAME, (UINT8 *) gBT_PWD);
					err = 0 ;
				}
			}
		}
	}

	if (!strcmp(argv[0], "disable")) {
		/* disable bt */
	}

	if (!strcmp(argv[0], "restart")) {
		/* disable bt */
	}

	if (!strcmp(argv[0], "info")) {
		if (argc == 2) {

			parmParseToArgv(argv[1], &pargc, pargv);
			if (pargc > 0) {
				i = -1;
				while (i < pargc - 1) {
					i++;
					if (!strcmp(pargv[i], "name")) {
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "name=%s", gBT_NAME);
						pbuffer = pbuffer + parm_msg_len;
					} else if (!strcmp(pargv[i], "pwd")) {
						if (parm_msg_len) {
							*pbuffer = ',';
							pbuffer++;
						}
						parm_msg_len = sprintf(pbuffer, "pwd=%s", gBT_PWD);
						pbuffer = pbuffer + parm_msg_len;
					} else {
						loop_err = loop_err + 1 ;
					}
				}
				if (( !loop_err ) && ( parm_msg_len ) ){
					err = 0 ;
					*pbuffer = '\0';
					snprintf(resp_msg, strlen(parm_msg) + 20, RSP_STR_BT_INFO, "bt bt info ", parm_msg, " err=", err);
					return appBTSend(resp_msg,strlen(resp_msg));
				}
			}
		}
	}

	snprintf(resp_msg, len + 7, "%s%s%d", btCmd, " err=", err);
	return appBTSend(resp_msg,strlen(resp_msg));
}

UINT32
appBTCmdParser(
	char *btCmd,
	UINT32(*appBTSend)(char *btCmd , UINT32 len))
{
	UINT32 err = FAIL;

	int   argc;
	char *argv[CMD_ARGC_MAX + 1];
	char  buffer[CMD_BUFFER_SIZE];

	if (btCmd == NULL) {
		printf("btCmd is NULL\n");
		return FAIL;
	}

	if ((strlen(btCmd) - 6) >= sizeof(buffer)) {
		printf("btCmd too long\n");
		return FAIL;
	}

	strcpy(buffer, (const char *)btCmd);

	cmdParseToArgv(buffer, &argc, argv);

	if (argc > 0) {

		if (strcmp(argv[0], "bt") == 0) {
			btCmd_t *pcmd = appBTGetCmdInfo(btCmdsList, argv[1]);
			if ((argc < 2) || (!pcmd)) {
				printf("bad cmd: %s\n", argv[1]);
				return FAIL;
			}

			HOST_ASSERT(pcmd->func);
			err = pcmd->func(argc - 2, argv + 2, btCmd, appBTSend);
		}


	}

	return err;
}

#endif /* BTON=YES in project defination */
