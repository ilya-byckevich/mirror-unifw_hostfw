/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#if SP5K_BTON

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "app_bt_device.h"
#include "app_bt_utility.h"
#include "gpio_custom.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"
#include "ykn_bfw_api/ybfw_global_api.h"

#include "ndk_types.h"
/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
/* MW */
#define ybfwGpioPullSet(a,b,c) ybfwGpioPullDirSet(a,b,c,0)

static int bt_glob_inited = FALSE;
static int bt_device_inited = FALSE;

static struct TTYDevInf ttyDevInf;

extern UINT8 gBT_NAME[];
extern UINT8 gBT_PWD[];

/**************************************************************************
 *        F U N C T I O N     D E C L A R A T I O N S                     *
 **************************************************************************/
static UINT32 appUartInit(void);
static UINT32 appUartRead(void *buf,UINT32 len,UINT32 timeout);
static UINT32 appUartWrite(void *buf,UINT32 len);
static UINT32 appUartSetBaudRate(UINT32 baudrate);
static UINT32 appUartSetFlowCtrl(BOOL bHwFlowCtrl);

extern int net_device_status(void);

/**************************************************************************
 *                         F U N C T I O N     D E F I N I T I O N         *
 **************************************************************************/
static NDKBtUartOps appBTUartOps = {
	.pf_init = appUartInit,
	.pf_set_baudrate = appUartSetBaudRate ,
	.pf_set_flowctrl = appUartSetFlowCtrl ,
	.pf_read = appUartRead ,
	.pf_write = appUartWrite ,
};


/*
	v50  UART: 0 1 2 3 4
	cpu  UART: 0 1
	peri UART:     2 3 4
*/
static UINT32
appUartDevConfig(
    void
)
{
	UINT32 pinmux;
	UINT32 uartport;
    UINT32 ret;
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if ( ttyDevInf.uart_channel ) {
        return SUCCESS ;
    }

    printf("BT_UART_PORT = %d\n", BT_UART_PORT);

    if (BT_UART_PORT == 2) {
        pinmux = BT_UART_PIN; /* pin select */
		uartport = YBFW_UART_PORT_PERI_0;
        switch (BT_UART_PIN)
        {
        case 0:
            printf("UART2 pinmux=0:FMIF2,3,21,22\n");
            break;
		case 1:
            printf("UART2 pinmux=1:FMIF28,29,30,31\n");
            ybfwGpioPullSet(SP5K_GPIO_GRP_FML, 0xF0000000, 0xF0000000); /* FMIO pull high */
            break;
		case 2:
			printf("UART2 pinmux=2:FMIF41,42,43,44\n");
            ybfwGpioPullSet(SP5K_GPIO_GRP_FMH, 0x00001E00, 0x00001E00);
			break;
        default:
            HOST_ASSERT(0);
			break;
        }

	} else if (BT_UART_PORT == 3 ) {
		ybfwGpioPullSet(SP5K_GPIO_GRP_FML, 0x0F000000, 0x0F000000 );
        printf("UART3 pinmux=0:FMIF24,25,26,27\n");
		uartport = YBFW_UART_PORT_PERI_1;
		pinmux = 0;
	} else if (BT_UART_PORT == 4 ) {
		/* FMIF: 52,53,54,55 redirect to GPIO: 0,1,2,3 */
        printf("UART4 pinmux=1:GPIO0,GPIO1,GPIO2,GPIO3\n");
		uartport = YBFW_UART_PORT_PERI_2;
		pinmux = 1;
	}else {
        HOST_ASSERT(0);
    }

	ret = ybfwOsMutexCreate(&ttyDevInf.uartlock, (char *)"UartDevLock", YBFW_TX_INHERIT);
	if ( ret != FAIL ) {
		printf("ttyDevInf configuration!\n");

	#if defined(SP5K_BT_VENDOR_BRCM)
		ttyDevInf.bFlowCtrl = 1;
		ttyDevInf.parity = YBFW_UART_PARITY_DISABLE;
	#elif defined(SP5K_BT_VENDOR_REALTEK)
		ttyDevInf.bFlowCtrl = 1; /* 0:SW flow ctrl, 1:HW flow ctrl */
		ttyDevInf.parity = YBFW_UART_PARITY_EVEN;
	#else
		#error "Unknow BT Vendor"
	#endif
		ttyDevInf.iomode = YBFW_UART_MODE_PIO; /* YBFW_UART_MODE_PIO , YBFW_UART_MODE_DMA*/
		ttyDevInf.baudrate = 2000000 ;
		ttyDevInf.init_baudrate = 115200 ;
		ttyDevInf.uart_channel = uartport;
		ttyDevInf.dev_id = 0 ;
		ttyDevInf.pinmux = pinmux ;
		ttyDevInf.timeout = 1000 ; /* 1000 ms */
		ttyDevInf.connected = FALSE ;
		ttyDevInf.pbuf = ybfwMallocCache(512);

		if( NULL == ttyDevInf.pbuf ) {
			printf("ttyDevInf mem alloc fail!\n");
			ybfwOsMutexDelete(&ttyDevInf.uartlock);
			return FAIL;
		}
	}

	return SUCCESS;
}


static void
appUartDevFree(
    void
)
{

    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    if ( ttyDevInf.uart_channel == 0 ) {
        return ;
    }

    ttyDevInf.uart_channel  = 0 ;

    if( ttyDevInf.uartlock ) {
        ybfwOsMutexDelete(&ttyDevInf.uartlock);
        ttyDevInf.uartlock = 0 ;
    }

    if( ttyDevInf.pbuf ) {
        ybfwFree(ttyDevInf.pbuf);
        ttyDevInf.pbuf = NULL ;
    }

}


static UINT32
appUartInit(
    void
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    UINT32 ret = SUCCESS;

    ret = ybfwUartModeSet(ttyDevInf.uart_channel, (UINT32)ttyDevInf.iomode );
    if ( ret != SUCCESS ) {
        printf("ybfwUartModeSet FAIL\n");
    }
	ret = ybfwUartCfgSet(ttyDevInf.uart_channel, YBFW_UART_FLOWCTRL, (UINT32)ttyDevInf.bFlowCtrl );
    if ( ret != SUCCESS ) {
        printf("ybfwUartCfgSet YBFW_UART_FLOWCTRL FAIL\n");
    }
	ret = ybfwUartCfgSet(ttyDevInf.uart_channel, YBFW_UART_PIN_SEL, (UINT32)ttyDevInf.pinmux );
    if ( ret != SUCCESS ) {
        printf("ybfwUartCfgSet YBFW_UART_PIN_SEL FAIL\n");
    }
	ret = ybfwUartCfgSet(ttyDevInf.uart_channel, YBFW_UART_PARITY, (UINT32)ttyDevInf.parity );
    if ( ret != SUCCESS ) {
        printf("ybfwUartCfgSet YBFW_UART_PARITY FAIL\n");
    }
	ret = ybfwUartInit(ttyDevInf.uart_channel, ttyDevInf.baudrate );
    if ( ret != SUCCESS ) {
        printf("ybfwUartInit FAIL\n");
    }
    return ret;
}


static UINT32
appUartRead(
	void *buf,
    UINT32 len,
    UINT32 timeout
)
{
//    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	UINT32 ret = SUCCESS ;
    if ( ttyDevInf.timeout != timeout) {
		ret = ybfwUartCfgSet(ttyDevInf.uart_channel, YBFW_UART_RX_TIMEOUT, timeout);
        if ( ret != SUCCESS ) {
            printf("ybfwUartCfgSet YBFW_UART_RX_TIMEOUT FAIL\n");
        }
		ttyDevInf.timeout = timeout;
	}
	ret = ybfwUartBufRead( ttyDevInf.uart_channel, (UINT8 *)buf, (UINT32)len);
    if ( ret < 0 ) {
        printf("ybfwUartBufRead FAIL\n");
    }
    return ret;
}

static UINT32
appUartWrite(
	void *buf,
	UINT32 len
)
{
//    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	UINT32 ret = 0;
    ybfwOsMutexGet(&ttyDevInf.uartlock , YBFW_TX_WAIT_FOREVER);
	ret = ybfwUartBufWrite(ttyDevInf.uart_channel, (UINT8 *)buf, (UINT32)len);
	ybfwOsMutexPut(&ttyDevInf.uartlock);
	return ret;
}


static UINT32
appUartSetBaudRate(
	UINT32 baudrate
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    printf("appUartSetBaudRate: baudrate => %u\n", baudrate);

    if ( ttyDevInf.baudrate == baudrate ) {
        return SUCCESS ;
    }

    if ( ybfwUartInit(ttyDevInf.uart_channel, baudrate ) != SUCCESS ) {
        printf("ybfwUartInit FAIL\n");
        return FAIL ;
    }
    ttyDevInf.baudrate = baudrate ;
    return SUCCESS ;
}


static UINT32
appUartSetFlowCtrl(
	BOOL bHwFlowCtrl
)
{
	if ( ttyDevInf.bFlowCtrl == bHwFlowCtrl ) {
		return SUCCESS ;
	}

	ybfwUartCfgSet( ttyDevInf.uart_channel , YBFW_UART_FLOWCTRL, bHwFlowCtrl ? 1 : 0);
	if ( ybfwUartInit(ttyDevInf.uart_channel, ttyDevInf.baudrate ) != SUCCESS ) {
		printf("ybfwUartInit FAIL\n");
		return FAIL ;
	}
    ttyDevInf.bFlowCtrl = bHwFlowCtrl ;
    return 0;
}


static UINT32
appBTUartHciInit(
	const char * apt_name
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	int err = 0 ;
	AppBtGlobConf *conf = appBTGetGlobConf();
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_adapter_enable");
	err = ndk_bt_adapter_enable(apt_name, NDK_BT_HWIF_UART, &appBTUartOps, conf->conf_dir, NULL );
	if (err != 0){
		printf("ndk_bt_start_service fail\n");
		return FAIL;
	}
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_hciattach");
	err = ndk_bt_hciattach(conf->hcia_type,
		ttyDevInf.init_baudrate ,
		ttyDevInf.baudrate ,
		ttyDevInf.bFlowCtrl,
		conf->hcia_extra_opts);
	if (0 != err){
		printf("ndk_bt_hciattach fail %d\n", err);
		return FAIL;
	}

	return SUCCESS ;
}


TTYDevInf_t*
appUartDevGetInf(
    void
)
{
    return &ttyDevInf ;
}


int appBTHCIEnable(
	UINT32 btParm
)
{
	UINT32 ret = 0 ;
	UINT32 state;
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	state = appBTInitStateGet();
	if (state & BT_HCI_LOAD) {
		return 0;
	}

	bt_system_init();
	bt_device_init();

	ret = appUartDevConfig();
	if ( ret == FAIL ) {
		printf("appUartDevConfig FAIL\n");
		return -1;
	}

	ret = appBTUartHciInit((const char * )gBT_NAME);

	if ( ret == FAIL ) {
		printf("appBTUartHciInit FAIL\n");
		/* Need free all resource */
		appUartDevFree();
		return -1;
	}

	appBTInitStateSet(BT_HCI_LOAD);
	return 0;
}

int appBTHCIDisable(
	UINT32 btParm
)
{
	appUartDevFree();
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_hcidetach");
	ndk_bt_hcidetach();
	HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_adapter_disable");
	ndk_bt_adapter_disable();
	bt_device_deinit();

	appBTInitStateUnset(BT_HCI_LOAD);

    return 0;
}



int bt_device_status(void)
{
	return bt_device_inited ;
}

void bt_device_poweroff(void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	if (GRP_BT_PWR_EN != SP5K_GPIO_GRP_NO_USE) {
		ybfwGpioCfgSet(GRP_BT_PWR_EN, 1 << CFG_BT_PWR_EN, 1 << CFG_BT_PWR_EN);
		SET_BT_PWR_EN(0);
		printf("power off %d for BT\n", CFG_BT_PWR_EN);
	}
	if (GRP_BT_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
		ybfwGpioCfgSet(GRP_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN);
		SET_BT_SHUTDOWN(1);
		printf("WFBT disable %d for BT\n", CFG_BT_SHUTDOWN);
	}
}


void bt_device_poweron(void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
    printf("WFBT bt_device_poweron start\n");
	if (GRP_BT_SHUTDOWN != SP5K_GPIO_GRP_NO_USE) {
		ybfwGpioCfgSet(GRP_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN, 1 << CFG_BT_SHUTDOWN);
		SET_BT_SHUTDOWN(1);
		ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 150); /* 150 ms */
		SET_BT_SHUTDOWN(0);
		printf("WFBT enable %d for BT\n", CFG_BT_SHUTDOWN);
	}
	//GRP_BT_PWR_EN is configured as BT_DEV_WAKE for brcm
	if (GRP_BT_PWR_EN != SP5K_GPIO_GRP_NO_USE) {
	        ybfwGpioCfgSet(GRP_BT_PWR_EN, 1 << CFG_BT_PWR_EN, 1 << CFG_BT_PWR_EN);
	        SET_BT_PWR_EN(0);
	        ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 50); /* 50 ms */
	        SET_BT_PWR_EN(1);
	        printf("power enable %d for BT\n", CFG_BT_PWR_EN);
	    }
	printf("WFBT bt_device_poweron end\n");
}


void bt_device_deinit(
    void
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s]",__FUNCTION__);
	if ( bt_device_inited == TRUE) {
		bt_device_inited = FALSE ;
		#if SP5K_BTON
			/* check wifi on ? , because bt/wifi cs is the same */
			if ( net_device_status() == FALSE )
		#endif
		bt_device_poweroff();
		printf("BT device deinit done.\n");
	}
}

void bt_device_init(
    void)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");
	HOST_PROF_LOG_PRINT(LEVEL_WARN, "[%s]",__FUNCTION__);
	if ( bt_device_inited == FALSE) {
//#if SP5K_BTON
//			/* check wifi on ? , because bt/wifi cs is the same */
//		if ( net_device_status() == FALSE ){
//#endif

		bt_device_poweron();
//#if SP5K_BTON
//	    }
//#endif
		bt_device_inited = TRUE ;
		printf("BT device init done.\n");
	}
}


void bt_system_init(
    void
)
{
    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT trace");

	if ( bt_glob_inited == FALSE ) {
	    HOST_PROF_LOG_PRINT(LEVEL_INFO,"WFBT NDK ndk_bt_global_init");
		ndk_bt_global_init(NULL);
		bt_glob_inited = TRUE;
	}

	printf("BT system init done.\n");
}



#endif /* BTON=YES in project defination */
