/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/
#ifndef _APP_BT_DEVICE_H_
#define _APP_BT_DEVICE_H_

/**************************************************************************
 *                         H E A D E R   F I L E S                        *
 **************************************************************************/
#include "common.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include <ndk_bluetooth.h>
#include <ndk_bluetooth_hidp.h>

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/
typedef struct TTYDevInf
{
	NDKBtTtyDev *dev;
	int         wpos;
    YBFW_MUTEX	uartlock;
    UINT32      timeout;
	UINT32      init_baudrate;
    UINT32      baudrate;

    UINT8       iomode;
    UINT8       bFlowCtrl;
    UINT8       parity;
    UINT8       pinmux;

    UINT8       uart_channel;
    UINT8       dev_id;
    UINT8       connected;
	UINT8       *pbuf;
} TTYDevInf_t ;

/**************************************************************************
 *                               M A C R O S                              *
 **************************************************************************/

/**************************************************************************
 *          M O D U L E   V A R I A B L E S   R E F E R E N C E S         *
 **************************************************************************/

/**************************************************************************
 *                F U N C T I O N   D E C L A R A T I O N S               *
 **************************************************************************/
int appBTHCIEnable(UINT32 btParm);
int appBTHCIDisable(UINT32 btParm);
TTYDevInf_t* appUartDevGetInf(void);

void bt_system_init(void);
int bt_device_status(void);
void bt_device_poweroff(void);
void bt_device_poweron(void);
void bt_device_deinit(void);
void bt_device_init(void);
#endif  /* _APP_BT_DEVICE_H_ */


