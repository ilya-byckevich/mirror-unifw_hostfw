/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_dq_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "app_common.h"

static void cmdDIQInit(){
    UINT32 ret = FAIL;
    INFO("diqinit\n");
    #if SP5K_DIQ_FRMWRK
    ret = ybfwDynIqCfgSet(YBFW_DYN_IQ_CFG_SYS_INIT);
    #endif
    HOST_ASSERT_MSG(ret == SUCCESS,"diq ini sts=%x",ret);
}


/*-------------------------------------------------------------------------
 *  Function Name : cmdOMF
 *  Description : open media framework cmd entrty
 *------------------------------------------------------------------------*/
int cmdDIQ(int argc,char **argv,UINT32* v){  
    char* cmd = *argv++;v++;argc--;

    /**/
    if(argc<0) {    return 0;}
    else if (isEqualStrings(cmd, "init", CCS_NO ) ) {    cmdDIQInit();}
    else{return 0;}
    return 1;
}
int cmdDIQ_(int argc,char **argv,UINT32* v){  
    char* cmd = *argv++;v++;argc--;

    /**/
    if(argc<0) {    return 0;}
    else if (isEqualStrings(cmd, "diqinit", CCS_NO ) ) {    cmdDIQInit();}
    else{return 0;}
    return 1;
}
