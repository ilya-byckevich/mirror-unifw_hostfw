/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_sensor_api.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"


#include "app_common.h"



enum UartPort_e {
    UART_PORT_0,
    UART_PORT_1
};

#define EXIT_KEY 'q'
#define EXIT_DEBUG_TIMEOUT  5


int callbackSample(int ch)
{
    if(ch == EXIT_KEY){
        ybfwUartRxCallbackSet(UART_PORT_0,NULL);
        INFO("exit from rx callback mode\n");
        return 0;
    }else
    {
        printf("[%c]\n", ch);
        return 1;
    }
}

void uart0Disable(){
    UINT32 c;
    INFO("Disable debug mode. And enable after %d seconds \n",EXIT_DEBUG_TIMEOUT);
    ybfwLogControl(YBFW_LOG_DISABLE);
    printf("test message after disable\m");
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, EXIT_DEBUG_TIMEOUT * 1000);
    ybfwLogControl(YBFW_LOG_ENABLE);
    INFO("Enable debug mode\n");
}


int cmdUart(int argc,char **argv,UINT32* v){
    char* cmd = *argv++;v++;argc--;
   /**/
   if(argc<0) {
       return 0;
   }
   else if (isEqualStrings(cmd, "disable", CCS_NO ) ) {
       uart0Disable();
   }
   else if (isEqualStrings(cmd, "rxcallback", CCS_NO ) ) {
       INFO("Enable rxcallback mode. To exit press '%c' key'\n",EXIT_KEY);
       ybfwUartRxCallbackSet(UART_PORT_0,callbackSample);
   }
   else{
       return 0;
   }
   return 1;
}

void cmdUartHelp(){
    printf("uart - test uart0 (debug port)\n");
    printf("\tuart disable  - temporary disable debug messages during %d seconds\n",EXIT_DEBUG_TIMEOUT);
    printf("\tuart rxcallback - test rx callback. Key '%c' to exit..\n",EXIT_KEY);

}



