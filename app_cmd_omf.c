/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "middleware/common_types.h"
#include "omf_api.h"


#include "app_common.h"


/*extern int FwInit(UINT32 size);
extern int FwUninit();*/
extern int FwCmd(const char *selecter,const char *p0,const char *p1,const char *p2);
static int cmdOmfShm();
static UINT32 appPipDirectModeDrawCb(frameBufInfo_t *pinfo, void *rsvd) {
    ybfwPipAttrSet(YBFW_PIP_CH_0, YBFW_PIP_DIRECT_MODE, 1, 1, 0, (UINT32)pinfo);
    return 0;
}

/*-------------------------------------------------------------------------
 *  Function Name : cmdOMF
 *  Description : open media framework cmd entrty
 *------------------------------------------------------------------------*/
int cmdOMF(int argc,char **argv,UINT32* v){  
    char* cmd = *argv++;v++;argc--;

    /**/
    if(argc<0) {
        return 0;
    }
    else if (isEqualStrings(cmd, "init", CCS_NO ) ) {
        /*FwInit(*v++);*/
        omfInit(0);
    }
    else if (isEqualStrings(cmd, "uninit", CCS_NO ) ) {
        /*FwUninit();*/
        omfUninit(0);
    }
    else if (isEqualStrings(cmd, "disp-hdmi", CCS_NO ) ) {
        FwCmd("disp",0,0,(void*)appPipDirectModeDrawCb);
    }
    else if (isEqualStrings(cmd, "shm", CCS_NO ) ) {
        cmdOmfShm();
    }
    else{
        FwCmd(cmd,*argv++,*argv++,*argv++);
    }
    return 1;
}

static int cmdOmfShm(){printf("%s\n",__FUNCTION__);
//    void *handle = omfCreate("V50ShmService", "ShmService",0);
//    returnIfErrC(0, !handle);
//    returnIfErrC(0, !omfStatusUp(handle,"play"));
    return 1;
}

