/**
 * @file common.h
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/


#ifndef SPHOST_CUSTOMIZATION_HOSTFW_AMP_COMMON_H_
#define SPHOST_CUSTOMIZATION_HOSTFW_AMP_COMMON_H_

#include "common.h"
/* #include "ykn_bfw_api/ybfw_cdsp_api.h" */
#define COUNT_OF(array) (sizeof(array)/sizeof(array[0]))
/* #define YUV_BUF_W_ALIGN(x)          (((x)+31) & (~31)) */
#define YUV_BUF_W_ALIGN(x) (x)
/* #define YUV_BUF_H_ALIGN(x)          (((x)+15) & (~15)) */
#define YUV_BUF_H_ALIGN(x)          (x)


#define YUV_ROI_ALIGN_XY(x)         ((x) & (~0x3))
#define YUV_ROI_ALIGN_CDSP(x)       ((x) & (~0x7))



#if 0
/** current sensor count */
#define SENSOR_COUNT 1

#define SENSOR_ACTIVE_ID  YBFW_SEN_ID_0
/** max of supported sensors */
#define SENSOR_COUNT_MAX 4



/* PiP and display related defines*/
#define PIP_CHANNEL YBFW_PIP_CH_0
#define DISPLAY_YUV_ID 3
#define DISPLAY_YUV YBFW_MODE_PREV_SENSOR_0_YUV_3
#define PIP_LAYER (YBFW_YUV_PIP_BASE + YBFW_PAGE_PIP_0_MAIN)
#define DISPLAY_PV_WIDTH  1920
#define DISPLAY_PV_HEIGHT 1080

#define DISPLAY_RATIO_W 16
#define DISPLAY_RATION_H 9

#define DISPLAY_PV_PIP_OVERLAY_WIDTH DISPLAY_PV_WIDTH
#define DISPLAY_PV_PIP_OVERLAY_HEIGHT DISPLAY_PV_HEIGHT

#define DISPLAY_CHANNEL YBFW_DISP_CHNL_1
#define DISPLAY_GFX_CHANNEL YBFW_GFX_CH_0_FLAG
#endif

#define LOW_LATENCY

#define IS_BIT_SET(mask,bit) ((mask & (1 << bit)))


typedef enum{
  CCS_NO = 0,
  CCS_YES = 1,
}CmpCaseSensitive;

UINT8 isEqualStrings(const UINT8 * str1, const UINT8 *str2, CmpCaseSensitive case_sens_state);

#endif /* SPHOST_CUSTOMIZATION_HOSTFW_AMP_COMMON_H_ */
