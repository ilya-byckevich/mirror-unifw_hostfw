/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"


#include "app_common.h"

#include "app_cmd_disk.h"
#include "app_cmd_rec/app_cmd_rec.h"
#include "app_cmd_wifi_bt.h"
#include "app_cmd_fw.h"
#include "app_cmd_uart.h"
#include "media_core/api/media_core_api.h"
/*************************************************************************/
typedef int(*funcCmd)(int argc, char **argv, UINT32* v);
typedef void (*funcHelp)();
typedef struct ItemCmd{
    char* key;
    funcCmd func;
    funcHelp help;
}ItemCmd;

/*************************************************************************/

int cmdOMF(int argc,char **argv,UINT32* v);
int cmdAE(int argc,char **argv,UINT32*v);
int cmdAF(int argc,char **argv,UINT32*v);
int cmdAWB(int argc,char **argv,UINT32*v);
int cmdCmd(int argc,char **argv,UINT32*v);
int cmdAMP(int argc,char** argv,UINT32*v);
int cmdAMP_(int argc,char** argv,UINT32*v);
int cmdDIQ(int argc,char **argv,UINT32* v);
int cmdDIQ_(int argc,char **argv,UINT32* v);
int cmd360(int argc,char **argv,UINT32* v);
int cmdEtherCam(int argc,char **argv,UINT32* v);

int cmdNaidz(int argc,char **argv,UINT32* v){
     printf("[Error] Hello %s\n", __FUNCTION__);
     return 1;
}


static ItemCmd _items[]={
/*        {"omf"        ,cmdOMF,NULL}, */
/*        {"hdmi"        ,cmdHdmi,cmdHdmiHelp}, */
        {"ae"        ,cmdAE,NULL},
        {"awb"        ,cmdAWB,NULL},
        {"af"        ,cmdAF,NULL},
        {"cmd"        ,cmdCmd,NULL},
/*        {"amp"        ,cmdAMP,NULL}, */
        {"diqs"        ,cmdDIQ,NULL},/* rename to avoid conflict with cmd_dq.c */
/*        {"ethercam"    ,cmdEtherCam,NULL}, */
#if SP5K_WIFION
        {"wifi"    ,  cmdWiFi,cmdWiFiHelp},
#endif
#if SP5K_BTON
        {"bt"    ,    cmdBT, cmdBTHelp},
#endif
        {"msdc",       cmdMSDC,cmdMSDCHelp},
/*        {"pip",        cmdPiP,cmdPiPHelp}, */
/*        {"zoom",      cmdZoom,cmdZoomHelp},*/
/*        {"drawosd",    cmdOSDDraw,cmdOSDDrawHelp}, */
/*        {"rec",        cmdRec,cmdRecHelp}, */
        {"ramdisk",    cmdRamDisk,cmdRamDiskHelp},
        {"fw",          cmdFw,cmdFwHelp},
        {"uart",        cmdUart,cmdUartHelp},
/*        {"mc",        cmdMc,cmdMcHelp}, */
/*        {0            ,cmdPv,cmdPvHelp}, //TODO make commands by ke */
/*        {0            ,cmdAMP_,NULL}, */
        {0            ,cmdDIQ_,NULL},
        {0,0},
};

#define CMD_ARGC_MAX 10
void
appMonitorCmd(
    int argc,
    char *argv0[]
)
{
    UINT32 i = 0;
    UINT8 findHelp = 0;
    char* argv[CMD_ARGC_MAX];
    UINT32 v[CMD_ARGC_MAX];
    memset(v, 0, sizeof(v));

    /*parse*/
    for (i = 0; i < argc && i < CMD_ARGC_MAX; i++) {
        argv[i]=argv0[i];
        v[i] = strtoul(argv[i], NULL, 0);
    }
    for(;i<CMD_ARGC_MAX;i++){
        argv[i]=0;
        v[i]=0;
    }
    if (isEqualStrings(argv[0], "hhelp", CCS_NO ) ){
        UINT8 arg2len =  (argc > 1) ? strlen(argv[1]): 0;

        for(i=0;i<sizeof(_items)/sizeof(*_items);i++){
            ItemCmd* item= _items+i;

            if(!item->func)break;
            if (arg2len){
                if (item->key && strstr(item->key,argv[1])){ //print help by sting template
                    item->help();
                    findHelp = 1;
                }
            }else if(item->help){ //print help of all commands
               item->help();
               findHelp = 1;
            }
        }
        if (findHelp){
            return;
        }
    }else {
        for(i=0;i<sizeof(_items)/sizeof(*_items);i++){ //execute command
            ItemCmd* item= _items+i;
            if(!item->func)break;
            if(item->key){
                if (isEqualStrings(argv[0], item->key, CCS_NO ) ){
                    item->func(argc-1,argv+1,v+1);
                    return;
                }
            }else{
                if(item->func(argc,argv,v))
                    return;
            }
        }
    }
    if (isEqualStrings(argv[0], "hhelp", CCS_NO ) && (!findHelp)){
        ERR("Can't find help for command '%s'\n",argv[1]);
    }else{
        ERR("unknown cmd:%s\n",argv[0]);
        INFO("Please input commands:\n");
        INFO("\t'help' - to show all system commands\n");
        INFO("\t'hhelp' - to show all hostfw size commands\n");
        INFO("\t\t'hhelp [str]' - show help of [str] command\n");
    }
}


