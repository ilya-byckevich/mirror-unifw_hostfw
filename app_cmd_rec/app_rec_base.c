/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ykn_bfw_api/ybfw_dzoom_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"


#include "app_common.h"
#include "app_cmd_rec/audio.h"
#include "app_cmd_rec/app_rec_common.h"
#include "app_cmd_pv.h"

static SINT32 gwRecHandle;
static UINT32 mediaFwInited = 0;
static UINT32 mediaEnter = 0;

void baseMediaInit(void)
{
    if (!mediaFwInited) {
        ybfwMediaCtrl(YBFW_MEDIA_CTRL_INIT); /* this API should be paired with YBFW_MEDIA_CTRL_UNINIT */
        ybfwProfLogPrintf(0, "YBFW_MEDIA_CTRL_INIT");
    }
    mediaFwInited++;
}

void baseMediaRelease(void)
{
    if (mediaFwInited) {
        if (--mediaFwInited == 0) {
            ybfwProfLogPrintf(0, "YBFW_MEDIA_CTRL_UNINIT");
            ybfwMediaCtrl(YBFW_MEDIA_CTRL_UNINIT); /* this API should be paired with YBFW_MEDIA_CTRL_INIT */
        }
    }
}

UINT32 appMediaPreviewEnter(void)
{
    UINT32 err = SUCCESS;

    if (mediaEnter == 0) {
        if ((err = ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER)) != SUCCESS) {
            printf("YBFW_MEDIA_CTRL_PREVIEW_ENTER fail(%d)\n", err);
        }
        ybfwProfLogPrintf(0, "#P#STREAMCFG#N#PvEnter");
    }
    mediaEnter++;

    return err;
}

UINT32 appMediaPreviewExit(void)
{
    UINT32 err = SUCCESS;

    if (mediaEnter) {
        if (--mediaEnter == 0) {
            if ((err = ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_EXIT)) != SUCCESS) {
                printf("YBFW_MEDIA_CTRL_PREVIEW_EXIT fail(%d)\n", err);
            }
            ybfwProfLogPrintf(0, "#P#STREAMCFG#N#PvExit");
        }
    }

    return err;
}



ybfwMediaRecCfg_t * baseRecCfgAlloc(UINT32 curRecmask)
{
    UINT32 i;
    ybfwMediaRecCfg_t *pRecCfg = (ybfwMediaRecCfg_t *)ybfwMallocCache(sizeof(ybfwMediaRecCfg_t));

    HOST_ASSERT(pRecCfg);

    memset(pRecCfg, 0x0, sizeof(ybfwMediaRecCfg_t));

    if (IS_BIT_SET(curRecmask,RECM_VIDEO)){  //enable video
        pRecCfg->vinNo = 1;
        DBG("Video RECORD is Enabled");
    }else{
        pRecCfg->vinNo = 0;
    }
    pRecCfg->vin[0].senId = 0; //select sensor 1 to record
    pRecCfg->vin[0].yuvId = 0; //select yuv0

    pRecCfg->vin[0].frameRateSensor = 60;
    pRecCfg->vin[0].frameRateInput  = 60;
    pRecCfg->vin[0].frameRateSave   = 60;

    pRecCfg->vin[0].fifoDepth       = 6 * 60;  /* 6s */
    pRecCfg->vin[0].brcType         = 0;/**< 0: CBR, 1: VBR */
    pRecCfg->vin[0].rotate          = 0;
    pRecCfg->vin[0].stopMode        = 0;

    /* YUV input ring buffer configuration */
    pRecCfg->vin[0].bufCnt          = 4; /* should be less than ring buffer count */

    pRecCfg->vin[0].bufSize         = 10 * 1024 * 1024;  /* 10 Mbyte */
    pRecCfg->vin[0].firstFrame      = 0;


    //TODO add support MJPEG HEVC configurations
    pRecCfg->vin[0].codecSel        = YBFW_MEDIA_VIDEO_H264;
    /* general config */
    pRecCfg->vin[0].codec.h264.qpFixed           = 0;
    pRecCfg->vin[0].codec.h264.profile           = YBFW_MEDIA_H264_PROFILE_HIGH;
    pRecCfg->vin[0].codec.h264.intraMode         = 1;/**< 0: I4+I16, 1: I8+I16, 2: I4+I8 */
    pRecCfg->vin[0].codec.h264.interlace         = 0;/**< 0: progressive, 1: interlace */
    pRecCfg->vin[0].codec.h264.sliceNo           = 1;
    pRecCfg->vin[0].codec.h264.vuiTag            = 1;
    pRecCfg->vin[0].codec.h264.idrInterval       = 1;
    pRecCfg->vin[0].codec.h264.audSeiInfo        = 0;
    pRecCfg->vin[0].codec.h264.sliceTypeOfs      = 0; /**< must 0 or 5 */
    pRecCfg->vin[0].codec.h264.colorParam        = 0;/**< 1: BT709, 0: full range */
    pRecCfg->vin[0].codec.h264.seiCbInitCallback = 0;
    /* H264 IPPP */
    pRecCfg->vin[0].bitRate
                        = pRecCfg->vin[0].bitRateMax
                        = pRecCfg->vin[0].bitRateMin
                        = 30000000; /* 30Mbit */
    pRecCfg->vin[0].codec.h264.entropy = 0;/**< 0 for CABAC, 1 for CAVLC */
    pRecCfg->vin[0].codec.h264.gopType = YBFW_MEDIA_GOP_TYPE_IPPP;/**< 0: IBBP, 1: IPPP, 2: IIII */
    pRecCfg->vin[0].codec.h264.gopNo    = 15;/**< 1 or 15 or 30 typically */
    /**< b[7:0] init, b[15:8] max, b[23:16] min, IP:0x0a2d10, B:0x0a2d25 */
    pRecCfg->vin[0].codec.h264.qpCfgI   = (30<<0) | (38<<8) | (20<<16); /* equal to sample code 0x14261e*/
    pRecCfg->vin[0].codec.h264.qpCfgP   = (30<<0) | (38<<8) | (20<<16);
    pRecCfg->vin[0].codec.h264.qpCfgB   = (30<<0) | (38<<8) | (20<<16);

    //De-Flicker is similar to 3D de-noise technoloty. which can effectively reduse flicker noise
    //pRecCfg->vin[0].codec.h264.deFlickerEn = 1;

    //First frame for thumbnail
    //pRecCfg->vin[0].furstFrame=1;


    if (! IS_BIT_SET(curRecmask,RECM_AUDIO)){
        pRecCfg->ainNo = 0;
        pRecCfg->ain[0].codecSel    = YBFW_MEDIA_AUDIO_UNKNOWN;
    }
    else {
        DBG("AUDIO RECORD is Enabled");
        pRecCfg->ainNo = 1;
//        pRecCfg->ain[0].devId       = YBFW_AUD_DEV_0_INT;
//        pRecCfg->ain[0].sampleBits  = VIDEO_AUDIO_SAMPLE_BIT;
//        pRecCfg->ain[0].samples     = 1024; /* AAC must be 1024*/
//        pRecCfg->ain[0].fifoDepth   = 256;
//        pRecCfg->ain[0].bitrate     = VIDEO_AUDIO_BIT_RATE;
//        pRecCfg->ain[0].codecSel    = YBFW_MEDIA_AUDIO_AAC;
        pRecCfg->ain[0].devId      = YBFW_AUD_DEV_0_INT;
        pRecCfg->ain[0].sampleBits = 16;
        pRecCfg->ain[0].fifoDepth  = 256;
#if 1
        pRecCfg->ain[0].samples    = 1024; /* AAC must be 1024*/
        pRecCfg->ain[0].codecSel   = YBFW_MEDIA_AUDIO_AAC;
#else  //PCM codec
        pRecCfg->ain[0].samples    = 1200; /* 25ms in 48000 */
        pRecCfg->ain[0].codecSel   = YBFW_MEDIA_AUDIO_PCM;
#endif
    }

    pRecCfg->muxSel = YBFW_MEDIA_FILE_TYPE_MOV;
    pRecCfg->mux.notifySize = 1000*1000*100; /* nofitication will be sent every 100Mb */

//    if(pVidData->mediaFormat == MEDIA_FORMAT_MOV || pVidData->mediaFormat == MEDIA_FORMAT_MP4){
        //TODO NAV investigate
        mediaMovTags_t* pmyTag = &pRecCfg->mux.muxer.mov.muxTag;
        pmyTag->major_brand = FOURCC(' ', ' ', 't', 'q');
        pmyTag->minor_version = 0x1;

        UINT32 mov_compatible_brands[1] = {FOURCC('q', 't', ' ', ' ')};
        pmyTag->compatible_brands = (UINT32*)mov_compatible_brands;
        pmyTag->compatible_brands_cnt = 1;

        pmyTag->udta_atom.name = FOURCC('u', 'd', 't', 'a');
        pmyTag->udta_atom.update_mode = 1;
        pmyTag->udta_atom.location  = 1;

        /*pmyTag->udta_atom.pCallBack = (UINT32) videoTagUdtaCb;*/
        pmyTag->pref_volume = 0x100;
        pmyTag->vendor = FOURCC('t', 'a', 'c', 'i');;
        pmyTag->component_manuf = FOURCC('t', 'a', 'c', 'i');;
        pmyTag->data_ref_type = FOURCC('a', 'l', 'i', 's');
        pmyTag->movie_time_scale      = 60000;
        pmyTag->time_scale            = 60000;
        pmyTag->track_id_start        = 1;

        pmyTag->tkhd_flags = 0xF;
        pmyTag->video_track_matrix[0] = 0x10000;
        pmyTag->video_track_matrix[4] = 0x10000;
        pmyTag->video_track_matrix[8] = 0x40000000;
        pmyTag->temporal_quality = 0x0;
        pmyTag->spatial_quality = 0x0;
        pmyTag->video_sample_per_chunk = 1;
        pmyTag->video_handler_minf_present = 1;
        pmyTag->video_track_lang_id = pmyTag->audio_track_lang_id = 0x55c4;
        memcpy(&pmyTag->video_compressor_name[0], "AVC Coding", 10);
        memcpy(pmyTag->video_handler_comp_name, "", 1);
        memcpy(pmyTag->video_handler_ref_comp_name, "", 1);
        pmyTag->AVCProfileIndication  = 0x4D;
        pmyTag->profile_compatibility = 0x40;

        pmyTag->audio_compression_id   = 0;
        pmyTag->audio_sample_per_chunk = 1;
        pmyTag->audio_handler_minf_present = 1;
        memcpy(pmyTag->audio_handler_comp_name, "", 1);
        memcpy(pmyTag->audio_handler_ref_comp_name, "", 1);
        pmyTag->video_has_color_param = 1;
        pmyTag->audio_has_edts        = 1;
        pmyTag->audio_tkhd_same_duration = 1;
        pmyTag->audio_mdhd_same_duration = 0;
        pmyTag->uuid_present             = 0;
        pmyTag->ios_support_box_add  = 1;/*for IOS,BSP-794*/
//    }
    //pRecCfg->pExt = pext; //ext data

    return pRecCfg;
}

void baseRecCfgFree(ybfwMediaRecCfg_t *pcfg)
{
    if (pcfg) {

        if (pcfg->pExt) {
            ybfwFree(pcfg->pExt);
            pcfg->pExt = NULL;
        }

        ybfwFree(pcfg);

    }
}

//NAV TODO ext manual pRecCfg->pExt;
UINT32 baseRecStart(ybfwMediaRecCfg_t *pRecCfg){
    UINT32 handle = 0;
    UINT32 err;
    UINT32 t = ybfwMsTimeGet();
//    UINT32 mode;

    HOST_ASSERT(pRecCfg);
    DBG("trace\n");
//    appVideoExtraData_t *pext = pRecCfg->pExt;

    baseMediaInit();

    ybfwProfLogPrintf(0, "#S#STREAMCFG%d%d#N#Start%p", pRecCfg->vin[0].senId, pRecCfg->vin[0].yuvId, pRecCfg);

//    ybfwModeGet(&mode);

    DBG("trace\n");
    handle = ybfwMediaRecCreate(pRecCfg);
    DBG("trace\n");
    if ((err = appMediaPreviewEnter()) != SUCCESS) {
        printf("appMediaPreviewEnter() fail(%d)\n", err);

    }
    else {
        DBG("trace\n");
        //enable audio if needed
        UINT32 recmask = getRecMask();
        if (IS_BIT_SET(recmask,RECM_AUDIO)){
            appAudioRecVolumeRestore(IS_BIT_SET(recmask,RECM_DMIC));
        }
        if ((err = ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, handle)) != SUCCESS) {
            ERR("Record did not start");
            ybfwMediaRecDestroy(handle);
            handle = 0;
            appMediaPreviewExit();
            printf("YBFW_MEDIA_CTRL_START fail(%d)\n", err);

        }
    }
    DBG("trace\n");
//    pext->startTime = ybfwMsTimeGet();
    ybfwProfLogPrintf(0, "#E#STREAMCFG#N#%x", handle);


//    if(handle == 0)
//    {

//        for (i = 0 ; i < pRecCfg->vinNo ; i++)
//        {
//            /* clear media flag for yuv */
//            yuvId = 1 << ((pRecCfg->vin[i].senId<<2) + pRecCfg->vin[i].yuvId);
//
//            /* FIXME : temp workaround, need to place the proper place. */
//            if(pext->manual){
//            }
//            else{
//                appYuvEnClear(mode, yuvId, YBFW_MODE_PV_CFG_STREAM_MEDIA);
//            }
//        }

//    }
//    ybfwHostMsgSend(YBFW_MSG_MEDIA_REC_START, handle, ybfwMsTimeGet() - t, 0);
    DBG("trace\n");
    return handle;
}


/*  end of recording */
void baseRecStop(UINT32 handle)
{
    UINT32 yuvId;
    UINT32 i;
    if (handle != 0){
        DBG("trace\n");
        ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, handle);
        DBG("trace\n");
        appMediaPreviewExit();
        DBG("trace\n");
        DBG("trace\n");
        ybfwMediaRecDestroy(handle);
        DBG("trace\n");
        appAudioRecVolumeMute();
        DBG("trace\n");
        baseMediaRelease();
    }
}



void baseRecCfgDump(ybfwMediaRecCfg_t *pRecCfg)
{
    UINT32 i;

//    printf("mediaFwInited=%d\n", mediaFwInited);
//    printf("mediaPvEnter=%d\n", mediaEnter);
//    printf("handles:\n");

    if (pRecCfg) {
        printf("media cfg \n");
        char *muxtype[] = {
            [YBFW_MEDIA_FILE_TYPE_MOV] = "mov",
            [YBFW_MEDIA_FILE_TYPE_MP4] = "mp4",
            [YBFW_MEDIA_FILE_TYPE_AVI] = "avi",
            [YBFW_MEDIA_FILE_TYPE_WAV] = "wav",
            [YBFW_MEDIA_FILE_TYPE_CUS] = "cus",
            [YBFW_MEDIA_FILE_TYPE_BIN] = "bin",
            [YBFW_MEDIA_FILE_TYPE_MTS] = "mts",
            [YBFW_MEDIA_FILE_TYPE_USB] = "usb",
            [YBFW_MEDIA_FILE_TYPE_AAC] = "aac",
            };
        char *codecV[] = {
            [YBFW_MEDIA_VIDEO_UNKNOWN] = "unknown",
            [YBFW_MEDIA_VIDEO_MJPG] = "mjpg",
            [YBFW_MEDIA_VIDEO_H264] = "h264",
            [YBFW_MEDIA_VIDEO_HEVC] = "hevc",
            };
        char *codecA[] = {
            [YBFW_MEDIA_AUDIO_UNKNOWN] = "unknown",
            [YBFW_MEDIA_AUDIO_PCM] = "pcm",
            [YBFW_MEDIA_AUDIO_AAC] = "aac",
            };
        UINT32 i;
        for (i = 0 ; i < pRecCfg->vinNo ; i++) {
            printf("video%d config:\n", i);
            printf("\tsrc sen%d, yuv%d\n", pRecCfg->vin[i].senId, pRecCfg->vin[i].yuvId);
            printf("\tyuv finCb=%p, inc=%p, infcb=%p\n", pRecCfg->vin[i].fpYuvFrameInputCb,
                                                    pRecCfg->vin[i].fpYuvPtrInputCb,
                                                    pRecCfg->vin[i].fpYuvPtrInputFreeCb);
            printf("\tYUV info:\n");
            printf("\t\tpbuf=%p\n", pRecCfg->vin[i].yuvInfo.pbuf);
            printf("\t\tW/H=%dx%d, ", pRecCfg->vin[i].yuvInfo.width, pRecCfg->vin[i].yuvInfo.height);
            printf("ROI(%d,%d), %dx%d\n", pRecCfg->vin[i].yuvInfo.roiX,
                                            pRecCfg->vin[i].yuvInfo.roiY,
                                            pRecCfg->vin[i].yuvInfo.roiW,
                                            pRecCfg->vin[i].yuvInfo.roiH);
            printf("\t\tfmt=%x\n", pRecCfg->vin[i].yuvInfo.fmt);
            printf("\tfps=(sen)%d/(in)%d/(out)%d\n", pRecCfg->vin[i].frameRateSensor,
                                                    pRecCfg->vin[i].frameRateInput,
                                                    pRecCfg->vin[i].frameRateSave);
            printf("\tbitrate=%d/[%d~%d]\n", pRecCfg->vin[i].bitRate,
                                            pRecCfg->vin[i].bitRateMin,
                                            pRecCfg->vin[i].bitRateMax);
            printf("\tfifo=%d(%ds)\n", pRecCfg->vin[i].fifoDepth, pRecCfg->vin[i].fifoDepth/pRecCfg->vin[i].frameRateSave);
            printf("\tbrc=%s\n", pRecCfg->vin[i].brcType?"VBR":"CBR"); /* 0: CBR, 1: VBR */
            printf("\trotate=%d\n", pRecCfg->vin[i].rotate);
            printf("\tstop mode=%d\n", pRecCfg->vin[i].stopMode); /* 0: gop, 1: immediately */
            printf("\tvlc buf:\n");
            printf("\t\tcnt=%d\n", pRecCfg->vin[i].bufCnt);
            printf("\t\tsize=%d\n", pRecCfg->vin[i].bufSize);
            printf("\t1st frame=%d\n", pRecCfg->vin[i].firstFrame);
            printf("\tpiv cb=%p\n", pRecCfg->vin[i].pivFrmCb);
            if (pRecCfg->vin[i].codecSel > YBFW_MEDIA_VIDEO_HEVC ||
                pRecCfg->vin[i].codecSel == YBFW_MEDIA_VIDEO_UNKNOWN) {
                printf("\tcodec %s\n", codecV[0]);
            }
            else {
                printf("\tcodec %s:\n", codecV[pRecCfg->vin[i].codecSel]);
                if (pRecCfg->vin[i].codecSel == YBFW_MEDIA_VIDEO_H264) {
                    printf("\t\t%s,", pRecCfg->vin[i].codec.h264.entropy?"CAVLC":"CABAC");
                    printf("%s,", pRecCfg->vin[i].codec.h264.gopType==2?"IIII":(pRecCfg->vin[i].codec.h264.gopType?"IPPP":"IBBP"));
                    printf("GOP=%d,", pRecCfg->vin[i].codec.h264.gopNo);
                    printf("profile %s\n", pRecCfg->vin[i].codec.h264.profile?"HIGH":"BASIC");
                    printf("\t\tQP(fix=%d): ", pRecCfg->vin[i].codec.h264.qpFixed);
                    printf("I=%d,[%d-%d], ", pRecCfg->vin[i].codec.h264.qpCfgI&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgI>>16)&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgI>>8)&0xff);
                    printf("P=%d,[%d-%d], ", pRecCfg->vin[i].codec.h264.qpCfgP&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgP>>16)&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgP>>8)&0xff);
                    printf("B=%d,[%d-%d]\n", pRecCfg->vin[i].codec.h264.qpCfgB&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgB>>16)&0xff,
                                                    (pRecCfg->vin[i].codec.h264.qpCfgB>>8)&0xff);
                    printf("\t\tintra=%d,", pRecCfg->vin[i].codec.h264.intraMode);
                    printf("%s\n", pRecCfg->vin[i].codec.h264.interlace?"interlace":"progressive");
                    printf("\t\tsliceNo=%d/vuiTag=%d/idrInterval=%d\n", pRecCfg->vin[i].codec.h264.sliceNo,
                                                            pRecCfg->vin[i].codec.h264.vuiTag,
                                                            pRecCfg->vin[i].codec.h264.idrInterval);
                    printf("\t\taudSeiInfo=%d/sliceTypeOfs=%d/colorParam=%d\n", pRecCfg->vin[i].codec.h264.audSeiInfo,
                                                            pRecCfg->vin[i].codec.h264.sliceTypeOfs,
                                                            pRecCfg->vin[i].codec.h264.colorParam);
                    printf("\t\tseiCbInitCallback=%p\n", pRecCfg->vin[i].codec.h264.seiCbInitCallback);
                    printf("\t\tdynamicBitRate=%d\n", pRecCfg->vin[i].codec.h264.dynamicBitRate);
                    printf("\t\tpframeSkip=%d\n", pRecCfg->vin[i].codec.h264.pframeSkip);
                    printf("\t\tmbQpDiff=%d\n", pRecCfg->vin[i].codec.h264.mbQpDiff);
                    printf("\t\tmvCtrl=%d\n", pRecCfg->vin[i].codec.h264.mvCtrl);
                    printf("\t\tqtAcSkip=%d\n", pRecCfg->vin[i].codec.h264.qtAcSkip);
                    printf("\t\trcScaler=%d\n", pRecCfg->vin[i].codec.h264.rcScaler);
                    printf("\t\tstaticSliceType=%d\n", pRecCfg->vin[i].codec.h264.staticSliceType);
                    printf("\t\tspsLength=%d,", pRecCfg->vin[i].codec.h264.spsLength);
                    printf("spsLevelIdc=%d,", pRecCfg->vin[i].codec.h264.spsLevelIdc);
                    printf("spsFrameCropping=%d,", pRecCfg->vin[i].codec.h264.spsFrameCropping);
                    printf("spsSeqScalingMatrix=%d\n", pRecCfg->vin[i].codec.h264.spsSeqScalingMatrix);
                    printf("\t\tppsLog2MaxFrameNum=%d,", pRecCfg->vin[i].codec.h264.ppsLog2MaxFrameNum);
                    printf("ppsLog2MaxPicOrderCntLsb=%d,", pRecCfg->vin[i].codec.h264.ppsLog2MaxPicOrderCntLsb);
                    printf("ppsPicInitQp=%d\n", pRecCfg->vin[i].codec.h264.ppsPicInitQp);
                    printf("\t\tsliceDeblockingFilterCtrl=%d\n", pRecCfg->vin[i].codec.h264.sliceDeblockingFilterCtrl);
                }
                else if (pRecCfg->vin[i].codecSel == YBFW_MEDIA_VIDEO_HEVC) {
                    printf("\t\t%s,", pRecCfg->vin[i].codec.hevc.gopType==2?"IIII":(pRecCfg->vin[i].codec.h264.gopType?"IPPP":"IBBP"));
                    printf("GOP=%d,", pRecCfg->vin[i].codec.hevc.gopNo);
                    printf("\t\tQP(fix=%d): ", pRecCfg->vin[i].codec.hevc.qpFixed);
                    printf("%s\n", pRecCfg->vin[i].codec.hevc.interlace?"interlace":"progressive");
                    printf("\t\tsliceNo=%d/vuiTag=%d/idrInterval=%d\n", pRecCfg->vin[i].codec.hevc.sliceNo,
                                                            pRecCfg->vin[i].codec.h264.vuiTag,
                                                            pRecCfg->vin[i].codec.h264.idrInterval);
                    printf("\t\tseiCbInitCallback=%p\n", pRecCfg->vin[i].codec.hevc.seiCbInitCallback);
                    printf("\t\tenc2Pic=%d,\n", pRecCfg->vin[i].codec.hevc.enc2Pic);
                    printf("\t\tqpInit=%d,\n", pRecCfg->vin[i].codec.hevc.qpInit);
                    printf("\t\tqpMin=%d,\n", pRecCfg->vin[i].codec.hevc.qpMin);
                    printf("\t\tqpMax=%d,\n", pRecCfg->vin[i].codec.hevc.qpMax);
                    printf("\t\ttileMode=%d,\n", pRecCfg->vin[i].codec.hevc.tileMode);
                    printf("\t\tcolorParam=%d,\n", pRecCfg->vin[i].codec.hevc.colorParam);
                    printf("\t\tidrInterval=%d,\n", pRecCfg->vin[i].codec.hevc.idrInterval);

                }
                else if (pRecCfg->vin[i].codecSel == YBFW_MEDIA_VIDEO_MJPG) {
                    printf("\t\tQ value=%d,[%d-%d]\n", pRecCfg->vin[i].codec.mjpg.qInit, pRecCfg->vin[i].codec.mjpg.qMax, pRecCfg->vin[i].codec.mjpg.qMin);
                    printf("\t\tCur Q value=%d\n", pRecCfg->vin[i].codec.mjpg.qCur);
                    printf("\t\tyuv format=%s\n", pRecCfg->vin[i].codec.mjpg.yuvFmt?"422":"420"); /* 0: 420, 1: 422 */
                    int x;
                    printf("\t\tq table, y:");
                    for (x = 0 ; x < 64 ; x++) {
                        if ((x % 8) == 0)   printf("\n\t\t");
                        printf("%-3d ", pRecCfg->vin[i].codec.mjpg.qTblY[x]);
                    }
                    printf("\n\t\tq table, uv:");
                    for (x = 0 ; x < 64 ; x++) {
                        if ((x % 8) == 0)   printf("\n\t\t");
                        printf("%-3d ", pRecCfg->vin[i].codec.mjpg.qTblUv[x]);
                    }
                    printf("\n");
                }
                else {
                    memdump((void *)&pRecCfg->vin[i].codec, sizeof(pRecCfg->vin[i].codec));
                }
            }
        }
        for (i = 0 ; i < pRecCfg->ainNo ; i++) {
            printf("audio%d config:\n", i);
            printf("\tdevice%d\n", pRecCfg->ain[i].devId);
            printf("\tbit/sample=%d\n", pRecCfg->ain[i].sampleBits);
            printf("\tsample/buffer=%d\n", pRecCfg->ain[i].samples);
            printf("\tbitrate=%d\n", pRecCfg->ain[i].bitrate);
            printf("\tfifo=%d\n", pRecCfg->ain[i].fifoDepth);
            printf("\tcodec %s\n", codecA[pRecCfg->ain[i].codecSel]);
            if (pRecCfg->ain[i].codecSel == YBFW_MEDIA_AUDIO_AAC)
                printf("\t\tadtsEn=%d\n", pRecCfg->ain[i].codec.aac.adtsEn);
        }
        printf("misc:\n");
        printf("\tfilename=%s\n", pRecCfg->filename);
        if (pRecCfg->muxSel <= YBFW_MEDIA_FILE_TYPE_MTS)
            printf("\tmux type=%s\n", muxtype[pRecCfg->muxSel]);
        else
            printf("\tmux type unknown\n");
        printf("\tdisk thr=%d\n", pRecCfg->mux.diskThrKb);
        printf("\tsize notify=%d\n", pRecCfg->mux.notifySize);
        printf("\tvfi en=%d\n", pRecCfg->mux.vfiEn);
        printf("\t\tvfi interval=%d\n", pRecCfg->mux.vfiInterval);

        printf("\tpre rec=%d\n", pRecCfg->preRecMs);
        printf("\tclip e=%d\n", pRecCfg->endClipMs);
        printf("\telapse=%d\n", pRecCfg->elapseTimeMs);
        printf("\thigh speed en=%d\n", pRecCfg->hsModeEn);
        printf("\t\thigh speed cb=%p\n", pRecCfg->fpHsModeCb);
        printf("\tseamless\n");
        printf("\t\ttime=%d\n", pRecCfg->slTime);
        printf("\t\tsize=%lld\n", pRecCfg->slSize);
        printf("\t\tcb=%p\n", pRecCfg->fpSlChange);
        printf("\thost data=%p\n", pRecCfg->pExt);
    }
}
