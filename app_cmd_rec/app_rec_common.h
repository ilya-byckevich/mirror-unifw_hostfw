/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef APP_CMD_REC_APP_REC_COMMON_H_
#define APP_CMD_REC_APP_REC_COMMON_H_

#include "ykn_bfw_api/ybfw_media_api.h"

#define FOURCC(a, b, c, d)  ((d<<24)|(c<<16)|(b<<8)|a)

typedef enum{
    RECM_min = 0,
    RECM_AUDIO = RECM_min,  ///< enable audio (record to file)
    RECM_VIDEO,             ///< enable video (record to file)
    RECM_SCREEN,            ///< enable Screen recording (osd record too)
    RECM_DMIC,              ///< use digital mic  (default analog mic)
    RECM_FAKE_REC,          ///< launch a/v codecs, but don't save to storage
    RECM_EXT_MP4LIB,        ///< use external mp4 lib (our implementation)
    RECM_max,
}RecMask;

void setRecMask(UINT32 curmask);
UINT32 getRecMask();

void baseRecCfgDump(ybfwMediaRecCfg_t *pRecCfg);
ybfwMediaRecCfg_t * baseRecCfgAlloc(UINT32 curRecmask);
void baseRecCfgFree(ybfwMediaRecCfg_t *pcfg);
UINT32 baseRecStart(ybfwMediaRecCfg_t *pRecCfg);
void baseRecStop(UINT32 handle);
void baseMediaInit(void);
void baseMediaRelease(void);
void osdPvVideoPvYuvOn(UINT32 yuvId);
void osdPvVideoPvYuvOff(UINT32 yuvId);
#endif /* APP_CMD_REC_APP_REC_COMMON_H_ */
