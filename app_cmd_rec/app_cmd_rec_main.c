//#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ykn_bfw_api/ybfw_dzoom_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_gfx_api.h"
//#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "app_common.h"
#include "app_cmd_rec/audio.h"
#include "app_cmd_rec/app_rec_common.h"
#include "app_cmd_pv.h"


#define DEFAULT_RECMASK  ( (1<< RECM_VIDEO) | (1 << RECM_AUDIO) | (1<< RECM_SCREEN) )

static UINT32 curRecmask=DEFAULT_RECMASK;

static ybfwMediaRecCfg_t *cfgRec = NULL;
static UINT32 isRecStarted = 0;
static int movNameFlag=0;
static UINT32 gwRecHandle;

//uncomment to do simple test of record
//#define TEST_SIMPLE_REC

typedef void (*funcPreinit)();
typedef struct {
    UINT8 * textInfo;
    funcPreinit funcPreinit;
}RecMaskInfo;


static RecMaskInfo recInfo[RECM_max]={
        [RECM_AUDIO] =      {.textInfo=(UINT8 *)"Add audio track to output file",.funcPreinit=NULL},
        [RECM_VIDEO] =      {.textInfo=(UINT8 *)"Add video track to output file",.funcPreinit=NULL},
        [RECM_SCREEN] =     {.textInfo=(UINT8 *)"Screen record (with OSD)",.funcPreinit=NULL},
        [RECM_DMIC] =       {.textInfo=(UINT8 *)"Use Digital Mic (default analog",.funcPreinit=NULL},
        [RECM_FAKE_REC] =   {.textInfo=(UINT8 *)"Do not create output file. Only encode media",.funcPreinit=NULL},
        [RECM_EXT_MP4LIB] = {.textInfo=(UINT8 *)"Use external implementation of mp4 container",.funcPreinit=NULL},
};



/* A thread synchronized with the callback function*/

void setRecMask(UINT32 curmask){
    curRecmask = curmask;
   // INFO("New mask record mode 0x%X will be applied on new record\n",curRecmask);
}
UINT32 getRecMask(){
    return curRecmask;
}

#ifdef TEST_SIMPLE_REC
//just for simple ltest rec

void videoPreviewSet(void)
{
    DBG("trace");
    if (!isAppPvStreamStarted()){
        appStreamPv(PV_DFLT_WIDTH,PV_DFLT_HEIGHT,PV_DFLT_FPS,PV_DFLT_CDSPFPS,PV_DFLT_RAWBIT_MODE,PV_DFLT_FMT);
        appStreamPvYuvEnable(1,0);//check recCfg->vin[0].yuvId
    }
}


void simpleTestRec(){
    UINT32 recHandle;
    INFO("Simple Rec Test \n");
    INFO("RECSTEP  ybfwMediaCtrl(YBFW_MEDIA_CTRL_INIT) \n");
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_INIT);
    ybfwMediaRecCfg_t *recCfg = (ybfwMediaRecCfg_t *)ybfwMalloc(sizeof(ybfwMediaRecCfg_t));
    memset(recCfg, 0x0, sizeof(ybfwMediaRecCfg_t));

    INFO("RECSTEP videoPreviewSet();\n");
    videoPreviewSet();
    INFO("RECSTEP Alloc rec config\n");
    recCfg->muxSel = YBFW_MEDIA_FILE_TYPE_MOV; /* MOV file format */
    if (IS_BIT_SET(curRecmask,RECM_VIDEO)){
        INFO("AUDIO Track is enabled\n");
        recCfg->vinNo = 1;
    }else{
        INFO("AUDIO Track is disabled\n");
        recCfg->vinNo = 0;
    }
    /* Recorded to test01.mov in H264 video format */
#if (SP5K_DISK_NAND || SP5K_DISK_EMMC || SP5K_DISK_ESD || SP5K_DISK_SPI)
    ybfwFsDirChange((UINT8*)"C:\\");
    sprintf(recCfg->filename, "C:\\%dKTEST_simple.mov", movNameFlag);
#endif
#if SP5K_DISK_SD
    ybfwFsDirChange((UINT8*)"D:\\");
    sprintf(recCfg->filename, "D:\\%dKTEST_simple.mov", movNameFlag);
#endif
    movNameFlag++;
//    sprintf(recCfg->filename, "D://test01.mov");
    recCfg->vin[0].codecSel = YBFW_MEDIA_VIDEO_H264;
    recCfg->vin[0].codec.h264.qpCfgI = 0x14261e; /**< b[7:0] init, b[15:8] max, b[23:16] min */
    recCfg->vin[0].codec.h264.qpCfgP = 0x14261e; /**< b[7:0] init, b[15:8] max, b[23:16] min */
    recCfg->vin[0].codec.h264.qpCfgB = 0x14261e; /**< b[7:0] init, b[15:8] max, b[23:16] min */
    recCfg->vin[0].codec.h264.gopType = 1;/**< 0: IBBP, 1: IPPP, 2: IIII */
    recCfg->vin[0].codec.h264.gopNo = 15;/**< 1 or 15 or 45 typically */
    recCfg->vin[0].codec.h264.profile = YBFW_MEDIA_H264_PROFILE_HIGH;
    recCfg->vin[0].codec.h264.intraMode = 1;/**< 0: I4+I16, 1: I8+I16, 2: I4+I8 */
    recCfg->vin[0].codec.h264.sliceNo = 1;
    recCfg->vin[0].codec.h264.idrInterval = 1;
    recCfg->vin[0].codec.h264.colorParam = 1;

    /* sensor input source, the output size will be yuv's size */
    recCfg->vin[0].senId = 0;
    recCfg->vin[0].yuvId = 0;
    /* sensor frame rate and video saved frame rate are 30 fps */
    recCfg->vin[0].frameRateSensor = 30;
    recCfg->vin[0].frameRateInput = 30;
    recCfg->vin[0].frameRateSave  = 30;

    /* YUV input ring buffer configuration */
    recCfg->vin[0].bufCnt = 4;

    /* VLC max buffer configuration, buffer number is 4 */
    recCfg->vin[0].bufSize = 10*1024*1024;
    recCfg->vin[0].fifoDepth = 64;

    /* video bitrate control type is constant bitrate and bitrate is 800kb/s */
    recCfg->vin[0].brcType = 0;
    recCfg->vin[0].bitRate = 60*1024*1024;

    if (IS_BIT_SET(curRecmask,RECM_AUDIO)){
        INFO("RECSTEP Enable audio subsystem();\n");
        INFO("AUDIO Track is enabled\n");
        ybfwAudDevEnable(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, 1);
        if (IS_BIT_SET(curRecmask,RECM_DMIC)){
            INFO("Apply MIC Digital\n");
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_DMIC_IN);
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL   |YBFW_AUD_CFG_MASK_CH_ALL, 14); /*14db*/
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN  |YBFW_AUD_CFG_MASK_CH_ALL, 12);
            ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, 48000);
            ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, 2);
        }else{
            INFO("Apply MIC Analog\n");
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_AMIC_IN);
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL  | YBFW_AUD_CFG_MASK_CH_ALL, 10); /*32db*/
            ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN | YBFW_AUD_CFG_MASK_CH_ALL, 5); /*12db*/
            ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, 48000);
            ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, 2);
        }
        recCfg->ainNo = 1;
    }else {
        INFO("AUDIO Track is disabled\n");
        recCfg->ainNo = 0;
    }
    /* audio record device */
    recCfg->ain[0].devId      = YBFW_AUD_DEV_0_INT;
    recCfg->ain[0].sampleBits = 16;
    recCfg->ain[0].samples    = 1200; /* 25ms in 48000 */
    recCfg->ain[0].fifoDepth  = 256;
    recCfg->ain[0].codecSel   = YBFW_MEDIA_AUDIO_PCM;

    baseRecCfgDump(recCfg);
    INFO("RECSTEP ybfwMediaRecCreate(recCfg)\n");
    recHandle = ybfwMediaRecCreate(recCfg);
    INFO("RECSTEP ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER);\n");
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER);
    INFO("RECSTEP ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);\n");
    ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
    INFO("RECSTEP ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);\n");
    ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);
    INFO("RECSTEP ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, recHandle);\n");
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_START, recHandle);
    INFO("Record 20 seconds\n");
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 20000);
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, recHandle);
    INFO("RECSTEP ybfwMediaCtrl(YBFW_MEDIA_CTRL_STOP, recHandle);\n");
    ybfwMediaRecDestroy(recHandle);
    INFO("RECSTEP ybfwMediaRecDestroy(recHandle);\n");
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_UNINIT);
    INFO("RECSTEP ybfwMediaCtrl(YBFW_MEDIA_CTRL_UNINIT);\n");
    ybfwFree(recCfg);
}
#endif


void fakeRecCb(UINT32 handle, ybfwMediaMuxerCusInfo_t *pinfo) {
    /* host customized */
//    printf("%c: %p(%d)@%lld\n", ((pinfo->type) ? 'A' : 'V'), pinfo->pbuf, pinfo->size, pinfo->stamp);
}

void fakeMuxReceiveCb (UINT32 handle, UINT32 type, void * pbuf, UINT32 size, UINT64 stamp) {
    /* type : 0 is video, 1 is audio */
    /* stamp : unit is microsecond, time stamp timing is (raw EOF stamp - (1000000 * recCfg.vin[0].frameRateInput Numerator / recCfg.vin[0].frameRateInput Denominator)) */
    //printf("handle 0x%x type %c pbuf 0x%p size %d stamp %lld\n", handle, ((type)?'A':'V'), pbuf, size, stamp);
}

void startRec(){
    UINT32 yuvId;
    UINT32 i;
    if (isRecStarted){
        INFO("The Record is already started\n");
    }
    DBG("Start record");
    if (!isAppPvStreamStarted()){
        appStreamPv(PV_DFLT_WIDTH,PV_DFLT_HEIGHT,PV_DFLT_FPS,PV_DFLT_CDSPFPS,PV_DFLT_RAWBIT_MODE,PV_DFLT_FMT);
    }
    DBG("trace\n");
    //configure record params
    cfgRec = baseRecCfgAlloc(getRecMask());
#if (SP5K_DISK_NAND || SP5K_DISK_EMMC || SP5K_DISK_ESD || SP5K_DISK_SPI)
    sprintf(cfgRec->filename, "C:\\%dKTEST.mp4", movNameFlag);
    ybfwFsDirChange((UINT8*)"C:\\");
#endif
#if SP5K_DISK_SD
    sprintf(cfgRec->filename, "D:\\%dKTEST.mp4", movNameFlag);
    ybfwFsDirChange((UINT8*)"D:\\");
#endif

    //Fake Store data
    if (IS_BIT_SET(curRecmask,RECM_FAKE_REC)){
        INFO ("ENABLE fake mode");
        cfgRec->muxSel = YBFW_MEDIA_FILE_TYPE_CUS;
        cfgRec->mux.muxer.cus.fpDataCb = fakeRecCb;
        cfgRec->mux.fpMuxReceiveCb     = fakeMuxReceiveCb;
    }

    DBG("trace\n");
    baseRecCfgDump(cfgRec);
    for (i = 0 ; i < cfgRec->vinNo ; i++) {
        /* set media flag for yuv */
//        yuvId = 1 << ((cfgRec->vin[i].senId<<2) + cfgRec->vin[i].yuvId); //NAV TODO check strange formula
        yuvId = cfgRec->vin[i].yuvId ;
//        if (! IS_BIT_SET(curRecmask,RECM_SCREEN)){
            appStreamPvYuvEnable(1,yuvId);
//        }else{
//            osdPvVideoPvYuvOn(yuvId);
//        }
    }
    //start record
    gwRecHandle = baseRecStart(cfgRec);
    DBG("trace\n");
    if (gwRecHandle == 0){
        ERR("ERROR: The record was not started\n");
        for (i = 0 ; i < cfgRec->vinNo ; i++) {
            /* set media flag for yuv */
//            yuvId = 1 << ((cfgRec->vin[i].senId<<2) + cfgRec->vin[i].yuvId);
            yuvId = cfgRec->vin[i].yuvId ;
//            if (! IS_BIT_SET(curRecmask,RECM_SCREEN)){
                appStreamPvYuvEnable(0,yuvId);
//            }else{
//                osdPvVideoPvYuvOff(yuvId);
//            }
        }
    }else{
        movNameFlag++;
        isRecStarted = 1;
    }
    DBG("trace\n");
}

void pauseRec(UINT8 is_pause){
    if (!isRecStarted){
        return;
    }
    if (is_pause){
        INFO("Set record to pause\n");
        ybfwMediaCtrl(YBFW_MEDIA_CTRL_PAUSE,gwRecHandle);
    }else{
        INFO("Resume recording....\n");
        ybfwMediaCtrl(YBFW_MEDIA_CTRL_RESUME,gwRecHandle);
    }
}

void stopRec(){
    UINT32 yuvId;
    UINT32 i;
    if(isRecStarted){
        DBG("Stop record\n");
        baseRecStop(gwRecHandle);
        for (i = 0 ; i < cfgRec->vinNo ; i++) {
            /* set media flag for yuv */
//            yuvId = 1 << ((cfgRec->vin[i].senId<<2) + cfgRec->vin[i].yuvId);
            yuvId = cfgRec->vin[i].yuvId ;
//            if (! IS_BIT_SET(curRecmask,RECM_SCREEN)){
                appStreamPvYuvEnable(0,yuvId);
//            }else{
//                osdPvVideoPvYuvOff(yuvId);
//            }
        }
        baseRecCfgFree(cfgRec);
        isRecStarted = 0;
    }else{
        INFO("The Record is not started\n");
    }
}

int cmdRec(int argc,char **argv,UINT32*v){
    char* cmd = *argv++;v++;argc--;
    UINT32 mask=*v++;
    if(argc<0) {
      return 0;
    }
    else if (isEqualStrings(cmd, (UINT8 *)"start", CCS_NO ) ) {
#ifndef TEST_SIMPLE_REC
        startRec();
#else
        simpleTestRec();
#endif

    }
    else if (isEqualStrings(cmd, (UINT8 *)"stop", CCS_NO ) ) {
        stopRec();
    }
    else if (isEqualStrings(cmd, (UINT8 *)"pause", CCS_NO ) ) {
        pauseRec(1);
    }
    else if (isEqualStrings(cmd, (UINT8 *)"resume", CCS_NO ) ) {
        pauseRec(0);
    }
    else if (isEqualStrings(cmd, (UINT8 *)"mode", CCS_NO ) ) {
        setRecMask(mask);
    }
    else{
      return 0;
    }
    return 1;
}



void cmdRecHelp(){
    SINT32 i;
    printf("rec - recording video\n");
    printf("\trec mode <digit mask>  - set record mode by mask. default %d. where:\n",DEFAULT_RECMASK);
    for (i = RECM_min ; i < RECM_max; i++){
        printf("\t\t\t bit  %d (mask val 0x%X)  - '%s' \n",i,1 << i,recInfo[i].textInfo);
    }
    printf("\n");
    printf("\trec start  - start record\n");
    printf("\trec stop   - stop record\n");
    printf("\trec pause  - pause recording\n");
    printf("\trec resume - resume recording\n");

}



