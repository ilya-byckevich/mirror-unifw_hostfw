/**************************************************************************
 *
 *       Copyright (c) 2012-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#define HOST_DBG 0
#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ybfw_rsvblk_api.h"
//#include "ybfw_disp_api.h"
//#include "ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_aud_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ybfw_dcf_api.h"
//#include "ybfw_media_api.h"
//#include "ybfw_disk_api.h"
#include "app_common.h"
#include "app_cmd_rec/audio.h"

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
/* If ALC enabled, the microphone gain and digital gain controlled by audio ALC, gain value is invalid */
#define AMIC_SELECT_EN          	    1  	/* 1->Amic Enable, Dmic Disable; 0->Amic disable, Dmic Enable */
#define ALC_EN	                		1  	/* 1->ALC Enable,   0->ALC disable */

#define APP_AMIC_DEFAULT_REC_MIC_GAIN	32	/* MIC Fister Gain-> Amic default gain: 32db */
#define APP_AMIC_DEFAULT_REC_D_GAIN     12	/* MIC Second Gain-> Suggest: Amic/Dmic default gain: 0db */

#define APP_DMIC_DEFAULT_REC_MIC_GAIN	14	/* MIC Fister Gain-> Dmic default gain:14db */
#define APP_DMIC_DEFAULT_REC_D_GAIN     10	/* MIC Second Gain-> Suggest: Amic/Dmic default gain: 0db */

#define APP_MUTE_REC_MIC_GAIN			-18	/* MIC Fister Gain-> Amic/Dmic Mute gain: -18db */
#define APP_MUTE_REC_D_GAIN				-48	/* MIC Second Gain-> Amic/Dmic Mute gain: -48db */
/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/

/**************************************************************************
 *                          D A T A    T Y P E S                          *
 **************************************************************************/

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
//appAudioCB_t gAudioCB;
UINT32 AudioRemainTime = 0;
UINT8 gAudioAlcEnable = 0;

static BOOL gIsAudRecMute=FALSE;
/*
static UINT32 gAudRecVol = APP_AMIC_DEFAULT_REC_MIC_GAIN;
static UINT32 gAudRecVolSave = APP_AMIC_DEFAULT_REC_MIC_GAIN;
*/

/**************************************************************************
 *                 E X T E R N A L    R E F E R E N C E S                 *
 **************************************************************************/

/**************************************************************************
 *               F U N C T I O N    D E C L A R A T I O N S               *
 **************************************************************************/
/*-------------------------------------------------------------------------
*  File Name : appAudioRecVolumeRestore
*------------------------------------------------------------------------*/
void appAudioRecVolumeRestore(AudioMicType mic)
{
	DBG("AUD Vol RESTORE\n");
	/* gAudRecVol = gAudRecVolSave; */

	if (mic == MIC_ANALOG)
	{
		ybfwAudDevEnable(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, 1);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_AMIC_IN);
		ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, VIDEO_AUDIO_SAMPLE_RATE);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_AMIC_IN);/*select Amic*/
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL   |YBFW_AUD_CFG_MASK_CH_ALL, APP_AMIC_DEFAULT_REC_MIC_GAIN);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN  |YBFW_AUD_CFG_MASK_CH_ALL, APP_AMIC_DEFAULT_REC_D_GAIN);
		ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, 2);
	}
	else
	{
		ybfwAudDevEnable(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, 1);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_DMIC_IN);
		ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, VIDEO_AUDIO_SAMPLE_RATE);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC|YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_DMIC_IN);/*select Dmic*/
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL   |YBFW_AUD_CFG_MASK_CH_ALL, APP_DMIC_DEFAULT_REC_MIC_GAIN);
		ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN  |YBFW_AUD_CFG_MASK_CH_ALL, APP_DMIC_DEFAULT_REC_D_GAIN);
		ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, 2);
	}
	gIsAudRecMute = FALSE;
}

/*-------------------------------------------------------------------------
*  File Name : appAudioRecVolumeMute
*------------------------------------------------------------------------*/
void appAudioRecVolumeMute(void)
{
    DBG("AUD Vol MUTE\n");
	/*
	gAudRecVolSave = gAudRecVol;
	gAudRecVol = APP_MUTE_REC_MIC_GAIN;
	*/
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_AVC_EN, 0);
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL   |YBFW_AUD_CFG_MASK_CH_ALL, APP_MUTE_REC_MIC_GAIN);
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN  |YBFW_AUD_CFG_MASK_CH_ALL, APP_MUTE_REC_D_GAIN);
	gIsAudRecMute = TRUE;
}

/*-------------------------------------------------------------------------
*  File Name : appAudioRecVolumeConfig
*------------------------------------------------------------------------*/
void
appAudioRecVolumeConfig(
	void
)
{
/*Remove ALC setting, have problem in V50 now*/
#if 0
	if (gAudioAlcEnable == 1) {
		ybfwMediaAlcParamFileLoad(RES_WAV_ALC_TXT);
		ybfwMediaRecCfgSet(YBFW_MEDIA_REC_ALC, YBFW_MEDIA_REC_ON);
	}
	else {
		ybfwMediaRecCfgSet(YBFW_MEDIA_REC_ALC, YBFW_MEDIA_REC_OFF);
	}

	gAudRecVol = gAudRecVolSave = APP_DEFAULT_REC_AUD_VOL;
	ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL, gAudRecVol);


	if (gAudioAlcEnable == 1) {
		ybfwMediaRecCfgSet(YBFW_MEDIA_REC_ALC_MODE, YBFW_MEDIA_REC_ALC_DRC_MODE); /* ALC mode set */
	}

	gIsAudRecMute = FALSE;
    /* Verified on HW_SBC*/
    {
    	ybfwAudFilterNotchCfg_t NotchCfg;

    	NotchCfg.chnlId= YBFW_AUD_FILTER_ALL;
    	NotchCfg.Fsample =44100;
    	NotchCfg.firstNotch.en=1;
    	NotchCfg.firstNotch.Fcentre=7900;
    	NotchCfg.firstNotch.Qfactor=5;
    	NotchCfg.secondNotch.en=1;
    	NotchCfg.secondNotch.Fcentre=4000;
    	NotchCfg.secondNotch.Qfactor=2;
    	NotchCfg.thirdNotch.en=1;
    	NotchCfg.thirdNotch.Fcentre=6000;
    	NotchCfg.thirdNotch.Qfactor=2;
    	NotchCfg.fourthNotch.en=1;
    	NotchCfg.fourthNotch.Fcentre=7800;
    	NotchCfg.fourthNotch.Qfactor=1;
    	NotchCfg.fifthNotch.en=1;
    	NotchCfg.fifthNotch.Fcentre=10000;
    	NotchCfg.fifthNotch.Qfactor=1;

    	ybfwAudFilterNotchCfg(&NotchCfg);

    	/* Equalizer filter */
    	ybfwAudFilterEqCfg_t eqFilter;

    	eqFilter.chnlId = YBFW_AUD_FILTER_ALL;
    	eqFilter.en = 1;
    	eqFilter.Fsample = 44100;

    	eqFilter.lowShelf.Fcentre = 300;
    	eqFilter.lowShelf.gain = -10;

    	eqFilter.firstPeak.Fcentre = 4000;
    	eqFilter.firstPeak.gain = 0;
    	eqFilter.firstPeak.Qfactor = 1;
    	eqFilter.secondPeak.Fcentre = 10;
    	eqFilter.secondPeak.gain = 0;
    	eqFilter.secondPeak.Qfactor = 1;

    	eqFilter.thirdPeak.Fcentre = 10;
    	eqFilter.thirdPeak.gain = 0;
    	eqFilter.thirdPeak.Qfactor = 1;

    	eqFilter.highShelf.Fcentre = 10000;
    	eqFilter.highShelf.gain = -10;
    	ybfwAudFilterEqCfg(&eqFilter);
    }
#endif
}

