/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef APP_CMD_REC_AUDIO_H_
#define APP_CMD_REC_AUDIO_H_

typedef enum{
    MIC_ANALOG = 0,
    MIC_DIGITAL,
}AudioMicType;

#define VIDEO_AUDIO_BIT_RATE                    128000
#define VIDEO_AUDIO_SAMPLE_RATE                 48000
#define VIDEO_AUDIO_SAMPLE_BIT                  16
#define VIDEO_AUDIO_WC_CENTER_FREQ              1000
#define VIDEO_AUDIO_WC_DAMPING_FAC              2828

void appAudioRecVolumeMute(void);
void appAudioRecVolumeRestore(AudioMicType mic);

#endif /* APP_CMD_REC_AUDIO_H_ */
