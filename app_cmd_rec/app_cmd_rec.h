/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/



/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_REC_H_
#define SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_REC_H_

int cmdRec(int argc,char **argv,UINT32*v);
void cmdRecHelp();


#endif /* SPHOST_CUSTOMIZATION_HOSTFW_CMD_APP_CMD_REC_H_ */
