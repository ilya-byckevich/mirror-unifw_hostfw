/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
//#include "ykn_bfw_api/ybfw_dzoom_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"

#include "app_common.h"
#include "app_cmd_rec/audio.h"
#include "app_cmd_rec/app_rec_common.h"

//static UINT8*  g_pipScaleBuf  = NULL;  //need




static ybfwGfxObj_t  g_resultYUV;      //Result Yuv to record
static UINT8*  g_resultYUVBuf  = NULL;


static ybfwGfxObj_t  g_pipLayer_gfxObjSrc; //PIP Layer related
static UINT8*  g_pipLayerBuf  = NULL;


static ybfwGfxObj_t g_osdLayerScale_gfxObjDst;  //OSD Layer related
static UINT8*  g_osdLayerScaleBuf = NULL;


YBFW_EVENT_FLAGS_GROUP osd_evt;

YBFW_THREAD *thrdOsd = NULL;


int ifOsdRefresh = 1;



//#define DBG_SAVE_OSD_RAW

void appYuvImgcpy(void *dst, void *src, UINT32 w, UINT32 h, UINT32 fmt)
{
    if (dst == NULL){
        ERR("dst is NULL !!!!\n");
        return ;
    }
    if (src == NULL){
        ERR("src is NULL !!!!\n");
        return ;
    }
    ybfwGfxObj_t srcObj = {
        .pbuf = src,
        .bufW = YUV_BUF_W_ALIGN(w),
        .bufH = YUV_BUF_H_ALIGN(h),
        .roiW = w,
        .roiH = h,
        .fmt = fmt
    };
    ybfwGfxObj_t dstObj = {
        .pbuf = dst,
        .bufW = YUV_BUF_W_ALIGN(w),
        .bufH = YUV_BUF_H_ALIGN(h),
        .roiW = w,
        .roiH = h,
        .fmt = fmt
    };

    //DBG( "#S#YUVMCPY#N#%dx%d \n", w, h);

    ybfwGfxObjectCopy(&srcObj, &dstObj);
    //DBG("#E#YUVMCPY#N# \n");
}


void osdBlend(ULONG param)
{
    ybfwGfxObj_t osd_gfxObjSrc;
    ULONG osdEvents;

    UINT32 pbflag = 0;
    ybfwGfxPageCapabDesc_t recPipPara;
    ybfwGfxPageCapabDesc_t osd_cap;
    //DBG("trace\n");

#ifdef DBG_SAVE_OSD_RAW
    int counter = 0;
    char tmpbuf[128]={0}; /*temporary*/
#endif

    while (1)
    {
        /*This flag bit is synchronized with the osdvideourgentCB callback function*/
        DBG("trace\n");
        ybfwOsEventFlagsGet(&osd_evt, 1, YBFW_TX_OR_CLEAR, &osdEvents,
                YBFW_TX_WAIT_FOREVER);

        ybfwGfxPageCapabilityGet(YBFW_GFX_PAGE_OSD_0, &osd_cap);
        osd_gfxObjSrc.pbuf = osd_cap.pbuf;
        osd_gfxObjSrc.roiX = osd_gfxObjSrc.roiY = 0;
        osd_gfxObjSrc.bufW = YUV_BUF_W_ALIGN(osd_cap.frmW);
        osd_gfxObjSrc.bufH = YUV_BUF_H_ALIGN(osd_cap.frmH);
        osd_gfxObjSrc.roiW = YUV_BUF_W_ALIGN(osd_cap.frmRoiW);
        osd_gfxObjSrc.roiH = YUV_BUF_H_ALIGN(osd_cap.frmRoiH);
        osd_gfxObjSrc.paplaneBuf = NULL;
        osd_gfxObjSrc.fmt = osd_cap.fmt;

#ifdef DBG_SAVE_OSD_RAW
        sprintf(tmpbuf,"D:\\OSD_0_RGB565_%dx%d_%d.%d.RGB",osd_gfxObjSrc.bufW , osd_gfxObjSrc.bufH,movNameFlag,counter);
        fsSimpleWrite( tmpbuf, osd_gfxObjSrc.pbuf, osd_gfxObjSrc.bufW * osd_gfxObjSrc.bufH * 2);
#endif

//        DBG("YBFW_GFX_PAGE_OSD_0YBFW_GFX_PAGE_OSD_0 ROI (%d,%d : %d,%d) WxH %dx%d FMT %d  buf %s\n",
//                osd_cap.frmRoiX,osd_cap.frmRoiY,osd_cap.frmRoiW,osd_cap.frmRoiH,osd_cap.frmW,osd_cap.frmH,osd_cap.fmt,osd_cap.pbuf ? "!= NULL":"= NULL");
//        DBG("osd_gfxObjSrc ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
//                osd_gfxObjSrc.roiX,osd_gfxObjSrc.roiY,osd_gfxObjSrc.roiW,osd_gfxObjSrc.roiH,osd_gfxObjSrc.bufW,osd_gfxObjSrc.bufH,osd_gfxObjSrc.fmt,osd_gfxObjSrc.pbuf ? "!= NULL":"= NULL");

        g_osdLayerScale_gfxObjDst.pbuf = g_osdLayerScaleBuf;
        g_osdLayerScale_gfxObjDst.roiX = g_osdLayerScale_gfxObjDst.roiY = 0;
        g_osdLayerScale_gfxObjDst.bufW = YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH);
        g_osdLayerScale_gfxObjDst.bufH = YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT);
        g_osdLayerScale_gfxObjDst.roiW = YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH);
        g_osdLayerScale_gfxObjDst.roiH = YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT);
        g_osdLayerScale_gfxObjDst.paplaneBuf = NULL;
        g_osdLayerScale_gfxObjDst.fmt = YBFW_IMG_FMT_YUV422_NV16;

//        DBG("g_osdLayerScale_gfxObjDst ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
//                g_osdLayerScale_gfxObjDst.roiX,g_osdLayerScale_gfxObjDst.roiY,g_osdLayerScale_gfxObjDst.roiW,g_osdLayerScale_gfxObjDst.roiH,g_osdLayerScale_gfxObjDst.bufW,g_osdLayerScale_gfxObjDst.bufH,g_osdLayerScale_gfxObjDst.fmt,g_osdLayerScale_gfxObjDst.pbuf ? "!= NULL":"= NULL");
//        DBG("BUF_OP: SCALE conv_gfxObjDst -> g_osdLayerScale_gfxObjDst \n");

        /*Convert osd rgb to yuv*/
         if (SUCCESS != ybfwGfxObjectConvert(&osd_gfxObjSrc, &g_osdLayerScale_gfxObjDst))
         {
             printf("osd convert unSuccessful\n");
         }
#ifdef DBG_SAVE_OSD_RAW
#if YBFW_DISK_SD
        sprintf(tmpbuf,"D:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
#if (YBFW_DISK_NAND || YBFW_DISK_EMMC || YBFW_DISK_ESD || YBFW_DISK_SPI)
        sprintf(tmpbuf,"C:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
        fsSimpleWrite( tmpbuf, g_osdLayerScale_gfxObjDst.pbuf, g_osdLayerScale_gfxObjDst.bufW * g_osdLayerScale_gfxObjDst.bufH * 2);
        counter++;
#endif

//        DBG("trace\n");
    }

}



/* callback function  */
UINT32 osdvideourgentCB( ybfwModePrevYuvInfo_t *pyuvInfo, UINT32 yuvNum)
{

    UINT32 ybfwmode;
    ybfwModeGet(&ybfwmode);
    //DBG("trace\n");
//    DBG("pyuvInfo->pframe ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
//            pyuvInfo->pframe.roiX,pyuvInfo->pframe.roiY,pyuvInfo->pframe.roiW,pyuvInfo->pframe.roiH,pyuvInfo->pframe.width,pyuvInfo->pframe.height,pyuvInfo->pframe.fmt,pyuvInfo->pframe.pbuf ? "!= NULL":"= NULL");

#if 1
    //NAV TODO fix using YBFW_MODE_PREV_SENSOR_0_YUV_0
    if ((yuvNum == YBFW_MODE_PREV_SENSOR_0_YUV_0) && ybfwmode == YBFW_MODE_STILL_PREVIEW)
    {DBG("ybfwmode == YBFW_MODE_STILL_PREVIEW\n");
        ybfwGfxObj_t srcPre;
        srcPre.pbuf = pyuvInfo->pframe.pbuf;
        srcPre.roiX = pyuvInfo->pframe.roiX;
        srcPre.roiY = pyuvInfo->pframe.roiY;
        srcPre.bufW = YUV_BUF_W_ALIGN(pyuvInfo->pframe.width);
        srcPre.bufH = YUV_BUF_H_ALIGN(pyuvInfo->pframe.height);
        srcPre.roiW = YUV_BUF_W_ALIGN(pyuvInfo->pframe.roiW);
        srcPre.roiH = YUV_BUF_W_ALIGN(pyuvInfo->pframe.roiH);
        srcPre.fmt = pyuvInfo->pframe.fmt;

        DBG("pyuvInfo->pframe ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
                pyuvInfo->pframe.roiX,pyuvInfo->pframe.roiY,pyuvInfo->pframe.roiW,pyuvInfo->pframe.roiH,pyuvInfo->pframe.width,pyuvInfo->pframe.height,pyuvInfo->pframe.fmt,pyuvInfo->pframe.pbuf ? "!= NULL":"= NULL");
        DBG("srcPre ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
                srcPre.roiX,srcPre.roiY,srcPre.roiW,srcPre.roiH,srcPre.bufW,srcPre.bufH,srcPre.fmt,srcPre.pbuf ? "!= NULL":"= NULL");

        DBG("BUF_OP: Blend g_resultYUV + srcPre -> g_resultYUV  \n");

        if (SUCCESS != ybfwGfxObjectBlend(&g_resultYUV, &srcPre, &g_resultYUV, (1 << 31) | 0))
        {
            printf("blend unSuccessful\n");
        }
        g_resultYUV.pbuf = g_resultYUVBuf;
        g_resultYUV.roiX = g_resultYUV.roiY = 0;
        g_resultYUV.bufW = YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH);
        g_resultYUV.bufH = YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT);
        g_resultYUV.roiW = YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH);
        g_resultYUV.roiH = YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT);
        g_resultYUV.paplaneBuf = NULL;
        g_resultYUV.fmt = YBFW_GFX_FMT_YUV422;
        DBG("g_resultYUV ROI (%d,%d : %d,%d) WxH %dx%d FMT %d\n",
                g_resultYUV.roiX,g_resultYUV.roiY,g_resultYUV.roiW,g_resultYUV.roiH,g_resultYUV.bufW,g_resultYUV.bufH,g_resultYUV.fmt,g_resultYUV.pbuf ? "!= NULL":"= NULL");

        DBG("BUF_OP: Blend g_resultYUV + g_osdLayerScale_gfxObjDst -> g_resultYUV  \n");

        if (SUCCESS  != ybfwGfxObjectBlend(&g_resultYUV, &g_osdLayerScale_gfxObjDst, &g_resultYUV,(1 << 31) | 0))
        {
            printf("blend unSuccessful\n");
        }
        DBG("trace\n");
        DBG("BUF_OP: CP  g_resultYUV  ->  pyuvInfo->pframe.pbuf \n");
        appYuvImgcpy(pyuvInfo->pframe.pbuf, g_resultYUV.pbuf, pyuvInfo->pframe.width,pyuvInfo->pframe.height, YBFW_IMG_FMT_YUV422_NV16);

    }
//    else
#endif
    /*This flag bit is synchronized with the osdBlend threaad*/
    ybfwOsEventFlagsSet(&osd_evt, 1, YBFW_TX_OR);
   // DBG("trace\n");
}

/* open yuv callback function */

void osdPvVideoPvYuvOn(UINT32 yuvId)
{

    DBG("trace\n");
    UINT32 ybfwmode, ret = FAIL;
    ybfwModePreviewYuvCustCfg_t modePvcfg;
    memset(&modePvcfg, 0, sizeof(modePvcfg));
    ybfwModeGet(&ybfwmode);
    ret = ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_GET, ybfwmode, (1<<yuvId),&modePvcfg);
    if (ret == SUCCESS)
    {
        if (modePvcfg.yuvEn & YBFW_MODE_PV_CFG_URGENT_CB)
        {
            printf("This yuv %x has been set to output yuv to URGENT_CB \n",(1<<yuvId));
        }
        else
        {
            printf("This yuv %x has not been set to output yuv to URGENT_CB  \n", (1<<yuvId));
            printf("start after set cb and enable cdsp yuv to output to URGENT_CB \n");
            modePvcfg.yuvEn |= YBFW_MODE_PV_CFG_URGENT_CB;

            ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, ybfwmode, (1<<yuvId), &modePvcfg);
            ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_RUNTIME_UPD, ybfwmode,YBFW_MODE_PV_CFG_CDSP_YUV);
        }
    }
    DBG("trace\n");
}

/*  close yuv callback function  */

void osdPvVideoPvYuvOff(UINT32 yuvId)
{

    UINT32 ybfwmode, ret = FAIL;
    DBG("trace\n");
    ybfwModeGet(&ybfwmode);
    ybfwModePreviewYuvCustCfg_t modePvcfg;
    memset(&modePvcfg, 0, sizeof(modePvcfg));
    INFO("turn off cdsp to output yuv to urgentCB \n");

    ret = ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_GET, ybfwmode, (1<<yuvId), &modePvcfg);
    DBG("trace\n");
    if (ret == SUCCESS)
    {
        if (modePvcfg.yuvEn & YBFW_MODE_PV_CFG_URGENT_CB)
        {
            printf("This yuv %x has been set to output yuv to URGENT_CB \n",
                    (1<<yuvId));
            modePvcfg.yuvEn &= ~YBFW_MODE_PV_CFG_URGENT_CB;

            ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, ybfwmode, (1<<yuvId),
                    &modePvcfg);
            ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_RUNTIME_UPD, ybfwmode,
                    YBFW_MODE_PV_CFG_CDSP_YUV);
        }
        else
        {
            printf(
                    "This yuv %x has not been set to output yuv to URGENT_CB  \n",
                    yuvId);
        }
    }
    DBG("trace\n");
}

/* Register YUV callback function */

void osdPvVideoCbReg(UINT32 yuvid, UINT32 enCb)
{
    DBG("trace\n");
    UINT32 ybfwmode;
    ybfwModeGet(&ybfwmode);
    ybfwPrevUrgentCallback_t cb =
    { .ctrl = 0, .interval = 1, .fp = osdvideourgentCB, };
    if (!enCb)
    {
        cb.fp = NULL;
    }
    if (ybfwmode == YBFW_MODE_STILL_PREVIEW)
        ybfwPreviewUrgentCallbackSet(yuvid, &cb);
    else
        ybfwVideoUrgentCallbackSet(yuvid, &cb);
}

/**
 * start OSD Blend Thread
 * @return 0  when OK
 */
UINT32 osdBlendThreadStart(){

    if (thrdOsd == NULL){
//        g_pipScaleBuf = (UINT8*) ybfwYuvBufferAlloc(YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH),YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT));
        g_resultYUVBuf = (UINT8*) ybfwYuvBufferAlloc(YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH),YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT));
        g_osdLayerScaleBuf = (UINT8*) ybfwYuvBufferAlloc(YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH),YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT));  //OSD Layer
//        g_pipVideoBuf = (UINT8*) ybfwYuvBufferAlloc(YUV_BUF_W_ALIGN(DISPLAY_PV_WIDTH),YUV_BUF_H_ALIGN(DISPLAY_PV_HEIGHT));

        ybfwOsEventFlagsCreate(&osd_evt, "osd_event");
        thrdOsd = ybfwOsThreadCreate((char*) "osdBlendThread", osdBlend, 0, 20,
                        0, 0, YBFW_TX_AUTO_START);
        HOST_ASSERT(thrdOsd);
    }else{
        INFO ("Thread osdBlend thread is already started\n");
        return 1;
    }
    return 0;
}

/**
 * start OSD Blend Thread
 * @return 0  when OK
 */
UINT32 osdDBlendThreadStop(){

    if (thrdOsd != NULL){
        ybfwOsThreadDelete(thrdOsd);
        thrdOsd = NULL ;
        ybfwOsEventFlagsDelete(&osd_evt);
        osd_evt = 0;

//        ybfwYuvBufferFree(g_pipScaleBuf);
        ybfwYuvBufferFree(g_resultYUVBuf);
        ybfwYuvBufferFree(g_osdLayerScaleBuf);
//        ybfwYuvBufferFree(g_pipVideoBuf);
    }else{
        INFO ("Thread osdBlend thread is already stopped\n");
       return 1;
    }
}

/* start to record with OSD*/
UINT32 osdRecStart(ybfwMediaRecCfg_t *pRecCfg)
{

    DBG("trace\n");

    osdBlendThreadStart();
    DBG("trace\n");
    //set callback
    osdPvVideoCbReg(YBFW_MODE_PREV_SENSOR_0_YUV_0, 1);


    DBG("trace\n");
    return baseRecStart(pRecCfg);
}


/*  end of recording */
void  osdPreviewRecStop(UINT32 handle)
{
    DBG("trace\n");
    baseRecStop(handle);
    if (handle != 0){
        DBG("trace\n");
        osdDBlendThreadStop();
        DBG("trace\n");
    }
    DBG("trace\n");
}
