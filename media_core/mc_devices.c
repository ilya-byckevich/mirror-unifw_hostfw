/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_ID "MC05"

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_media_api.h"
#include "ykn_bfw_api/ybfw_aud_api.h"
#include "ykn_bfw_api/ybfw_disp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_errors.h"
#include "ykn_bfw_api/ybfw_sensor_api.h"
#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"



#include "app_common.h"

#include "mc_devices_sencfgs.h"
#include "mc_context.h"
#include "mc_common.h"
#include "mc_devices.h"


enum{
    SENS_IDITEM_RAW,
    SENS_IDITEM_YUV,
    SENS_IDITEM_max,
};


static UINT32 sedIdItems[YBFW_SEN_ID_MAX][SENS_IDITEM_max] = {
    [YBFW_SEN_ID_0]={YBFW_MODE_PREV_SENSOR_0_RAW, YBFW_MODE_PREV_SENSOR_0_YUV_0,},
    [YBFW_SEN_ID_1]={YBFW_MODE_PREV_SENSOR_1_RAW, YBFW_MODE_PREV_SENSOR_1_YUV_0,},
    [YBFW_SEN_ID_2]={YBFW_MODE_PREV_SENSOR_2_RAW, YBFW_MODE_PREV_SENSOR_2_YUV_0,},
    [YBFW_SEN_ID_3]={YBFW_MODE_PREV_SENSOR_3_RAW, YBFW_MODE_PREV_SENSOR_3_YUV_0,},
};

static UINT32 sedIdYUVs[YBFW_SEN_ID_MAX][YBFW_MODE_PREV_SENSOR_YUV_COUNT] = {
    [YBFW_SEN_ID_0]={YBFW_MODE_PREV_SENSOR_0_YUV_0,YBFW_MODE_PREV_SENSOR_0_YUV_1,
                     YBFW_MODE_PREV_SENSOR_0_YUV_2,YBFW_MODE_PREV_SENSOR_0_YUV_3
    },
    [YBFW_SEN_ID_0]={YBFW_MODE_PREV_SENSOR_1_YUV_0,YBFW_MODE_PREV_SENSOR_1_YUV_1,
                     YBFW_MODE_PREV_SENSOR_1_YUV_2,YBFW_MODE_PREV_SENSOR_1_YUV_3
    },
    [YBFW_SEN_ID_0]={YBFW_MODE_PREV_SENSOR_2_YUV_0,YBFW_MODE_PREV_SENSOR_2_YUV_1,
                    YBFW_MODE_PREV_SENSOR_2_YUV_2,YBFW_MODE_PREV_SENSOR_2_YUV_3
    },
    [YBFW_SEN_ID_0]={YBFW_MODE_PREV_SENSOR_3_YUV_0,YBFW_MODE_PREV_SENSOR_3_YUV_1,
                    YBFW_MODE_PREV_SENSOR_3_YUV_2,YBFW_MODE_PREV_SENSOR_3_YUV_3
    },
};

typedef struct {
    const char* name;
    SINT32 width, height;
} resolution_t;

static UINT8 pvStarted = 0;


UINT8 isAppPvStreamStarted(){
    return pvStarted;
}



//execute before sleep mode
static ErrCode_t sensorInit(ybfwSenId_t senId,  SensModeCfg  *senCfg){
    ErrCode_t err = ERR_SUCCESS;

    RETURN_IF_COND(senCfg == NULL,ERR_PARAM_INVALID);

    DBG("apply config mode 0x%X, WxH %dx%d",senCfg->smode, senCfg->w,senCfg->h);


    LOG_IF_ERROR(ybfwPreviewExpAgcSet(senId, senCfg->exp, senCfg->agc)); //TODO think obout AGC and EXP

    //set mode
    LOG_IF_ERROR(ybfwSensorModeCfgSet(senId ,YBFW_MODE_VIDEO_PREVIEW, YBFW_SENSOR_MODE_PREVIEW|senCfg->smode));

    /**sensor param**/
    ybfwCapabilityPreview_t senprevCap;
    LOG_IF_ERROR(ybfwModeCfgGet(YBFW_MODE_CFG_PV_CAP_GET, senId, &senprevCap, sizeof(senprevCap)));

    /**cdsp performance set**/
    if(senCfg->cdspfps){
        ybfwModePreviewCdspAttr_t cdspattr;
        LOG_IF_ERROR(ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_ATTR_GET, YBFW_MODE_VIDEO_PREVIEW, senId, &cdspattr ));
        cdspattr.pvcdspDoTargetFps = senCfg->cdspfps;
        LOG_IF_ERROR(ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_ATTR_CFG, YBFW_MODE_VIDEO_PREVIEW, senId, &cdspattr));
    }

    /**cdsp raw config**/
    ybfwModePreviewRawCustCfg_t modepvRawcfg={
        .rawBufNum  = 4,
        .rawBitMode =  YBFW_IMG_FMT_RAW_8BIT, /*YBFW_IMG_FMT_RAW_V50*/
        .rawWidth = 0,
        .rawHeight = 0,
#ifdef EXTEND_VERSION
        .rawToModule = YBFW_MODE_PV_CFG_URGENT_CB,
        .rawOpenPvEn = 1,
#endif
    };
    if (senprevCap.hsize >= 3840) {
        modepvRawcfg.rawWidth = 3840;
    }
    LOG_IF_ERROR(ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_RAW_CFG, YBFW_MODE_VIDEO_PREVIEW,sedIdItems[senId][SENS_IDITEM_RAW],&modepvRawcfg));

    ybfwModePreviewYuvCustCfg_t modePvcfg;
    memset(&modePvcfg, 0, sizeof(modePvcfg));
    LOG_IF_ERROR(ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, YBFW_MODE_VIDEO_PREVIEW, sedIdItems[senId][SENS_IDITEM_YUV], &modePvcfg));
    LOG_IF_ERROR(ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER));

    //Enable YUV
    modePvcfg.yuvEn |= YBFW_MODE_PV_CFG_STREAM_MEDIA ;
    modePvcfg.yuvEn |= YBFW_MODE_PV_CFG_DISP_CB;
    modePvcfg.yuv.width =   YUV_ROI_ALIGN_CDSP(senCfg->w);
    modePvcfg.yuv.height =  YUV_ROI_ALIGN_CDSP(senCfg->h);
    modePvcfg.yuv.roix = 0;
    modePvcfg.yuv.roiy = 0;
    modePvcfg.yuv.roiw = senCfg->w;
    modePvcfg.yuv.roih = senCfg->h;
    modePvcfg.fmt =  YBFW_IMG_FMT_YUV422_NV16;
    modePvcfg.bufNum = 4;
    DBG("YUV CFG ROI (%d,%d,%dx%d", modePvcfg.yuv.roix, modePvcfg.yuv.roiy,modePvcfg.yuv.roiw,modePvcfg.yuv.roih);
    LOG_IF_ERROR(ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, YBFW_MODE_VIDEO_PREVIEW, sedIdItems[senId][SENS_IDITEM_YUV], &modePvcfg));

    return err;
}



ErrCode_t mc_senInit(){
    ybfwSenId_t senId = CUR_SENSOR; //TODO sensor id change
    ErrCode_t err = ERR_SUCCESS;
    DBG("trace");

#if 0 //NAV return
    ResolutionInfo_t * resInfo = mcGetResolInfo(RK_TXT,CFG_SENSOR_0_RESOLUTION);
    RETURN_IF_COND(resInfo == NULL, ERR_PARAM_INVALID);
    DBG("trace");

    SensModeCfg * sensorConfig = getSensorConfig((UINT8*)CFG_SENSOR_0_TYPE,resInfo->resW,resInfo->resH,CFG_SENSOR_0_FPS);
    RETURN_IF_COND(sensorConfig == NULL, ERR_PARAM_INVALID);
    DBG("trace");

    RETURN_IF_ERROR(sensorInit(senId,sensorConfig));
#endif
    DBG("trace");
    return err;
}

ErrCode_t mc_senGetRawYuvId(ybfwSenId_t senId,UINT32 * rawId, UINT32 * yuvId){
    ErrCode_t err = ERR_SUCCESS;
    RETURN_IF_COND((senId < YBFW_SEN_ID_MIN)|| (senId >= YBFW_SEN_ID_MAX),ERR_PARAM_INVALID);
    if (rawId){
        *rawId = sedIdItems[senId][SENS_IDITEM_RAW];
    }
    if (yuvId){
        *yuvId = sedIdItems[senId][SENS_IDITEM_RAW];
    }
    return err;
}

ErrCode_t mc_senRelease(){
    ErrCode_t err = ERR_SUCCESS;
    return err;
}



//----------------------------------------------
typedef enum {
  DISP_TG_LCD = 0,
  DISP_TG_HDMI,
  DISP_TG_TVOUT,
  DISP_TG_max,
}DispTargetType_t;


ErrCode_t mc_lcdInit(){
    return 0;
}

ErrCode_t mc_lcdRelease(){
    return 0;
}


static struct{
    UINT32 w;
    UINT32 h;
    UINT32 fps;
    ybfwEdidVideoInfoCode_t id;
} _hdmi_tbl[]={
        {.w=1920,.h=1080,.fps = 60,.id = YBFW_EDID_1920X1080P_60HZ_16T9},
        {.w=1920,.h=1080,.fps = 30,.id = YBFW_EDID_1920X1080P_30HZ_16T9},
        {.w=3840,.h=2160,.fps = 30,.id = YBFW_EDID_3840X2160P_30HZ_16T9},
        {.w=1280,.h=720,.fps = 60,.id = YBFW_EDID_1280X720P_60HZ_16T9},
        {.w=640, .h=480,.fps = 60,.id = YBFW_EDID_640X480P_60HZ_4T3},
        /*{0, 0,0,0},*/
};

ybfwEdidVideoInfoCode_t appHdmiIDGet(SINT32 w,SINT32 h,SINT32 fps){
    SINT32 i=0, last_id;
    SINT32 c=sizeof(_hdmi_tbl)/sizeof(*_hdmi_tbl);

    for(i=0;i<c;i++){
        if((!w || _hdmi_tbl[i].w==w) &&
            (!h ||_hdmi_tbl[i].h==h) &&
            (!fps || _hdmi_tbl[i].fps==fps)
            )
            last_id = i;
            return _hdmi_tbl[i].id;
    }
    ERR("%s:not match hdmi para:w=%d,h=%d,fps=%d\n",__FUNCTION__,w,h,fps);
    INFO("use last id mode %d\n",last_id);

    return _hdmi_tbl[last_id].id;
}



typedef struct {
    ybfwEdidVideoInfoCode_t  edid;
    ybfwDispChnlId_t  dispCh;
    ybfwPipCh_t pipCh;
    UINT32  pipW;
    UINT32  pipH;
}dispInfo_t;

//TODO set Actual  settings for displays
dispInfo_t dispInfoDflt[DISP_TG_max]={
    [DISP_TG_LCD] =  {.edid = YBFW_EDID_1920X1080P_60HZ_16T9,.dispCh = YBFW_DISP_CHNL_0, .pipCh= YBFW_PIP_CH_0,.pipW = 1920, .pipH = 1080},
    [DISP_TG_HDMI] =  {.edid = YBFW_EDID_1920X1080P_60HZ_16T9,.dispCh = YBFW_DISP_CHNL_1, .pipCh= YBFW_PIP_CH_0,.pipW = 1920, .pipH = 1080},
    [DISP_TG_TVOUT] = {.edid = YBFW_EDID_1920X1080P_60HZ_16T9,.dispCh = YBFW_DISP_CHNL_1, .pipCh= YBFW_PIP_CH_0,.pipW = 1920, .pipH = 1080},

};


ErrCode_t mc_hdmiInit(){
    ErrCode_t err = ERR_SUCCESS;
    // SINT32 id = appHdmiIDGet(w,h,fps); //TODO move up
    DBG("trace");
    dispInfo_t dispInfo = dispInfoDflt[DISP_TG_HDMI];

    DBG("uses edid %d dispCh %d pipCh %d pipW %d  pipH %d",dispInfo.edid,dispInfo.dispCh,dispInfo.pipCh,dispInfo.pipW,dispInfo.pipH);
    /* current V50 basefw seems have some problem, this cmd may need run twice for HDMI TV */
    LOG_IF_ERROR(ybfwDispPowerOff(YBFW_DISP_CHNL_0, 0)); /* V50 ES1 cannot support dual panel well */
    DBG("trace");
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 500);
    DBG("trace");
    ybfwDispPowerOff(dispInfo.dispCh, 0);
    DBG("trace");
    LOG_IF_ERROR(ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 500));
    DBG("trace");
    LOG_IF_ERROR(ybfwDispCfgSet(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, 0, 0));  /* RGB444. */
    LOG_IF_ERROR(ybfwDispCfgSet(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, 1, 0));  /* color depth not indicated. */
    LOG_IF_ERROR(ybfwDispCfgSet(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, 2, 0));  /* default quantization range. */
    LOG_IF_ERROR(ybfwDispCfgSet(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, 3, 0));  /* disable DVI mode. */
    LOG_IF_ERROR(ybfwDispCfgSet(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, 5, 0));  /* disable SCDC function. */
    LOG_IF_ERROR(ybfwDispPowerOn(dispInfo.dispCh, YBFW_DISP_CHNL_1_HDMI_TV, dispInfo.edid, 0));
    DBG("trace");
#ifdef EXTEND_VERSION
    /* PIP Layer */
    LOG_IF_ERROR(ybfwPipTerm(dispInfo.pipCh));
    DBG("trace");
    LOG_IF_ERROR(ybfwPipInitCfgSet(dispInfo.pipCh, YBFW_PIP_INIT_MAIN_FRAME_SIZE, dispInfo.pipW, dispInfo.pipH, 0, 0));
    LOG_IF_ERROR(ybfwPipInitCfgSet(dispInfo.pipCh, PIP_INIT_CFG_TOGGFLE_FRAME_NUM, 4, 0, 0,0));
    DBG("trace");
    LOG_IF_ERROR(ybfwPipInit(dispInfo.pipCh));
    LOG_IF_ERROR(ybfwDispAttrSet(dispInfo.dispCh, YBFW_DISP_IMG_ACTIVE, 1));
    DBG("trace");
#endif
    return err;
}

ErrCode_t mc_hdmiRelease(){
    ErrCode_t err = ERR_SUCCESS;
    dispInfo_t dispInfo = dispInfoDflt[DISP_TG_HDMI];

    ybfwDispAttrSet(dispInfo.dispCh, YBFW_DISP_IMG_ACTIVE, 0);
    ybfwDispPowerOff(dispInfo.dispCh, 0);
    ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 500);
    return err;
}

ErrCode_t mc_audInit(){
    ErrCode_t err = ERR_SUCCESS;
    return err;
}

ErrCode_t mc_audRelease(){
    ErrCode_t err = ERR_SUCCESS;
    return err;
}

ErrCode_t mc_tvoutInit(){
    ErrCode_t err = ERR_SUCCESS;
    return err;
}

ErrCode_t mc_tvoutRelease(){
    ErrCode_t err = ERR_SUCCESS;
    return err;
}
