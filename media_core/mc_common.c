/*
 * mc_common.c
 *
 *  Created on: Mar 19, 2020
 *      Author: naydinav
 */

#include <stdarg.h>

#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "mc_common.h"



#define FILE_ID "MC02"
#include "ykn_bfw_api/ybfw_dbg_api.h"


static ResolutionInfo_t resInfo[REST_max]={
    [REST_QVGA] ={.name = (UINT8 *)"QVGA",.resTxt = (UINT8 *)"320;240",.resW = 320,.resH = 240,.ratioW = 4, .ratioH = 3},
    [REST_VGA]  ={.name = (UINT8 *)"VGA", .resTxt = (UINT8 *)"640;480",.resW = 640,.resH = 480,.ratioW = 4,.ratioH = 3},
    [REST_SVGA] ={.name = (UINT8 *)"SVGA",.resTxt = (UINT8 *)"800;600",.resW = 800,.resH = 600,.ratioW = 4,.ratioH = 3},
    [REST_XGA]  ={.name = (UINT8 *)"XGA", .resTxt = (UINT8 *)"1024;768",.resW = 1024,.resH = 768,.ratioW = 4,.ratioH = 3},
    [REST_720P] ={.name = (UINT8 *)"720p", .resTxt = (UINT8 *)"1280;720",.resW = 1280,.resH = 720,.ratioW = 16,.ratioH = 9},
    [REST_900P] ={.name = (UINT8 *)"900p", .resTxt = (UINT8 *)"1600;900",.resW = 1600,.resH = 900,.ratioW = 16,.ratioH = 9},
    [REST_1080P]={.name = (UINT8 *)"1080p",.resTxt = (UINT8 *)"1920;1080",.resW = 1920,.resH = 1080,.ratioW = 16,.ratioH = 9},
    [REST_2160P]={.name = (UINT8 *)"2160p", .resTxt =(UINT8 *)"3840;2160",.resW = 3840,.resH = 2160,.ratioW = 16,.ratioH = 9},
};




ResolutionInfo_t * mcGetResolInfo(ResolutionKey_t key,...){

    ResolutionInfo_t * res =  NULL;
    va_list args;
    va_start(args, key);
    ResolutionTypes_t i;
    DBG("trace");
    switch (key){
        case RK_NAME:
        {
            // param1: string name
            UINT8 * tmp = va_arg(args, UINT8 *);
            if (tmp){
                for (i = REST_min; i < REST_max; i++){
                    if (isEqualStrings(tmp,resInfo[i].name, CCS_NO)){
                        res = &resInfo[i];
                        break;
                    }
                }
            }
            break;
        }
        case RK_TXT:
        {
            //param1: string resolution
            UINT8 * tmp = va_arg(args, UINT8 *);
            DBG("trace");
            if (tmp){
                for (i = REST_min; i < REST_max; i++){
                    DBG(".resTxt %s  tmp %s",resInfo[i].resTxt,tmp);
                    if (strstr(tmp,resInfo[i].resTxt)!= NULL){
                        res = &resInfo[i];
                        DBG("trace");
                        break;
                    }
                }
            }else{
                ERR("tmp is null");
            }
            break;
        }
        case RK_TYPE:
        {
            //param1: ResolutionTypes_t
            ResolutionTypes_t type = (ResolutionTypes_t) va_arg(args, ResolutionTypes_t);
            if ((type >= REST_min) && (type < REST_max)){
                res = &resInfo[type];
            }
            break;
        }
        case RK_WxH:
        {
            //param1: UINT32 width, param2: UINT32 height
            UINT32 w = va_arg(args, UINT32);
            UINT32 h = va_arg(args, UINT32);
            for (i = REST_min; i < REST_max; i++){
                if ((resInfo[i].resW == w) && (resInfo[i].resH == h)){
                    res = &resInfo[i];
                    break;
                }
            };
            break;
        }

        case RK_FIND:
        {
            //param1 : UINT32 ratioW, param2: UINT32 ratioH, param3: UINT32 known_side (0 - W, 1-H), param4 : UINT32 side_val);
            UINT32 ratioW = va_arg(args, UINT32);
            UINT32 ratioH = va_arg(args, UINT32);
            UINT32 known_side = va_arg(args, UINT32);
            UINT32 side_val = va_arg(args, UINT32);
            ResolutionInfo_t * res_first = NULL;
            DBG("ratioW %d ratioH %d side %d val %d",ratioW,ratioH,known_side,side_val);
            for (i = REST_min; i < REST_max; i++){
                if ((resInfo[i].ratioW == ratioW) && (resInfo[i].ratioH == ratioH)){
                    UINT32 check_val = (known_side == 0) ? resInfo[i].resW : resInfo[i].resH;
                    DBG(".resTxt %s  sude_val %d vs cur_val %d ",resInfo[i].resTxt,check_val,side_val);
                    if (side_val >= check_val){
                        DBG("Select .resTxt %s ",resInfo[i].resTxt);
                        res = &resInfo[i];
                    }else if (res_first == NULL){ //get first actual
                        res_first = &resInfo[i];
                    }
                }
            }
            if (!res){
                res = res_first;
            }
            break;
        }
        default:
            break;
    }

    va_end(args);
    if (res){
        DBG("res %s %dx%d",res->name,res->resW,res->resH);
    }
    return res;
}




UINT32 mappingBuffers(ybfwGfxObj_t * p_srcObj,ybfwGfxObj_t * p_dstObj, UINT8 stretch, UINT8 bg_color, UINT8 interpolation){

    if (!p_srcObj || !p_dstObj){
        DBG("buffs is null");
        return 1;
    }
    ybfwGfxMapProjectParam_t  proPrm;
    memset(&proPrm, 0, sizeof(proPrm));
    proPrm.mapAttr.itpAlg =  interpolation;
    proPrm.mapAttr.bgClrY = bg_color;
    proPrm.mapAttr.bgClrU = 0x80;
    proPrm.mapAttr.bgClrV = 0x80;
    proPrm.mapAttr.waitMode = YBFW_GFX_MAP_MODE_DIRECT_WAIT;
//#if 1 //always stretch
    proPrm.vec.src[0].x = p_srcObj->roiX;
    proPrm.vec.src[0].y = p_srcObj->roiY;
    proPrm.vec.src[1].x = p_srcObj->roiX+p_srcObj->roiW-1;
    proPrm.vec.src[1].y = p_srcObj->roiY;
    proPrm.vec.src[2].x = p_srcObj->roiX+p_srcObj->roiW-1;
    proPrm.vec.src[2].y = p_srcObj->roiY+p_srcObj->roiH-1;
    proPrm.vec.src[3].x = p_srcObj->roiX;
    proPrm.vec.src[3].y = p_srcObj->roiY+p_srcObj->roiH-1;

    proPrm.vec.dst[0].x = p_dstObj->roiX;
    proPrm.vec.dst[0].y = p_dstObj->roiY;
    proPrm.vec.dst[1].x = p_dstObj->roiX+p_dstObj->roiW-1;
    proPrm.vec.dst[1].y = p_dstObj->roiY;
    proPrm.vec.dst[2].x = p_dstObj->roiX+p_dstObj->roiW-1;
    proPrm.vec.dst[2].y = p_dstObj->roiY+p_dstObj->roiH-1;
    proPrm.vec.dst[3].x = p_dstObj->roiX;
    proPrm.vec.dst[3].y = p_dstObj->roiY+p_dstObj->roiH-1;

//#else
//    proPrm.vec.src[0].x = p_srcObj->roiX;
//    proPrm.vec.src[0].y = p_srcObj->roiY;
//    proPrm.vec.src[1].x = p_srcObj->roiX+p_srcObj->roiW-1;
//    proPrm.vec.src[1].y = p_srcObj->roiY;
//    proPrm.vec.src[2].x = p_srcObj->roiX+p_srcObj->roiW-1;
//    proPrm.vec.src[2].y = p_srcObj->roiY+p_srcObj->roiH-1;
//    proPrm.vec.src[3].x = p_srcObj->roiX;
//    proPrm.vec.src[3].y = p_srcObj->roiY+p_srcObj->roiH-1;
//
//    UINT32 srcWTodstH = p_srcObj->roiW * p_dstObj->roiH;
//    UINT32 srcHTodstW = p_srcObj->roiH * p_dstObj->roiW;
//    UINT32 imgRoiX, imgRoiY, imgRoiW, imgRoiH;
//    if( stretch == 0){
//        if ( srcWTodstH > srcHTodstW ) {
//            imgRoiH = srcHTodstW / p_srcObj->roiW;
//            imgRoiY = ( p_dstObj->roiH - imgRoiH ) / 2 + p_dstObj->roiY;
//
//            imgRoiW = p_dstObj->roiW;
//            imgRoiX = p_dstObj->roiX;
//        }
//        else if ( srcWTodstH < srcHTodstW ) {
//            imgRoiW = srcWTodstH / p_srcObj->roiH;
//            imgRoiX = ( p_dstObj->roiW - imgRoiW ) / 2 + p_dstObj->roiX;
//
//            imgRoiH = p_dstObj->roiH;
//            imgRoiY = p_dstObj->roiY;
//        }
//        else {
//            imgRoiW = p_dstObj->roiW;
//            imgRoiX = p_dstObj->roiX;
//
//            imgRoiH = p_dstObj->roiH;
//            imgRoiY = p_dstObj->roiY;
//        }
//    }else{
//        imgRoiW = p_dstObj->roiW;
//        imgRoiX = p_dstObj->roiX;
//
//        imgRoiH = p_dstObj->roiH;
//        imgRoiY = p_dstObj->roiY;
//    }
//
//    proPrm.vec.dst[0].x = imgRoiX;
//    proPrm.vec.dst[0].y = imgRoiY;
//    proPrm.vec.dst[1].x = imgRoiX+imgRoiW-1;
//    proPrm.vec.dst[1].y = imgRoiY;
//    proPrm.vec.dst[2].x = imgRoiX+imgRoiW-1;
//    proPrm.vec.dst[2].y = imgRoiY+imgRoiH-1;
//    proPrm.vec.dst[3].x = imgRoiX;
//    proPrm.vec.dst[3].y = imgRoiY+imgRoiH-1;
//#endif

    ybfwGfxObjectMapping(p_srcObj, p_dstObj, YBFW_GFX_MAPPING_ID_PROJECT, (UINT32)&proPrm);
    return 0;
}



#if 0

void main(){

     ResolutionInfo_t * info = NULL;

     info = mcGetResolInfo(RK_NAME,"XGA");
     if (info){
         printf("RK_NAME name '%s' , text = '%s' , WxH %dx%d , ratio %d:%d \n",
                 info->name,info->resTxt,info->resW,info->resH,info->ratioW,info->ratioH);
     }

     info = mcGetResolInfo(RK_TXT,"800;600");
     if (info){
         printf("RK_TXT name '%s' , text = '%s' , WxH %dx%d , ratio %d:%d \n",
                 info->name,info->resTxt,info->resW,info->resH,info->ratioW,info->ratioH);
     }

     info = mcGetResolInfo(RK_TYPE,REST_2160P);
     if (info){
         printf("RK_TYPE name '%s' , text = '%s' , WxH %dx%d , ratio %d:%d \n",
                 info->name,info->resTxt,info->resW,info->resH,info->ratioW,info->ratioH);
     }

     info = mcGetResolInfo(RK_WxH,320,240);
     if (info){
         printf("RK_WxH name '%s' , text = '%s' , WxH %dx%d , ratio %d:%d \n",
                 info->name,info->resTxt,info->resW,info->resH,info->ratioW,info->ratioH);
     }

}

#endif

