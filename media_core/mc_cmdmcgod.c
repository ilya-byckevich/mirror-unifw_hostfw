/**
 * @file mc_cmdmcgod.c
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/


#define FILE_ID "MC12"

#include "media_core_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "common.h"
#include "app_common.h"

static UINT8 mccmds_godmode_allow = 1; //TODO NAV set to 0

ErrCode_t cmdMcGodModeInit(){
    //NAV TODO check
    mccmds_godmode_allow  = 1;
    return ERR_CMD_SUCCESS;
}



ErrCode_t cmdMcGod(int argc,char **argv,UINT32* v){

    if (! mccmds_godmode_allow){
        return ERR_CMD_NOT_FOUND;
    }

    return ERR_CMD_NOT_FOUND;
}

ErrCode_t cmdMcGodHelp(){

    return ERR_CMD_SUCCESS;
}
