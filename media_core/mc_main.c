/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdarg.h>

#define FILE_ID "MC07"

#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"

#include "media_core_api.h"
#include "mc_context.h"
#include "mc_osd.h"
#include "mc_thread_scale.h"
#include "mc_devices.h"



static UINT8 ctxInited = 0;

static mcContext_t * pctx = NULL;



extern void cmdAEOn();
extern void cmdAWBInit();
extern void cmdAWBOn();
static ErrCode_t coreStart(UINT32 mask){
    ErrCode_t err = ERR_SUCCESS;



    LOG_IF_ERROR(mc_senInit());

    cmdAEOn();
    cmdAWBInit();
    cmdAWBOn();
    err = mcThreadScaleStart();
    DBG("trace");


    LOG_IF_ERROR(mc_hdmiInit());
    err = mc_osdStart();  //now after hdmi
    DBG("trace");

    ctx_Dump(0);
    //Start preview mode show on display
    ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
    ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);


    return err;
}

static ErrCode_t coreStop(UINT32 mask){
    ErrCode_t err = ERR_SUCCESS;
    ybfwModeSet( YBFW_MODE_STANDBY );
    ybfwModeWait( YBFW_MODE_STANDBY );
    err = mcThreadScaleStop();
    if (err == ERR_SUCCESS){
        err = mc_osdStop();
    }
    return err;
}



ErrCode_t mcCoreCreate(){
    ErrCode_t err = ERR_SUCCESS;
    if (!ctxInited){
        err = ctxInit(NULL,NULL,NULL);
#if 1
        ctx_Dump(0);
#endif
        pctx = ctx_getmcContext();
        if (err == ERR_SUCCESS){
            ctxInited = 1;
        }
    }
    return err;
}
ErrCode_t mcCoreDestroy(){
    ErrCode_t err = ERR_SUCCESS;
    if (ctxInited){
        err = ctxDestroy();
        ctxInited=0;
        pctx = NULL;
    }
    return err;
}

ErrCode_t mcCoreCfgSet(mcCtrl_t ctrl, ...){
    ErrCode_t err = ERR_SUCCESS;
    ybfwSenId_t sen_id;
    ybfwDispChnlId_t disp_id;
    SwitchState state;
    McDeviceCfg dev_cfg;
    McScaleCfg scale_cfg;
    WinTypes win_type;
    SINT32 val[AXIS_TOT];
    if (!ctxInited){
        return ERR_FAIL;
    }

    va_list args;
    va_start(args, ctrl);
    switch (ctrl) {
        case MCCFG_CORE_STATE:
        {
            state = va_arg(args, SwitchState);
            RETURN_IF_COND((state < SWITCH_START ) || (state >= SWITCH_TOT ),ERR_PARAM_INVALID);
            //err =  coreStart(0);
        }
            break;
        case MCCFG_DEV_SENS_STATE:
        {
            sen_id = va_arg(args, ybfwSenId_t);
            state = va_arg(args, SwitchState);

            RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
            RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND((state < SWITCH_START ) || (state >= SWITCH_TOT ),ERR_PARAM_INVALID);

            //pctx->sensState[sen_id] = state;

        }
            break;
        case MCCFG_DEV_DISPCH_STATE:
        {
            disp_id = va_arg(args, ybfwDispChnlId_t);
            state = va_arg(args, SwitchState);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND((state < SWITCH_START ) || (state >= SWITCH_TOT ),ERR_PARAM_INVALID);

            //pctx->dispState[disp_id] = state;

        }
            break;
        case MCCFG_CFG_DEV_SENS:
        {
            sen_id =  va_arg(args, ybfwSenId_t);
            dev_cfg = va_arg(args, McDeviceCfg);

            RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
            RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);

            //memcpy(&pctx->sensCfg[sen_id],&dev_cfg, sizeof(McDeviceCfg));
        }
            break;
        case MCCFG_CFG_DEV_DISPCH:
        {
            disp_id = va_arg(args, ybfwDispChnlId_t);
            dev_cfg = va_arg(args, McDeviceCfg);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);

            //memcpy(&pctx->dispCfg[disp_id],&dev_cfg, sizeof(McDeviceCfg));
        }
            break;
        case MCCFG_CFG_SCALE:
        {
            scale_cfg = va_arg(args, McScaleCfg);

            //memcpy(&pctx->scaleCfg,&scale_cfg, sizeof(McScaleCfg));
        }
            break;
        case MCCFG_CFG_BIAS:
        {
            McBiasType bias_type= va_arg(args, McBiasType);

            RETURN_IF_COND((bias_type < MCBIAS_TYPE_START ) || (sen_id >= MCBIAS_TYPE_TOT ),ERR_PARAM_INVALID);

            switch (bias_type) {
                case MCBIAS_TYPE_SEN:
                    sen_id =  va_arg(args, ybfwSenId_t);
                    val = va_arg(args, SINT32[AXIS_TOT] );

                    RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
                    RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);
                    RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);

                    break;
                case MCBIAS_TYPE_DISPCH:
                    disp_id = va_arg(args, ybfwDispChnlId_t);
                    val = va_arg(args, SINT32[AXIS_TOT] );

                    RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
                    RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);
                    RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);

                    break;
                default:
                    break;
            }
        }
            break;
        case MCCFG_CFG_WINDOW:
        {
#if 0 //NAV TODO return
            disp_id = va_arg(args, ybfwSenId_t);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);

            win_type = va_arg(args, WinTypes);
            RETURN_IF_COND((win_type < WT_START) ||(win_type >= WT_TOT),ERR_PARAM_INVALID);

            val = va_arg(args, SINT32[AXIS_TOT]  );
            RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);
#endif
        }
            break;
        default:
            err =  ERR_PARAM_INVALID;
            break;
    }
    va_end(args);
    return err;
}

ErrCode_t mcCoreCfgGet(mcCtrl_t ctrl, ...){
    ErrCode_t err = ERR_SUCCESS;
    ybfwSenId_t sen_id;
    ybfwDispChnlId_t disp_id;
    SwitchState * state;
    McDeviceCfg * dev_cfg;
    McScaleCfg * scale_cfg;
    WinTypes win_type;
    SINT32 val[AXIS_TOT];
    if (!ctxInited){
        return ERR_FAIL;
    }

    va_list args;
    va_start(args, ctrl);
    switch (ctrl) {
        case MCCFG_CORE_STATE:
        {
            state = va_arg(args, SwitchState *);
            RETURN_IF_COND((state < SWITCH_START ) || (state >= SWITCH_TOT ),ERR_PARAM_INVALID);
            RETURN_IF_COND(state == NULL ,ERR_PARAM_INVALID);
            //err =  coreStart(0);
        }
            break;
        case MCCFG_DEV_SENS_STATE:
        {
            sen_id = va_arg(args, ybfwSenId_t);
            state = va_arg(args, SwitchState *);

            RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
            RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND(state == NULL ,ERR_PARAM_INVALID);

            *state = pctx->sensState[sen_id];
        }
            break;
        case MCCFG_DEV_DISPCH_STATE:
        {
            disp_id = va_arg(args, ybfwDispChnlId_t);
            state = va_arg(args, SwitchState *);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND(state == NULL ,ERR_PARAM_INVALID);

            *state = pctx->dispState[sen_id];

        }
            break;
        case MCCFG_CFG_DEV_SENS:
        {
            sen_id =  va_arg(args, ybfwSenId_t);
            dev_cfg = va_arg(args, McDeviceCfg *);

            RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
            RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND(dev_cfg == NULL ,ERR_PARAM_INVALID);
            memcpy(dev_cfg, &pctx->sensCfg[sen_id],sizeof(McDeviceCfg));
        }
            break;
        case MCCFG_CFG_DEV_DISPCH:
        {
            disp_id = va_arg(args, ybfwDispChnlId_t);
            dev_cfg = va_arg(args, McDeviceCfg *);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);
            RETURN_IF_COND(dev_cfg == NULL ,ERR_PARAM_INVALID);
            memcpy(dev_cfg,&pctx->dispCfg[disp_id],sizeof(McDeviceCfg));
        }
            break;
        case MCCFG_CFG_SCALE:
        {
            scale_cfg = va_arg(args, McScaleCfg * );
            RETURN_IF_COND(scale_cfg == NULL ,ERR_PARAM_INVALID);
            memcpy(scale_cfg,&pctx->scaleCfg, sizeof(McScaleCfg));
        }
            break;
        case MCCFG_CFG_BIAS:
        {
            McBiasType bias_type= va_arg(args, McBiasType);

            RETURN_IF_COND((bias_type < MCBIAS_TYPE_START ) || (sen_id >= MCBIAS_TYPE_TOT ),ERR_PARAM_INVALID);

            switch (bias_type) {
                case MCBIAS_TYPE_SEN:
                    sen_id =  va_arg(args, ybfwSenId_t);
                    val = va_arg(args, SINT32[AXIS_TOT] );
                    RETURN_IF_COND((sen_id < YBFW_SEN_ID_MIN ) || (sen_id >= YBFW_SEN_ID_MAX ),ERR_PARAM_INVALID);
                    RETURN_IF_COND(sen_id >= CFG_HAL_SENS_COUNT,ERR_PARAM_INVALID);
                    RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);

                    memcpy(val,&pctx->senAdj[sen_id], sizeof(val));
                    break;
                case MCBIAS_TYPE_DISPCH:
                    disp_id = va_arg(args, ybfwDispChnlId_t);
                    val = va_arg(args, SINT32[AXIS_TOT] );
                    RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
                    RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);
                    RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);

                    memcpy(val,&pctx->dispAdj[disp_id], sizeof(val));
                    break;
                default:
                    break;
            }
        }
            break;
        case MCCFG_CFG_WINDOW:
        {
#if 0 //NAV TODO return
            disp_id = va_arg(args, ybfwSenId_t);

            RETURN_IF_COND(disp_id != YBFW_DISP_CHNL_NULL && ((disp_id < YBFW_DISP_CHNL_START ) || (disp_id >= YBFW_DISP_CHNL_TOT )) ,ERR_PARAM_INVALID);
            RETURN_IF_COND(disp_id >= CFG_HAL_DISP_COUNT,ERR_PARAM_INVALID);

            win_type = va_arg(args, WinTypes);
            RETURN_IF_COND((win_type < WT_START) ||(win_type >= WT_TOT),ERR_PARAM_INVALID);

            val = va_arg(args, SINT32[AXIS_TOT]  );
            RETURN_IF_COND(val == NULL,ERR_PARAM_INVALID);
#endif
        }
            break;
        default:
            err =  ERR_PARAM_INVALID;
            break;

    }
    va_end(args);
    return err;
}


ErrCode_t mcZoomWinSet(WinTypes wt){
    ErrCode_t err = ERR_SUCCESS;

    return err;
}

ErrCode_t mcZoomWinGet(WinTypes *wt){
    ErrCode_t err = ERR_SUCCESS;

    return err;
}
ErrCode_t mcZoomFactorSet(UINT32 factor){
    ErrCode_t err = ERR_SUCCESS;

    return err;
}
ErrCode_t mcZoomFactorGet(UINT32 *factor){
    ErrCode_t err = ERR_SUCCESS;

    return err;
}
ErrCode_t mcZoomInc(McZoomStepMode mode,McZoomDirection dir, UINT32 * factor){
    ErrCode_t err = ERR_SUCCESS;

    return err;
}
