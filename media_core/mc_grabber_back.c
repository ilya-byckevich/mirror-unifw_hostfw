
#define FILE_ID "MC05"

#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "mc_context.h"
#include "mc_devices.h"
#include "mc_common.h"
#include "autoconf.h"




static mcContext_t * pctx = NULL;


//#define ONLY_CAPTURE

//#define PIP_EXT_BUFF

#define BG_COLOR 0xFF
// Scaler buf
#ifdef ONE_THREAD
enum{
 BUF_YUV = 0,
 BUF_ZOOM,
 BUF_TOT,
};

static ybfwGfxObj_t g_ScalerBuf[CFG_HAL_SENS_COUNT][BUF_TOT];
#ifdef PIP_EXT_BUFF
static ybfwGfxObj_t g_ScalerBufPiP[CFG_HAL_SENS_COUNT][BUF_TOT];
#endif

#else
static ybfwGfxObj_t g_ScalerBuf[CFG_HAL_SENS_COUNT][SCALE_BUF_PER_SENSOR_COUNT];
#ifdef PIP_EXT_BUFF
static ybfwGfxObj_t g_ScalerBufPiP[CFG_HAL_SENS_COUNT][SCALE_BUF_PER_SENSOR_COUNT];
#endif
#endif

//Source and destination ROI of drawing images
static UINT8 sbAlign[CFG_HAL_SENS_COUNT]; //g_ScalerBufAlign
static UINT8 sbStrechWin[CFG_HAL_DISP_COUNT][WT_max] = {0}; //g_stretchWin

SINT32 disp_pip_pages[YBFW_PIP_CH_MAX][DPP_MAX]={
 [YBFW_PIP_CH_0] = {YBFW_PAGE_PIP_0_SHOWN,YBFW_PAGE_PIP_0_HIDDEN,YBFW_PAGE_PIP_0_MAIN},
 [YBFW_PIP_CH_1] = {YBFW_PAGE_PIP_0_SHOWN,YBFW_PAGE_PIP_0_HIDDEN,YBFW_PAGE_PIP_0_MAIN},
};

UINT8 yuv_to_scale_interp = YBFW_GFX_MAP_ITP_BILINEAR;
UINT8 scale_to_hdmi_interp = YBFW_GFX_MAP_ITP_BILINEAR;


UINT8 dsp_do = 1;
UINT8 fill_black = 1;
// 0  - disabled
// 1 - sp5kGfxObjectBilinearScale
// 2 - ybfwGfxObjectMapping
UINT8 yuv_to_scale_mode = 1;
// 0 - disabled
// 1 - sp5kGfxObjectBilinearScale
// 2 - ybfwGfxObjectMapping
// 3 - sp5kGfxObjectCopy()
UINT8 scale_to_hdmi_mode = 1;



/* 0 : for still pv,  1 : for video pv
 * to Log which yuv strm to cfg YBFW_MODE_PV_CFG_DISP_CB
 */
static UINT32 yuvDispMsk[2];

static UINT32 grRawCb( ybfwModePrevYuvInfo_t *ppvRawInfo, UINT32 ppvRawInfoNum){

    static UINT64 perf_counter = 0;
    static UINT64 frame_skipped = 0;
    UINT32 tmr_start = 0;
    UINT32 time_dspdo = 0;
    UINT32 time_yuv2scale = 0;
    UINT32 time_scale2hdmi = 0;
//    DBG("trace\n");
    UINT32 i, retBuf, mode, yuvDispMskCfg;
    ybfwModePrevYuvInfo_t pvyuv[YBFW_PV_MAX_YUV_PER_RAW];

    ybfwGfxObj_t srcImg[1];
    UINT32 srcImgIQinfo[1] = {0};
    ybfwGfxObj_t dstImg[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwGfxRoi_t dstImgSrcRoi[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwPrevYuvRoi_t srcRoi;
    UINT32 ret = FAIL;
    UINT32 dstCnt=0, srcCnt=0, isPipToggle = 0;
    ybfwGfxObjectCdspDoParam_t param;
    UINT32 appendparam  = (1<<16);
    UINT32 tmpSpeedCtrl = (1<<31);
    UINT32 pipChnl, pipHidden, pipGetTime0, pipGetTime1;
    ybfwGfxPageCapabDesc_t appPipPara;
    ybfwGfxMapProjectParam_t  proPrm;
    ybfwModeGetPeek(&mode);

    yuvDispMskCfg = yuvDispMsk[ (mode== YBFW_MODE_VIDEO_PREVIEW) ?  1 : 0];
    /* adjust the speed of RawToYuv */
    param.cdspDoTargetFps = 120;

    memset(&param, 0, sizeof(param));
    memset(&srcImg, 0, sizeof(srcImg));
    memset(&dstImg, 0, sizeof(dstImg));
    memset(&dstImgSrcRoi, 0, sizeof(dstImgSrcRoi));
    memset(&srcImgIQinfo, 0, sizeof(srcImgIQinfo));
//    if (appHdmiWithOutPowerOffFlagGet() && appIsHdmiPlugIn()) {
//        pipChnl = YBFW_PIP_CH_1;
//        pipHidden = YBFW_PAGE_PIP_1_HIDDEN;
//    } else {
        pipChnl = YBFW_PIP_CH_0;
        pipHidden = YBFW_PAGE_PIP_0_HIDDEN;
//    }


    if( !(ppvRawInfo->attr & (YBFW_MODE_PV_CB_IMG_STOP_IN_PV_EXIT|YBFW_MODE_PV_CB_IMG_PAUSE_IN_PV) )) {
//        DBG("trace\n");
        tmr_start = ybfwMsTimeGet();
#ifdef ONE_THREAD
        UINT8 curBufIndex =  BUF_YUV;
#else
        UINT8 curBufIndex =  pctx->sbIndex[CUR_SENSOR];
#endif
        param.srcBufHandle[srcCnt] = ppvRawInfo->pvCdspBufHandle;
        srcImgIQinfo[srcCnt] = ppvRawInfo->pcdspDoInfo;
        srcImg[srcCnt].pbuf  = ppvRawInfo->pframe.pbuf;
        srcImg[srcCnt].bufW  = ppvRawInfo->pframe.width;
        srcImg[srcCnt].bufH  = ppvRawInfo->pframe.height;
        srcImg[srcCnt].roiX  = (SINT32)ppvRawInfo->pframe.roiX;
        srcImg[srcCnt].roiY  = (SINT32)ppvRawInfo->pframe.roiY;
        srcImg[srcCnt].fmt   = ppvRawInfo->pframe.fmt;
        srcImg[srcCnt].roiW  = ppvRawInfo->pframe.roiW;
        srcImg[srcCnt].roiH  = ppvRawInfo->pframe.roiH;
        srcCnt = 1;
//DBG("----");
        i = 0;
//        for( i = 0 ; i < YBFW_PV_MAX_YUV_PER_RAW; i ++){
            memset(&pvyuv[i], 0, sizeof(ybfwModePrevYuvInfo_t));
            ybfwModeCfgGet(YBFW_MODE_CFG_PVCB_YUV_SRC_ROI_GET, ppvRawInfo, &srcRoi, i);
//                DBG("trace\n");
                if( srcRoi.roiw && srcRoi.roih ){
                    dstImgSrcRoi[ dstCnt ].roiX = srcRoi.roix;
                    dstImgSrcRoi[ dstCnt ].roiY = srcRoi.roiy;
                    dstImgSrcRoi[ dstCnt ].roiW = srcRoi.roiw;
                    dstImgSrcRoi[ dstCnt ].roiH = srcRoi.roih;

                    if (g_ScalerBuf[CUR_SENSOR][curBufIndex].pbuf){
                        dstImg[ dstCnt ].pbuf = g_ScalerBuf[CUR_SENSOR][curBufIndex].pbuf;
                        dstImg[ dstCnt ].bufW = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufW;
                        dstImg[ dstCnt ].bufH = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufH;
                        dstImg[ dstCnt ].fmt  = g_ScalerBuf[CUR_SENSOR][curBufIndex].fmt;
                        dstImg[ dstCnt ].roiX = g_ScalerBuf[CUR_SENSOR][curBufIndex].roiX;
                        dstImg[ dstCnt ].roiY = g_ScalerBuf[CUR_SENSOR][curBufIndex].roiY;
                        dstImg[ dstCnt ].roiW = g_ScalerBuf[CUR_SENSOR][curBufIndex].roiW;
                        dstImg[ dstCnt ].roiH = g_ScalerBuf[CUR_SENSOR][curBufIndex].roiH;
                        dstCnt++;
                        isPipToggle = 0;
                    }else{
                        DBG("g_ScalerBuf[%d][%d] buff is null",CUR_SENSOR, pctx->sbIndex[CUR_SENSOR]);
                    }
//                    DBG("SEN (%d,%d; %d x %d) -> YUV (%d,%d; %d x %d)",
//                            srcImg[srcCnt-1].roiX,srcImg[srcCnt-1].roiY,srcImg[srcCnt-1].roiW,srcImg[srcCnt-1].roiH,
//                            dstImg[srcCnt-1].roiX,dstImg[srcCnt-1].roiY,dstImg[srcCnt-1].roiW,dstImg[srcCnt-1].roiH);

#ifdef PIP_EXT_BUFF
                    dstImgSrcRoi[ dstCnt ].roiX = srcRoi.roix;
                   dstImgSrcRoi[ dstCnt ].roiY = srcRoi.roiy;
                   dstImgSrcRoi[ dstCnt ].roiW = srcRoi.roiw;
                   dstImgSrcRoi[ dstCnt ].roiH = srcRoi.roih;
                    if (g_ScalerBufPiP[CUR_SENSOR][curBufIndex].pbuf){
                        dstImg[ dstCnt ].pbuf = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].pbuf;
                        dstImg[ dstCnt ].bufW = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].bufW;
                        dstImg[ dstCnt ].bufH = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].bufH;
                        dstImg[ dstCnt ].fmt  = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].fmt;
                        dstImg[ dstCnt ].roiX = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].roiX;
                        dstImg[ dstCnt ].roiY = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].roiY;
                        dstImg[ dstCnt ].roiW = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].roiW;
                        dstImg[ dstCnt ].roiH = g_ScalerBufPiP[CUR_SENSOR][curBufIndex].roiH;
                        dstCnt++;
                        isPipToggle = 0;
                    }else{
                        DBG("g_ScalerBufPiP[%d][%d] buff is null",CUR_SENSOR, pctx->sbIndex[CUR_SENSOR]);
                    }
#endif
                }
                else{
                    HOST_ASSERT_MSG(0, "mode 0x%02x yuv_%d's roiw(=%d) roih(=%d) is zero, this yuv is not exist, but yuvDispMskCfg is %x \n",
                        mode, i, srcRoi.roiw , srcRoi.roih, yuvDispMskCfg );
                }
//        }

        if( srcCnt && dstCnt  && dsp_do) {
            ret = ybfwGfxObjectCdspDo( srcImg, dstImgSrcRoi, dstImg,
                dstCnt | ( srcCnt << 8 ) | appendparam | tmpSpeedCtrl,
                YBFW_GFX_OBJECT_CDSP_DO_YUV, srcImgIQinfo[0],
                YBFW_MODE_STILL_PREVIEW,  &param );

            if(ret == FAIL) HOST_ASSERT_MSG(0,"CDSP DO err!!!");
        }
        time_dspdo = ybfwMsTimeGet() - tmr_start;
#ifdef ONE_THREAD

            //getMutex
            //DBG("ybfwOsMutexGet");
            ybfwOsMutexGet(&pctx->mutex, YBFW_TX_WAIT_FOREVER);

            ybfwGfxObj_t scrGfxObj, dstGfxObj;
            ybfwGfxPageCapabDesc_t  pipPara;

    //        DBG("trace\n");
            //curBufIndex = pctx->sbIndex[CUR_SENSOR];
            //pctx->sbIndex[CUR_SENSOR]  = (pctx->sbIndex[CUR_SENSOR] + 1)% SCALE_BUF_PER_SENSOR_COUNT;
            memset(&pipPara, 0, sizeof(pipPara));
            memset(&scrGfxObj, 0, sizeof(ybfwGfxObj_t));
            memset(&dstGfxObj, 0, sizeof(ybfwGfxObj_t));
//DBG("trace");
    //        DBG("trace\n");
            scrGfxObj.pbuf = g_ScalerBuf[CUR_SENSOR][curBufIndex].pbuf;
            scrGfxObj.bufW = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufW;
            scrGfxObj.bufH = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufH;
            scrGfxObj.fmt  = g_ScalerBuf[CUR_SENSOR][curBufIndex].fmt;
            scrGfxObj.roiX = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiX;
            scrGfxObj.roiY = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiY;
            scrGfxObj.roiW = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiW;
            scrGfxObj.roiH = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiH;

			curBufIndex = BUF_ZOOM;
            dstGfxObj.pbuf = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].pbuf;
            dstGfxObj.fmt  = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].fmt;
            dstGfxObj.bufW = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].bufW;
            dstGfxObj.bufH = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].bufH;
            dstGfxObj.roiX = 0;//pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiX;
            dstGfxObj.roiY = 0;//pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiY;
            dstGfxObj.roiW = SCALE_BUF_DRAW_W;//pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiW;
            dstGfxObj.roiH = SCALE_BUF_DRAW_H;//pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiH;

//            DBG("YUV (%d,%d; %d x %d) -> ZOOM (%d,%d; %d x %d)",
//                    scrGfxObj.roiX,scrGfxObj.roiY,scrGfxObj.roiW,scrGfxObj.roiH,
//                    dstGfxObj.roiX,dstGfxObj.roiY,dstGfxObj.roiW,dstGfxObj.roiH);

            //move to prebuffer;
            if (yuv_to_scale_mode == 1){
                sp5kGfxObjectBilinearScale(&scrGfxObj, &dstGfxObj);
            }else if (yuv_to_scale_mode == 2){
                mappingBuffers(&scrGfxObj, &dstGfxObj,1,BG_COLOR,yuv_to_scale_interp);
            }

            time_yuv2scale = ybfwMsTimeGet() - tmr_start - time_dspdo;
    //        DBG("trace\n");
//DBG("trace");
            ret = ybfwGfxPageCapabilityGet(YBFW_YUV_PIP_BASE + disp_pip_pages[CUR_PIP_CHANNEL][DPP_HIDDEN], &pipPara);
    //        DBG("trace\n");
            if(ret == SUCCESS && pipPara.pbuf){


             if (fill_black){ //black screen  //TODO delete perfomance
                dstGfxObj.pbuf = pipPara.pbuf;
                dstGfxObj.fmt  = pipPara.fmt;
                dstGfxObj.bufW = pipPara.frmW;
                dstGfxObj.bufH = pipPara.frmH;
                dstGfxObj.roiX = pipPara.frmRoiX;
                dstGfxObj.roiY = pipPara.frmRoiY;
                dstGfxObj.roiW = pipPara.frmRoiW;
                dstGfxObj.roiH = pipPara.frmRoiH;
                ybfwGfxObjectFill(&dstGfxObj, 0x800080);
             }
//DBG("trace");
                scrGfxObj.pbuf = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].pbuf;
                scrGfxObj.fmt  = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].fmt;
                scrGfxObj.bufW = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].bufW;
                scrGfxObj.bufH = g_ScalerBuf[CUR_SENSOR][BUF_ZOOM].bufH;
                scrGfxObj.roiX = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiX;
                scrGfxObj.roiY = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiY;
                scrGfxObj.roiW = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiW;
                scrGfxObj.roiH = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiH;

                //move to hdmi
                dstGfxObj.pbuf = pipPara.pbuf;
                dstGfxObj.fmt  = pipPara.fmt;
                dstGfxObj.bufW = pipPara.frmW;
                dstGfxObj.bufH = pipPara.frmH;
                dstGfxObj.roiX = DRAW_X;
                dstGfxObj.roiY = DRAW_Y;
                dstGfxObj.roiW = DRAW_W;
                dstGfxObj.roiH = DRAW_H;

//                DBG("ZOOM (%d,%d; %d x %d) -> HDMI (%d,%d; %d x %d)",
//                        scrGfxObj.roiX,scrGfxObj.roiY,scrGfxObj.roiW,scrGfxObj.roiH,
//                        dstGfxObj.roiX,dstGfxObj.roiY,dstGfxObj.roiW,dstGfxObj.roiH);

                //DBG("ybfwOsMutexPut");
                ybfwOsMutexPut(&pctx->mutex);
            }
            else{
                printf("error \n");
                return ERR_SUCCESS;
            }

//            MAP_BUFFERS(&scrGfxObj, &dstGfxObj,pctx->sbStretchWin[CUR_SENSOR][WT_MAIN],BG_COLOR);
            if (scale_to_hdmi_mode == 1){
                sp5kGfxObjectBilinearScale(&scrGfxObj, &dstGfxObj);
            }else if (scale_to_hdmi_mode == 2){
                mappingBuffers(&scrGfxObj, &dstGfxObj,1,BG_COLOR,scale_to_hdmi_interp);
            }else if (scale_to_hdmi_mode == 3){
                ybfwGfxObjectCopy(&scrGfxObj,&dstGfxObj);
            }

    //       DBG("trace\n");
           UINT32 isNoWait = 1;
           ybfwPipAttrSet(CUR_PIP_CHANNEL, YBFW_PIP_TOGGLE, 1, isNoWait, 0, 0);
           time_scale2hdmi = ybfwMsTimeGet() - tmr_start - time_dspdo - time_yuv2scale;
#endif



#ifndef ONE_THREAD
        ybfwOsEventFlagsSet(&pctx->sbGrbEvt[CUR_SENSOR], 1, YBFW_TX_OR);
#endif
//        DBG("trace");
#if 1
        if ((perf_counter %100) == 0){
//#error sdfd

            tmr_start = ybfwMsTimeGet() - tmr_start;
            printf("grRawCb() Perf total %d ms  dsp %d ms yuv2scale %d ms scale2hdmi %d ms "
                    "\n\tsrc params fmt %d frBuf %d x %d  ROI (%d,%d;%d x %d)\n",
                    tmr_start, time_dspdo,time_yuv2scale,time_scale2hdmi,
                    ppvRawInfo->pframe.fmt,
                    ppvRawInfo->pframe.width,ppvRawInfo->pframe.height,
                    ppvRawInfo->pframe.roiX,ppvRawInfo->pframe.roiX,
                    ppvRawInfo->pframe.roiW,ppvRawInfo->pframe.roiH);
            perf_counter = 0;
        }
        perf_counter++;
#endif
//        for( i = 0 ; i < YBFW_PV_MAX_YUV_PER_RAW; i ++){
//            if(pvyuv[i].pvCdspBufHandle)
//                ybfwModeCfgSet(YBFW_MODE_CFG_OPENPV_BUF_RELEASE, &pvyuv[i]);
//        }
    }
    //DBG("trace\n");
    return ERR_SUCCESS;
}


#ifndef ONE_THREAD
//    ybfwOsEventFlagsSet(&osd_evt, 1, YBFW_TX_OR);
void scalerThrMain(ULONG param)
{

    ybfwGfxObj_t scrGfxObj, dstGfxObj;
    ULONG scalerEvents;
    UINT32 ret = SUCCESS;
    ybfwGfxPageCapabDesc_t  pipPara;
    UINT32 tmr = 0;
    static UINT64 perf_counter = 0;
    UINT8 curBufIndex = 0;
    DBG("trace\n");

#ifdef DBG_SAVE_SCALER_RAW
    int counter = 0;
    char tmpbuf[128]={0}; /*temporary*/
#endif

    while (1)
    {
        /*This flag bit is synchronized with the osdvideourgentCB callback function*/
//        DBG("trace\n");
        tmr = ybfwMsTimeGet();
        ybfwOsEventFlagsGet(&pctx->sbGrbEvt[CUR_SENSOR], 1, YBFW_TX_OR_CLEAR, &scalerEvents, YBFW_TX_WAIT_FOREVER);
//        DBG("trace\n");
        curBufIndex = pctx->sbIndex[CUR_SENSOR];
        pctx->sbIndex[CUR_SENSOR]  = (pctx->sbIndex[CUR_SENSOR] + 1)% SCALE_BUF_PER_SENSOR_COUNT;
        memset(&pipPara, 0, sizeof(pipPara));
        memset(&scrGfxObj, 0, sizeof(ybfwGfxObj_t));
        memset(&dstGfxObj, 0, sizeof(ybfwGfxObj_t));

//        DBG("trace\n");
        scrGfxObj.pbuf = g_ScalerBuf[CUR_SENSOR][curBufIndex].pbuf;
        scrGfxObj.bufW = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufW;
        scrGfxObj.bufH = g_ScalerBuf[CUR_SENSOR][curBufIndex].bufH;
        scrGfxObj.fmt  = g_ScalerBuf[CUR_SENSOR][curBufIndex].fmt;
        scrGfxObj.roiX = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiX;
        scrGfxObj.roiY = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiY;
        scrGfxObj.roiW = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiW;
        scrGfxObj.roiH = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiH;
//        DBG("trace\n");

        ret = ybfwGfxPageCapabilityGet(YBFW_YUV_PIP_BASE + disp_pip_pages[CUR_PIP_CHANNEL][DPP_HIDDEN], &pipPara);
//        DBG("trace\n");
        if(ret == SUCCESS && pipPara.pbuf){
#if 1 //black screen  //TODO delete perfomance
            dstGfxObj.pbuf = pipPara.pbuf;
            dstGfxObj.fmt  = pipPara.fmt;
            dstGfxObj.bufW = pipPara.frmW;
            dstGfxObj.bufH = pipPara.frmH;
            dstGfxObj.roiX = pipPara.frmRoiX;
            dstGfxObj.roiY = pipPara.frmRoiY;
            dstGfxObj.roiW = pipPara.frmRoiW;
            dstGfxObj.roiH = pipPara.frmRoiH;
            ybfwGfxObjectFill(&dstGfxObj, 0x800080);
#endif
            dstGfxObj.pbuf = pipPara.pbuf;
            dstGfxObj.fmt  = pipPara.fmt;
            dstGfxObj.bufW = pipPara.frmW;
            dstGfxObj.bufH = pipPara.frmH;
            dstGfxObj.roiX = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiX;
            dstGfxObj.roiY = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiY;
            dstGfxObj.roiW = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiW;
            dstGfxObj.roiH = pctx->drawSelRoiVID[CUR_DISPLAY][WT_MAIN].roiH;
        }
        else{
            printf("error \n");
            return;
        }

        MAP_BUFFERS(&scrGfxObj, &dstGfxObj,pctx->sbStretchWin[CUR_SENSOR][WT_MAIN],BG_COLOR);

        //draw pip
       if ((pctx->grabCfg[CUR_SENSOR].zmPIP) && pipPara.pbuf){

           scrGfxObj.roiX = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiX;
           scrGfxObj.roiY = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiY;
           scrGfxObj.roiW = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiW;
           scrGfxObj.roiH = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiH;

           dstGfxObj.pbuf = pipPara.pbuf;
           dstGfxObj.fmt  = pipPara.fmt;
           dstGfxObj.bufW = pipPara.frmW;
           dstGfxObj.bufH = pipPara.frmH;
           dstGfxObj.roiX = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiX;
           dstGfxObj.roiY = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiY;
           dstGfxObj.roiW = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiW;
           dstGfxObj.roiH = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiH;

           MAP_BUFFERS(&scrGfxObj, &dstGfxObj,pctx->sbStretchWin[CUR_SENSOR][WT_MAIN],BG_COLOR);
       }

//       DBG("trace\n");
       UINT32 isNoWait = 1;
       ybfwPipAttrSet(CUR_PIP_CHANNEL, YBFW_PIP_TOGGLE, 1, isNoWait, 0, 0);

#if 0
       if ((perf_counter %2000) == 0){
           tmr = ybfwMsTimeGet() - tmr;
           printf("MThread Perf proccess frame %d ms \n "
                   "PiP params frBuf %d x %d  ROI (%d,%d;%d x %d) win (%d,%d;%d x %d)\n",
                   tmr,
                   pipPara.frmW,pipPara.frmH,
                   pipPara.frmRoiX,pipPara.frmRoiY,pipPara.frmRoiW,pipPara.frmRoiH,
                   pipPara.winX,pipPara.winY,pipPara.winW,pipPara.winH);
           perf_counter = 0;
       }
       perf_counter++;
#endif
       //DBG("trace\n");

#ifdef DBG_SAVE_SCALER_RAW
#if YBFW_DISK_SD
        sprintf(tmpbuf,"D:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
#if (YBFW_DISK_NAND || YBFW_DISK_EMMC || YBFW_DISK_ESD || YBFW_DISK_SPI)
        sprintf(tmpbuf,"C:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
        fsSimpleWrite( tmpbuf, g_osdLayerScale_gfxObjDst.pbuf, g_osdLayerScale_gfxObjDst.bufW * g_osdLayerScale_gfxObjDst.bufH * 2);
        counter++;
#endif

//        DBG("trace\n");
    }

}
#endif

UINT32 scalerThreadStart(){

    UINT32  i;
    UINT8 * buf[32]={0};
    DBG("trace\n");
    if (pctx->sbThrGrb[CUR_SENSOR] == NULL){
        DBG("trace\n");
#ifdef ONE_THREAD
        {
            i = BUF_YUV;
            g_ScalerBuf[CUR_SENSOR][i].fmt  = pctx->grabSenCfg[CUR_SENSOR].yuvFmt;
            g_ScalerBuf[CUR_SENSOR][i].roiX = pctx->sbFullYuvSize[CUR_SENSOR].roiX;
            g_ScalerBuf[CUR_SENSOR][i].roiY = pctx->sbFullYuvSize[CUR_SENSOR].roiY;
            g_ScalerBuf[CUR_SENSOR][i].roiW = pctx->sbFullYuvSize[CUR_SENSOR].roiW;
            g_ScalerBuf[CUR_SENSOR][i].roiH = pctx->sbFullYuvSize[CUR_SENSOR].roiH;
            g_ScalerBuf[CUR_SENSOR][i].bufW = YUV_BUF_W_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR].roiX + pctx->sbFullYuvSize[CUR_SENSOR].roiW));
            g_ScalerBuf[CUR_SENSOR][i].bufH = YUV_BUF_H_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR].roiY + pctx->sbFullYuvSize[CUR_SENSOR].roiH));

            DBG("MAIN WIN ROI[%d] (%d,%d; %d x %d)",i,g_ScalerBuf[CUR_SENSOR][i].roiX,g_ScalerBuf[CUR_SENSOR][i].roiY,
                    g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);
            g_ScalerBuf[CUR_SENSOR][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);

            if (g_ScalerBuf[CUR_SENSOR][i].pbuf == NULL){
                ERR("g_ScalerBuf[CUR_SENSOR][%d].pbuf is NULL",i);
            }

            i = BUF_ZOOM;
            g_ScalerBuf[CUR_SENSOR][i].fmt  = pctx->grabSenCfg[CUR_SENSOR].yuvFmt;
            g_ScalerBuf[CUR_SENSOR][i].roiX = 0;//pctx->sbFullYuvSize[CUR_SENSOR][i].roiX;
            g_ScalerBuf[CUR_SENSOR][i].roiY = 0;//pctx->sbFullYuvSize[CUR_SENSOR][i].roiY;
            g_ScalerBuf[CUR_SENSOR][i].roiW = SN_W;//pctx->sbFullYuvSize[CUR_SENSOR][i].roiW;
            g_ScalerBuf[CUR_SENSOR][i].roiH = SN_H;//pctx->sbFullYuvSize[CUR_SENSOR][i].roiH;
            g_ScalerBuf[CUR_SENSOR][i].bufW = YUV_BUF_W_ALIGN((SN_W));
            g_ScalerBuf[CUR_SENSOR][i].bufH = YUV_BUF_H_ALIGN((SN_H));

            DBG("MAIN WIN ROI[%d] (%d,%d; %d x %d)",i,g_ScalerBuf[CUR_SENSOR][i].roiX,g_ScalerBuf[CUR_SENSOR][i].roiY,
                    g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);
            g_ScalerBuf[CUR_SENSOR][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);
#else
            for (i = 0 ; i < SCALE_BUF_PER_SENSOR_COUNT; i++){
                g_ScalerBuf[CUR_SENSOR][i].fmt  = pctx->grabSenCfg[CUR_SENSOR].yuvFmt;
                g_ScalerBuf[CUR_SENSOR][i].roiX = pctx->sbFullYuvSize[CUR_SENSOR][i].roiX;
                g_ScalerBuf[CUR_SENSOR][i].roiY = pctx->sbFullYuvSize[CUR_SENSOR][i].roiY;
                g_ScalerBuf[CUR_SENSOR][i].roiW = pctx->sbFullYuvSize[CUR_SENSOR][i].roiW;
                g_ScalerBuf[CUR_SENSOR][i].roiH = pctx->sbFullYuvSize[CUR_SENSOR][i].roiH;
                g_ScalerBuf[CUR_SENSOR][i].bufW = YUV_BUF_W_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR][i].roiX + pctx->sbFullYuvSize[CUR_SENSOR][i].roiW));
                g_ScalerBuf[CUR_SENSOR][i].bufH = YUV_BUF_H_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR][i].roiY + pctx->sbFullYuvSize[CUR_SENSOR][i].roiH));

                DBG("MAIN WIN ROI[%d] (%d,%d; %d x %d)",i,g_ScalerBuf[CUR_SENSOR][i].roiX,g_ScalerBuf[CUR_SENSOR][i].roiY,
                        g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);
                g_ScalerBuf[CUR_SENSOR][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(g_ScalerBuf[CUR_SENSOR][i].bufW,g_ScalerBuf[CUR_SENSOR][i].bufH);

                if (g_ScalerBuf[CUR_SENSOR][i].pbuf == NULL){
                    ERR("g_ScalerBuf[CUR_SENSOR][%d].pbuf is NULL",i);
                }
#endif

#ifdef PIP_EXT_BUFF
            g_ScalerBufPiP[CUR_SENSOR][i].fmt  = pctx->grabSenCfg[CUR_SENSOR].yuvFmt;
            g_ScalerBufPiP[CUR_SENSOR][i].roiX = pctx->sbFullYuvSize[CUR_SENSOR][i].roiX;
            g_ScalerBufPiP[CUR_SENSOR][i].roiY = pctx->sbFullYuvSize[CUR_SENSOR][i].roiY;
            g_ScalerBufPiP[CUR_SENSOR][i].roiW = pctx->sbFullYuvSize[CUR_SENSOR][i].roiW;
            g_ScalerBufPiP[CUR_SENSOR][i].roiH = pctx->sbFullYuvSize[CUR_SENSOR][i].roiH;
            g_ScalerBufPiP[CUR_SENSOR][i].bufW = YUV_BUF_W_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR][i].roiX + pctx->sbFullYuvSize[CUR_SENSOR][i].roiW));
            g_ScalerBufPiP[CUR_SENSOR][i].bufH = YUV_BUF_H_ALIGN((pctx->sbFullYuvSize[CUR_SENSOR][i].roiY + pctx->sbFullYuvSize[CUR_SENSOR][i].roiH));

            DBG("PiPbuf MAIN WIN ROI[%d] (%d,%d; %d x %d)",i,g_ScalerBufPiP[CUR_SENSOR][i].roiX,g_ScalerBufPiP[CUR_SENSOR][i].roiY,
                    g_ScalerBufPiP[CUR_SENSOR][i].bufW,g_ScalerBufPiP[CUR_SENSOR][i].bufH);
            g_ScalerBufPiP[CUR_SENSOR][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(g_ScalerBufPiP[CUR_SENSOR][i].bufW,g_ScalerBufPiP[CUR_SENSOR][i].bufH);

            if (g_ScalerBufPiP[CUR_SENSOR][i].pbuf == NULL){
                ERR("g_ScalerBuf[CUR_SENSOR][%d].pbuf is NULL",i);
            }
#endif
        }//for

#ifndef ONE_THREAD
        DBG("trace\n");
        sprintf((CHAR *) buf,"sbGrbEvt%d",CUR_SENSOR);
        DBG("trace\n");
        ybfwOsEventFlagsCreate(&pctx->sbGrbEvt[CUR_SENSOR], (CHAR *) buf);
        /*
         * TODO params example
         *  UINT32 *par = ybfwMallocCache(4*sizeof(UINT32));
            par[0] = (UINT32)pfunc;
            par[1] = param;
            par[3] = 0;
         */
        DBG("trace\n");
        sprintf((CHAR *) buf,"sbThrGrb%d",CUR_SENSOR);
        pctx->sbThrGrb[CUR_SENSOR] = ybfwOsThreadCreate((CHAR *) buf, scalerThrMain, 0, 20,
                        0, 0, YBFW_TX_AUTO_START);
        HOST_ASSERT(pctx->sbThrGrb[CUR_SENSOR]);
#endif
    }else{
        INFO ("Scaler thread is already started\n");
        return 1;
    }

    return 0;
}


UINT32 scalerThreadStop(){

    if (pctx->sbThrGrb[CUR_SENSOR] != NULL){

        ybfwOsThreadDelete(pctx->sbThrGrb[CUR_SENSOR]);
        pctx->sbThrGrb[CUR_SENSOR] = NULL ;

        UINT32 i;
        for(i=0; i < SCALE_BUF_PER_SENSOR_COUNT; i++){
            UINT8* tmp = g_ScalerBuf[CUR_SENSOR][i].pbuf;
            g_ScalerBuf[CUR_SENSOR][i].pbuf = NULL;
            if (tmp){
                ybfwYuvBufferFree(tmp);
            }else{
                INFO ("g_mainWinYUVBuf is already NULL\n");
            }
#ifdef PIP_EXT_BUFF
            tmp = g_ScalerBufPiP[CUR_SENSOR][i].pbuf;
            g_ScalerBufPiP[CUR_SENSOR][i].pbuf = NULL;
            if (tmp){
                ybfwYuvBufferFree(tmp);
            }else{
                INFO ("g_mainWinYUVBuf is already NULL\n");
            }
#endif
        }

        ybfwOsEventFlagsDelete(&pctx->sbGrbEvt[CUR_SENSOR]);
        pctx->sbGrbEvt[CUR_SENSOR] = 0;

    }else{
        INFO ("Scaler thread is already stopped\n");
       return ERR_FAIL;
    }
    return ERR_SUCCESS;
}

#if 0
static UINT32 pvRawCbDirectShow(ybfwModePrevYuvInfo_t *ppvRawInfo, UINT32 ppvRawInfoNum)
{
    UINT32 i, retBuf, mode, yuvDispMskCfg;
    ybfwModePrevYuvInfo_t pvyuv[YBFW_PV_MAX_YUV_PER_RAW];
//    DBG("trace\n");
    ybfwGfxObj_t srcImg[1];
    UINT32 srcImgIQinfo[1] = {0};
    ybfwGfxObj_t dstImg[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwGfxRoi_t dstImgSrcRoi[ YBFW_PV_MAX_YUV_PER_RAW ];
    ybfwPrevYuvRoi_t srcRoi;
    UINT32 ret = FAIL;
    UINT32 dstCnt=0, srcCnt=0, isPipToggle = 0;
    ybfwGfxObjectCdspDoParam_t param;
    UINT32 appendparam  = (1<<16);
    UINT32 tmpSpeedCtrl = (1<<31);
    UINT32 pipChnl, pipHidden, pipGetTime0, pipGetTime1;
    ybfwGfxPageCapabDesc_t appPipPara;
    ybfwGfxMapProjectParam_t  proPrm;
    ybfwModeGetPeek(&mode);

//    yuvDispMskCfg = yuvDispMsk[ (mode== YBFW_MODE_VIDEO_PREVIEW) ?  1 : 0];
    /* adjust the speed of RawToYuv */
    param.cdspDoTargetFps = 120;
//    DBG("trace\n");
    memset(&param, 0, sizeof(param));
    memset(&srcImg, 0, sizeof(srcImg));
    memset(&dstImg, 0, sizeof(dstImg));
    memset(&dstImgSrcRoi, 0, sizeof(dstImgSrcRoi));
    memset(&srcImgIQinfo, 0, sizeof(srcImgIQinfo));
//    if (appHdmiWithOutPowerOffFlagGet() && appIsHdmiPlugIn()) {
//        pipChnl = YBFW_PIP_CH_1;
//        pipHidden = YBFW_PAGE_PIP_1_HIDDEN;
//    } else {
        pipChnl = YBFW_PIP_CH_0;
        pipHidden = YBFW_PAGE_PIP_0_HIDDEN;
//    }
//        DBG("trace\n");
    if( !(ppvRawInfo->attr & (YBFW_MODE_PV_CB_IMG_STOP_IN_PV_EXIT|YBFW_MODE_PV_CB_IMG_PAUSE_IN_PV) )) {
//        DBG("trace\n");
        param.srcBufHandle[srcCnt] = ppvRawInfo->pvCdspBufHandle;
        srcImgIQinfo[srcCnt] = ppvRawInfo->pcdspDoInfo;
        srcImg[srcCnt].pbuf  = ppvRawInfo->pframe.pbuf;
        srcImg[srcCnt].bufW  = ppvRawInfo->pframe.width;
        srcImg[srcCnt].bufH  = ppvRawInfo->pframe.height;
        srcImg[srcCnt].roiX  = (SINT32)ppvRawInfo->pframe.roiX;
        srcImg[srcCnt].roiY  = (SINT32)ppvRawInfo->pframe.roiY;
        srcImg[srcCnt].fmt   = ppvRawInfo->pframe.fmt;
        srcImg[srcCnt].roiW  = ppvRawInfo->pframe.roiW;
        srcImg[srcCnt].roiH  = ppvRawInfo->pframe.roiH;
//                DBG("SRC buf WxH %ux%u, roi (%d,%d,%uX%u) fmt %u",ppvRawInfo->pframe.width,ppvRawInfo->pframe.height,
//                        ppvRawInfo->pframe.roiX,ppvRawInfo->pframe.roiY,ppvRawInfo->pframe.roiW, ppvRawInfo->pframe.roiH, srcImg[srcCnt].fmt);
//        DBG("SRC buf WxH %ux%u, roi (%d,%d,%uX%u) fmt %u",srcImg[srcCnt].bufW,srcImg[srcCnt].bufH,
//                       srcImg[srcCnt].roiX,srcImg[srcCnt].roiY,srcImg[srcCnt].roiW, srcImg[srcCnt].roiH, srcImg[srcCnt].fmt);

        srcCnt = 1;

        i = 0;
//        for( i = 0 ; i < YBFW_PV_MAX_YUV_PER_RAW; i ++){
            memset(&pvyuv[i], 0, sizeof(ybfwModePrevYuvInfo_t));
            ybfwModeCfgGet(YBFW_MODE_CFG_PVCB_YUV_SRC_ROI_GET, ppvRawInfo, &srcRoi, i);
//            if( yuvDispMskCfg & (1<<i) ){
//                DBG("trace\n");
                if( srcRoi.roiw && srcRoi.roih ){
                    dstImgSrcRoi[ dstCnt ].roiX = srcRoi.roix;
                    dstImgSrcRoi[ dstCnt ].roiY = srcRoi.roiy;
                    dstImgSrcRoi[ dstCnt ].roiW = srcRoi.roiw;
                    dstImgSrcRoi[ dstCnt ].roiH = srcRoi.roih;
//                    DBG("trace\n");
                    memset(&appPipPara, 0, sizeof(appPipPara));
                    pipGetTime0 = ybfwMsTimeGet();
                    ret = ybfwGfxPageCapabilityGet((YBFW_YUV_PIP_BASE+pipHidden), &appPipPara);
                    pipGetTime1 = ybfwMsTimeGet();
//                    DBG("trace\n");
                    if( ret == FAIL || !appPipPara.pbuf ) {
                        HOST_ASSERT_MSG(0, "Get Pip Hidden Fail ret=%x, pbuf=%p",ret, appPipPara.pbuf);
                    }
                    else{
                        HOST_ASSERT_MSG( (pipGetTime1-pipGetTime0) < 5,
                            "Get Pip Hidden Too long, need to enlarge YBFW_PIP_INIT_TOGGFLE_FRAME_NUM  (%d, %d)",
                            pipGetTime1, pipGetTime0);
                    }
//                    DBG("trace\n");
                    dstImg[ dstCnt ].pbuf = appPipPara.pbuf;
                    dstImg[ dstCnt ].bufW = appPipPara.frmW;
                    dstImg[ dstCnt ].bufH = appPipPara.frmH;
                    dstImg[ dstCnt ].fmt  = appPipPara.fmt;
                    dstImg[ dstCnt ].roiX = appPipPara.frmRoiX;
                    dstImg[ dstCnt ].roiY = appPipPara.frmRoiY;
                    dstImg[ dstCnt ].roiW = appPipPara.frmRoiW;
                    dstImg[ dstCnt ].roiH = appPipPara.frmRoiH;
//                    DBG("DST buf WxH %ux%u, roi (%d,%d,%uX%u) fmt %u",dstImg[dstCnt].bufW,dstImg[dstCnt].bufH,
//                                                         dstImg[dstCnt].roiX,dstImg[dstCnt].roiY,dstImg[dstCnt].roiW, dstImg[dstCnt].roiH, dstImg[dstCnt].fmt);
                    dstCnt++;
//                    DBG("trace\n");
                    isPipToggle = 1;

                }
                else{
                    HOST_ASSERT_MSG(0, "mode 0x%02x yuv_%d's roiw(=%d) roih(=%d) is zero, this yuv is not exist, but yuvDispMskCfg is %x \n",
                        mode, i, srcRoi.roiw , srcRoi.roih, yuvDispMskCfg );
                }

//                DBG("trace\n");
#if 1
        if( srcCnt && dstCnt ) {
//            DBG("trace\n");
            ret = ybfwGfxObjectCdspDo( srcImg, dstImgSrcRoi, dstImg,
                dstCnt | ( srcCnt << 8 ) | appendparam | tmpSpeedCtrl,
                YBFW_GFX_OBJECT_CDSP_DO_YUV, srcImgIQinfo[0],
                YBFW_MODE_STILL_PREVIEW,  &param );
//            DBG("trace\n");
            if(ret == FAIL) HOST_ASSERT_MSG(0,"CDSP DO err!!!");
        }
#endif
        if(isPipToggle)
            ybfwPipAttrSet(pipChnl, YBFW_PIP_TOGGLE, 1, 0, 0, 0);

    }

}
#endif
ErrCode_t mcThreadScaleStart(){
    ErrCode_t err;
    ybfwPrevUrgentCallback_t pvcb;
    ybfwSenId_t senId = CUR_SENSOR;
    UINT32 rawId = 0;
    if (!pctx) pctx = ctx_getmcContext();

    LOG_IF_ERROR(mc_senGetRawYuvId(senId, &rawId, NULL));
#ifdef EXTEND_VERSION
    DBG("trace\n");
    RETURN_IF_ERROR(scalerThreadStart());

//
    DBG("trace\n");
    /* reg rawCb to proc raw data to yuv */
    pvcb.ctrl     = 0;
    pvcb.interval = 1;
    pvcb.fp       = grRawCb; //pvRawCbDirectShow;
    LOG_IF_ERROR(ybfwVideoUrgentCallbackSet(rawId, &pvcb));


    DBG("trace\n");
#else
    DBG("enable mode DISABLE EXTEND version");
#endif

    return ERR_SUCCESS;
}
ErrCode_t mcThreadScaleStop(){
    ErrCode_t err;
    ybfwPrevUrgentCallback_t pvcb;
    UINT32 rawId = 0;
    ybfwSenId_t senId = CUR_SENSOR;
    if (!pctx) pctx = ctx_getmcContext();

    LOG_IF_ERROR(mc_senGetRawYuvId(senId, &rawId, NULL));

    DBG("trace\n");
    pvcb.ctrl     = 0;
    pvcb.interval = 0;
    pvcb.fp       = NULL;
    err = ybfwPreviewUrgentCallbackSet (rawId, &pvcb);
    if (err != ERR_SUCCESS){
        ERR("Preview urgent callback does't unset");
    }
    DBG("trace\n");
    LOG_IF_ERROR(scalerThreadStop());

    return ERR_SUCCESS;
}

