
#ifndef MEDIA_CORE_MC_THREAD_SCALE_H_
#define MEDIA_CORE_MC_THREAD_SCALE_H_

/**
 * Start scale thread
 *
 * @return result
 */
ErrCode_t mcThreadScaleStart();

/**
 * Stop scale thread
 *
 * @return result
 */
ErrCode_t mcThreadScaleStop();


#endif /* MEDIA_CORE_MC_THREAD_SCALE_H_ */
