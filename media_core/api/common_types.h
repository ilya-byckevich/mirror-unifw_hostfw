/**
 * @file common_types.h
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef MEDIA_CORE_API_COMMON_TYPES_H_
#define MEDIA_CORE_API_COMMON_TYPES_H_



/**
 * Axis
 */
typedef enum axis_t{
    AXIS_START = 0,
    AXIS_X = AXIS_START,//!< AXIS_X  X position
    AXIS_Y,    //!< AXIS_Y  Y positions
    AXIS_TOT,  //!< AXIS_TOT total
}Axis;

typedef enum switch_state_t { //NAV TODO to common
    SWITCH_START = 0,
    SWITCH_ON = SWITCH_START,
    SWITCH_OFF,
    SWITCH_TOT,
}SwitchState;


#endif /* MEDIA_CORE_API_COMMON_TYPES_H_ */
