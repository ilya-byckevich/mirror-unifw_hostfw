/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef MEDIA_CORE_MEDIA_CORE_API_H_
#define MEDIA_CORE_MEDIA_CORE_API_H_



#include "ykn_bfw_api/ybfw_errors.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"

#include "common_types.h"

#include "autoconf.h"


/**
 * Zoom Increment alg
 */
typedef enum mc_zoom_step_mode_t{
    MCZSM_STEP = 0,//!< MCZSM_STEP  - level 1x 2x 4x ...
    MCZSM_SMOOTH,  //!< MCZSM_SMOOTH  - current zoomFactor + zoomStep
    MCZSM_BALANCE, //!< MCZSM_BALANCE - balanced zoom zoomFactor + zoomStep*X
}McZoomStepMode;

/**
 * Zoom Direction
 */
typedef enum mc_zoom_direction_t{
    MCZD_INC,//!< MCZD_INC  - Increment
    MCZD_DEC,//!< MCZD_DEC  - Decrement
}McZoomDirection;



/**
 * Scale General Mode
 */
typedef enum mc_scale_mode_t{
 MCSCALE_MODE_DIGITAL,//!< SCALE_MODE_DIGITAL  - Digital zoom mode
 MCSCALE_MODE_MICRO,  //!< SCALE_MODE_MICRO - Micro Zoom mode
}McScaleMode;

/**
 * Scale config
 */
typedef struct mc_scale_cfg_t{
    McScaleMode  mode;  //!< mode - mode
    SINT32 factorMin; //!< factor_min  - min factor  (*1000)
    SINT32 factorMax; //!< maxFactor  - max factor  (*1000)
    SINT32 step;      //!< default step of zoom (*1000)
}McScaleCfg;

/**
 * Zoom config
 */
typedef struct zoom_state_t{
    SINT32    zmFactor; //!< Current Zoom factor
    UINT8     zmPIP;    //!< PiP mode enabled
    SINT32       zmPointShiftMax[AXIS_TOT];   ///< max adjustment [0]-x , [1] - y
    SINT32       zmPointShiftCur[AXIS_TOT];  ///< current Ajustment
}ZoomState;



typedef struct mc_device_cfg{
    UINT32 cur_res[AXIS_TOT];  ///< current resolution
    UINT32 ratio[AXIS_TOT];     ///< ration [0] - x [1] - y
    UINT32 max_res[AXIS_TOT];  ///< max resolution
}McDeviceCfg;



/**
 * Draw windows type
 */
typedef enum win_types_t{
    WT_START = 0,
    WT_MAIN = WT_START, //!< WT_MAIN  - Main window
    WT_PIP1,            //!< WT_PIP1  - PiP 1
    WT_PIP2,            //!< WT_PIP2  - PiP 2

    WT_TOT
}WinTypes;

//typedef struct{
//    SINT32        drawMaxSizeOSD[AXIS_TOT];
//    SINT32        drawMaxSizeVID[AXIS_TOT];
//    SINT32        drawMaxShiftAdj[AXIS_TOT];   ///< max adjustment [0]-x , [1] - y
//    SINT32        drawCurShiftAdj[AXIS_TOT];
//    ybfwGfxRoi_t  drawSelRoiOSD;
//    ybfwGfxRoi_t  drawSelRoiVID[WT_TOT];
//}mcDrawCfg_t;


/**
 *
 */
typedef enum mc_bias_type_t{
    MCBIAS_TYPE_START = 0,
    MCBIAS_TYPE_SEN = MCBIAS_TYPE_START,//!< MCBIAS_TYPE_SEN  - sensor
    MCBIAS_TYPE_DISPCH,                  //!< MCBIAS_TYPE_DISPCH - display
    MCBIAS_TYPE_SCALE,                   //!< MCBIAS_TYPE_SCALE - display
    MCBIAS_TYPE_TOT
}McBiasType;

typedef enum mc_wincfg_op_t{
    MCWIN_CFG_OP_START = 0,
    MCWIN_CFG_OP_POS = MCWIN_CFG_OP_START,
    MCWIN_CFG_OP_ROI,
    MCWIN_CFG_OP_TOT,
}McWinCfgOp;

typedef struct mc_cfg_limits_t {

}McCfgLimits;

/**
 * Media Core Controls
 */
typedef enum{
   MCCFG_MIN=0,
   // controls
   MCCFG_CORE_STATE = MCCFG_MIN, //!< MCCFG_CORE_STATE (set)/(get) - core state, SwitchState (set) / SwitchState * (get))
   MCCFG_DEV_SENS_STATE,          //!< MCCFG_SENSOR  (set)/(get)  - sensor state  (param1: ybfwSenId_t ID, param2 : SwitchState (set) / SwitchState * (get))
   MCCFG_DEV_DISPCH_STATE,         //!< MCCFG_DISPLAY  (set)/(get)  - display state (param1: ybfwDispChnlId_t ID, param2 : SwitchState (set) / SwitchState * (get))
   MCCFG_DEV_WIN_STATE,            //!< MCCFG_DEV_WIN_STATE (set)/(get)  - set windows state
   // configs
   MCCFG_CFG_LIMITS,             //!< MCCFG_CFG_DEV_SENS /(get) param: McCfgLimits *
   MCCFG_CFG_DEV_SENS,           //!< MCCFG_CFG_DEV_SENS (set)/(get) - sensor cfg  (param1: ybfwSenId_t ID, param2 : McDeviceCfg (set) / McDeviceCfg * (get) )
   MCCFG_CFG_DEV_DISPCH,           //!< MCCFG_CFG_DEV_DISPCH (set)/(get) - display cfg (param1: ybfwDispChnlId_t ID, param2 : McDeviceCfg (set) / McDeviceCfg * (get) )
   MCCFG_CFG_SCALE,              //!< MCCFG_CFG_SCALE  (set)/(get) - scale cfg (param1: McScaleCfg (set) / McScaleCfg * (get))
   MCCFG_CFG_BIAS,                //!< MCCFG_CFG_BIAS (set)/(get)  - bias (param1: McBiasType, param2: ybfwSenId_t or ybfwDispChnlId_t  param3: SINT32 val[AXIS_TOT])
   MCCFG_CFG_WINDOW,         //!< MCCFG_CFG_WINDOW (set)/(get) -widow pos (param1 ybfwDispChnlId_t, param2:WinTypes, McWinCfgOp, param3:SINT32 pos[AXIS_TOT] or ybfwGfxRoi_t )
   MCCFG_MAX,
}mcCtrl_t;


/**
 * Create media core
 * @return error code
 */
ErrCode_t mcCoreCreate();

/**
 * Destroy media core
 * @return error code
 */
ErrCode_t mcCoreDestroy();

/**
 * Set configuration
 * @param ctrl selector
 * @return error code
 */
ErrCode_t mcCoreCfgSet(mcCtrl_t ctrl, ...);
/**
 * Get configuration
 * @param ctrl selector
 * @return error code
 */
ErrCode_t mcCoreCfgGet(mcCtrl_t ctrl, ...);

/**
 * Activate window for zoomming
 * @param wt window type
 * @return error code
 */
ErrCode_t mcZoomWinSet(WinTypes wt);

/**
 * Get activated window for zooming
 * @param wt windows type
 * @return error code
 */
ErrCode_t mcZoomWinGet(WinTypes *wt);

/**
 * Set current zoom  (factor)
 * @param factor  value    McScaleCfg.factorMin <= factor  <= McScaleCfg.factorMax
 * @return error code
 */
ErrCode_t mcZoomFactorSet(UINT32 factor);

/**
 * Get current zoom
 * @param factor value
 * @return error code
 */
ErrCode_t mcZoomFactorGet(UINT32 *factor);



/**
 *  Increment zoom
 * @param mode - zoom mode
 * @param dir  - direction
 * @param factor return value of zoom
 * @return error code
 */
ErrCode_t mcZoomInc(McZoomStepMode mode,McZoomDirection dir, UINT32 * factor);





//NAV TODO  OSD API as Separate API

//Commands
/**
 * Init mc command group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 */
ErrCode_t cmdMcInit();
/**
 * Init mcgod command group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 */
ErrCode_t cmdMcGodModeInit();

/**
 * Process commands of mc group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 * @retval ERR_CMD_NOT_FOUND -  the command was not found in this group
 */
ErrCode_t cmdMc(int argc,char **argv,UINT32* v);
/**
 * Process commands of mcgod group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 * @retval ERR_CMD_NOT_FOUND -  the command was not found in this group
 */
ErrCode_t cmdMcGod(int argc,char **argv,UINT32* v);

/**
 * Print help of mc command group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 */
ErrCode_t cmdMcHelp();

/**
 * Print help of mcgod command group
 * @return error code
 * @retval ERR_CMD_SUCCESS - ok
 * @retval ERR_CMD_FAIL -  fail
 */
ErrCode_t cmdMcGodHelp();

#endif /* MEDIA_CORE_MEDIA_CORE_API_H_ */
