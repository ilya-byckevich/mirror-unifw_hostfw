/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#define FILE_ID "MC08"

#include "mc_osd.h"

#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_rsvblk_api.h"
#include "middleware/fs_def.h"
#include "mc_context.h"
#include "autoconf.h"

static mcContext_t * pctx = NULL;
static int gfxInited = 0;
static UINT8  animate_mode = 0;
static YBFW_THREAD *ptrRefreshTread = NULL;

typedef enum{
    OSDM_min = 0,
    OSDM_PANEL = OSDM_min,
    OSDM_PIP_RECT,
    OSDM_RETICLE,
    OSDM_TIMESTAMP,
    OSDM_max
}OSDMask;

#define OFFSET_RET_X  (+64)
#define OFFSET_RET_Y  (+64)


#define ID_FONT_ICON    "UI\\ICON\\ICON_32.SFN"
#define ID_FONT_DATESTAMP "UI\\FONT\\DATSTAMP.SFN"

#define PIP_COLOR565_WHITE          0xFFFF
#define PIP_COLOR565_RED            0xF800
#define PIP_COLOR565_GREEN          0x07E0
#define PIP_COLOR565_BLUE           0x001F
#define PIP_COLOR565_GRAY           ((0x0F << 11)|(0x1F << 5)|(0x0F << 0))

#define ID_ICON_BATTERY_25      0x0007
#define ID_ICON_BATTERY_50      0x0008
#define ID_ICON_BATTERY_75      0x0009
#define ID_ICON_BATTERY_CHARGING        0x000a
#define ID_ICON_BATTERY_EMPTY       0x000b
#define ID_ICON_BATTERY_FULL        0x000c
#define ID_ICON_BATTERY_HALF        0x000d
#define ID_ICON_BATTERY_LOW     0x000e
#define ID_ICON_BATTERY_THREE       0x000f

#define DEFAULT_OSDDRAW  ((1 << OSDM_PANEL)| (1<< OSDM_PIP_RECT)|(1<< OSDM_RETICLE)|(1<< OSDM_TIMESTAMP))
static UINT32 cur_draw_mask =  DEFAULT_OSDDRAW;// (1<< OSDM_RETICLE);


UINT32 appOsdSFNFileLoad(UINT32 id, char *pfilename)
{
    #define fileHeaderSize  0x3F
    #define SnappyFlag      0x50414e53
    #define LZOFlag         0xFA820BD0

    UINT8 ext[4];
    UINT8 fileHeader[fileHeaderSize];
    UINT32 fd;
    UINT32 fileFlag = 0;
    UINT32 fileSize=0;
    UINT32 ret = SUCCESS;
    UINT8 fileName[128];
    UINT32 idBufSize;

    if ( strlen( pfilename ) < 4 ) {
        return FAIL;
    }
    if ( pfilename[ strlen( pfilename ) - 4 ] != '.' ) {
        return FAIL;
    }

    strncpy( ext, pfilename + strlen( pfilename ) - 3, 3 );
    ext[3] = 0;

    if ( 0 != strncmp( ext, "SFN",3) ) {
        ERR("Only support SFN file\n");
        return FAIL;
    }

    sprintf(fileName,"A:\\RO_RES\\%s",pfilename); // add prefix
    fd = ybfwFsFileOpen(fileName,FS_OPEN_RDONLY | FS_OPEN_BINARY);
    if(!fd){
        ERR("%s open fail\n",fileName);
        return FAIL;
    }

    ybfwFsFileRead(fd,fileHeader,fileHeaderSize);

    //check SFN header
    if((strncmp(fileHeader + 0x08,"SPFNmark",8)!=0) || (strncmp(fileHeader+0x14,"Sunplus ",8)!=0)){
        ERR("Not support!\n");
        return FAIL;
    }

    ybfwFsFileSeek(fd,0x36,YBFW_FS_SEEK_SET);
    ybfwFsFileRead(fd,(UINT8 *)&fileFlag,sizeof(UINT32));

    if(fileFlag == SnappyFlag){
        DBG("Snappy file:%s\n", pfilename);
        fileFlag = 1;
    }else if(fileFlag == LZOFlag){
        DBG("LZO file:%s\n", pfilename);
        fileFlag = 2;
    }else{
        fileFlag = 0;
    }

    if(fileFlag == 1){
        ybfwFsFileSeek(fd,0x04,YBFW_FS_SEEK_SET);
        ybfwFsFileRead(fd,(UINT8 *)&fileSize,sizeof(UINT32));
        fileSize += 8;
    }else if(fileFlag == 2){
        ybfwFsFileSeek(fd,0x3A,YBFW_FS_SEEK_SET);
        ybfwFsFileRead(fd,(UINT8 *)&fileSize,sizeof(UINT32));
    }else{
        fileSize= ybfwFsFileSizeGet(fd);
    }

    ybfwFsFileClose(fd);

    idBufSize = _resBufArrGet(id);
    if(idBufSize < fileSize)
    {
        ERR( "ERROR!! allocated buf size(%d) less than dec file size(%d),please check \n",idBufSize,fileSize);
        return FAIL;
    }

    ret = ybfwResourceFileLoad(id,fileName);
    INFO("Load size = %d, %s\n",fileSize,(ret==SUCCESS)?"SUCCESS":"FAIL");
    return ret;
}



ErrCode_t appGfxOSDInit(UINT32 wSize, UINT32 hSize){
    ErrCode_t err= ERR_SUCCESS;
    DBG("trace");
    ybfwDispAttrSet(CUR_DISPLAY, YBFW_DISP_OSD_WINDOW, 0, 0, wSize, hSize);

    /* osd page frame init */
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_AB_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_DEMO_MODE, 1, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_DISP_CHNL, CUR_DISPLAY, 0, 0, 0);

    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_PAGE_TOT, 1, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FMT, YBFW_GFX_FMT_RGB565, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_WINDOW, 0, 0, wSize, hSize);


    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PIP_PAGE_TOT, 0, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PIP_FMT, YBFW_GFX_FMT_RGB565, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PIP_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PIP_WINDOW, 0, 0, wSize, hSize);

    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PF_PAGE_TOT, 1, 0, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PF_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_PF_WINDOW, 0, 0, wSize, hSize);
    ybfwGfxInitCfgSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_VCT_PAGE_TOT, 0, 0, 0, 0);

    ybfwGfxAttrSet(CUR_DISPLAY_GFX_CHANNEL|YBFW_GFX_BLEND_FACTOR, 128, 0, 0, 0);

    ybfwGfxInitCfgSet(YBFW_GFX_CH_SEL, CUR_DISPLAY_GFX_CHANNEL, 0, 0, 0);
    ybfwGfxInit();
    ybfwGfxAttrSet(CUR_DISPLAY_GFX_CHANNEL | YBFW_GFX_PIC_ROTATE, YBFW_GFX_ROTATE_EXT, 0, 0, 0);
    ybfwDispAttrSet(CUR_DISPLAY, YBFW_DISP_OSD_ACTIVE, 1);
    gfxInited =1;

   // load font

    appOsdSFNFileLoad(YBFW_RES_OSD_ICON, ID_FONT_ICON);
    appOsdSFNFileLoad(YBFW_RES_OSD_FONT, ID_FONT_DATESTAMP);

    return err;
}

void drawAnimCircle(UINT32 dispWidth,UINT32 dispHeight){
    static int  step = 15;
    static  int posX = 0;

    if (posX < (dispWidth >> 2)){
        posX= (dispWidth >> 2);
        step = 15;
    }else if (posX > (dispWidth -(dispWidth >> 2))){
        posX= (dispWidth -(dispWidth >> 2));
        step = -15;
    }

    ybfwGfxAttrSet(YBFW_GFX_FILL_COLOR, PIP_COLOR565_RED, 0, 0, 0);
    ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_RED, 0, 0, 0);
    ybfwGfxCircleDraw(YBFW_GFX_PAGE_OSD_0,posX,dispHeight -(dispHeight >> 4),30);

    posX+=step;
}


void drawPrimitives(int dx, int dy) {

#define PANEL_HEIGHT  40
#define CROSS_SIZE  25
#define APPVIEW_ICON_SIZEX                  36
#define APPVIEW_ICON_SIZEY                  32
#if 0 //NAV GLOBAL disable
    UINT32 dispWidth = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_X];
    UINT32 dispHeight = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_Y];
    DBG("trace  display %d x %d", dispWidth,dispHeight);
    typedef enum {
        APP_BATT_LEVEL_0  = 0,  /* battery empty */
        APP_BATT_LEVEL_1,       /* 25%  */
        APP_BATT_LEVEL_2,       /* 50% */
        APP_BATT_LEVEL_3,       /* 75% */
        APP_BATT_LEVEL_FULL,    /* battery full */
        APP_BATT_LEVEL_TOTAL,
    } appBatteryLevel_e;

    static int battery_idx = APP_BATT_LEVEL_0;

    ybfwGfxPageClear(YBFW_GFX_PAGE_OSD_0, 0);

//    //draw animate circle
    if (animate_mode){
        drawAnimCircle(dispWidth,dispHeight);
    }

    tmx_t rtcn;
    static tmx_t prev_rtcn;
    ybfwRtcDateTimeGet( YBFW_DATE_TIME_OPTION, &rtcn);

    //TODO  investigate clean regions
//    if (prev_rtcn.tmx_sec != rtcn.tmx_sec){ //refresh one second
//        prev_rtcn = rtcn;
        //black panel on top

    // draw object Panel
    //draw battery
    int APPVIEW_BATTERY_X = (dispWidth - APPVIEW_ICON_SIZEX);
    int APPVIEW_BATTERY_Y = (PANEL_HEIGHT - APPVIEW_ICON_SIZEY)/2;
    if (IS_BIT_SET(cur_draw_mask,OSDM_PANEL)){


        ybfwGfxAttrSet(YBFW_GFX_FILL_COLOR, PIP_COLOR565_GRAY, 0, 0, 0);
        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_GRAY, 0, 0, 0);
        ybfwGfxRectDraw(YBFW_GFX_PAGE_OSD_0,0, 0,dispWidth,PANEL_HEIGHT);


        switch(battery_idx){
            case APP_BATT_LEVEL_0:
                ybfwGfxIconDraw(YBFW_GFX_PAGE_OSD_0, APPVIEW_BATTERY_X, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, ID_ICON_BATTERY_LOW);
                break;
            case APP_BATT_LEVEL_1:
                ybfwGfxIconDraw(YBFW_GFX_PAGE_OSD_0, APPVIEW_BATTERY_X, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, ID_ICON_BATTERY_25);
                break;
            case APP_BATT_LEVEL_2:
                ybfwGfxIconDraw(YBFW_GFX_PAGE_OSD_0, APPVIEW_BATTERY_X, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, ID_ICON_BATTERY_50);
                break;
            case APP_BATT_LEVEL_3:
                ybfwGfxIconDraw(YBFW_GFX_PAGE_OSD_0, APPVIEW_BATTERY_X, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, ID_ICON_BATTERY_75);
                break;
            case APP_BATT_LEVEL_FULL:
                ybfwGfxIconDraw(YBFW_GFX_PAGE_OSD_0, APPVIEW_BATTERY_X, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, ID_ICON_BATTERY_FULL);
                break;
            default:
                break;
        }
        battery_idx = (battery_idx+1) % APP_BATT_LEVEL_TOTAL;

    }

    if (IS_BIT_SET(cur_draw_mask,OSDM_TIMESTAMP)){
        //draw timestamp

        char stillStampDateBuf[128] = {0};
        sprintf(stillStampDateBuf,  "%02d/%02d/%d  %02d:%02d:%02d",
                rtcn.tmx_mday,rtcn.tmx_mon,rtcn.tmx_year+1900,
                rtcn.tmx_hour, rtcn.tmx_min, rtcn.tmx_sec);

        ybfwGfxStringDraw(YBFW_GFX_PAGE_OSD_0, 0, APPVIEW_BATTERY_Y, YBFW_GFX_ALIGN_TOP_LEFT, YBFW_GFX_ENCODE_ASCII, (UINT8 *)stillStampDateBuf);
    }




    if (IS_BIT_SET(cur_draw_mask,OSDM_RETICLE)){
        //------------ other graphic
#define RETICLE_SIZE 300
        //cross lines

        int x_c,y_c;
        x_c=dispWidth/2+OFFSET_RET_X/2;
        y_c=dispHeight/2+OFFSET_RET_Y/2;

        ybfwGfxAttrSet(YBFW_GFX_PEN_WIDTH, 1, 0, 0, 0);
        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_GREEN, 0, 0, 0);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c+dx-RETICLE_SIZE, y_c+dy, x_c+dx + RETICLE_SIZE, y_c+dy);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c+dx, y_c+dy-RETICLE_SIZE, x_c+dx, y_c+dy+RETICLE_SIZE);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0, 0, 1919, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0,1079,1919, 1079);

        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 0, 0, 0, 1079);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, 1919,0,1919, 1079);

        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_RED, 0, 0, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c, y_c-RETICLE_SIZE, x_c, y_c-RETICLE_SIZE+20);

        // center of screen
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, dispWidth/2-10, dispHeight/2, dispWidth/2 + 10, dispHeight/2);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, dispWidth/2, dispHeight/2-10, dispWidth/2, dispHeight/2+10);

        // reticle
        ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_BLUE, 0, 0, 0);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c-10, y_c, x_c + 10, y_c);
        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c, y_c-10, x_c, y_c+10);

    }

   // DBG("sec = %d\n",rtcn.tmx_sec);
    ybfwGfxAttrSet(YBFW_GFX_REFRESH_ACTIVE,1, 1, 0, 0);
#endif
}

ErrCode_t appGfxOSDTerm(){
    ErrCode_t err= ERR_SUCCESS;
    return err;
}


void _OsdRefrashThread(ULONG init_data)
{

    DBG("%s start\n",__FUNCTION__);

    while(1)
    {
        drawPrimitives(0,0);
        ybfwTimeDelay(YBFW_TIME_DELAY_1MS, 100);
    }
}


void animateSwitch(UINT32 on){

    if (on){
        if (!animate_mode){
            ptrRefreshTread = ybfwOsThreadCreate(
                "OsdRefrashThread",
                _OsdRefrashThread,
                0,
                8,
                8,
                5,
                YBFW_TX_AUTO_START);
            animate_mode = 1;
        }
    }else{
        if (animate_mode){
            if ( ptrRefreshTread) {
                ybfwOsThreadDelete(ptrRefreshTread);
                ptrRefreshTread = NULL ;
            }
            animate_mode = 0;
        }

    }
}


ErrCode_t mc_osdStart(){
    DBG("trace");
    if (!pctx) pctx = ctx_getmcContext();
#if 0
    if (!gfxInited){
        DBG("trace");
        appGfxOSDInit(pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_X],pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_Y]);
    }

    animateSwitch(1); // drawPrimitives(0,0);
#else
#define RETICLE_SIZE 100
#define DISPLAY_PV_WIDTH  1920
#define DISPLAY_PV_HEIGHT 1080
#define DISPLAY_GFX_CHANNEL YBFW_GFX_CH_0_FLAG
#define OFFSET_RET_X 0
#define OFFSET_RET_Y 0
#define PIP_COLOR565_WHITE          0xFFFF
#define PIP_COLOR565_RED            0xF800
#define PIP_COLOR565_GREEN          0x07E0
#define PIP_COLOR565_BLUE           0x001F
#define PIP_COLOR565_GRAY           ((0x0F << 11)|(0x1F << 5)|(0x0F << 0))
#define DISPLAY_CHANNEL YBFW_DISP_CHNL_1  //hdmi
    UINT32 dx = 0;
    UINT32 dy = 0;
#if 0 //NAV GLOBAL disable
    UINT32 wSize = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_X];
    UINT32 hSize = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_Y];
    UINT32 dispWidth = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_X];;
    UINT32 dispHeight = pctx->devCfg.dispSize[CUR_DISPLAY][MCADJCL_Y];


    //cross lines
    ybfwDispAttrSet(DISPLAY_CHANNEL, YBFW_DISP_OSD_WINDOW, 0, 0, wSize, hSize);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_AB_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_DEMO_MODE, 1, 0, 0, 0);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_DISP_CHNL, DISPLAY_CHANNEL, 0, 0, 0);

    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_PAGE_TOT, 1, 0, 0, 0);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FMT, YBFW_GFX_FMT_RGB565, 0, 0, 0);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FRAME_SIZE, wSize, hSize, 0, 0);
    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_WINDOW, 0, 0, wSize, hSize);

//    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_USR_PAGE_TOT, 1, 0, 0, 0);
//    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FMT, YBFW_GFX_FMT_RGB565, 0, 0, 0);
//    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_OSD_FRAME_SIZE, wSize, hSize, 0, 0);
//    ybfwGfxInitCfgSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_USR_WINDOW, 0, 0, wSize, hSize);

    ybfwGfxAttrSet(DISPLAY_GFX_CHANNEL|YBFW_GFX_BLEND_FACTOR, 128, 0, 0, 0);

    ybfwGfxInitCfgSet(YBFW_GFX_CH_SEL, DISPLAY_GFX_CHANNEL, 0, 0, 0);
    ybfwGfxInit();
    ybfwGfxAttrSet(DISPLAY_GFX_CHANNEL | YBFW_GFX_PIC_ROTATE, YBFW_GFX_ROTATE_EXT, 0, 0, 0);
    ybfwDispAttrSet(DISPLAY_CHANNEL, YBFW_DISP_OSD_ACTIVE, 1);


    int x_c,y_c;
    UINT32 center_X = pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW/2;
    UINT32 center_Y = pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH/2;

    x_c=center_X+OFFSET_RET_X/2;
    y_c=center_Y+OFFSET_RET_Y/2;



//#define GRID_STEP 16
//   ybfwGfxAttrSet(YBFW_GFX_PEN_WIDTH, 1, 0, 0, 0);
//    ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_GREEN, 0, 0, 0);
//    SINT32 pos_val = pctx->drawSelRoiOSD[CUR_DISPLAY].roiX;
//    do{
//        pos_val+=GRID_STEP;
//        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0,
//                pos_val, pctx->drawSelRoiOSD[CUR_DISPLAY].roiY, pos_val,
//                pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH);
//    }while(pos_val < (pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW));
//    pos_val = pctx->drawSelRoiOSD[CUR_DISPLAY].roiY;
//    do{
//        pos_val+=GRID_STEP;
//        ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0,
//                pctx->drawSelRoiOSD[CUR_DISPLAY].roiX,
//                pos_val,
//                pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW,
//                pos_val);
//    }while(pos_val < (pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH));


#define FREE_SPACE  0
#if 0
    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, 0,
            0,
            dispWidth - 1,
            pctx->drawSelRoiOSD[CUR_DISPLAY].roiY - FREE_SPACE,
            PIP_COLOR565_GRAY);

    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, 0,
            0,
            pctx->drawSelRoiOSD[CUR_DISPLAY].roiX-FREE_SPACE,
            dispHeight - 1,
            PIP_COLOR565_GRAY);

    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW + FREE_SPACE,
            0,
            dispWidth - 1,
            dispHeight - 1,
            PIP_COLOR565_GRAY);

    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, 0,
            pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH + FREE_SPACE,
            dispWidth - 1,
            dispHeight - 1,
            PIP_COLOR565_GRAY);
//#else
    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW + FREE_SPACE,
            0,
            dispWidth - 1,
            dispHeight - 1,
            PIP_COLOR565_GRAY);

    ybfwGfxRectFill(YBFW_GFX_PAGE_OSD_0, 0,
            pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH + FREE_SPACE,
            dispWidth - 1,
            dispHeight - 1,
            PIP_COLOR565_GRAY);
#endif
    ybfwGfxAttrSet(YBFW_GFX_PEN_WIDTH, 1, 0, 0, 0);
    ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_GREEN, 0, 0, 0);
    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX, //left
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiX,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH);

    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX, //top
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY);

    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW, //right
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH);

    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX, //botto,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY  + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH);

    ybfwGfxRectDraw(YBFW_GFX_PAGE_OSD_0, pctx->drawSelRoiOSD[CUR_DISPLAY].roiX, //left
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiX + pctx->drawSelRoiOSD[CUR_DISPLAY].roiW,
                                         pctx->drawSelRoiOSD[CUR_DISPLAY].roiY + pctx->drawSelRoiOSD[CUR_DISPLAY].roiH);

    // center of screen
    ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_WHITE, 0, 0, 0);
    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, center_X -20, center_Y, center_X + 20, center_Y);
    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, center_X, center_Y-20, center_X, center_Y+20);

    // reticle
    ybfwGfxAttrSet(YBFW_GFX_PEN_COLOR, PIP_COLOR565_BLUE, 0, 0, 0);
    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c-10, y_c, x_c + 10, y_c);
    ybfwGfxLineDraw(YBFW_GFX_PAGE_OSD_0, x_c, y_c-10, x_c, y_c+10);
    ybfwGfxAttrSet(YBFW_GFX_REFRESH_ACTIVE,1, 1, 0, 0);
#endif

#endif
    return ERR_SUCCESS;
}
ErrCode_t mc_osdStop(){
    if (!pctx) pctx = ctx_getmcContext();
    animateSwitch(0);
    return ERR_SUCCESS;
}
