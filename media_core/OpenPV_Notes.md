# TODO

* задать вопрос китайцам каково будет поведение если мы не будем использовать энкодинг

# возможности скейлинга. 

Под скейлингом подразумевается подраузмевается операция, которая позволяет 
преобразовать одно разрешение в другое.

## Скейлинг средствами сенсора 
В сенсоре поддерживаются разные режимы выставления разрешения. v50 позвоялет это делать Операция переключения занимает около 1 секунды (подтормаживает видео) из-за:

* для кадого режима есть AGC настройки, которые сразу же подгружаются.
* буфферы (их минимум 4-ре) сразу очищаются

В реалтайме перелючать нет смысла. Но имеет смысл использовать исходя из перерасчета на итоговую область вывода на экран.
 
Имеет смысл это задействовать в случае если будет в явном виде приниматся проектное решение о искуственном ограничении разрешения. 


## Скейлинг средствами сенсора c заданием ROI
В продолжении предыдущего. Мы еще можем  варьироваеть поле зрения сенсора.
Задавая ROI. ROI можно задать только в меньшую сторону. 
Есть ограничения в плане выравнивания данных во всех сырых форматах:

* выравнивание по ширине 8
* выравнивание по высото 2

Но тогда придетяся подстраивать и YUV так как UPScale не работает.
К каждому разрешению свои настройки AGC идут и AE их тоже нужно настраивать

в плане лагов - результат такой же. 





# результаты тестов и ограничения

1. raw кадр малого разрешения масштабируется до нужного (тупо растягивается)
2. буффер выше 4k нет смысла делать. будет большая задержка
4. если разрешение сенсора от fullHD машстабировать можем до 4k. но выше чем на 30 fpa можем не рассчитывать
5. тепловизионные сенсоры имеет смысл масштабировать до 2x на входном кадре, чтобы компенсировать выравнивание





# вопросы которые предстоит решить

1. режим фьюжен как будет реализован
    1. два сенсора с разными разрешениями один скейлится, другой нет.
    2. между сенсорами
2. режим PiP  - зум в картинке
    1. не понятно еще как ROI выравнивание компенсировать в PIP. возможно нужен будет промежуточный буффер
3. зум
4. запись экрана
5. стрим



#Насадка. специфика

Юстировка насадки <https://yukonww.atlassian.net/wiki/spaces/PR18/pages/1064927237> . То есть двигаем на сенсоре ROI

Юстировка угла дисплея <https://yukonww.atlassian.net/wiki/spaces/PR18/pages/1066991681>. Подразумевается двигание офсета дисплея  

Юстировка микрозума. <https://yukonww.atlassian.net/wiki/spaces/PR18/pages/1051590657> управление растягиванием. Разбежка на 0,3 




#Общая информация

не рекомендуют использовать разные разрешения YUV если в этом действительно нет смысла

Подумать над реализацией когда:

* всего одно разрешение идет потока на дисплей и на запись.
* делаем все на сыры


# калбэки которые мы можем навесить

__Preview display callback__ 

`ybfwDispUrgentCallbackSet( (ybfwPipCh_t)pipChnl, (ybfwPrevUrgentCallback_t *)ppvCbCfg)` Host must install this callback to get YUV image data from pvCdsp then draw it on PIPlayer to show on display , if custom display is required.

Как мы можем использовать:

* с помощью этого калбэка будет выполняться операция зума. а не стандартный


__Video preview YUV callback__ 

`ybfwVideoUrgentCallbackSet( (ybfwModePreviewYuvList_t)yuvMap, (ybfwPrevUrgentCallback_t *)ppvCbCfg)` This urgent callback function will be called each YUV frame in video preview mode.

Как мы можем использовать

* если будет приемлимая задержка - то можем исползовавать при записи видео с экраном. 
* если есть возможность и стримить - туда же пошлем

__Video preview raw callback__ 

`ybfwVideoUrgentCallbackSet( (ybfwModePreviewRawList_t)rawMap, (ybfwPrevUrgentCallback_t *)ppvCbCfg)` This urgent callback function will be called each raw frame in video preview mode.

как будем использовать:

* будем забирать для захвата сырых данных.


_ybfwPreviewCallbackSetis obsolete from V50. Host must finish these urgent callback function as soon as possible, otherwise skip frame will happens or preview stuck._ 



# Выравнивание данных в буфферах

мы используем

    #define PV_DFLT_RAWBIT_MODE  YBFW_IMG_FMT_RAW_8BIT
    #define PV_DFLT_FMT  YBFW_IMG_FMT_YUV422_NV16

выравнивание определено следующее


<table >
<tbody>
    <tr>
       <th> Raw</th>
       <th> bufW</th>
       <th> bufH</th>
       <th> roiX</th>
       <th> roiY </th>
       <th> roiW </th>
      <th> roiH </th>
    </tr>
    <tr>
       <td><code>YBFW_IMG_FMT_RAW_8BIT</code></td>
       <td> 8 </td>
       <td> 2 </td>
       <td> 8 </td>
       <td> 2 </td>
       <td> 8 </td>
       <td> 2</td>
    </tr>
    <tr>
       <td><code>YBFW_IMG_FMT_RAW_12BIT</code></td>
       <td> 8 </td>
       <td> 16 </td>
       <td> 8 </td>
       <td> 2 </td>
       <td> 8 </td>
       <td> 2</td>
    </tr>
    <tr>
       <td><code>YBFW_IMG_FMT_RAW_16BIT</code></td>
       <td> 8 </td>
       <td> 2 </td>
       <td> 8 </td>
       <td> 2 </td>
       <td> 8 </td>
       <td> 2</td>
    </tr>
    <tr>
       <td><code>YBFW_IMG_FMT_RAW_V50</code></td>
       <td> 16 </td>
       <td> 4 </td>
       <td> 16 </td>
       <td> 4 </td>
       <td> 16 </td>
       <td> 4 </td>
    </tr>
    <tr>
      <td><code>YBFW_IMG_FMT_YUV422_NV16</code></td>
        <td> 32 </td>
        <td> 16 </td>
        <td> 2 </td>
        <td> 2 </td>
        <td > 2 </td>
        <td > 2 </td>
    </tr>
    <tr>
       <td><code>YBFW_IMG_FMT_YUV420_NV16</code></td>
       <td > 32 </td>
       <td > 16 </td>
       <td > 2 </td>
       <td > 2 </td>
       <td > 2 </td>
       <td > 2 </td>
    </tr>
    <tr class="row7">
       <td ><code>YBFW_IMG_FMT_YUV420_V50</code> </td>
       <td > 32 </td>
       <td > 16 </td>
       <td > 8 </td>
       <td > 8 </td>
       <td > 8 </td>
       <td > 8 </td>
    </tr>
</tbody>
</table>



Limitation in V50 pv ZoomRoi (YUV source Roi):

<table>
    <tbody>
    <tr>
        <th >Yuv Format </th>
        <th> YroiX &amp; wight of YUV Src  </th>
        <th > roiY &amp; height of YUV Src </th>
    </tr>
    <tr class="row1">
        <td ><code>YUV422_NV16</code></td>
        <td > 4N </td>
        <td > 2N </td>
    </tr>
    <tr class="row2">
        <td ><code>YUV420_NV16</code></td>
        <td > 4N </td>
        <td > 2N </td>
    </tr>
    <tr class="row3">
        <td ><code>YUV420_V50</code></td>
        <td > 4N </td>
        <td > 2N </td>
    </tr>
</tbody></table>


    buf[3][USED] addr 0x42b7a980, buf 0x42b7bb80, sn 2509
    Rls: 0000001000000000 (Qen: 0000001000000000)
    DoYuv: 5 ms, Lag 0 ms <====Raw2Yuv take 5ms,(the laterncy from raw eof to raw2yuv is 5 ms)
    Lvl 2: PvCB, Lag 5 ms, held 3 ms <====Pv urgent callback take 3ms, (the laterncy from raw eof to PvCB is 5 ms)
    Lvl 3: PvDisp, Lag 8 ms, holding <====PvDisp is using this buf, (the laterncy from raw eof to PvDisp is 8 ms)
    : Video, Lag 8 ms, holding <====Video is using this buf, (the laterncy from raw eof to Video is 8 ms)



###  GFX

__выравнивание gfx объектов и лимиты__

Data format Limitation 

Source Settings

<table class="inline">
    <tr class="row0">
        <th class="col0 leftalign"> Format  </th><th class="col1 leftalign"> Limit  </th>
    </tr>
    <tr class="row1">
        <td class="col0"> YUV plane</td><td class="col1"> bufW MUST be 32X, bufH MUST be 2X </td>
    </tr>
</table


Destination setting

<table class="inline">
    <tr class="row0">
        <th class="col0 leftalign"> Format  </th><th class="col1 leftalign"> Limit  </th>
    </tr>
    <tr class="row1">
        <td class="col0"> YUV_NV16 plane </td><td class="col1"> Src and Dst bufW MUST be 32X, bufH MUST be 2X </td>
    </tr>
    <tr class="row2">
        <td class="col0 leftalign">  </td><td class="col1"> Src and Dst RoiX, RoiY MUST be 2X </td>
    </tr>
    <tr class="row3">
        <td class="col0 leftalign">  </td><td class="col1"> Src and Dst RoiW, RoiH MUST be 2X </td>
    </tr>
    <tr class="row4">
        <td class="col0"> YUV_V50 plane </td><td class="col1"> Src and Dst bufW MUST be 32X, bufH MUST be 8X </td>
    </tr>
    <tr class="row5">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiX, RoiY MUST be 2X, Dst RoiX, RoiY MUST be 8X </td>
    </tr>
    <tr class="row6">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiW, RoiH MUST be 2X, Dst RoiW, RoiH MUST be 8X </td>
    </tr>
    <tr class="row7">
        <td class="col0"> Y_NV16 plane </td><td class="col1"> Src and Dst bufW MUST be 32X, bufH MUST be 1X </td>
    </tr>
    <tr class="row8">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiX, RoiY MUST be 1X, Dst RoiX, RoiY MUST be 1X </td>
    </tr>
    <tr class="row9">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiW, RoiH MUST be 1X, Dst RoiW, RoiH MUST be 1X </td>
    </tr>
    <tr class="row10">
        <td class="col0"> IDX8 plane </td><td class="col1"> Src and Dst bufW MUST be 32X, bufH MUST be 1X </td>
    </tr>
    <tr class="row11">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiX, RoiY MUST be 1X, Dst RoiX, RoiY MUST be 1X </td>
    </tr>
    <tr class="row12">
        <td class="col0 leftalign">  </td><td class="col1"> Src RoiW, RoiH MUST be 1X, Dst RoiW, RoiH MUST be 1X </td>
    </tr>
</table>


Resolution Limitation: maximum WxH   16384x16384


Function Limitation

* Scaling: ROI minimum size is 2*2, no obvious for scaling rate.
* Blend: None of blending API (global or per-pixel blend) support scale .



[ybfwGfxObjectScale()](https://k-public2.icatchtek.com/doku.php?id=public:api:sp5kgfxobjectscale)

Scale ROI region of the source GFX object to ROI of destination GFX object. Note that this API return FAIL if source ROI size equals to destination ROI size.

Examples:

* [gfx object sample](https://k-public2.icatchtek.com/doku.php?id=public:sample:gfxobj_demo)
* [idx8 format scale : Y only scale](https://k-public2.icatchtek.com/doku.php?id=public:sample:gfxobj_idx8_scale)
* [Gfx two pass scale in vertical](https://k-public2.icatchtek.com/doku.php?id=public:sample:gfxobj_vertical_two_pass_scale)


<table class="inline">
    <tr class="row0">
        <th> Platform </th><th> Engine </th><th> Supported format </th>
    </tr>
    <tr class="row1">
        <td> V37</td><td> MAE </td><td><code>YBFW_GFX_FMT_YUV422</code>, <code>YBFW_GFX_FMT_YUV420</code>, <code>YBFW_GFX_FMT_YUV420_V50</code><br/>
<code>YBFW_GFX_FMT_YUV422_Y</code>, <code>YBFW_GFX_FMT_YUV422_UV</code>, <code>YBFW_GFX_FMT_YUV420_UV</code><br/>
<code>YBFW_GFX_FMT_IDX8</code>, <code>YBFW_GFX_FMT_RGB565</code></td>
    </tr>
    <tr class="row2">
        <td class="col0 leftalign"> V50      </td><td class="col1"> IME </td><td class="col2"><code>YBFW_GFX_FMT_YUV422</code>, <code>YBFW_GFX_FMT_YUV420</code>, <code>YBFW_GFX_FMT_YUV420_V50</code><br/>
<code>YBFW_GFX_FMT_YUV422_Y</code>, <code>YBFW_GFX_FMT_YUV422_UV</code>, <code>YBFW_GFX_FMT_YUV420_UV</code><br/>
<code>YBFW_GFX_FMT_IDX8</code></td>
    </tr>
</table>


[sp5kGfxObjectBilinearScale()](https://k-public2.icatchtek.com/doku.php?id=public:api:sp5kgfxobjectbilinearscale)

Bilinear scale ROI region of the source GFX object to ROI of destination GFX object with GPE HW engine.

Example

* [yuv422 format scale](https://k-public2.icatchtek.com/doku.php?id=public:sample:gfxobj_yuv422_scale)

<table>
    <tr class="row0">
        <th class="col0"><abbr title="Application Programming Interface">API</abbr> </th>
        <th class="col1"> HW &amp; algorithm </th>
    </tr>
    <tr class="row1">
        <td class="col0"><code>sp5kGfxObjectBilinearScale()</code></td>
        <td class="col1"> IME bilinear</td>
    </tr>
    <tr class="row2">
        <td class="col0"><code>sp5kGfxObjectScale()</code></td>
        <td class="col1"> scale down: <abbr title="Matrix Operation Engine">MOE</abbr> ACC<br/>
scale up: <abbr title="Matrix Operation Engine">MOE</abbr> hermite</td>
    </tr>
    <tr class="row3">
        <td class="col0"><code>sp5kGpeScale</code> </th>
        <td class="col1"> <abbr title="Matrix Operation Engine">MOE</abbr> ACC&amp;hermite + IME hermite</td>
    </tr>
</table>


[sp5kGfxObjectCopy()](https://k-public2.icatchtek.com/doku.php?id=public:api:sp5kgfxobjectcopy)

Copy the ROI region from the frame buffer of the source gfx object to that of the destination gfx object. This api only supports on SPCA5330 or later.

Scaling is not supported on this API. 

Examples:

* [gfx object sample](https://k-public2.icatchtek.com/doku.php?id=public:sample:gfxobj_demo)
* [extract y component then bilinear scale](https://k-public2.icatchtek.com/doku.php?id=public:sample:exycopyandscale)


__ybfwGfxObjectMapping()__
Mapping the ROI region from the frame buffer of the source gfx object to the destination gfx object. 

V50 support the following image format :

* YBFW_GFX_FMT_YUV422
* YBFW_GFX_FMT_YUV420
* YBFW_GFX_FMT_YUV420_V50
* YBFW_GFX_FMT_YUV422_Y
* YBFW_GFX_FMT_YUV422_UV
* YBFW_GFX_FMT_YUV420_UV

To avoid HW resource conflict, you can replace ybfwGfxObjectCopy() (using MOE engine) by using ybfwGfxObjectMapping() (using IME HW) with following code.

``` 
void Sample_Copy(void)
{
    ybfwGfxObj_t src, dst;
    ybfwGfxMapProjectParam_t  proPrm;
    memset(&proPrm, 0x0, sizeof(proPrm));
 
    ... /* need src roiw&roih == dst roiw&roih */ 
 
    proPrm.mapAttr.itpAlg = YBFW_GFX_MAP_ITP_NEAREST_NEIGHBOR;
    proPrm.vec.src[0].x = src.roiX;
    proPrm.vec.src[0].y = src.roiY;
    proPrm.vec.src[1].x = src.roiX+src.roiW-1;
    proPrm.vec.src[1].y = src.roiY;
    proPrm.vec.src[2].x = src.roiX+src.roiW-1;
    proPrm.vec.src[2].y = src.roiY+src.roiH-1;
    proPrm.vec.src[3].x = src.roiX;
    proPrm.vec.src[3].y = src.roiY+src.roiH-1;
 
    proPrm.vec.dst[0].x = dst.roiX;
    proPrm.vec.dst[0].y = dst.roiY;
    proPrm.vec.dst[1].x = dst.roiX+dst.roiW-1;
    proPrm.vec.dst[1].y = dst.roiY;
    proPrm.vec.dst[2].x = dst.roiX+dst.roiW-1;
    proPrm.vec.dst[2].y = dst.roiY+dst.roiH-1;
    proPrm.vec.dst[3].x = dst.roiX;
    proPrm.vec.dst[3].y = dst.roiY+dst.roiH-1;
    /* background color as Black*/
    proPrm.mapAttr.bgClrY = 0x00;
    proPrm.mapAttr.bgClrU = 0x80;
    proPrm.mapAttr.bgClrV = 0x80;
 
    ybfwGfxObjectMapping(&src, &dst, YBFW_GFX_MAPPING_ID_PROJECT, (UINT32)&proPrm);
}
```

__ybfwGfxObjectConvert()__

Convert source GFX object to destination GFX object.

### анализ производительности
команда pvbuf




#выдержки по openPV

In the same Raw stream, Y uv Must be done according to the timing of Raw's fid (sequential ID of this frame):

* First Input First Do.
* Ex: Can do this (fid=30, fid=31, fid=35, fid=40, fid=41, fid=45).
* Ex: CANNOT do this (fid=30, fid=31, fid=35, fid=34 (NG !!!, fid=35 has been acquired)).

С точки зрения openpv мы должны получив один сырой кадр пройтись по всем YUV и преобразовать их. Вопрос вознимакет. нужно ли по всем 4 колбасится если не все они включены. 

