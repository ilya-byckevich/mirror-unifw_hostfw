/*
 * mc_common.h
 *
 *  Created on: Mar 19, 2020
 *      Author: naydinav
 */

#ifndef MEDIA_CORE_MC_COMMON_H_
#define MEDIA_CORE_MC_COMMON_H_

#include <stdarg.h>

#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "app_common.h"

/**
 * supported resolutions
 */
typedef enum{
    REST_min,
    REST_QVGA = REST_min,
    REST_VGA,
    REST_SVGA,
    REST_XGA,
    REST_720P,
    REST_900P,
    REST_1080P,
    REST_2160P,
    REST_max,
}ResolutionTypes_t;

/**
 * Resolution Info
 */
 typedef struct {
    UINT8 *  name;
    UINT8 *  resTxt;
    UINT32 resW;
    UINT32 resH;
    UINT32 ratioW;
    UINT32 ratioH;
 }ResolutionInfo_t;

 /**
  * Keys to get resolution info
  */
 typedef enum{
    RK_NAME = 0, ///by Name.  param1: string name
    RK_TXT,     ///by Text param1: string resolution
    RK_TYPE,    ///by Type param1: ResolutionTypes_t
    RK_WxH,    ///by Width and height. param1: UINT32 width, param2: UINT32 height
    RK_FIND,   ///Find closed. param1 : UINT32 ratioW, param2: UINT32 ratioH, param3: UINT32 known_side (0 - W, 1-H), param4 : UINT32 side_val);
 }ResolutionKey_t;

 /**
  * get resolution info by key
  */
 ResolutionInfo_t * mcGetResolInfo(ResolutionKey_t key,...);

typedef struct cfg_parser_t{

};
 /**
  * Mapping 2 buffers
  * @param p_srcObj  source
  * @param p_dstObj  destination
  * @param stretch   is stretch source to destination without aspect ration
  * @param bg_color  background coloer (gradient of gray. 0x00 - black, 0xFF - white)
  * @param interpolation type of interpolation
  * @return
  */
 UINT32 mappingBuffers(ybfwGfxObj_t * src_obj,ybfwGfxObj_t * dst_obj, UINT8 stretch, UINT8 bgcolor, UINT8 interpolation);




#define EXTEND_VERSION

#endif /* MEDIA_CORE_MC_COMMON_H_ */
