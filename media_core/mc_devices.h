/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef V50mc_DEVICES_H_
#define V50mc_DEVICES_H_

#include "ykn_bfw_api/ybfw_cdsp_api.h"


ErrCode_t mc_senInit();
ErrCode_t mc_senGetRawYuvId(ybfwSenId_t senId,UINT32 * rawId, UINT32 * yuvId);
ErrCode_t mc_senRelease();

ErrCode_t mc_lcdInit();
ErrCode_t mc_lcdRelease();

ErrCode_t mc_hdmiInit();
ErrCode_t mc_hdmiRelease();

ErrCode_t mc_tvoutInit();
ErrCode_t mc_tvoutRelease();



ErrCode_t mc_audInit();
ErrCode_t mc_audStop();



#endif /* V50mc_DEVICES_H_ */
