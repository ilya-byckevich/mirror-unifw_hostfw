/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef MEDIA_CORE_MC_DEVICES_SENCFGS_H_
#define MEDIA_CORE_MC_DEVICES_SENCFGS_H_

#include "app_common.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"


typedef struct sens_mode_cfg_t {
    UINT32 w;
    UINT32 h;
    UINT32 fps;
    UINT32 cdspfps;
    UINT32 smode;
    UINT32 agc;
    UINT32 exp;
}SensModeCfg;

/**
 *  get sensor mode config
 *
 * @param sen_id  sensor id
 * @param mode_str   mode in string format (W;H;FPS)
 * @retval NULL  if mode was not found
 */
SensModeCfg * getSensModeCfg(ybfwSenId_t sen_id, UINT8 * mode_str);

#endif /* MEDIA_CORE_MC_DEVICES_SENCFGS_H_ */
