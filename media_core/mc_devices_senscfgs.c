/**
 * @file mc_devices_senscfgs.c
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/


#define FILE_ID "MC04"
#include "ykn_bfw_api/ybfw_dbg_api.h"

#include "mc_devices_sencfgs.h"
#include "autoconf.h"


//NAV TODO Add directives of compilation to reduce of range sensors
#if defined(CFG_HAL_SENS_0_TYPE_PANAMN34110) || defined(CFG_HAL_SENS_1_TYPE_PANAMN34110)

static SensModeCfg sencfg_PANAMN34110 [] = {
     {3840,2160, 30,30, 0xC, 160, 160}, /* 4k */
     {1920,1080, 60,60, 0x2, 160, 160}, /* fhd 60 mode12-59.999fps*/
     {1920,1080, 30,30, 0xA, 160, 160}, /* fhd 30 mode12-30fps*/
     {1280,720,  60,60, 0x2, 160, 160}, /* 720p 60*/
     {1280,720,  30,30, 0xA, 160, 160}, /* 720p 30*/
};
#endif

#if defined(CFG_HAL_SENS_0_TYPE_IMX291) || defined(CFG_HAL_SENS_1_TYPE_IMX291)
static SensModeCfg sencfg_SONYIMX291[] = {
    {1920,1080,60,60, 0x0, 160, 160}, /* fhd 60 fps*/
    {1920,1080,30,30,0x1, 160, 160}, /* fhd 30 fps*/
    {1280,720 ,60,60,0x3, 160, 160}, /* 720p */
    {1280,720 ,30,30,0x4, 160, 160}, /* 720p */
};
#endif

#if defined(CFG_HAL_SENS_0_TYPE_FAKE) || defined(CFG_HAL_SENS_1_TYPE_FAKE)
static SensModeCfg sencfg_FAKE[] = {
    {1920,1080,30,0, 0x3, 160, 160}, /* fhd*/
    {1280,720 ,30,0,0x2, 160, 160}, /* 720p */
};
#endif



/**
 * Translate kBuild CFG to array for sensor#1
 * @param len  size of array
 * @return SensModeCfg array
 */
SensModeCfg * getSens0Array(SINT32 * len){
#if defined(CFG_HAL_SENS_0_TYPE_PANAMN34110)
    if (len) *len=sizeof(sencfg_PANAMN34110)/sizeof(*sencfg_PANAMN34110);
    return sencfg_PANAMN34110;
#elif defined(CFG_HAL_SENS_0_TYPE_IMX291)
    if (len) *len=sizeof(sencfg_SONYIMX291)/sizeof(*sencfg_SONYIMX291);
    return sencfg_SONYIMX291;
#elif defined(CFG_HAL_SENS_0_TYPE_FAKE)
    if (len) *len=sizeof(sencfg_FAKE)/sizeof(*sencfg_FAKE);
    return sencfg_FAKE;
#else
    return NULL;
#endif

}

/**
 * Tranclate kBuild CFG to array  sensor#2
 * @param len  size of array
 * @return SensModeCfg array
 */
SensModeCfg * getSens1Array(SINT32 * len){
#if defined(CFG_HAL_SENS_1_TYPE_PANAMN34110)
    if (len) *len=sizeof(sencfg_PANAMN34110)/sizeof(*sencfg_PANAMN34110);
    return sencfg_PANAMN34110;
#elif defined(CFG_HAL_SENS_1_TYPE_IMX291)
    if (len) *len=sizeof(sencfg_SONYIMX291)/sizeof(*sencfg_SONYIMX291);
    return sencfg_SONYIMX291;
#elif defined(CFG_HAL_SENS_1_TYPE_FAKE)
    if (len) *len=sizeof(sencfg_FAKE)/sizeof(*sencfg_FAKE);
    return sencfg_FAKE;
#else
    return NULL;
#endif
}


/**
 *  get sensor config
 *
 * @param w  target width
 * @param h  target height
 * @param fps target fps
 * @return config
 * @retval
 */
SensModeCfg * getSensModeCfg(ybfwSenId_t sen_id, UINT8 * mode_str){
    SINT32 i;
    SensModeCfg *pconf = NULL;
    UINT8 tmp_buf[32];
    SINT32  len = 0;
    if (!mode_str){
        DBG("mode_str is null\n");
        return NULL;
    }
    switch (sen_id) {
        case YBFW_SEN_ID_0:
            pconf = getSens0Array(&len);
            break;
        case YBFW_SEN_ID_1:
            pconf = getSens0Array(&len);
            break;
        default:
            pconf  = NULL;
            break;
    }
    if (pconf == NULL){
        LOG_ERROR(ERR_FAIL);
        return NULL;
    }
    DBG("len =%d\n",len);
    for (i = 0; i < len; i++){
        DBG("item %s  vs %d;%d;%d ",mode_str,  pconf->w, pconf->h,pconf->fps);
        sprintf((char *)tmp_buf,"%d;%d;%d",pconf->w,pconf->h,pconf->fps);
        if(strstr((char *)tmp_buf,(char *)mode_str) != NULL){
            DBG("selected configItem[%d]",i);
            return pconf;
        }
        pconf++;
    }
    return NULL;
}

#if 0

void main(){

    SensModeCfg * cfg = NULL;
    cfg =  getSensModeCfg(YBFW_SEN_ID_0, "3840;2160;30" );
    if (cfg){
        DBG("cfg vals WxH %dx%d fps %d ",cfg->w, cfg->h,cfg->fps);
    }else{
        DBG("is not found");
    }

    cfg =  getSensModeCfg(YBFW_SEN_ID_0, "3840;2160;60" );
    if (cfg){
       DBG("cfg vals WxH %dx%d fps %d ",cfg->w, cfg->h,cfg->fps);
    }else{
        DBG("is not found");
    }

    cfg =  getSensModeCfg(YBFW_SEN_ID_0, "1280;720;30" );
    if (cfg){
       DBG("cfg vals WxH %dx%d fps %d ",cfg->w, cfg->h,cfg->fps);
    }else{
        DBG("is not found");
    }
}

#endif
