/**
 * @file mc_thread_raw.h
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef MEDIA_CORE_MC_THREAD_RAW_H_
#define MEDIA_CORE_MC_THREAD_RAW_H_

/**
 * Start raw thread
 *
 * @return result
 */
ErrCode_t mcThreadRawStart();

/**
 * Stop raw thread
 *
 * @return result
 */
ErrCode_t mcThreadRawStop();



#endif /* MEDIA_CORE_MC_THREAD_RAW_H_ */
