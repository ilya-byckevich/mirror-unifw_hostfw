/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef MEDIA_CORE_MC_OSD_H_
#define MEDIA_CORE_MC_OSD_H_

#include "ykn_bfw_api/ybfw_errors.h"

ErrCode_t mc_osdStart();
ErrCode_t mc_osdStop();

#endif /* MEDIA_CORE_MC_OSD_H_ */
