/**
 * @file mock_mc_common.c
 * @author ilya(i.byckevich@yukonww.com)
 * @brief May 14, 2020
 *
 * Put here only mocked functions
*/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdbool.h>
#include <cmocka.h>

#include "stdio.h"
#include "string.h"

#include "common.h"
#include "mc_common.h"
#include "app_common.h"


UINT8 __wrap_isEqualStrings(const UINT8 * str1, const UINT8 *str2, CmpCaseSensitive case_sens_state)
{
    if ((!str1)||(!str2)) return 0;

    if (case_sens_state == CCS_YES){
        return ((strcmp(str1, str2) == 0) && (strlen(str1) == strlen(str2))) ? 1 : 0;
    }else{
        return ((strcasecmp(str1, str2) == 0) && (strlen(str1) == strlen(str2))) ? 1 : 0;
    }
}

ErrCode_t __wrap_ybfwGfxObjectMapping(ybfwGfxObj_t *psrcObj, ybfwGfxObj_t *pdstObj, UINT32 paraId, UINT32 para1)
{
	ErrCode_t ret = ERR_SUCCESS;

	return ret;
}

