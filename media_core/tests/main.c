/**
 * @file main.c
 * @author ilya(i.byckevich@yukonww.com)
 * @brief May 12, 2020
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <setjmp.h>
#include <cmocka.h>

#include "mc_common.h"
#include "app_common.h"


static void failing_test(void **state)
{
    assert_int_equal(0, 42);
}

static void check_isEqualStrings(void **state)
{
    assert_true(isEqualStrings("One", "One", CCS_YES));
    assert_false(isEqualStrings("One", "ONE", CCS_YES));
}

static void check_mcGetResolInfo(void **state)
{
    LargestIntegralType valid_values[] = { RK_NAME, RK_TXT, RK_TYPE, RK_WxH,
                                           RK_FIND };
    ResolutionInfo_t *info_xga = NULL;
    ResolutionInfo_t *info_svga = NULL, info_svga_valid =  {.name = (UINT8 *)"SVGA",
                                                    .resTxt = (UINT8 *)"800;600",
                                                    .resW = 800,.resH = 600,
                                                    .ratioW = 4,.ratioH = 3};
    ResolutionInfo_t  *info_qvga = NULL, info_qvga_valid = {.name = (UINT8 *)"QVGA",
                                                    .resTxt = (UINT8 *)"320;240",
                                                    .resW = 320,.resH = 240,
                                                    .ratioW = 4, .ratioH = 3};

    /* ------- */
    info_xga = mcGetResolInfo(RK_NAME, "XGA");
    assert_non_null(info_xga);
    assert_string_equal(info_xga->resTxt, "1024;768");

    /* ------- */
    info_svga = mcGetResolInfo(RK_TXT, "800;600");
    assert_non_null(info_svga);
    assert_string_equal(info_svga->name, "SVGA");
    assert_memory_equal(&info_svga->resW, &info_svga_valid.resW, sizeof(UINT32));

    /* ------- */
    info_svga = mcGetResolInfo(RK_TYPE, REST_2160P);
    assert_non_null(info_svga);

    /* ------- */
    info_qvga = mcGetResolInfo(RK_WxH, 320, 240);
    assert_non_null(info_qvga);
    assert_memory_equal(&info_qvga->resW, &info_qvga_valid.resW, sizeof(UINT32));
    assert_memory_equal(&info_qvga->resH, &info_qvga_valid.resH, sizeof(UINT32));

    /* ------- */
    assert_null(mcGetResolInfo(RK_NAME, "blablalba"));
}

static void check_ybfwGfxObjectMapping(void **state)
{
    assert_int_equal(ybfwGfxObjectMapping(NULL, NULL, 0, 0), ERR_SUCCESS);
}

int main(void)
{
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(check_isEqualStrings),
            cmocka_unit_test(check_mcGetResolInfo),
            cmocka_unit_test(check_ybfwGfxObjectMapping),
    /*        cmocka_unit_test(failing_test), */
    };

    return cmocka_run_group_tests_name("success_test", tests, NULL, NULL);
}

