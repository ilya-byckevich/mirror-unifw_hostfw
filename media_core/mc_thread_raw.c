/**
 * @file mc_thread_raw.c
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief module to put convert raw data to yuv
 *
*/

#define FILE_ID "MC11"

#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "mc_context.h"
#include "mc_devices.h"
#include "mc_common.h"
#include "autoconf.h"

static mcContext_t * pctx = NULL;



static UINT32 grRawCb( ybfwModePrevYuvInfo_t *ppv_rawinfo, UINT32 ppv_rawinfo_num){

    UINT32 tmr_start = 0;

    ybfwModePrevYuvInfo_t pvyuv;

    ybfwGfxObj_t src_img;
    UINT32 src_img_iqinfo;
    ybfwGfxObj_t dst_img;
    ybfwGfxRoi_t dst_img_src_roi;
    ybfwPrevYuvRoi_t srcRoi;
    UINT32 ret = FAIL;
    ybfwGfxObjectCdspDoParam_t param;
    UINT32 appendparam  = (1<<16);
    UINT32 tmp_speed_ctrl = (1<<31);
    UINT8 buf_idx;

    /* adjust the speed of RawToYuv */
    param.cdspDoTargetFps = 120;

    memset(&param, 0, sizeof(param));
    memset(&src_img, 0, sizeof(src_img));
    memset(&dst_img, 0, sizeof(dst_img));
    memset(&dst_img_src_roi, 0, sizeof(dst_img_src_roi));
    memset(&src_img_iqinfo, 0, sizeof(src_img_iqinfo));

    //    DBG("trace\n");

    if( !(ppv_rawinfo->attr & (YBFW_MODE_PV_CB_IMG_STOP_IN_PV_EXIT|YBFW_MODE_PV_CB_IMG_PAUSE_IN_PV) )) {
//        DBG("trace\n");
        tmr_start = ybfwMsTimeGet();


        buf_idx =  pctx->yuvBufIndex[CUR_SENSOR];


        param.srcBufHandle[0] = ppv_rawinfo->pvCdspBufHandle;
        src_img_iqinfo = ppv_rawinfo->pcdspDoInfo;
        src_img.pbuf  = ppv_rawinfo->pframe.pbuf;
        src_img.bufW  = ppv_rawinfo->pframe.width;
        src_img.bufH  = ppv_rawinfo->pframe.height;
        src_img.roiX  = (SINT32)ppv_rawinfo->pframe.roiX;
        src_img.roiY  = (SINT32)ppv_rawinfo->pframe.roiY;
        src_img.fmt   = ppv_rawinfo->pframe.fmt;
        src_img.roiW  = ppv_rawinfo->pframe.roiW;
        src_img.roiH  = ppv_rawinfo->pframe.roiH;

//DBG("----");

        memset(&pvyuv, 0, sizeof(ybfwModePrevYuvInfo_t));
        //is need get real YUV buffer zize all time. it is can change size when is changed sensor resolution
        ybfwModeCfgGet(YBFW_MODE_CFG_PVCB_YUV_SRC_ROI_GET, ppv_rawinfo, &srcRoi, 0); // first yuv
//DBG("trace\n");
        if( srcRoi.roiw && srcRoi.roih ){
            dst_img_src_roi.roiX = srcRoi.roix;
            dst_img_src_roi.roiY = srcRoi.roiy;
            dst_img_src_roi.roiW = srcRoi.roiw;
            dst_img_src_roi.roiH = srcRoi.roih;

            if (pctx->yuvBuf[CUR_SENSOR][buf_idx].pbuf){
                dst_img.pbuf = pctx->yuvBuf[CUR_SENSOR][buf_idx].pbuf;
                dst_img.bufW = pctx->yuvBuf[CUR_SENSOR][buf_idx].bufW;
                dst_img.bufH = pctx->yuvBuf[CUR_SENSOR][buf_idx].bufH;
                dst_img.fmt  = pctx->yuvBuf[CUR_SENSOR][buf_idx].fmt;
                dst_img.roiX = pctx->yuvBuf[CUR_SENSOR][buf_idx].roiX;
                dst_img.roiY = pctx->yuvBuf[CUR_SENSOR][buf_idx].roiY;
                dst_img.roiW = pctx->yuvBuf[CUR_SENSOR][buf_idx].roiW;
                dst_img.roiH = pctx->yuvBuf[CUR_SENSOR][buf_idx].roiH;

            }else{
                DBG("pctx->yuvBuf[%d][%d] buff is null",CUR_SENSOR, buf_idx);
            }
//            DBG("SEN (%d,%d; %d x %d) -> YUV (%d,%d; %d x %d)",
//                    srcImg[srcCnt-1].roiX,srcImg[srcCnt-1].roiY,srcImg[srcCnt-1].roiW,srcImg[srcCnt-1].roiH,
//                    dstImg[srcCnt-1].roiX,dstImg[srcCnt-1].roiY,dstImg[srcCnt-1].roiW,dstImg[srcCnt-1].roiH);

            if(pctx->perf.procDSP) {
                ret = ybfwGfxObjectCdspDo( &src_img, &dst_img_src_roi, &dst_img,
                     0x101/*dstCnt | ( srcCnt << 8 )*/ | appendparam | tmp_speed_ctrl,
                    YBFW_GFX_OBJECT_CDSP_DO_YUV, src_img_iqinfo,
                    YBFW_MODE_STILL_PREVIEW,  &param );

                if(ret == FAIL) HOST_ASSERT_MSG(0,"CDSP DO err!!!");
            }
        }
        else{
            HOST_ASSERT_MSG(0, "roiw(=%d) roih(=%d) is zero, this yuv is not exist\n",
                srcRoi.roiw , srcRoi.roih );
        }

        pctx->perf.perfDspdo = ybfwMsTimeGet() - tmr_start;

        //send complete event
        ybfwOsEventFlagsSet(&pctx->yuvBufReadyFlag[CUR_SENSOR], 1, YBFW_TX_OR);
    }

    //DBG("trace\n");
    return ERR_SUCCESS;
}



ErrCode_t mcThreadRawStart(){
    ErrCode_t err  = ERR_SUCCESS;
    ybfwPrevUrgentCallback_t pvcb;
    ybfwSenId_t senId = CUR_SENSOR;
    UINT32 rawId = 0;
    if (!pctx) pctx = ctx_getmcContext();

    LOG_IF_ERROR(mc_senGetRawYuvId(senId, &rawId, NULL));

    DBG("trace\n");
    /* reg rawCb to proc raw data to yuv */
    pvcb.ctrl     = 0;
    pvcb.interval = 1;
    pvcb.fp       = grRawCb; //pvRawCbDirectShow;
    LOG_IF_ERROR(ybfwVideoUrgentCallbackSet(rawId, &pvcb));

    return err;
}

ErrCode_t mcThreadRawStop(){
    ErrCode_t err  = ERR_SUCCESS;
    ybfwPrevUrgentCallback_t pvcb;
    UINT32 rawId = 0;
    ybfwSenId_t senId = CUR_SENSOR;
    if (!pctx) pctx = ctx_getmcContext();

    LOG_IF_ERROR(mc_senGetRawYuvId(senId, &rawId, NULL));

    DBG("trace\n");
    pvcb.ctrl     = 0;
    pvcb.interval = 0;
    pvcb.fp       = NULL;
    err = ybfwPreviewUrgentCallbackSet (rawId, &pvcb);
    if (err != ERR_SUCCESS){
        ERR("Preview urgent callback does't unset");
    }

    return err;
}
