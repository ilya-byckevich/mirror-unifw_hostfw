/**
 * @file mc_cmd.c
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/


#define FILE_ID "MC01"

#include "media_core_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "common.h"
#include "app_common.h"
#include "mc_context.h"

static UINT8 mccmds_allow = 1; //TODO NAV set to 0

#define TAG_INIT  "init"
#define TAG_DESTROY  "destroy"
#define TAG_SEN0  "sen0"
#define TAG_SEN1  "sen1"
#define TAG_CORE  "core"
#define TAG_DCH0  "dch0"
#define TAG_DCH1  "dch1"
#define TAG_ON  "on"
#define TAG_OFF  "off"
#define TAG_LIMITS  "limits"
#define TAG_SCALE  "scale"
#define TAG_SEN0_BIAS  "sen0bias"
#define TAG_SEN1_BIAS  "sen1bias"
#define TAG_DCH0_BIAS  "dch0bias"
#define TAG_DCH1_BIAS  "dch1bias"
#define TAG_SCALE_BIAS"scalebias"
#define TAG_WIN0  "win0"
#define TAG_WIN1  "win1"
#define TAG_WIN2  "win2"
#define TAG_ROI  "roi"
#define TAG_STATE  "state"
#define TAG_INFO  "info"
#define TAG_ZOOM  "zoom"
#define TAG_STEP  "step"
#define TAG_SMOOTH  "smooth"
#define TAG_BALANCE  "balance"
#define TAG_INC  "inc"
#define TAG_DEC  "dec"
#define TAG_TEST  "test"
#define TAG_SET  "set"


#define SHOW_PROC_FAIL_INFO(fmt, ...) do {printf("%s:" fmt"... Please see help.\n", __FUNCTION__,  ##__VA_ARGS__); }while(0)

void procCmdMcEntity(UINT8 op, SINT32 argc, UINT8 **argv, UINT32* v){
    ErrCode_t err = ERR_SUCCESS;
    if (op == 1){
        LOG_IF_ERROR(mcCoreCreate());
    }else{
        LOG_IF_ERROR(mcCoreDestroy());
    }
}

void procCmdMcState(SINT32 argc, UINT8 **argv, UINT32* v){
    ErrCode_t err = ERR_SUCCESS;
    UINT8 * param1 = *argv++;v++;argc--;
    UINT8 * param2 = *argv++;v++;argc--;
    SwitchState state = SWITCH_OFF;
    if (!param1 || !param2){
        SHOW_PROC_FAIL_INFO("wrong params (p1 %s p2 %s)",param1 ? param1:"null",param2 ? param2:"null");
        return;
    }

    if (isEqualStrings(param2, (UINT8 *) TAG_ON, CCS_NO )){
        state = SWITCH_ON;
    }else if (isEqualStrings(param2, (UINT8 *) TAG_OFF, CCS_NO )){
        state = SWITCH_OFF;
    }else{
        SHOW_PROC_FAIL_INFO("p2 ( %s) is't parsed",param2 );
        return;
    }
    if (isEqualStrings(param1, (UINT8 *) TAG_CORE, CCS_NO ) ) {
        LOG_IF_ERROR(mcCoreCfgSet(MCCFG_CORE_STATE,state));

    }else if (isEqualStrings(param1, (UINT8 *) TAG_SEN0, CCS_NO ) ) {
        LOG_IF_ERROR(mcCoreCfgSet(MCCFG_DEV_SENS_STATE,YBFW_SEN_ID_0,state));

    }else if (isEqualStrings(param1, (UINT8 *) TAG_SEN1, CCS_NO ) ) {
        LOG_IF_ERROR(mcCoreCfgSet(MCCFG_DEV_SENS_STATE,YBFW_SEN_ID_1,state));

    }else if (isEqualStrings(param1, (UINT8 *) TAG_DCH0, CCS_NO ) ) {
        LOG_IF_ERROR(mcCoreCfgSet(MCCFG_DEV_DISPCH_STATE,YBFW_DISP_CHNL_0,state));

    }else if (isEqualStrings(param1, (UINT8 *) TAG_DCH1, CCS_NO ) ) {
        LOG_IF_ERROR(mcCoreCfgSet(MCCFG_DEV_DISPCH_STATE,YBFW_DISP_CHNL_1,state));
    }
}

void procCmdMcSet(SINT32 argc, UINT8 **argv, UINT32* v){
    ErrCode_t err = ERR_SUCCESS;
    UINT8 * param1 = *argv++;v++;argc--;
    mcCtrl_t ctrl;
    ybfwSenId_t sen_id;
    ybfwDispChnlId_t disch_id;
    McScaleCfg scale_cfg;
    McBiasType bias_type;
    if (!param1 ){
       SHOW_PROC_FAIL_INFO("wrong params (p1 %s )",param1 ? param1:"null");
       return;
    }

    if (isEqualStrings(param1, (UINT8 *) TAG_SEN0, CCS_NO) || isEqualStrings(param1, (UINT8 *) TAG_SEN1, CCS_NO) ){
        ctrl = MCCFG_CFG_DEV_SENS;
        sen_id = isEqualStrings(param1, (UINT8 *) TAG_SEN0, CCS_NO) ? YBFW_SEN_ID_0 : YBFW_SEN_ID_1;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_DCH0, CCS_NO) || isEqualStrings(param1, (UINT8 *) TAG_DCH1, CCS_NO) ){
        ctrl = MCCFG_CFG_DEV_DISPCH;
        disch_id =  isEqualStrings(param1, (UINT8 *) TAG_DCH0, CCS_NO) ? YBFW_DISP_CHNL_0 : YBFW_DISP_CHNL_1;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_SCALE, CCS_NO ) ) {
        ctrl = MCCFG_CFG_SCALE;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_DCH0_BIAS, CCS_NO) || isEqualStrings(param1, (UINT8 *) TAG_DCH1_BIAS, CCS_NO) ){
        ctrl = MCCFG_CFG_BIAS;
        bias_type = MCBIAS_TYPE_DISPCH;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_SEN0_BIAS, CCS_NO) || isEqualStrings(param1, (UINT8 *) TAG_SEN0_BIAS, CCS_NO) ){
        ctrl = MCCFG_CFG_BIAS;
        bias_type = MCBIAS_TYPE_SEN;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_SCALE_BIAS, CCS_NO ) ) {
        ctrl = MCCFG_CFG_BIAS;
        bias_type = MCBIAS_TYPE_SCALE;
    }else if (isEqualStrings(param1, (UINT8 *) TAG_DCH1, CCS_NO ) ) {
        ctrl = MCCFG_CFG_WINDOW;
    }

}

void procCmdMcZoom(SINT32 argc, UINT8 **argv, UINT32* v){

}

ErrCode_t cmdMcInit(){

    mccmds_allow  = 1;
    return ERR_CMD_SUCCESS;
}


ErrCode_t cmdMc(int argc,char **argv,UINT32* v){
    UINT8 * cmd = *argv++;v++;argc--;

    if (! mccmds_allow){
        return ERR_CMD_NOT_FOUND;
    }

    if(argc<0) {
        return 0;
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_INIT, CCS_NO ) ) {
        procCmdMcEntity(1,(SINT32)argc,(UINT8**) argv,v);
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_DESTROY, CCS_NO ) ) {
        procCmdMcEntity(0,(SINT32)argc,(UINT8**) argv,v);
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_STATE, CCS_NO ) ) {
        procCmdMcState((SINT32)argc,(UINT8**) argv,v);
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_INFO, CCS_NO ) ) {
        procCmdMcState((SINT32)argc,(UINT8**) argv,v);
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_SET, CCS_NO ) ) {
        procCmdMcSet((SINT32)argc,(UINT8**) argv,v);
    }else if (isEqualStrings(cmd, (UINT8 *) TAG_ZOOM, CCS_NO ) ) {
        procCmdMcZoom((SINT32)argc,(UINT8**) argv,v);
    }else{
        return ERR_CMD_NOT_FOUND;
    }
    return ERR_CMD_SUCCESS;
}



ErrCode_t cmdMcHelp(){
    printf("mc - media pipeline core control\n");
    printf("\tmc start   -  start media pipeline core\n");
    printf("\tmc start   -  start media pipeline core\n");

    return ERR_CMD_SUCCESS;
}


