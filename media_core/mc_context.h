/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/
#ifndef CONTEXT_H_
#define CONTEXT_H_

#include "autoconf.h"

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_disp_api.h"
#include "app_common.h"


#include "media_core_api.h"
#include "mc_devices_sencfgs.h"


//Temporary implementation. use only 1 sensor and 1 display at time
#define CUR_DISPLAY YBFW_DISP_CHNL_0
#define CUR_SENSOR YBFW_SEN_ID_0
#define CUR_PIP_CHANNEL YBFW_PIP_CH_0
#define CUR_DISPLAY_GFX_CHANNEL YBFW_GFX_CH_0_FLAG


//#define YUV_BUF_W_ALIGN(x)          (((x)+31) & (~31))
#define YUV_BUF_W_ALIGN(x) (x)
//#define YUV_BUF_H_ALIGN(x)          (((x)+15) & (~15))
#define YUV_BUF_H_ALIGN(x)          (x)


#define YUV_ROI_ALIGN_XY(x)         ((x) & (~0x3))
#define YUV_ROI_ALIGN_CDSP(x)       ((x) & (~0x7))


//#define OFFSET_RET_X  (-11)
//#define OFFSET_RET_Y  (-7)


/** current sensor count */
#define V50mc_SENSOR_COUNT 1

#define V50mc_SENSOR_ACTIVE_ID  YBFW_SEN_ID_0
/** max of supported sensors */
#define V50mc_SENSOR_COUNT_MAX 4  //to config



/* PiP and display related defines*/
#define V50mc_PIP_CHANNEL YBFW_PIP_CH_0
#define V50mc_DISPLAY_YUV_ID 3
#define V50mc_DISPLAY_YUV YBFW_MODE_PREV_SENSOR_0_YUV_3
#define V50mc_PIP_LAYER (YBFW_YUV_PIP_BASE + YBFW_PAGE_PIP_0_MAIN)
#define V50mc_PIP_LAYER_HIDDEN (YBFW_YUV_PIP_BASE + YBFW_PAGE_PIP_0_HIDDEN)
#define V50mc_DISPLAY_PV_WIDTH  1920
#define V50mc_DISPLAY_PV_HEIGHT 1080

#define V50mc_DISPLAY_RATIO_W 16
#define V50mc_DISPLAY_RATION_H 9

#define V50mc_DISPLAY_PV_PIP_OVERLAY_WIDTH (DISPLAY_PV_WIDTH)
#define V50mc_DISPLAY_PV_PIP_OVERLAY_HEIGHT (DISPLAY_PV_HEIGHT)

#define V50mc_DISPLAY_CHANNEL YBFW_DISP_CHNL_1
#define V50mc_DISPLAY_GFX_CHANNEL YBFW_GFX_CH_0_FLAG


#define YUV_ROI_ALIGN_XY(x)         ((x) & (~0x3))
#define YUV_ROI_ALIGN_CDSP(x)       ((x) & (~0x7))
#define YUV_ROI_ALIGN(x)            ((x) & (~1))





//NAV TODO
#define SN_W 3840
#define SN_H 2160
//#define SN_W 1920
//#define SN_H 1080
#define DRAW_W 1920
#define DRAW_H 1080
#define DRAW_X ((1920-DRAW_W)/2)
#define DRAW_Y ((1080-DRAW_H)/2)

#define SCALE_BUF_DRAW_W 1952
#define SCALE_BUF_DRAW_H 1098
#define P2P_W (SCALE_BUF_DRAW_W*DIMENTION/SN_W)
#define P2P_H (SCALE_BUF_DRAW_H*DIMENTION/SN_H)


#define DFLT_DISPSIZE_W 1920
#define DFLT_DISPSIZE_H 1080
#define DFLT_ZOOM_FACTOR_MIN 1000
#define DFLT_ZOOM_FACTOR_MAX 32000
#define DFLT_ZOOM_STEP 100




typedef enum{
    ADJVL_MIN = 0,
    ADJVL_MAX,
    ADJVL_TOT
}AdjValLimits;

typedef enum{
    DPP_SHOWN,
    DPP_HIDDEN,
    DPP_MAIN,
    DPP_MAX,
}DispPiPPages;;



#define BUF_QUEUE_LEN 2 ///< Count of buffers per operation
#define MAX_PIP_WINDOWS  2 ///< Maximum pip windows;
/*
 * Что нужно
 *  Сенсор
 *  - текущее разрешение захватываемое
 *  - формат (нужен другим подсистемам)
 *  - fps
 *
 *
 * размер YUVBuf по каждому сенсору
 *  - текущее значение (в который конвертируем)
 *  - размер буффера (максимально возможное значение которое может задаваться)
 *
 *  scaleBuf[WT_MAX]. По каждому
 *  - размер.максимальный. зависит от типа окна. WT_MAIN привязывается к максимальному YUVBuf
 *  - WT_PIP1,WT_PIP2  -размер pip плюс zoom компенсация
 *
 *
 *  video overlay по каждому дисплею
 *  roiVideo - область для отображения
 *  overlay size - размер оверлея
 *
 *  OSD Overlay по каждому дисплею
 *  roiOverlay - область для отображения
 *  overlay size - размер оверлея
 *
 *  Потоки и синхронизация
 *  - ScalerThrMain
 *    - 1 pid, 1 event
 *  - Display thread
 *     - 1 pid,
 *
 * расчеты зума
 * zoom factor
 * по каждому параметру
 * cal_raw
 * calc_tail
 * calc_use
 *
 *
 */

typedef struct{
    UINT32  stateBits;
     //perf settings
     UINT8 fillBlack;
     // 0  - disabled
     // 1 - sp5kGfxObjectBilinearScale
     // 2 - ybfwGfxObjectMapping
     UINT8 ModeYuvToScale;
     // 0 - disabled
     // 1 - sp5kGfxObjectBilinearScale
     // 2 - ybfwGfxObjectMapping
     // 3 - sp5kGfxObjectCopy()
     UINT8 ModeScaleToDisp;
     SINT32 perfDspdo;
     UINT8 procDSP;

}McPerf;

typedef struct{
    McPerf perf;

   //raw -> yuv
   ybfwImgFmt_t rawBufFmt; ///< format of raw buffer
   UINT8  yuvBufIndex[CFG_HAL_SENS_COUNT];
   ybfwGfxObj_t yuvBuf[CFG_HAL_SENS_COUNT][BUF_QUEUE_LEN];
   ybfwGfxRoi_t yuvBufSize[CFG_HAL_SENS_COUNT];
   ybfwImgFmt_t yuvBufFmt; ///< yuv format of buffer
   YBFW_EVENT_FLAGS_GROUP yuvBufReadyFlag[CFG_HAL_SENS_COUNT];
   ybfwGfxObj_t extYuvBuf[BUF_QUEUE_LEN]; //for pip2 buff


   UINT8        sbFactor[WT_TOT];      ///< scale factor.
   ybfwGfxObj_t scaleBuf[WT_TOT][BUF_QUEUE_LEN];
   ybfwImgFmt_t scaleBufFmt; ///< yuv format of buffer
   ybfwGfxRoi_t scaleBufSize[WT_TOT];
   UINT8  scaleBufIndex[CFG_HAL_SENS_COUNT];
   ybfwGfxMappingItp_t yuvToScaleItp;// = YBFW_GFX_MAP_ITP_BILINEAR;

   McScaleCfg scaleCfg;
   ZoomState zoomState;
//   sensorConfig_t grabSenCfg[CFG_HAL_SENS_COUNT]; ///< Current sensor config




   ybfwGfxMappingItp_t scaleToDispItp;// = YBFW_GFX_MAP_ITP_BILINEAR;




   //Scaler Settings

   ybfwGfxRoi_t sbSelRoi[CFG_HAL_SENS_COUNT][WT_TOT];  ///< selected roi
   UINT8 sbStretchWin[CFG_HAL_SENS_COUNT][WT_TOT];
   YBFW_THREAD *sbThrGrb[CFG_HAL_DISP_COUNT];

  // mcDrawCfg_t  drawCfg[CFG_HAL_DISP_COUNT]; ///< Draw on display related contex

   // Phisical Display and sensor related context


   McDeviceCfg sensCfg[CFG_HAL_SENS_COUNT];    ///< Sensor config
   SwitchState sensState[CFG_HAL_SENS_COUNT];
   SINT32 senAdj[CFG_HAL_SENS_COUNT][AXIS_TOT];

   McDeviceCfg dispCfg[CFG_HAL_DISP_COUNT];   ///< Display config
   SwitchState dispState[CFG_HAL_DISP_COUNT];
   SINT32 dispAdj[CFG_HAL_DISP_COUNT][AXIS_TOT];
   //context related params
   YBFW_MUTEX mutex;  //mutex to access;
   UINT8 inited;   //Context already inited;
}mcContext_t;

ErrCode_t ctxInit(UINT32 senCapSize[CFG_HAL_SENS_COUNT][AXIS_TOT],UINT32 dispSize[CFG_HAL_DISP_COUNT][AXIS_TOT],McScaleCfg zmCfg[CFG_HAL_SENS_COUNT]);



void ctx_Dump(UINT8 show_short);


ErrCode_t ctx_setDispAdj(UINT32 index, SINT32 dispAdj[AXIS_TOT]);
ErrCode_t ctx_setSenAdj(UINT32 index, SINT32 senAdj[AXIS_TOT]);
ErrCode_t ctx_setZoomFactor(UINT32 index, UINT32 factor);
ErrCode_t ctx_setZoomInc(UINT32 index,McZoomStepMode mode,McZoomDirection dir, UINT32 * retZoomFactor);

//#define GOD_MODE
#ifdef GOD_MODE //debug only
ErrCode_t ctx_setDispAdjHand(UINT32 index, UINT32 dispAdj[AXIS_TOT]);
ErrCode_t ctx_setSenAdjHand(UINT32 index, UINT32 senAdj[AXIS_TOT]);
ErrCode_t ctx_setDispRoiHand(UINT32 index, ybfwGfxRoi_t roi);
ErrCode_t ctx_setSenRoiHand(UINT32 index, ybfwGfxRoi_t roi);
#endif
//debug only
mcContext_t * ctx_getmcContext();

#endif /* CONTEXT_H_ */
