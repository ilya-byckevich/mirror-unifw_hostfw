/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <string.h>

#define FILE_ID "MC03"


#include "autoconf.h"

#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"
#include "mc_context.h"
#include "mc_common.h"


//#define DISPLAY_PV_PIP_OVERLAY_WIDTH (CFG_V50mc_DISP_W)
//#define DISPLAY_PV_PIP_OVERLAY_HEIGHT (CFG_V50mc_DISP_H)



//works only throw function ctx_getmcContext()
static mcContext_t  ctx = {.inited = 0};  ///< local variable

/**
 * Return current context
 * @return
 */
mcContext_t * ctx_getmcContext(){
    return &ctx;
}


void ctx_Dump(UINT8 show_short){
    SINT32 i;

    mcContext_t * pctx = ctx_getmcContext();

    if (show_short == 0){
        printf("---------------- pctx dump -------------------------------\n");
        printf("\nDevices:");
        for (i = 0; i < CFG_HAL_SENS_COUNT; i++){
            printf("\n\t\t Sensor[%d] %dx%d (max %dx%d)  ratio %d/%d \n ", i,
                        pctx->sensCfg[i].cur_res[AXIS_X],pctx->sensCfg[i].cur_res[AXIS_Y],
                        pctx->sensCfg[i].max_res[AXIS_X],pctx->sensCfg[i].max_res[AXIS_Y],
                        pctx->sensCfg[i].ratio[AXIS_X],pctx->sensCfg[i].ratio[AXIS_Y]);
        }
        for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
            printf("\n\t\t Display[%d] %dx%d (max %dx%d)  ratio %d/%d \n ", i,
                        pctx->dispCfg[i].cur_res[AXIS_X],pctx->dispCfg[i].cur_res[AXIS_Y],
                        pctx->dispCfg[i].max_res[AXIS_X],pctx->dispCfg[i].max_res[AXIS_Y],
                        pctx->dispCfg[i].ratio[AXIS_X],pctx->dispCfg[i].ratio[AXIS_Y]);
        }
#if 0
        printf("\nZoom:\n\t\t Cfg(mode=%s, min=%d max=%d step=%d) ; curFactor=%d\n",
                pctx->zoomState.zmCfg.zMode == MCZM_DIGITAL? "Digital" : "Micro",
                pctx->zoomState.zmCfg.minFactor,pctx->zoomState.zmCfg.maxFactor,pctx->zoomState.zmCfg.step,
                pctx->zoomState.zmFactor);
#endif

        for (i = 0; i < WT_TOT; i++){
            printf("\nScaleBuf[%d]:\n\t\t Factor %d ; FullSize (%d,%d, %dx%d)  maxShiftAdj +-%dW,+-%dH"
                    "\n\t\t selShiftAdj %dW,%dH  selRoi[WT_MAIN] (%d,%d, %dx%d)  selRoi[WT_PIP1] (%d,%d, %dx%d)\n",
                i, pctx->sbFactor[i],
                pctx->scaleBufSize[i].roiX,pctx->scaleBufSize[i].roiY,
                pctx->scaleBufSize[i].roiW,pctx->scaleBufSize[i].roiH,
                pctx->zoomState.zmPointShiftMax[AXIS_X],pctx->zoomState.zmPointShiftMax[AXIS_Y],
                pctx->zoomState.zmPointShiftCur[AXIS_X],pctx->zoomState.zmPointShiftCur[AXIS_Y],
                pctx->sbSelRoi[i][WT_MAIN].roiX,pctx->sbSelRoi[i][WT_MAIN].roiY,pctx->sbSelRoi[i][WT_MAIN].roiW,pctx->sbSelRoi[i][WT_MAIN].roiH,
                pctx->sbSelRoi[i][WT_PIP1].roiX,pctx->sbSelRoi[i][WT_PIP1].roiY,pctx->sbSelRoi[i][WT_PIP1].roiW,pctx->sbSelRoi[i][WT_PIP1].roiH);
        }
#if 0
        for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
            printf("\nDraw[%d]:\n\t\t maxPosAdj +-%dW,+-%dH  curShiftAdj %dW,%dH "
                    "\n\t\tOSD drawSelRoiOSD (%d,%d, %dx%d)   maxWxH %dx%d"
                    "\n\t\tVID drawSelRoiVID [WT_MAIN](%d,%d, %dx%d)  [WT_PIP1](%d,%d, %dx%d) maxWxH %dx%d\n",
                i,
                pctx->drawCfg[i].drawMaxShiftAdj[AXIS_X],pctx->drawCfg[i].drawMaxShiftAdj[AXIS_Y],
                pctx->drawCfg[i].drawCurShiftAdj[AXIS_X],pctx->drawCfg[i].drawCurShiftAdj[AXIS_Y],
                pctx->drawCfg[i].drawSelRoiOSD.roiX,pctx->drawCfg[i].drawSelRoiOSD.roiY,
                pctx->drawCfg[i].drawSelRoiOSD.roiW,pctx->drawCfg[i].drawSelRoiOSD.roiH,
                pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X], pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y],
                pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiX,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiY,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiW,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiH,
                pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiX,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiY,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiW,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiH,
                pctx->drawCfg[i].drawMaxSizeVID[AXIS_X],pctx->drawCfg[i].drawMaxSizeVID[AXIS_Y]);
        }
#endif
        printf("-------------------------------------------------------------\n");
    }else{
        for (i = 0; i < CFG_HAL_SENS_COUNT; i++){
            printf("\nScaleBuf[%d]:Factor %d ; selRoi[WT_MAIN] (%d,%d, %dx%d)  selRoi[WT_PIP1] (%d,%d, %dx%d)\n",
                i, pctx->sbFactor[i],
                pctx->sbSelRoi[i][WT_MAIN].roiX,pctx->sbSelRoi[i][WT_MAIN].roiY,pctx->sbSelRoi[i][WT_MAIN].roiW,pctx->sbSelRoi[i][WT_MAIN].roiH,
                pctx->sbSelRoi[i][WT_PIP1].roiX,pctx->sbSelRoi[i][WT_PIP1].roiY,pctx->sbSelRoi[i][WT_PIP1].roiW,pctx->sbSelRoi[i][WT_PIP1].roiH);
        }
#if 0
        for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
            printf("\nDraw[%d]: VID drawSelRoiVID [WT_MAIN](%d,%d, %dx%d)  [WT_PIP1](%d,%d, %dx%d) maxWxH %dx%d\n",
                i,
                pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiX,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiY,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiW,pctx->drawCfg[i].drawSelRoiVID[WT_MAIN].roiH,
                pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiX,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiY,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiW,pctx->drawCfg[i].drawSelRoiVID[WT_PIP1].roiH,
                pctx->drawCfg[i].drawMaxSizeVID[AXIS_X],pctx->drawCfg[i].drawMaxSizeVID[AXIS_Y]);
        }
#endif
    }
}

#define CONST_SB_MAX_SHIFT_ADJ 40
#define CONST_MAX_FBSCALE_W 3840
#define CONST_MAX_FBSCALE_H 2160
#define CONST_MAX_DISP_ANGLE_W  200
#define CONST_MAX_DISP_ANGLE_H  200

#define CONST_MAX_OSD_SIZE_W 1024
#define CONST_MAX_OSD_SIZE_H 768



static void recalcContextParams(mcContext_t  *pctx ,UINT8 sen_id, UINT8 disp_id){
#if 0

    //WT_MAIN
    pctx->sbSelRoi[sen_id][WT_MAIN].roiW = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiW * 1000/ pctx->zoomState[sen_id].zmFactor);
    pctx->sbSelRoi[sen_id][WT_MAIN].roiH = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiH * 1000/ pctx->zoomState[sen_id].zmFactor);
    pctx->sbSelRoi[sen_id][WT_MAIN].roiX = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiW/2 - pctx->sbSelRoi[sen_id][WT_MAIN].roiW/2 - pctx->zoomState[sen_id].zmPointShiftCur[AXIS_X]*pctx->sbFactor[sen_id]);
    pctx->sbSelRoi[sen_id][WT_MAIN].roiY = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiH/2 - pctx->sbSelRoi[sen_id][WT_MAIN].roiH/2 - pctx->zoomState[sen_id].zmPointShiftCur[AXIS_Y]*pctx->sbFactor[sen_id]);
    //WT_PIP1
    pctx->sbSelRoi[sen_id][WT_PIP1].roiW = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiW * 1000/ pctx->zoomState[sen_id].zmFactor);
    pctx->sbSelRoi[sen_id][WT_PIP1].roiH = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiH * 1000/ pctx->zoomState[sen_id].zmFactor);
    pctx->sbSelRoi[sen_id][WT_PIP1].roiX = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiW/2 - pctx->sbSelRoi[sen_id][WT_PIP1].roiW/2 - pctx->zoomState[sen_id].zmPointShiftCur[AXIS_X]*pctx->sbFactor[sen_id]);
    pctx->sbSelRoi[sen_id][WT_PIP1].roiY = YUV_ROI_ALIGN(pctx->sbFullYuvSize[sen_id].roiH/2 - pctx->sbSelRoi[sen_id][WT_PIP1].roiH/2 - pctx->zoomState[sen_id].zmPointShiftCur[AXIS_Y]*pctx->sbFactor[sen_id]);


    pctx->drawCfg[disp_id].drawSelRoiOSD.roiW = pctx->drawCfg[disp_id].drawMaxSizeOSD[AXIS_X];
    pctx->drawCfg[disp_id].drawSelRoiOSD.roiH = pctx->drawCfg[disp_id].drawMaxSizeOSD[AXIS_Y];
    pctx->drawCfg[disp_id].drawSelRoiOSD.roiX = (pctx->dispCfg[disp_id].cur_res[AXIS_X] -  pctx->drawCfg[disp_id].drawSelRoiOSD.roiW)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[AXIS_X];
    pctx->drawCfg[disp_id].drawSelRoiOSD.roiY = (pctx->dispCfg[disp_id].cur_res[AXIS_Y] -  pctx->drawCfg[disp_id].drawSelRoiOSD.roiH)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[AXIS_Y];


    pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiW = DRAW_W; //pctx->drawCfg[disp_id].drawMaxSizeVID[MCADJCL_X];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiH = DRAW_H; //pctx->drawCfg[disp_id].drawMaxSizeVID[MCADJCL_Y];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiX = DRAW_X; //(pctx->devCfg.dispSize[disp_id][MCADJCL_X] -  pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiW)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[MCADJCL_X];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiY = DRAW_Y; //(pctx->devCfg.dispSize[disp_id][MCADJCL_Y] -  pctx->drawCfg[disp_id].drawSelRoiVID[WT_MAIN].roiH)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[MCADJCL_Y];

    pctx->drawCfg[disp_id].drawSelRoiVID[WT_PIP1].roiW = DRAW_W; //pctx->drawCfg[disp_id].drawMaxSizeVID[MCADJCL_X];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_PIP1].roiH = DRAW_H; //pctx->drawCfg[disp_id].drawMaxSizeVID[MCADJCL_Y];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_PIP1].roiX = DRAW_X; //(pctx->devCfg.dispSize[disp_id][MCADJCL_X] -  pctx->drawCfg[disp_id].drawSelRoiVID[WT_PIP1].roiW)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[MCADJCL_X];
    pctx->drawCfg[disp_id].drawSelRoiVID[WT_PIP1].roiY = DRAW_Y; //(pctx->devCfg.dispSize[disp_id][MCADJCL_Y] -  pctx->drawSelRoiVID[disp_id][WT_PIP1].roiH)/2 + pctx->drawCfg[disp_id].drawCurShiftAdj[MCADJCL_Y];

    //ctx_Dump(1);
#endif
}

ErrCode_t initYuvBuf(mcContext_t  *pctx, UINT8 senId){
    ErrCode_t err  = ERR_SUCCESS;
    SINT32 i;

    for (i = 0 ; i < BUF_QUEUE_LEN; i++){
        pctx->yuvBuf[senId][i].fmt  = pctx->yuvBufFmt;
        pctx->yuvBuf[senId][i].roiX = pctx->yuvBufSize[senId].roiX;
        pctx->yuvBuf[senId][i].roiY = pctx->yuvBufSize[senId].roiY;
        pctx->yuvBuf[senId][i].roiW = pctx->yuvBufSize[senId].roiW;
        pctx->yuvBuf[senId][i].roiH = pctx->yuvBufSize[senId].roiH;
        pctx->yuvBuf[senId][i].bufW = YUV_BUF_W_ALIGN((pctx->yuvBufSize[senId].roiX + pctx->yuvBufSize[senId].roiW));
        pctx->yuvBuf[senId][i].bufH = YUV_BUF_H_ALIGN((pctx->yuvBufSize[senId].roiY + pctx->yuvBufSize[senId].roiH));

        pctx->yuvBuf[senId][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(pctx->yuvBuf[senId][i].bufW,pctx->yuvBuf[senId][i].bufH);

        BREAK_IF_COND(pctx->yuvBuf[senId][i].pbuf == NULL, ERR_MEM_ALLOC_FAIL);

        DBG("MAIN WIN ROI[%d] (%d,%d; %d x %d)",i,pctx->yuvBuf[senId][i].roiX,pctx->yuvBuf[senId][i].roiY,
                                pctx->yuvBuf[senId][i].bufW,pctx->yuvBuf[senId][i].bufH);
    }
    return err;
}

ErrCode_t initIPC(mcContext_t  *pctx){
    ErrCode_t err  = ERR_SUCCESS;
    UINT8 * buf[32]={0};
    SINT32 i,j;
    ybfwGfxRoi_t tmp_roi;
    ybfwGfxRoi_t main_scalebufsize;
    ybfwGfxRoi_t pip_scalebufsize;

    memset(&main_scalebufsize,0,sizeof(ybfwGfxRoi_t));
    memset(&pip_scalebufsize,0,sizeof(ybfwGfxRoi_t));

    //Allocate sensor related buffers

    memset(&tmp_roi,0,sizeof(ybfwGfxRoi_t));
    for (i = 0 ; i < CFG_HAL_SENS_COUNT; i++){
        initYuvBuf(pctx,i);
        if ((main_scalebufsize.roiW < pctx->sensCfg[i].max_res[AXIS_X]) ||
            (main_scalebufsize.roiH < pctx->sensCfg[i].max_res[AXIS_Y])){
            main_scalebufsize.roiW = pctx->sensCfg[i].max_res[AXIS_X];
            main_scalebufsize.roiH = pctx->sensCfg[i].max_res[AXIS_Y];
        }
    }
    //NAV TODO return
    //pip_scalebufsize.roiW = CFG_MPCORE_PIP_WIN_W;
    //pip_scalebufsize.roiH = CFG_MPCORE_PIP_WIN_H;

    //allocate scale buffers
    for (j = WT_START; j < WT_TOT; j++ ){
        for (i = 0 ; i < BUF_QUEUE_LEN; i++){

           if (j == WT_MAIN){
               memcpy(&pctx->scaleBuf[j][i],&main_scalebufsize,sizeof(ybfwGfxRoi_t));
           }else{
               memcpy(&pctx->scaleBuf[j][i],&pip_scalebufsize,sizeof(ybfwGfxRoi_t));
           }

           //pctx->scale_buf[WT_MAIN] max capture size sensors
           pctx->scaleBuf[WT_MAIN][i].fmt  = pctx->scaleBufFmt;
           pctx->scaleBuf[WT_MAIN][i].roiX = pctx->scaleBuf[j][i].roiX;
           pctx->scaleBuf[WT_MAIN][i].roiY = pctx->scaleBuf[j][i].roiY;
           pctx->scaleBuf[WT_MAIN][i].roiW = pctx->scaleBuf[j][i].roiW;
           pctx->scaleBuf[WT_MAIN][i].roiH = pctx->scaleBuf[j][i].roiH;
           pctx->scaleBuf[WT_MAIN][i].bufW = YUV_BUF_W_ALIGN(pctx->scaleBuf[j][i].roiW);
           pctx->scaleBuf[WT_MAIN][i].bufH = YUV_BUF_H_ALIGN(pctx->scaleBuf[j][i].roiH);


           pctx->scaleBuf[WT_MAIN][i].pbuf = (UINT8*) ybfwYuvBufferAlloc(pctx->scaleBuf[WT_MAIN][i].bufW,pctx->scaleBuf[WT_MAIN][i].bufH);

           TO_EXIT_IF_COND(pctx->scaleBuf[WT_MAIN][i].pbuf == NULL, ERR_MEM_ALLOC_FAIL);

           DBG("%s WIN ROI[%d] (%d,%d; %d x %d)",j== WT_MAIN ? "MAIN":"PIPx",i,pctx->scaleBuf[WT_MAIN][i].roiX,pctx->scaleBuf[WT_MAIN][i].roiY,
                   pctx->scaleBuf[WT_MAIN][i].bufW,pctx->scaleBuf[WT_MAIN][i].bufH);
        }
    }
    //allocate ipc
    for (i = 0 ; i < CFG_HAL_SENS_COUNT; i++){
        sprintf((CHAR *) buf,"yvRdFlg%d",i);
        ybfwOsEventFlagsCreate(&pctx->yuvBufReadyFlag[i], (CHAR *) buf);
        TO_EXIT_IF_COND(pctx->yuvBufReadyFlag[i] == 0 ,ERR_IPC_CREATE_FAIL);
    }

    ybfwOsMutexCreate(&pctx->mutex, "mcCtxMtx", YBFW_TX_INHERIT);
    TO_EXIT_IF_COND(pctx->mutex == 0 ,ERR_IPC_CREATE_FAIL);
EXIT:
    return err;
}


ErrCode_t ctxInit(UINT32 senCapSize[CFG_HAL_SENS_COUNT][AXIS_TOT],UINT32 dispSize[CFG_HAL_DISP_COUNT][AXIS_TOT],McScaleCfg zmCfg[CFG_HAL_SENS_COUNT]){
    SINT32 i;

    mcContext_t * pctx = ctx_getmcContext();

    RETURN_IF_COND(pctx->inited, ERR_FAIL);

    memset(pctx,0,sizeof(mcContext_t));

    //1 Input params
#if 0 //NAV GLOBAL disable

    if (zmCfg){
        memcpy(&pctx->zoomState.zmCfg, &zmCfg[i],sizeof(ZoomCfg_t));
    }else{
        pctx->zoomState.zmCfg.zMode = MCZM_DIGITAL;
        pctx->zoomState.zmCfg.minFactor = DFLT_ZOOM_FACTOR_MIN;
        pctx->zoomState.zmCfg.maxFactor = DFLT_ZOOM_FACTOR_MAX;
        pctx->zoomState.zmCfg.step = DFLT_ZOOM_STEP;
    }

    //sensor params
    if (senCapSize){
        for (i = 0; i < CFG_HAL_SENS_COUNT; i++){
            memcpy(&pctx->sensCfg[i], &senCapSize[i],sizeof(pctx->sensCfg[i]));
        }
    }else{
#define DFLT_CAPSIZE_W  SN_W
#define DFLT_CAPSIZE_H  SN_H
        for (i = 0; i < CFG_HAL_SENS_COUNT; i++){
            pctx->sensCfg[i].cur_res[AXIS_X]= pctx->sensCfg[i].max_res[AXIS_X] = DFLT_CAPSIZE_W;
            pctx->sensCfg[i].cur_res[AXIS_Y]= pctx->sensCfg[i].max_res[AXIS_Y] = DFLT_CAPSIZE_H;
        }
    }

    //display params
    if (dispSize){
        for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
            memcpy(&pctx->dispCfg[i], &dispSize[i],sizeof(pctx->dispCfg[i]));
        }
    }else{

        for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
            pctx->dispCfg[i].cur_res[AXIS_X]= pctx->dispCfg[i].max_res[AXIS_X] = DFLT_DISPSIZE_W;
            pctx->dispCfg[i].cur_res[AXIS_Y]= pctx->dispCfg[i].max_res[AXIS_Y] = DFLT_DISPSIZE_H;
        }
    }

    //2 Calculate params
    for (i = 0; i < CFG_HAL_SENS_COUNT; i++){
        if (pctx->zoomState.zmCfg.zMode ==  MCZM_MICRO){
            if (pctx->zoomState.zmCfg.minFactor <= 1000) {
                pctx->zoomState.zmFactor = 1000;
            }else{
                //TODO calculate middle
                pctx->zoomState.zmFactor = pctx->zoomState.zmCfg.minFactor;
            }
        }else{
            pctx->zoomState.zmFactor=pctx->zoomState.zmCfg.minFactor;
        }

        pctx->sensCfg[i].ratio[AXIS_X] = pctx->devCfg.senCapSize[i][AXIS_X]*1000/pctx->devCfg.senCapSize[i][AXIS_Y] < 1400 ? 4 : 16;
        pctx->sensCfg[i].ratio[AXIS_Y] = pctx->devCfg.senRatio[i][AXIS_X] == 4 ? 3 : 9;


        pctx->sbFactor[i] = 1;//(pctx->devCfg.senCapSize[i][MCADJCL_X] <= CONST_MAX_FBSCALE_W/2) && (pctx->devCfg.senCapSize[i][MCADJCL_Y] <= CONST_MAX_FBSCALE_H/2) ? 2 : 1;
        pctx->sbFullYuvSize[i].roiX = 0;
        pctx->sbFullYuvSize[i].roiY = 0;
        pctx->sbFullYuvSize[i].roiW = pctx->devCfg.senCapSize[i][AXIS_X] * pctx->sbFactor[i];
        pctx->sbFullYuvSize[i].roiH = pctx->devCfg.senCapSize[i][AXIS_Y] * pctx->sbFactor[i];
        pctx->sbStretchWin[i][WT_MAIN] = 0;
        pctx->sbStretchWin[i][WT_PIP1] = 0;
        pctx->zoomState.zmPointShiftMax[AXIS_Y] = CONST_SB_MAX_SHIFT_ADJ;
        pctx->zoomState.zmPointShiftMax[AXIS_X] = (pctx->sbFullYuvSize[i].roiH - pctx->zoomState.zmPointShiftMax[AXIS_Y]*pctx->sbFactor[i]);
        pctx->zoomState.zmPointShiftMax[AXIS_X] = YUV_ROI_ALIGN((pctx->sbFullYuvSize[i].roiW - pctx->zoomState.zmPointShiftMax[AXIS_X]) * pctx->devCfg.senRatio[i][AXIS_X] / pctx->devCfg.senRatio[i][AXIS_Y]);

        pctx->zoomState.zmPointShiftCur[AXIS_X] = 0;
        pctx->zoomState.zmPointShiftCur[AXIS_Y] = 0;
    }

    for (i = 0; i < CFG_HAL_DISP_COUNT; i++){
        pctx->devCfg.dispRatio[i][AXIS_X] = pctx->devCfg.dispSize[i][AXIS_X]*1000/pctx->devCfg.dispSize[i][AXIS_Y] < 1400 ? 4 : 16;
        pctx->devCfg.dispRatio[i][AXIS_Y] = pctx->devCfg.dispRatio[i][AXIS_X] == 4 ? 3 : 9;

#if 0
        pctx->drawCfg[i].drawMaxShiftAdj[AXIS_X] = YUV_ROI_ALIGN(pctx->devCfg.dispSize[i][AXIS_X] - pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X] - pctx->zoomState.zmCfg.maxFactor/1000)/2;
        pctx->drawCfg[i].drawMaxShiftAdj[AXIS_Y] = YUV_ROI_ALIGN(pctx->devCfg.dispSize[i][AXIS_Y] - pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] - pctx->zoomState.zmCfg.maxFactor/1000)/2;
        pctx->drawCfg[i].drawMaxSizeVID[AXIS_Y] = pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] + pctx->zoomState.zmCfg.maxFactor/1000;;


        pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] = CONST_MAX_OSD_SIZE_H;
        pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X] = pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] * pctx->devCfg.dispRatio[i][AXIS_X] / pctx->devCfg.dispRatio[i][AXIS_Y] ;//CONST_MAX_OSD_SIZE_W;

        pctx->drawCfg[i].drawMaxSizeVID[AXIS_X] = pctx->drawCfg[i].drawMaxSizeVID[AXIS_Y] * pctx->devCfg.dispRatio[i][AXIS_X]  / pctx->devCfg.dispRatio[i][AXIS_Y] ;
                  //pctx->drawCfg[i].drawMaxSizeOSD[MCADJCL_X] + pctx->grabCfg[i].zmCfg.maxFactor/1000;  //aspect ration ??
#else

        pctx->drawCfg[i].drawMaxShiftAdj[AXIS_Y] = CFG_MPCORE_DISP_ANGLADJ_MAX_H * 2  + pctx->zoomState.zmCfg.maxFactor/1000;

        UINT32 calc_height = pctx->devCfg.dispSize[i][AXIS_Y] - pctx->drawCfg[i].drawMaxShiftAdj[AXIS_Y];

        DBG("calc_height %d  (disp size %d  ajsu %d)",calc_height,pctx->devCfg.dispSize[i][AXIS_Y],pctx->drawCfg[i].drawMaxShiftAdj[AXIS_Y]);

        ResolutionInfo_t * res = mcGetResolInfo(RK_FIND,
                pctx->devCfg.dispRatio[i][AXIS_X], //ratio X
                pctx->devCfg.dispRatio[i][AXIS_Y],  //ratio Y
                (UINT32)1,  //height
                calc_height);
        LOG_IF_COND(res == NULL,ERR_FAIL);
        if (res){
            DBG("Selected resolution %d x %d", res->resW, res->resH);
            pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] =  YUV_ROI_ALIGN(res->resH);
            pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X] =  YUV_ROI_ALIGN(res->resW);
            pctx->drawCfg[i].drawMaxShiftAdj[AXIS_X] = YUV_ROI_ALIGN(pctx->devCfg.dispSize[i][AXIS_X] - pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X]);

            pctx->drawCfg[i].drawMaxSizeVID[AXIS_Y] = pctx->drawCfg[i].drawMaxSizeOSD[AXIS_Y] ;// + pctx->grabCfg[i].zmCfg.maxFactor/1000;
            pctx->drawCfg[i].drawMaxSizeVID[AXIS_X] = pctx->drawCfg[i].drawMaxSizeOSD[AXIS_X] ;// + pctx->grabCfg[i].zmCfg.maxFactor/1000;
        }else{
            //TODO default values
            DBG("Resi Is null;");

        }

#endif
        pctx->drawCfg[i].drawCurShiftAdj[AXIS_X] = 0;
        pctx->drawCfg[i].drawCurShiftAdj[AXIS_Y] = 0;
    }




    recalcContextParams();
#endif
    return ERR_SUCCESS;;
}


ErrCode_t ctxDestroy(){
    ErrCode_t err  = ERR_SUCCESS;
    return err;
}


ErrCode_t ctx_setDispAdj(UINT32 index, SINT32 dispAdj[AXIS_TOT]){
    mcContext_t * pctx = ctx_getmcContext();
#if 0 //NAV GLOBAL disable
    DBG(" curADJ[%d] (%d,%d) max (%d,%d)",index,dispAdj[AXIS_X],dispAdj[MCADJCL_Y],pctx->drawCfg[index].drawMaxShiftAdj[AXIS_X],pctx->drawCfg[index].drawMaxShiftAdj[AXIS_Y]);
    if ((dispAdj[AXIS_X]*dispAdj[AXIS_X] <=  pctx->drawCfg[index].drawMaxShiftAdj[AXIS_X]*pctx->drawCfg[index].drawMaxShiftAdj[AXIS_X])
            && (dispAdj[AXIS_Y]*dispAdj[AXIS_Y] <=  pctx->drawCfg[index].drawMaxShiftAdj[AXIS_Y]*pctx->drawCfg[index].drawMaxShiftAdj[AXIS_Y])){
        pctx->drawCfg[index].drawCurShiftAdj[AXIS_X] = dispAdj[AXIS_X];
        pctx->drawCfg[index].drawCurShiftAdj[AXIS_Y] = dispAdj[AXIS_Y];
        recalcContextParams();
        return ERR_SUCCESS;
    }else{
        DBG("wrong disp ADJ params > limits");
    }
#endif
    return ERR_PARAM_INVALID;
}
ErrCode_t ctx_setSenAdj(UINT32 index, SINT32 senAdj[AXIS_TOT]){
#if 0 //NAV GLOBAL disable
    mcContext_t * pctx = ctx_getmcContext();
    DBG(" curADJ[%d] (%d,%d) max (%d,%d)",index,senAdj[AXIS_X],senAdj[MCADJCL_Y],pctx->zoomState[index].zmPointShiftMax[AXIS_X],pctx->zoomState[index].zmPointShiftMax[AXIS_Y]);
    if ((senAdj[AXIS_X]*senAdj[AXIS_X] <=  pctx->zoomState[index].zmPointShiftMax[AXIS_X]*pctx->zoomState[index].zmPointShiftMax[AXIS_X])
            && (senAdj[AXIS_Y]*senAdj[AXIS_Y] <=  pctx->zoomState[index].zmPointShiftMax[AXIS_Y]*pctx->zoomState[index].zmPointShiftMax[AXIS_Y])){
        pctx->zoomState[index].zmPointShiftCur[AXIS_X] = senAdj[AXIS_X];
        pctx->zoomState[index].zmPointShiftCur[AXIS_Y] = senAdj[AXIS_Y];
        recalcContextParams();
        return ERR_SUCCESS;
    }else{
        DBG("wrong sens ADJ params > limits");
    }
#endif
    return ERR_PARAM_INVALID;
}

ErrCode_t ctx_setZoomFactor(UINT32 index, UINT32 factor){
    DBG(" curFactor[%d] %d ",index,factor);
#if 0 //NAV GLOBAL disable
    mcContext_t * pctx = ctx_getmcContext();
    if ((factor >= pctx->zoomState[index].zmCfg.minFactor) && (factor <= pctx->zoomState[index].zmCfg.maxFactor) && (factor % pctx->zoomState[index].zmCfg.step == 0)){
        pctx->zoomState[index].zmFactor = factor;
        recalcContextParams();
        return ERR_SUCCESS;
    }else{
        DBG("wrong zoom factor ");
    }
#endif
    return ERR_PARAM_INVALID;
}

/**
 * Increment zoom
 *
 * @param mode  selected mode
 * @param dir  direction
 * @param retZoomFactor  return filue
 * @return 0 - ok.  1 - wrong params for current zoom mode (digital or mzoom)
 */
ErrCode_t ctx_setZoomInc(UINT32 index,McZoomStepMode mode,McZoomDirection dir, UINT32 * retZoomFactor){
    SINT32 resZoom = 0;
    mcContext_t * pctx = ctx_getmcContext();
#if 0 //NAV GLOBAL disable
    static UINT8 balance_coefs[CFG_MPCORE_ZMFACTOR_MAX/1000][2] ={
      {2,2}, //2
      {2,2}, //3
      {4,2}, //4
      {4,4}, //5
      {4,4}, //6
      {4,4}, //7
      {8,8}, //8
      {8,8}, //9
      {8,8}, //10
      {8,8}, //11
      {8,8}, //12
      {8,8}, //13
      {8,8}, //14
      {8,8}, //15
      {16,8}, //16
      {16,8}, //17
      {16,16}, //18
      {16,16}, //19
      {16,16}, //20
      {16,16}, //21
      {16,16}, //22
      {16,16}, //23
      {16,16}, //24
      {16,16}, //25
      {16,16}, //26
      {16,16}, //27
      {16,16}, //28
      {16,16}, //29
      {16,16}, //30
      {16,16}, //31
      {16,16} //32
    };

    switch (mode) {
        case MCZSM_STEP:
            if (dir == MCZD_INC){
                resZoom = (pctx->zoomState[index].zmFactor)*2;
                resZoom = resZoom > pctx->zoomState[index].zmCfg.maxFactor ? pctx->zoomState[index].zmCfg.minFactor : resZoom;
            }else{
                resZoom = (pctx->zoomState[index].zmFactor)/2;
                resZoom = resZoom < pctx->zoomState[index].zmCfg.minFactor ? pctx->zoomState[index].zmCfg.maxFactor : resZoom;
            }
            break;
        case MCZSM_BALANCE:
            resZoom = pctx->zoomState[index].zmFactor/1000; //get Index of table
            resZoom = (dir == MCZD_INC) ? resZoom -   1: resZoom ;
            resZoom = resZoom >= (CFG_MPCORE_ZMFACTOR_MAX/1000) ? (CFG_MPCORE_ZMFACTOR_MAX/1000)-1 : resZoom;
            resZoom = resZoom < 0 ? 0 : resZoom;
            printf("ind %02d ",resZoom + 1);

            if (dir == MCZD_INC){
                resZoom = pctx->zoomState[index].zmCfg.step * balance_coefs[resZoom][MCZD_INC];
                printf("step %05d ",resZoom);
                resZoom = pctx->zoomState[index].zmFactor + resZoom;
                resZoom = resZoom < pctx->zoomState[index].zmCfg.maxFactor ? resZoom : pctx->zoomState[index].zmCfg.maxFactor;
            }else{
                resZoom = pctx->zoomState[index].zmFactor/1000; //get Index of table
                resZoom = (dir == MCZD_INC) ? resZoom -   1: resZoom ;
                resZoom = resZoom >= CFG_MPCORE_ZMFACTOR_MAX/1000 ? (CFG_MPCORE_ZMFACTOR_MAX/1000)-1 : resZoom;
                resZoom = resZoom < 0 ? 0 : resZoom;
                printf("ind %02d ",resZoom + 1);
                resZoom = pctx->zoomState[index].zmCfg.step * balance_coefs[resZoom][MCZD_DEC];
                printf("step %05d ",resZoom);
                resZoom = pctx->zoomState[index].zmFactor - resZoom;
                resZoom = resZoom > pctx->zoomState[index].zmCfg.minFactor ? resZoom : pctx->zoomState[index].zmCfg.minFactor;
            }
            break;
        case MCZSM_SMOOTH:
        default:
            if (dir == MCZD_INC){
                resZoom = (pctx->zoomState[index].zmFactor + pctx->zoomState[index].zmCfg.step);
                resZoom = resZoom < pctx->zoomState[index].zmCfg.maxFactor ? resZoom : pctx->zoomState[index].zmCfg.maxFactor;
            }else{
                resZoom = (pctx->zoomState[index].zmFactor - pctx->zoomState[index].zmCfg.step);
                resZoom = resZoom > pctx->zoomState[index].zmCfg.minFactor ? resZoom : pctx->zoomState[index].zmCfg.minFactor;
            }
            break;
    }
    if (retZoomFactor){
        * retZoomFactor = resZoom;
    }
    pctx->zoomState[index].zmFactor = resZoom;
    recalcContextParams();
#endif
    return ERR_SUCCESS;
}




#ifdef GOD_MODE
ErrCode_t ctx_setDispAdjHand(UINT32 dispId , UINT32 dispAdj[AXIS_TOT]){
    //DBG(" GOD_MOD dispAdj[%d] (%d,%d) ",index,dispAdj[MCADJCL_X],dispAdj[MCADJCL_Y]);
    mcContext_t * pctx = ctx_getmcContext();
    pctx->drawCfg[dispId].drawSelRoiVID.roiX += dispAdj[AXIS_X];
    pctx->drawCfg[dispId].drawSelRoiVID.roiY += dispAdj[AXIS_Y];
    //ctx_Dump(1);
    return ERR_SUCCESS;
}
ErrCode_t ctx_setSenAdjHand(UINT32 index, UINT32 senAdj[AXIS_TOT]){
    DBG(" GOD_MOD senAdj[%d] (%d,%d) ",index,senAdj[AXIS_X],senAdj[AXIS_Y]);
    mcContext_t * pctx = ctx_getmcContext();
    pctx->sbSelRoi[index][WT_MAIN].roiX += senAdj[AXIS_X];
    pctx->sbSelRoi[index][WT_MAIN].roiY += senAdj[AXIS_Y];
    //ctx_Dump(1);
    return ERR_SUCCESS;
}
ErrCode_t ctx_setDispRoiHand(UINT32 dispId, ybfwGfxRoi_t roi){
//    DBG(" GOD_MOD RoiVID[%d] (%d,%d, %dx%d) ",index,roi.roiX,roi.roiY,roi.roiW,roi.roiH);
    mcContext_t * pctx = ctx_getmcContext();
    pctx->drawCfg[dispId].drawSelRoiVID.roiW = roi.roiW;
    pctx->drawCfg[dispId].drawSelRoiVID.roiH = roi.roiH;
    pctx->drawCfg[dispId].drawSelRoiVID.roiX = roi.roiX;
    pctx->drawCfg[dispId].drawSelRoiVID.roiY = roi.roiY;
    //ctx_Dump(1);
    return ERR_SUCCESS;
}
ErrCode_t ctx_setSenRoiHand(UINT32 index, ybfwGfxRoi_t roi){
//    DBG(" GOD_MOD SelRoi[%d] (%d,%d, %dx%d) ",index,roi.roiX,roi.roiY,roi.roiW,roi.roiH);
    mcContext_t * pctx = ctx_getmcContext();
    pctx->sbSelRoi[index][WT_MAIN].roiW = roi.roiW;
    pctx->sbSelRoi[index][WT_MAIN].roiH = roi.roiH;
    pctx->sbSelRoi[index][WT_MAIN].roiX = roi.roiX;
    pctx->sbSelRoi[index][WT_MAIN].roiY = roi.roiY;
    //ctx_Dump(1);
    return ERR_SUCCESS;

}

#endif
