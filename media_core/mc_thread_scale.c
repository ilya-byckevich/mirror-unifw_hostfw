
#define FILE_ID "MC06"

#include "ykn_bfw_api/ybfw_pip_api.h"
#include "ykn_bfw_api/ybfw_gfx_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "mc_context.h"
#include "mc_devices.h"
#include "mc_common.h"
#include "autoconf.h"




static mcContext_t * pctx = NULL;


//#define ONLY_CAPTURE

//#define PIP_EXT_BUFF

#define BG_COLOR 0xFF
// Scaler buf
#ifdef ONE_THREAD
enum{
 BUF_YUV = 0,
 BUF_ZOOM,
 BUF_TOT,
};


#else


#endif


SINT32 disp_pip_pages[YBFW_PIP_CH_MAX][DPP_MAX]={
 [YBFW_PIP_CH_0] = {YBFW_PAGE_PIP_0_SHOWN,YBFW_PAGE_PIP_0_HIDDEN,YBFW_PAGE_PIP_0_MAIN},
 [YBFW_PIP_CH_1] = {YBFW_PAGE_PIP_0_SHOWN,YBFW_PAGE_PIP_0_HIDDEN,YBFW_PAGE_PIP_0_MAIN},
};





//prefomance metrics
static UINT32 time_dspdo = 0;



void scalerThrMain(ULONG param)
{

    ybfwGfxObj_t scr_gfxobj, dst_gfxObj;
    ULONG events;
    UINT32 ret = SUCCESS;
    ybfwGfxPageCapabDesc_t  pip_param;
    UINT32 tmr_start = 0;
    UINT8 yuv_idx = 0;
    UINT8 scale_idx = 0;


    DBG("trace\n");

#ifdef DBG_SAVE_SCALER_RAW
    int counter = 0;
    char tmpbuf[128]={0}; /*temporary*/
#endif

    while (1)
    {
#if 0
        memset(&pip_param, 0, sizeof(pip_param));
        memset(&scr_gfxobj, 0, sizeof(ybfwGfxObj_t));
        memset(&dst_gfxObj, 0, sizeof(ybfwGfxObj_t));

//        DBG("trace\n");
        ybfwOsEventFlagsGet(&pctx->yuvBufReadyFlag[CUR_SENSOR], 1, YBFW_TX_OR_CLEAR, &events, YBFW_TX_WAIT_FOREVER);
//        DBG("trace\n");

        tmr_start = ybfwMsTimeGet();

        yuv_idx = pctx->yuvBuf_index[CUR_SENSOR];
        pctx->yuvBuf_index[CUR_SENSOR]  = (yuv_idx + 1)% BUF_QUEUE_LEN;

        scale_idx = pctx->scale_buf_index[CUR_SENSOR];

        //proccess WT_MAIN buff

        scr_gfxobj.pbuf = pctx->yuvBuf[CUR_SENSOR][yuv_idx].pbuf;
        scr_gfxobj.bufW = pctx->yuvBuf[CUR_SENSOR][yuv_idx].bufW;
        scr_gfxobj.bufH = pctx->yuvBuf[CUR_SENSOR][yuv_idx].bufH;
        scr_gfxobj.fmt  = pctx->yuvBuf[CUR_SENSOR][yuv_idx].fmt;
        scr_gfxobj.roiX = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiX;
        scr_gfxobj.roiY = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiY;
        scr_gfxobj.roiW = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiW;
        scr_gfxobj.roiH = pctx->sbSelRoi[CUR_SENSOR][WT_MAIN].roiH;
//        DBG("trace\n");

        dst_gfxObj.pbuf = pctx->scale_buf[CUR_SENSOR][scale_idx].pbuf;
        dst_gfxObj.fmt  = pctx->scale_buf[CUR_SENSOR][scale_idx].fmt;
        dst_gfxObj.bufW = pctx->scale_buf[CUR_SENSOR][scale_idx].bufW;
        dst_gfxObj.bufH = pctx->scale_buf[CUR_SENSOR][scale_idx].bufH;
        dst_gfxObj.roiX = pctx->drawSelRoiVID[CUR_SENSOR][WT_MAIN].roiX; //NAV TODO scale size param
        dst_gfxObj.roiY = pctx->drawSelRoiVID[CUR_SENSOR][WT_MAIN].roiY;
        dst_gfxObj.roiW = pctx->drawSelRoiVID[CUR_SENSOR][WT_MAIN].roiW;
        dst_gfxObj.roiH = pctx->drawSelRoiVID[CUR_SENSOR][WT_MAIN].roiH;

        //if pip mode
        MAP_BUFFERS(&scr_gfxobj, &dst_gfxObj,pctx->sbStretchWin[CUR_SENSOR][WT_MAIN],BG_COLOR);

        //draw pip
       if ((pctx->grabCfg[CUR_SENSOR].zmPIP) && pip_param.pbuf){

           scr_gfxobj.roiX = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiX;
           scr_gfxobj.roiY = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiY;
           scr_gfxobj.roiW = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiW;
           scr_gfxobj.roiH = pctx->sbSelRoi[CUR_SENSOR][WT_PIP1].roiH;

           dst_gfxObj.pbuf = pip_param.pbuf;
           dst_gfxObj.fmt  = pip_param.fmt;
           dst_gfxObj.bufW = pip_param.frmW;
           dst_gfxObj.bufH = pip_param.frmH;
           dst_gfxObj.roiX = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiX;
           dst_gfxObj.roiY = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiY;
           dst_gfxObj.roiW = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiW;
           dst_gfxObj.roiH = pctx->drawSelRoiVID[CUR_DISPLAY][WT_PIP1].roiH;

           MAP_BUFFERS(&scr_gfxobj, &dst_gfxObj,pctx->sbStretchWin[CUR_SENSOR][WT_MAIN],BG_COLOR);
       }

//       DBG("trace\n");
       UINT32 isNoWait = 1;
       ybfwPipAttrSet(CUR_PIP_CHANNEL, YBFW_PIP_TOGGLE, 1, isNoWait, 0, 0);

#if 0
       if ((perf_counter %2000) == 0){
           tmr = ybfwMsTimeGet() - tmr;
           printf("MThread Perf proccess frame %d ms \n "
                   "PiP params frBuf %d x %d  ROI (%d,%d;%d x %d) win (%d,%d;%d x %d)\n",
                   tmr,
                   pip_param.frmW,pip_param.frmH,
                   pip_param.frmRoiX,pip_param.frmRoiY,pip_param.frmRoiW,pip_param.frmRoiH,
                   pip_param.winX,pip_param.winY,pip_param.winW,pip_param.winH);
           perf_counter = 0;
       }
       perf_counter++;
#endif
       //DBG("trace\n");

#ifdef DBG_SAVE_SCALER_RAW
#if YBFW_DISK_SD
        sprintf(tmpbuf,"D:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
#if (YBFW_DISK_NAND || YBFW_DISK_EMMC || YBFW_DISK_ESD || YBFW_DISK_SPI)
        sprintf(tmpbuf,"C:\\OSD_0_CONVERT_NV16_%dx%d_%d.%d.YUV",g_osdLayerScale_gfxObjDst.bufW , g_osdLayerScale_gfxObjDst.bufH,movNameFlag,counter);
#endif
        fsSimpleWrite( tmpbuf, g_osdLayerScale_gfxObjDst.pbuf, g_osdLayerScale_gfxObjDst.bufW * g_osdLayerScale_gfxObjDst.bufH * 2);
        counter++;
#endif

//        DBG("trace\n");

#endif
    }

}



ErrCode_t mcThreadScaleStart(){

    if (!pctx) pctx = ctx_getmcContext();
    UINT8 * buf[32]={0};
    DBG("trace\n");


    if (pctx->sbThrGrb[CUR_SENSOR] == NULL){
        DBG("trace\n");
        sprintf((CHAR *) buf,"mcyuvbuf%d",CUR_SENSOR);
        DBG("trace\n");

        /*
         * TODO params example
         *  UINT32 *par = ybfwMallocCache(4*sizeof(UINT32));
            par[0] = (UINT32)pfunc;
            par[1] = param;
            par[3] = 0;
         */
        DBG("trace\n");
        sprintf((CHAR *) buf,"sbThrGrb%d",CUR_SENSOR);
        pctx->sbThrGrb[CUR_SENSOR] = ybfwOsThreadCreate((CHAR *) buf, scalerThrMain, 0, 20,
                        0, 0, YBFW_TX_AUTO_START);
        HOST_ASSERT(pctx->sbThrGrb[CUR_SENSOR]);
    }else{
        INFO ("Scaler thread is already started\n");
        return ERR_FAIL;
    }

    return ERR_SUCCESS;
}
ErrCode_t mcThreadScaleStop(){

    if (!pctx) pctx = ctx_getmcContext();

    if (pctx->sbThrGrb[CUR_SENSOR] != NULL){

        ybfwOsThreadDelete(pctx->sbThrGrb[CUR_SENSOR]);
        pctx->sbThrGrb[CUR_SENSOR] = NULL ;

        UINT32 i;
        for(i=0; i < BUF_QUEUE_LEN; i++){
            UINT8* tmp = pctx->yuvBuf[CUR_SENSOR][i].pbuf;
            pctx->yuvBuf[CUR_SENSOR][i].pbuf = NULL;
            if (tmp){
                ybfwYuvBufferFree(tmp);
            }else{
                INFO ("g_mainWinYUVBuf is already NULL\n");
            }
#ifdef PIP_EXT_BUFF
            tmp = pctx->yuvBufPiP[CUR_SENSOR][i].pbuf;
            pctx->yuvBufPiP[CUR_SENSOR][i].pbuf = NULL;
            if (tmp){
                ybfwYuvBufferFree(tmp);
            }else{
                INFO ("g_mainWinYUVBuf is already NULL\n");
            }
#endif
        }

        ybfwOsEventFlagsDelete(&pctx->yuvBufReadyFlag[CUR_SENSOR]);
        pctx->yuvBufReadyFlag[CUR_SENSOR] = 0;

    }else{
        INFO ("Scaler thread is already stopped\n");
       return ERR_FAIL;
    }
    return ERR_SUCCESS;
}

