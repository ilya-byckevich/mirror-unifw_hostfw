/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_sensor_api.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
#include "ykn_bfw_api/ybfw_fs_api.h"
#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_utility_api.h"

#include "app_common.h"
#include "app_cmd_fw.h"

#define OPT_HOST_DELAY     (3*1000) //3 seconds
#define OPT_HOST_NORMAL     0
#define OPT_HOST_DEAD       -1
#define SYS_PWR_OFF_NORMAL     ybfwPowerCtrl(YBFW_POWER_OFF, OPT_HOST_NORMAL)
#define SYS_PWR_OFF_DEAD    ybfwPowerCtrl(YBFW_POWER_OFF, OPT_HOST_DEAD)    /*must avoid memory flush after program dead*/
#define SYS_PWR_OFF_DELAY   do{  \
                                INFO("The system power off after %d seconds\n",OPT_HOST_DELAY/1000); \
                                ybfwPowerCtrl(YBFW_POWER_OFF, OPT_HOST_NORMAL); \
                            }while(0)



/*
ybfw_utility_api.h :
typedef void (*fpybfwUtilCardIspCallback_t)(UINT32 elapseFileCnt, UINT32 totalFileCnt);
*/

static void demoAlarmSet(UINT32 afterSecond);


void demoCardIspCb(UINT32 elapseFileCnt,UINT32 totalFileCnt)
{
    printf("elapseFileCnt=%u, totalFileCnt=%u.\n", elapseFileCnt, totalFileCnt);

    if (elapseFileCnt >= totalFileCnt ) {
        /**
         If you want to power off after CARD ISP
         Please "check" Auto Power Off while using FRM.exe to generate BRN.
        */
        demoAlarmSet(30); /* Set RTC to alarm (restart) after 30 second */

        /**
         The restart time (30 second) is only for demo.
         Please set suitable value by yourself.
        */
    }
    return;
}

void startIsp(char *fw_name){
    /* switch to Idle mode first to make sure buffer enough */
    UINT32 curMode;
#if 0
    ybfwModeGet(&curMode);
    if (curMode != YBFW_MODE_STANDBY) {
        ybfwModeSet(YBFW_MODE_STANDBY);
        ybfwModeWait(YBFW_MODE_STANDBY);
    }
#endif
    /* set USB mode to USB mode */
    ybfwDscFirmwareUpdate(fw_name, demoCardIspCb);
    //DBG("trace\n");
//    SYS_PWR_OFF_DELAY ;
}
static void demoAlarmSet(UINT32 afterSecond)
{
    ybfwTmx_t tTime;

    printf("Alarm after %u seconds\n", afterSecond);

    ybfwRtcDateTimeGet( YBFW_DATE_TIME_OPTION, &tTime );
    #if 1
    printf("Current Time: %04u/%02u/%02u %02u:%02u:%02u.\n\n",
        tTime.tmx_year, tTime.tmx_mon, tTime.tmx_mday,
        tTime.tmx_hour, tTime.tmx_min, tTime.tmx_sec);
    #endif

    tTime.tmx_sec += afterSecond;
    if( tTime.tmx_sec > 59 ) {
        tTime.tmx_min += (tTime.tmx_sec / 60);
        tTime.tmx_sec %= 60;
        if( tTime.tmx_min > 59 ) {
            tTime.tmx_hour += (tTime.tmx_min / 60);
            tTime.tmx_min %= 60;
            if( tTime.tmx_hour > 23 ) {
                tTime.tmx_mday += (tTime.tmx_hour / 24);
                tTime.tmx_hour %= 24;
            }
        }
    }

    #if 1
    printf("Alarm Time: %04u/%02u/%02u %02u:%02u:%02u.\n\n",
        (tTime.tmx_year+1900), tTime.tmx_mon, tTime.tmx_mday,
        tTime.tmx_hour, tTime.tmx_min, tTime.tmx_sec);
    #endif

    tmrUsWait(100); /*Wait a while for printf msg output*/
    ybfwRtcDateTimeSet( YBFW_ALARM_OPTION, &tTime );
}

void identifyFW(){
    UINT32 info;
    ybfwSystemCfgGet(YBFW_SYS_CFG_BOOT_FW_INFO, &info);
    INFO("I am %s\n", info == 0x80000001 ? "FW2" : "FW1");
}

int test_event_cb(fw_state_t state, void *p)
{
    INFO("Update failed with %d!", *(UINT32*)p);
    return 0;
}

int test(UINT8 *fw_name)
{
    struct fw_param_args params = {0};
    UINT32 ret = FAIL;

    params.delete_fw_enabled = FALSE;
    params.param = paramActionOnFail;
    params.ev_action_cb = test_event_cb;
    params.savelog_enabled = FALSE;
    params.wathdog_def_timeout_ms = 0;

    if(strlen(fw_name) > MAX_FW_NAME){
        return FAIL;
    }
    strcpy(params.fw_name, fw_name);
    fw_start_update(params);

    return SUCCESS;
}

int cmdFw(int argc, char **argv, UINT32* v){

    char *cmd = argv[0];
    char* fw_name = NULL;

    if(argc < 0 || !cmd) {
        return FAIL;
    }

   if (isEqualStrings(cmd, "update", CCS_NO )) {

       if (argc == 2 && argv[1] ){
          INFO("upgrade firmware from file '%s'", argv[1]);
          startIsp( argv[1]);
       }else{
           INFO("Invalid firmware name !");
       }
    }
    else if (isEqualStrings(cmd, "ident", CCS_NO ) ) {
       identifyFW();
    }
    else if (isEqualStrings(cmd, "poweroff", CCS_NO ) ) {
       SYS_PWR_OFF_NORMAL;
    }
    else if (isEqualStrings(cmd, "reboot", CCS_NO ) ) {
       SYS_PWR_OFF_DEAD;
    }
    else if (isEqualStrings(cmd, "test", CCS_NO ) ) {
       if (argc == 2 && argv[1] ){
           test(argv[1]);
       }else{
           INFO("Invalid firmware name !");
       }
    }
    else{
       INFO("Unknown command '%s'",cmd);
       return FAIL;
    }
    return SUCCESS;
}

void cmdFwHelp(){
    printf("fw update    - firmware related functions\n");
    printf("\tfw update  <firmware full path>  - update firmware\n");
    printf("\t\t <firmware full path>  - full path in C-string notation. Example:  C:\\\\fw.brn \n");
    printf("\tfw ident    - Identify the firmware type (FW1 or FW2)\n");
    printf("\tfw poweroff    - Power off device\n");
    printf("\tfw reboot    -  reboot device\n");
}
