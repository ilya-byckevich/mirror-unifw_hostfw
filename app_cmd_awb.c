/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "ykn_bfw_api/ybfw_cdsp_api.h"
#include "ykn_bfw_api/ybfw_awb_api.h"

#include "app_common.h"



extern UINT32 dualAwb,saveAwb;
static void cmdAWBOff(){
    ybfwAwbModeSet(YBFW_3A_WIN_0, YBFW_AWB_MODE_OFF);
}



extern void appAwbInit(void);

void cmdAWBOn(){
    extern void awb_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
    dualAwb=0;


    appAwbInit();

    printf("------- AWB On ------\n");
    ybfwAwbCfgSet(YBFW_3A_WIN_0,YBFW_AWB_USE_BAYER_PATTERN, 1 );
    ybfwAwbCustomAwbwinSizeSet( YBFW_SEN_ID_0, 16 , 16 );
    ybfwAwbCfgSet( YBFW_3A_WIN_0,YBFW_AWB_ACCUM_PERIOD , 1 );
    ybfwAwbCustomCbSet(YBFW_SEN_ID_0,YBFW_AEAWB_INFO_BIT_AWB_RGB, awb_Cb );
    ybfwAwbModeSet(YBFW_3A_WIN_0,YBFW_AWB_MODE_INFO_ONLY );
}
static void cmdAWBDualOn(){
    extern void awb_Cb( const aaaProcInfo_t * pinfo, aaaProcResult_t * presult );
    dualAwb=1;
    ybfwAwbCfgSet(YBFW_3A_WIN_0,YBFW_AWB_USE_BAYER_PATTERN, 1 );
    ybfwAwbCustomAwbwinSizeSet( YBFW_3A_WIN_0, 16 , 16 );
    ybfwAwbCfgSet( YBFW_3A_WIN_0,YBFW_AWB_ACCUM_PERIOD , 1 );
    ybfwAwbCustomCbSet(YBFW_3A_WIN_0,YBFW_AEAWB_INFO_BIT_AWB_RGB, awb_Cb );
    ybfwAwbModeSet(YBFW_3A_WIN_0,YBFW_AWB_MODE_INFO_ONLY );
}
static void cmdAWBSave(){
    saveAwb = 1;
}
void cmdAWBInit(){
    appAwbInit();
}

int cmdAWB(int argc,char **argv,UINT32* v){  
    char* cmd = *argv++;v++;argc--;

    /**/
    if(argc<0) {cmdAWBInit();    }
    else if (isEqualStrings(cmd, "off", CCS_NO ) ) {cmdAWBOff();}
    else if (isEqualStrings(cmd, "on", CCS_NO ) ) {cmdAWBOn();}
    else if (isEqualStrings(cmd, "dualon", CCS_NO ) ) {cmdAWBDualOn();}
    else if (isEqualStrings(cmd, "save", CCS_NO ) ) {cmdAWBSave();}
    else{return 0;}
    return 1;
}



