/**
 * @file ${file_name}
 * @author Naidzin Andrei (a.naidzin@yukonww.com)
 *
 * @brief short description if necessary
 *
 * Detailed description if necessary
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
#include "ykn_bfw_api/ybfw_disk_api.h"
#include "ykn_bfw_api/ybfw_usb_api.h"
//#include "ykn_bfw_api/ybfw_msg_def.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
#include "common.h"

#include "app_common.h"

static int is_mounted=0;
static UINT32 is_RamDiskCreated = 0;

#ifdef SP5K_DISK_SPI
#warning "-----------------USE C disk YBFW_DISK_SPI ---------------------"
#endif

#ifdef SP5K_DISK_EMMC
#warning "-----------------USE C disk YBFW_DISK_EMMC ---------------------"
#endif
#ifdef SP5K_DISK_SD
#warning "-----------------USE C disk YBFW_DISK_SD ---------------------"
#endif
static void appUsbMsdcModeSet(void)
{
    UINT32 lunNo = 0;
    if (!is_mounted){

        ybfwUsbCfgSet(YBFW_USB_MSDC_CUSTOM_MOUNT_SET, 1);
        ybfwUsbModeSet(YBFW_USB_MODE_MSDC);

#if (SP5K_DISK_NAND || SP5K_DISK_EMMC || SP5K_DISK_ESD || SP5K_DISK_SPI)
    	ybfwUsbCfgSet(YBFW_USB_MSDC_LUN_SET,YBFW_DRIVE_NAND, lunNo++);
#endif
#if SP5K_DISK_SD
        ybfwUsbCfgSet(YBFW_USB_MSDC_LUN_SET,YBFW_DRIVE_SD, lunNo++);
#endif
#ifdef SP5K_DISK_RAM
        if (is_RamDiskCreated){
            ybfwUsbCfgSet(YBFW_USB_MSDC_LUN_SET, YBFW_DRIVE_RAM, lunNo++);
        }
#endif

        ybfwUsbSwCon(1);
        is_mounted = 1;
    }else{
        INFO("MSDC is already enabled. Ignore command\n");
    }
}

static void appUsbMsdcStateClose()
{
    if (is_mounted){
        ybfwUsbSwCon(0);
#if SP5K_DISK_SD
        ybfwDiskMount(YBFW_DRIVE_SD);
#endif
#if (SP5K_DISK_NAND || SP5K_DISK_EMMC || SP5K_DISK_ESD || SP5K_DISK_SPI)
        ybfwDiskMount(YBFW_DRIVE_NAND);
#endif
#ifdef SP5K_DISK_RAM
        if (is_RamDiskCreated){
            ybfwDiskMount(YBFW_DRIVE_RAM);
        }
#endif
        is_mounted = 0;
    }else{
        INFO("MSDC is not enabled. Ignore command\n");
    }
}


UINT32 cmdMSDC(int argc,char **argv,UINT32* v){
    char* cmd = *argv++;v++;argc--;
   /**/
   if(argc<0) {
       return 0;
   }
   else if (isEqualStrings(cmd, "on", CCS_NO ) ) {
       appUsbMsdcModeSet();
   }
   else if (isEqualStrings(cmd, "off", CCS_NO ) ) {
       appUsbMsdcStateClose();
   }
   else{
       return 0;
   }
   return 1;
}

void cmdMSDCHelp(){
    printf("msdc - usb msdc mode change\n");
    printf("\tmsdc on\n");
    printf("\tmsdc off\n");
}

//--------------------------------------------------------------




#define DFLT_RAM_DISK_SIZE   (12 * 1024 * 1024)  //in Mb

static UINT32 ramDiskSize = DFLT_RAM_DISK_SIZE;


void ramDiskSetSize(UINT32 size){
    if (size != 0){
        ramDiskSize = size;
        DBG("Changed ram disk size to %d Bytes (%d Mb)",ramDiskSize, ramDiskSize /2048);
    }
}

UINT32 ramDiskMount(){
#ifdef SP5K_DISK_RAM
    diskRamOpen();
    ybfwDiskCfgSet(YBFW_DISK_DEV_CFG, YBFW_DRIVE_RAM, 0, ramDiskSize);
    ybfwDiskMountDirect(YBFW_DRIVE_RAM); /* memeory is allocated on mount */
    /* Following two lines are for DCF, remove them if DCF is not needed */
    ybfwDcfFsInit(YBFW_DRIVE_RAM, 0, 0);
    ybfwDcfFsActive(YBFW_DRIVE_RAM);
    is_RamDiskCreated =1;
#else
    ERR("The FW doesn't support Ramdisk");
#endif
    return 0;
}

UINT32 ramDiskUnmount(){
#ifdef SP5K_DISK_RAM
    if (!is_mounted){
        ybfwDcfFsTerm(YBFW_DRIVE_RAM); /* For DCF only */
        ybfwDiskUnMount(YBFW_DRIVE_RAM | YBFW_DRIVE_REMOVE);
        diskRamClose(); /* release the ram disk */
        is_RamDiskCreated = 0;
    }else{
        INFO("Operation was canceled. Because MSDC mode enabled");
    }
#else
    ERR("The FW doesn't support Ramdisk");
#endif
    return 0;
}


int cmdRamDisk(int argc,char **argv,UINT32* v){
    char* cmd = *argv++;v++;argc--;
   /**/
   if(argc<0) {
       return 0;
   }
   else if (isEqualStrings(cmd, "on", CCS_NO ) ) {
       ramDiskMount();
   }
   else if (isEqualStrings(cmd, "off", CCS_NO ) ) {
       ramDiskUnmount();
   }
   else if (isEqualStrings(cmd, "size", CCS_NO ) ) {
       UINT32 size=*v++;
       ramDiskSetSize(size);
   }
   else{
       return 0;
   }
   return 1;
}

void cmdRamDiskHelp(){
    printf("ramdisk - use ram disk\n");
    printf("\tramdisk on  - create and mount disk\n");
    printf("\tramdisk off - unmount and free disk\n");
    printf("\tramdisk size  <value in bytes>  - set size of ramdisk\n");

}



