/**************************************************************************
 *
 *       Copyright (c) 2016-2018 by iCatch Technology, Inc.
 *
 *  This software is copyrighted by and is the property of iCatch
 *  Technology, Inc.. All rights are reserved by iCatch Technology, Inc..
 *  This software may only be used in accordance with the corresponding
 *  license agreement. Any unauthorized use, duplication, distribution,
 *  or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice MUST not be removed or modified without prior
 *  written consent of iCatch Technology, Inc..
 *
 *  iCatch Technology, Inc. reserves the right to modify this software
 *  without notice.
 *
 *  iCatch Technology, Inc.
 *  19-1, Innovation First Road, Science-Based Industrial Park,
 *  Hsin-Chu, Taiwan.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ykn_bfw_api/ybfw_global_api.h"
//#include "ykn_bfw_api/ybfw_os_api.h"
#include "ykn_bfw_api/ybfw_dbg_api.h"
//#include "ykn_bfw_api/ybfw_modesw_api.h"
//#include "ykn_bfw_api/ybfw_disp_api.h"
//#include "ykn_bfw_api/ybfw_cdsp_api.h"
//#include "ykn_bfw_api/ybfw_fs_api.h"
//#include "ykn_bfw_api/ybfw_gfx_api.h"
//#include "ykn_bfw_api/ybfw_pip_api.h"
//#include "ykn_bfw_api/ybfw_sensor_api.h"
//#include "ykn_bfw_api/ybfw_utility_api.h"
//#include "ykn_bfw_api/ybfw_media_api.h"
//#include "ykn_bfw_api/ybfw_aud_api.h"
//#include "ykn_bfw_api/ybfw_amp_api.h"


#include "app_common.h"


extern int FwInit(UINT32 size);
extern int FwUninit();
extern int FwCmd(const char *selecter,const char *p0,const char *p1,const char *p2);

typedef struct {
    const char* name;
    int width, height;
} resolution_t;

static resolution_t rtspYuvRes[] = {
    { "4k2k",3840,2160 },
    { "1080p",1920,1080 },
    { "720p",1280,720 },
    { "480p",720,480 },
    { "vga",640,360 },
    { "qvga",320,240 },
    { 0,0,0 }
};

static UINT32 rtspSenIds[][SENSOR_COUNT_MAX] = {
    {
        YBFW_MODE_PREV_SENSOR_0_YUV_0,
        YBFW_MODE_PREV_SENSOR_0_YUV_1,
        YBFW_MODE_PREV_SENSOR_0_YUV_2,
        YBFW_MODE_PREV_SENSOR_0_YUV_3
    },
    {
        YBFW_MODE_PREV_SENSOR_1_YUV_0,
        YBFW_MODE_PREV_SENSOR_1_YUV_1,
        YBFW_MODE_PREV_SENSOR_1_YUV_2,
        YBFW_MODE_PREV_SENSOR_1_YUV_3
    },
    {
        YBFW_MODE_PREV_SENSOR_2_YUV_0,
        YBFW_MODE_PREV_SENSOR_2_YUV_1,
        YBFW_MODE_PREV_SENSOR_2_YUV_2,
        YBFW_MODE_PREV_SENSOR_2_YUV_3
    },
    {
        YBFW_MODE_PREV_SENSOR_3_YUV_0,
        YBFW_MODE_PREV_SENSOR_3_YUV_1,
        YBFW_MODE_PREV_SENSOR_3_YUV_2,
        YBFW_MODE_PREV_SENSOR_3_YUV_3
    },
};
static UINT32 appSensorModeSelect(int w,int h){
    struct sencfg_t {
            UINT8 m4k;    /* 4k mode */
            UINT8 agc4k;  /* 4k agc */
            UINT8 exp4k;  /* 4k exp */
            UINT8 mfhd;   /* fhd mode */
            UINT8 agcfhd; /* fhd agc */
            UINT8 expfhd; /* fhd exp */
            UINT8 m720p;   /* 720p mode */
            UINT8 agc720p; /* 720p agc */
            UINT8 exp720p; /* 720p exp */
        } sencfg = {
#if SP5K_SENSOR_PANAMN34110 || SP5K_SENSOR_YKN_PANAMN
            12, 160, 160, /* 4k */
             2, 160, 160, /* fhd */
             7, 160, 160, /* 720p */
#elif SP5K_SENSOR_SONYIMX083
             9, 160, 160, /* 4k */
             0, 160, 160, /* fhd */
             3, 160, 160, /* 720p */
#elif SP5K_SENSOR_SONYIMX183
            12, 160, 192, /* 4k */
             3, 160, 160, /* fhd */
            14, 160, 160, /* 720p */
#elif SP5K_SENSOR_SONYIMX147
             7, 160, 160, /* 4k */
             0, 160, 160, /* fhd */
             8, 160, 160, /* 720p */
#elif SP5K_SENSOR_OV4689 || SP5K_SENSOR_OV4689_OV4689 ||SP5K_SENSOR_OV4689_OV4689_CSI
             0, 130, 130, /* 4K */
             4, 130, 130, /* fhd */
             1, 150, 150, /* 720p */
#elif SP5K_SENSOR_YKN_IMX291
            12, 160, 192, /* 4k */
             3, 160, 160, /* fhd */
            14, 160, 160, /* 720p */
#elif SP5K_SENSOR_SONYIMX291
            12, 160, 192, /* 4k */
            3, 160, 160, /* fhd */
            14, 160, 160, /* 720p */
#endif
    };

    if (w>=3840) {
        ybfwPreviewExpAgcSet(0, sencfg.exp4k, sencfg.agc4k);
        return sencfg.m4k;
    }
    else if (h==720){
        ybfwPreviewExpAgcSet(0, sencfg.exp720p, sencfg.agc720p);
        return sencfg.m720p;
    }
    else {
        ybfwPreviewExpAgcSet(0, sencfg.expfhd, sencfg.agcfhd);
        return sencfg.mfhd;
    }
    return 0;
}
/**
 * @brief enable preview mode
 * @param width capture width
 * @param height capture height
 * @param fps    view fps
 * @param smode0
 */
static void appStreamPv(int width,int height,int fps,int smode0){

    if(!width){
        width = PV_DEFAULT_WIDTH;
        DBG(" set default width to %d",width);
    }
    if(!height){
        height=PV_DEFAULT_HEIGHT;
        DBG(" set default height to %d",height);
    }
    if(!fps){
        fps=PV_DEFAULT_FPS;
        DBG(" set default fps to %d",fps);
    }

    INFO("appSreamPv:w=%d,h=%d,fps=%d,smode0=0x%02x\n",width,height,fps,smode0);
    int id_sensor = 0;
    /**sersor mode**/
    UINT32 smode = smode0 ? smode0 : appSensorModeSelect(width,height);
    INFO("stream pv sensor mode:0x%02x\n",smode);

    ybfwSensorModeCfgSet(YBFW_SEN_ID_0 ,YBFW_MODE_VIDEO_PREVIEW, SENSOR_MODE_PREVIEW|smode);

    /**sensor param**/
    ybfwCapabilityPreview_t senprevCap;
    ybfwModeCfgGet(YBFW_MODE_CFG_PV_CAP_GET, YBFW_SEN_ID_0, &senprevCap, sizeof(senprevCap));
    INFO("stream pv bayer =[%x]\n",senprevCap.cfa_pattern);
    INFO("stream pv cap size=[%d %d],ratio[%d %d]\n",senprevCap.hsize,senprevCap.vsize, senprevCap.hratio,senprevCap.vratio);

    /**cdsp performance set**/
    ybfwModePreviewCdspAttr_t cdspattr;
    ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_ATTR_GET, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr );
    cdspattr.pvcdspDoTargetFps = fps;
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_ATTR_CFG, YBFW_MODE_VIDEO_PREVIEW, YBFW_SEN_ID_0, &cdspattr);

    /**cdsp raw config**/
    ybfwModePreviewRawCustCfg_t modepvRawcfg={
        .rawBufNum  = 4,
        .rawBitMode = YBFW_IMG_FMT_RAW_V50,
        .rawWidth = width,
        .rawHeight = height,
    };
    if (senprevCap.hsize >= 3840){
        modepvRawcfg.rawWidth = 3840;
    }
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_RAW_CFG,    YBFW_MODE_VIDEO_PREVIEW,YBFW_MODE_PREV_SENSOR_0_RAW,&modepvRawcfg);

    /**preview yuv para**/
    ybfwModePreviewYuvCustCfg_t modePvcfg;
    memset(&modePvcfg, 0, sizeof(modePvcfg));
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, YBFW_MODE_VIDEO_PREVIEW, YBFW_MODE_PREV_YUV_MAX, &modePvcfg);
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER);

    UINT32 *ids = rtspSenIds[id_sensor];
    resolution_t *ress = rtspYuvRes;
    int i = 0;
    for(i=0;i<sizeof(rtspYuvRes)/sizeof(*rtspYuvRes);i++,ress++){
        if(ress->width<=width){
            break;
        }
    }

    for (i=0;i < SENSOR_COUNT;i++) {
        resolution_t *res = ress+i;
        INFO("set id %d width %d heght %d\n", i, res->width, res->height);
        modePvcfg.yuvEn = 0;
        modePvcfg.yuv.width = res->width;
        modePvcfg.yuv.height = res->height;
        modePvcfg.yuv.roix = 0;
        modePvcfg.yuv.roiy = 0;
        modePvcfg.yuv.roiw = res->width;
        modePvcfg.yuv.roih = res->height;
        modePvcfg.fmt =i<2?YBFW_IMG_FMT_YUV420_V50:YBFW_IMG_FMT_YUV422_NV16;
        modePvcfg.bufNum = 8;
        ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG, YBFW_MODE_VIDEO_PREVIEW, ids[i], &modePvcfg);
    }

    /**display**/
    INFO("display yuv 3  \n");
    ybfwDispAttrSet(YBFW_DISP_CHNL_0, YBFW_DISP_IMG_ACTIVE, 3);
    modePvcfg.yuvEn      = YBFW_MODE_PV_CFG_DISP_CB;
    modePvcfg.yuv.width  = 640;
    modePvcfg.yuv.height = (modePvcfg.yuv.width * senprevCap.vratio) / senprevCap.hratio;
    modePvcfg.yuv.roix= 0;
    modePvcfg.yuv.roiy= 0;
    modePvcfg.yuv.roiw= modePvcfg.yuv.width;
    modePvcfg.yuv.roih= modePvcfg.yuv.height;
    modePvcfg.fmt = YBFW_IMG_FMT_YUV422_NV16;
    modePvcfg.bufNum = 8;
    ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG,YBFW_MODE_VIDEO_PREVIEW,    ids[3],&modePvcfg);

    /**mode switch*/
    ybfwModeSet(YBFW_MODE_VIDEO_PREVIEW);
    ybfwModeWait(YBFW_MODE_VIDEO_PREVIEW);

}

/**
 *  enable yuv streams
 *
 * @param yuven  1 - on, 0 - off
 * @param yuvid  id sensor
 */
static void appStreamPvYuvEnable(int yuven,int yuvid){
    ybfwModePreviewYuvCustCfg_t yuvCfg;
    memset(&yuvCfg, 0, sizeof(yuvCfg));
    INFO("pvyuv:yuven=%d,yuvid=%d\n",yuven,yuvid);

    UINT32 ybfwmode;
    ybfwMediaCtrl(YBFW_MEDIA_CTRL_PREVIEW_ENTER);
    ybfwModeGet(&ybfwmode);
    UINT32 ret = ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_GET,ybfwmode, (1<<yuvid), &yuvCfg);
    if( ret == SUCCESS ){
        if(yuven)
            yuvCfg.yuvEn |= YBFW_MODE_PV_CFG_STREAM_MEDIA;
        else
            yuvCfg.yuvEn &= ~(YBFW_MODE_PV_CFG_STREAM_MEDIA);
        ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_CFG,ybfwmode, (1<<yuvid), &yuvCfg);
        ybfwModeCfgSet(YBFW_MODE_CFG_PREVIEW_RUNTIME_UPD, ybfwmode, YBFW_MODE_PV_CFG_CDSP_YUV);
    }else{
        ERR("ybfwModeCfgGet(YBFW_MODE_CFG_PREVIEW_CDSP_YUV_GET,ybfwmode, (1<<yuvid), &yuvCfg) Fail\n");
    }
}
static void appStreamPvPcmEnable(int rate,int ch){
    if(!rate)rate=48000;
    if(!ch)ch=2;
    INFO("pvpcm:rate=%d,ch=%d\n",rate,ch);
    ybfwAudDevEnable(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, 1);
    ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_IO_SRC | YBFW_AUD_CFG_MASK_CH_ALL, YBFW_AUD_DEV_IO_SRC_AMIC_IN);
    ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_VOL  | YBFW_AUD_CFG_MASK_CH_ALL, 32);    /*32db*/
    ybfwAudDevCfgSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_CFG_GAIN | YBFW_AUD_CFG_MASK_CH_ALL, 24);    /*24db*/
    ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_SAMPLE_RATE, rate/*48000*/);
    ybfwAudDevAttrSet(YBFW_AUD_DEV_0_INT, YBFW_AUD_DEV_REC, YBFW_AUD_DEV_ATTR_CHANNELS, ch/*2*/);
}

static UINT32 appPipDirectModeDrawCb(frameBufInfo_t *pinfo, void *rsvd) {
    ybfwPipAttrSet(YBFW_PIP_CH_0, YBFW_PIP_DIRECT_MODE, 1, 1, 0, (UINT32)pinfo);
    return 0;
}


/*-------------------------------------------------------------------------
 *  Function Name : cmdOMF
 *  Description : open media framework cmd entrty
 *------------------------------------------------------------------------*/
void cmdOMF(UINT32 argc,char **argv,UINT32* v){  
    /* jump 1st cmd "omf"*/
    if(argc<=1){
        ERR("%s(...) miss param\n",__FUNCTION__);
        return;
    }
    /*preprocess param*/
    char* cmd = argv[1];
    char* argvs[10];
    UINT32 vs[10];
    int i=0;
    for(i=0;i<argc-2;i++){
        argvs[i]=argv[i+2];
        vs[i]=v[i+2];
    }
    for(;i<10;i++){
        argvs[i]=0;
        vs[i]=0;
    }
    argc-=2;
    argv=argvs;
    v=vs;

    /**/
    if(0) {

    }
    else if (isEqualStrings(cmd, "init", CCS_NO ) ) {
        FwInit(*v++);
    }
    else if (isEqualStrings(cmd, "uninit", CCS_NO ) ) {
        FwUninit();
    }
    else if (isEqualStrings(cmd, "h264-clk", CCS_NO ) ) {
        ybfwSystemCfgSet(YBFW_CLK_HVM_CLK_CFG, *v++/*324000*/ /* clock in KHz */);
    }
    else if (isEqualStrings(cmd, "pv", CCS_NO ) ) {
        appStreamPv(*v++,*v++,*v++,*v++);
    }
    else if (isEqualStrings(cmd, "disp-hdmi", CCS_NO ) ) {
        FwCmd("disp",0,0,(void*)appPipDirectModeDrawCb);
    }
    else if (isEqualStrings(cmd, "pcmen", CCS_NO) ) {
        appStreamPvPcmEnable(*v++,*v++);
    }
    else if (isEqualStrings(cmd, "yuven", CCS_NO ) ) {
        appStreamPvYuvEnable(*v++,*v++);
    }
    else{
        FwCmd(cmd,*argv++,*argv++,*argv++);
    }
}

